layui.use(['jquery', 'layer'], function(){
	var $ = layui.jquery
	,layer = layui.layer;
	
	//初始化转盘大小
	var tHeight = $('.bg').height() * 0.55;
	var tWidth = $('.bg').width();
	if(tHeight > tWidth){
		tHeight = tWidth * 0.95;
	}
	$('.turntable').css("height", tHeight + "px");
	
	//获取站点域名配置
	$('.menu').html(getDomainKv('turntable'));
	
	//显示活动时间
	showActTime();
	
	//活动规格按钮监听
	$('.ruleBtn').on('click', function(){
		$('#shade').show();
		$('.ruleBox').show();
	});
	
	//查看奖品按钮监听
	$('.checkReward').on('click', function(){
		$('#shade').show();
		$('.rewardBox').show();
	});
	
	//我的奖品按钮监听
	$('#myReward').on('click', function(){
		if($('#memberId').val() == null || $('#memberId').val() == ''){
			$('#shade').show();
			$('.login').show();
		}else{
			$('#shade').show();
			$('.recordBox').show();
			var content = getMyReward();
			$('#record').html(content);
		}
	});
	
	//点击关闭弹窗按钮隐藏弹窗
	var closeValue = ['.login', '.smashedEgg', '.ruleBox', '.rewardBox', '.recordBox', '#shade'];
	closePopup(closeValue);
	$('.closeBtn').on('click', function(){
		$('.gitReward').hide();
		$('#shade').hide();
	});
	
	var flag = 1;
	//转盘监听
	$('#start').on('click', function(){
		//判断是否登录
		if($('#memberId').val() == null || $('#memberId').val() == ''){
			$('#shade').show();
			$('.login').show();
		}else{
			//获取奖品
			if(flag == 0){
				return;
			}
			flag = 0;
			$('#turntable').css("transition", "all 0s");
			$('#turntable').css("transform", "rotate(0deg)");
			$.ajax({ 
				url: "/ruleVerify/turntableRuleVerify", 
				data: {actId: $('#actId').val(), centralActId: $('#mainActId').val()},
				dataType: 'json',
				success: function(result){
					if(result.code == '000000'){
						var turnNum = 0;
						if(result.data.rewardId != null && result.data.rewardId != ""){
							var reward = result.data.reward;
							var rewardId = result.data.rewardId;
							if(result.data.reward != null && result.data.reward != ''){
								$('.rewardCon').html("<div>恭喜你，中奖了</div>获得" + reward);
							}else{
								$('.rewardCon').html("该奖品已被抽完");
							}
							for(var i = 0;i < rewards.length;i++){
								if(rewards[i] != null && rewards[i].id == rewardId){
									if(rewards.length == 5){
										turnNum = (i) * 72;
									}else if(rewards.length == 6){
										turnNum = (i) * 60;
									}else if(rewards.length == 7){
										turnNum = (i) * 51 + (i / 2);
									}
								}
							}
						}else{
							$('.rewardCon').html("谢谢参与");
							for(var i = 0;i < rewards.length;i++){
								if(rewards[i] == null){
									if(rewards.length == 5){
										turnNum = (i) * 72;
									}else if(rewards.length == 6){
										turnNum = (i) * 60;
									}else if(rewards.length == 7){
										turnNum = (i) * 51 + (i / 2);
									}
								}
							}
						}
						$('#turntable').css("transform", "rotate(" + (3960 - turnNum) + "deg)");
						$('#turntable').css("transition", "all 5s");
						
						setTimeout(function(){
							$('#shade').show();
							$('.gitReward').show();
							getMemberAct();
							flag = 1;
							getRewardRecordByActId();
						}, 5500);
					}else{
						layer.msg(result.msg);
						flag = 1;
					}
				}
			});
		}
	});
	
    getRewards();
    //获取该活动奖品
    function getRewards(){
    	$.ajax({ 
			url: "/ruleVerify/getRewardList", 
			data: {actId: $('#actId').val()},
			dataType: 'json',
			async: true,
			success: function(result){
				if(result.code == '000000'){
					if(result.data != null){
						rewards = result.data;
						//显示在转盘上
						var count = rewards.length;
						
						var gridNum = $('#turnTableBg').val() == null ? "" : $('#turnTableBg').val();
						if(gridNum != null && gridNum.indexOf("_") > -1){
							gridNum = gridNum.substring(gridNum.indexOf("_") + 1, gridNum.indexOf("."));
							$('#turntable').removeClass();
							$('#turntable').addClass('turntable' + gridNum);
							$('#frame').attr("src", "/activityPage/turntable/image/frame" + gridNum + ".png");
						}else{
							layer.msg("获取转盘失败");
							return ;
						}
						if(count == 0){
							return ;
						}
						var content = '';
						var rewardList = [];
						if(count < gridNum){
							var j = 0;
							var k = 1
							for(var i = 0;i < gridNum;i++){
								var num = parseInt(gridNum / (k * (gridNum - count)));
								if(i == num){
									rewardList[i] = null;
									k++;
								}else{
									rewardList[i] = rewards[j++];
								}
							}
							rewards = rewardList;
						}else if(count > gridNum){
							for(var i = 0;i < gridNum;i++){
								rewardList[i] = rewards[j];
							}
							rewards = rewardList;
						}
						
						for(var i = 0;i < gridNum;i++){
							if(rewards[i] == null){
								content += '<div class="sector">';
								content += '<div class="sector-inner">';
								content += '<span>谢谢参与</span>';
								content += '<img src="/activityPage/turntable/image/gift.png" class="rewardImg">';
								content += '</div>';
								content += '</div>';
							}else{
								content += '<div class="sector">';
								content += '<div class="sector-inner">';
								content += '<span>' + (rewards[i].rewardOption == null ? '' : rewards[i].rewardOption) + '</span>';
								content += '<img src="' + rewards[i].rewardImg + '" onerror="this.src=\'/activityPage/turntable/image/gift.png\'" class="rewardImg">';
								content += '</div>';
								content += '</div>';
							}
						}
						$('#turntableBox').html(content);
					}
				}
			}
    	});
    }
    
    //中奖记录滚动
	var rewardRecords = getRewardRecordByActId($('#actId').val());
	$('#recordInfo').html(rewardRecords);
	//中奖记录动画
	rewardFlash();
	//获取奖品
    rewards = getRewards();
    
});
