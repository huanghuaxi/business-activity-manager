layui.use(['jquery', 'layer', 'flow'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,flow = layui.flow;
	
// 		flow.lazyimg(); 
	
	getRewardRecordByActId();
	
	var flag = 1;
	
	//获取站点域名配置
	$('.menu').html(getDomainKv('box'));
	
	//显示活动时间
	showActTime();
	
	//查看活动规则
	$('#activityRules').on('click', function(){
		$('#actRule').show();
		$('#shade').show();
	});
	
	//查看已获得的奖品
	$('#prices').on('click', function(){
		flag = 2;
		getRewardRecord();
	});
	
	//菜单按钮监听
	$('.menuBtn').on('click', function(){
		$('.menuBtn').hide();
		$('.menu').slideDown();
		//5秒后关闭菜单
		setTimeout(function(){
			$('.menu').slideUp('slow', function(){$('.menuBtn').show();});
		}, 5000);
	});
	
	//登录弹窗按钮监听
	$('#loginBtn').on('click', function(){
		$('#shade').show();
		$('.login_layer').show();
	});
	
	//如果已经登录获取活动会员数据
	if($('#memberId').val() != null && $('#memberId').val() != ''){
		getMemberAct();
	}
	
	//登录框
	$('#memberLogin').on('click', function(){
		//登录
		var memberName = $('#memberName').val();
		if(memberName == null || memberName == ""){
			layer.msg("请输入会员账号，非会员可输入手机号参与");
			return ;
		}
		//登录
		$.ajax({ 
			url: "/ruleVerify/memberLogin", 
			data: {actId: $('#actId').val(), memberName: memberName, inviter: $('#inviter').val()},
			dataType: 'json',
			success: function(result){
				if(result.code == "000000"){
					/*$('#memberId').val(result.data.id);
					var isJoin = getMemberAct();
					$('.treasureBox_layer').hide();
					$('.login_layer').hide();
					$('#shade').hide();
					if(isJoin){
						if(flag == 1){
							getBox();
						}else{
							getRewardRecord();
						}
					}*/
					var url = window.location.href;
					if(url.indexOf("&inviter=") > -1){
						var index = url.indexOf("&inviter=");
						var str = url.substring(index + 1);
						var index2 = str.indexOf("&");
						if(index2 == -1){
							url = url.substring(0, index);
						}else{
							url = url.substring(0, index) + url.substring(index + 1).substring(index2);
						}
					}
					window.location.href = url + '&inviter=' + result.data.id;
				}else{
					layer.msg(result.msg);
				}
			}
		});
	});
	
	//开启宝箱
	$('#openBox').on('click', function(){
		$.ajax({ 
			url: "/ruleVerify/boxRuleVerify", 
			data: {actId: $('#actId').val(), centralActId: $('#mainActId').val()},
			dataType: 'json',
			success: function(result){
				if(result.code == "000000"){
					var reward = result.data;
					if(reward == null || reward == ""){
						$('#reward').text("谢谢参与");
					}else{
						$('#reward').text("恭喜获得" + reward);
						getRewardRecordByActId();
					}
					$('.bg_layer').show();
					$('#shade').show();
					getMemberAct();
				}else{
					layer.msg(result.msg);
				}
			}
		});
	});
	
	//获取等级宝箱
    $('.box_wrapper>.img').click(function () {
    	flag = 1;
    	getBox();
    })
    $('.close').click(function () {
        $('.layer').hide();
        $('#shade').hide();
    })
    
    
    $('.bg_layer').click(function () {
        $('.bg_layer').hide();
    })
    
    function animate() {
        var i = 0;
        i++;
        if (i < $('.msg').length) {
            $('.info').animate({
                left: -(185 * $('.msg').length) + 'px'
            }, 5000 * $('.msg').length, 'linear', function () {
                $('.info').css({ 'left': '0px' });
                i = 0;
            })
        }
    }
    
    setInterval(function () {
    	$('.info').width($('.msg').length * 185 + "px");
        animate();
    });
    
    var memberAct;
    
    //获取活动会员信息
    function getMemberAct(){
    	var isJoin = false;
    	$.ajax({ 
			url: "/ruleVerify/getMemberAct", 
			data: {memberId: $('#memberId').val(), actId: $('#actId').val()},
			dataType: 'json',
			async: false,
			success: function(result){
				if(result.code == "000000"){
					memberAct = result.data;
					console.log(memberAct);
					if(memberAct == null){
						layer.msg("您没有参与此活动");
					}else{
						if(memberAct.allTimes != null && memberAct.allTimes != ''){
							$('#times').text("剩余" + memberAct.allTimes + "次抽奖机会");
						}else{
							$('#times').text("剩余0次抽奖机会");
						}
						
						$('#level').val(memberAct.level);
						isJoin = true;
					}
				}else{
					layer.msg("获取活动会员信息失败");
				}
			}
		});
    	return isJoin;
    }
    
    //获取该活动中奖纪录
    function getRewardRecordByActId(){
    	$.ajax({ 
			url: "/ruleVerify/getRewardRecordByActId", 
			data: {actId: $('#actId').val()},
			dataType: 'json',
			async: false,
			success: function(result){
				if(result.code == "000000"){
					var data = result.data;
					if(data != null){
						var content = '';
						for(var i = 0;i < data.length;i++){
							content += '<div class="msg">';
							content += ' 恭喜<span class="name">';
							content += data[i].remName;
							content += '</span>';
							content += '获得<span class="price">';
							content += data[i].rewardCon;
							content += '</span>大奖';
							content += '</div>';
						}
						$('#recordInfo').html(content);
					}
				}
			}
		});
    }
    
    //获取宝箱
    function getBox(){
    	//判断是否登录
    	if($('#memberId').val() == null || $('#memberId').val() == ""){
			$('.login_layer').show();
			$('#shade').show();
		}else{
			var boxImg = getLevelBox();
			if(boxImg == null || boxImg == ""){
				layer.msg("没有可以开启的宝箱");
			}else{
				$('#leveBox').attr("src", "/activityPage/box/image/" + boxImg + ".jpg");
				$('.treasureBox_layer').show();
				$('#shade').show();
			}
		}
    }
    
    //获取该用户中奖纪录
    function getRewardRecord(){
    	if($('#memberId').val() == null || $('#memberId').val() == ""){
			$('.login_layer').show();
			$('#shade').show();
			return ;
		}
		$.ajax({ 
			url: "/ruleVerify/getRewardRecord", 
			data: {actId: $('#actId').val(), memberId: $('#memberId').val(), centralActId: $('#mainActId').val()},
			dataType: 'json',
			success: function(result){
				if(result.code == "000000"){
					var data = result.data;
					if(data != null){
						var content = '';
						for(var i = 0;i < data.length;i++){
							/* content += '<span class="text">';
							content += createTime(data[i].createTime);
							content += '获得';
							content += data[i].rewardCon;
							content += '</span>' */
							content += '<div class="row">';
							content += '<div class="time">' + createTime(data[i].createTime) + '</div>';
							content += '<div class="reward">' + data[i].rewardCon + '</div>';
							if(data[i].isSend == 1){
								content += '<div class="isSend">已派送</div>';
							}else{
								content += '<div class="isSend">未派送</div>';
							}
							content += '</div>';
						}
						$('#record').html(content);
						$('#rewardRecord').show();
						$('#shade').show();
					}
				}else{
					layer.msg("中奖纪录获取失败");
				}
			}
		});
    }
    
    //获取相同等级宝箱
    function getLevelBox(){
    	var level = $('#level').val();
//    	console.log(level);
    	if(level == '青铜'){
    		return "11";
    	}else if(level == '白银'){
    		return "22";
    	}else if(level == '黄金'){
    		return "33";
    	}else if(level == "铂金"){
    		return "44";
    	}else if(level == "钻石"){
    		return "55";
    	}else if(level == "星耀"){
    		return "66";
    	}else if(level == "至尊"){
    		return "77";
    	}else{
    		return "";
    	}
    }
});

//倒计时
function getTime(){
	var newTime = new Date();
	var beginTimeStr = document.getElementById("beginTime").value.replace("/-/g", "/");//时间转换
	var beginTime = new Date(beginTimeStr);
	var endTimeStr = document.getElementById("endTime").value.replace("/-/g", "/");//时间转换
	var endTime = new Date(endTimeStr);
	var dateDiff;
	var text = '';
	if(newTime >= beginTime){
		if(newTime <= endTime){
			text = "距离结束还有";
			dateDiff = endTime.getTime() - newTime.getTime();//时间差的毫秒数
		}else{
			dateDiff = 0;
			text = "活动已结束";
			document.getElementById("msg").innerHTML = text;
			document.getElementById("msg").style.display = "block";
			document.getElementById("time").style.display = "none";
			return;
		}
	}else{
		dateDiff = beginTime.getTime() - newTime.getTime(); //时间差的毫秒数
	    text = "距离开始还有";
	}
	var dayDiff = Math.floor(dateDiff / (24 * 3600 * 1000));//计算出相差天数
    var leave1 = dateDiff % (24 * 3600 * 1000)    //计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000))//计算出小时数
    //计算相差分钟数
    var leave2 = leave1 % (3600 * 1000)    //计算小时数后剩余的毫秒数
    var minutes = Math.floor(leave2 / (60 * 1000))//计算相差分钟数
    //计算相差秒数
    var leave3 = leave2 % (60 * 1000)      //计算分钟数后剩余的毫秒数
    var seconds = Math.round(leave3 / 1000);
    document.getElementById('timeText').innerHTML = text;
    document.getElementById('day').innerHTML = dayDiff > 999 ? "999+" : dayDiff;
    document.getElementById('hour').innerHTML = hours < 10 ? "0" + hours : hours;
    document.getElementById('minutes').innerHTML = minutes < 10 ? "0" + minutes : minutes;
    document.getElementById('seconds').innerHTML = seconds < 10 ? "0" + seconds : seconds;
}
