
var $, layer;
layui.use(['jquery', 'layer'], function(){
	$ = layui.jquery
	,layer = layui.layer;
	
	//初始化高度
	$('.bg').height(document.documentElement.clientHeight);
	
	//后退按钮是否显示
	if($('#mainActId').val() != null && $('#mainActId').val() != ''){
		$('.backBtn').css("display", "flex");
	}
	
	//菜单按钮监听
	$('.menuBtn').on('click', function(){
		$('.menuBtn').hide();
		$('.menu').slideDown();
		//5秒后关闭菜单
		setTimeout(function(){
			$('.menu').slideUp('slow', function(){$('.menuBtn').show();});
		}, 5000);
	});
	
	//返回按钮监听
	$('.backBtn').on('click', function(){
		window.location.href = "/ruleVerify/toActivityPage?actId=" + $('#mainActId').val()
	});
	
	//分享按钮监听
	$('.shareBtn').on('click', function(){
		$('#shade').show();
		$('.shareBox').show();
	});
	
	//登录按钮监听
	$('#login').on('click', function(){
		var memberName = $('#memberName').val();
		if(memberName == null || memberName == ""){
			layer.msg("请输入会员账号，非会员可输入手机号参与");
			return ;
		}
		login();
	});
	
	//登录弹窗按钮监听
	$('#loginBtn').on('click', function(){
		$('#shade').show();
		$('.login').show();
	});
	
	//活动规格按钮监听
	$('.ruleBtn').on('click', function(){
		$('#shade').show();
		$('.ruleBox').show();
	});
	
	/*wecharConfig();
	//分享到朋友圈按钮监听
	$('#momentsShare').on('click', function(){
		var title = "活动分享测试";
		var desc = "活动描述";
		var link = "http://47.75.88.142:8080/ruleVerify/toActivityPage?actId=76";
		var imgUrl = "http://47.75.88.142:8080/activityPage/card/image/card.png";
		updateAppMessageShareData(title, desc, link, imgUrl);
	});*/
	
	//如果已经登录获取活动会员数据
	if($('#memberId').val() != null && $('#memberId').val() != ''){
		getMemberAct();
	}
});

//显示活动时间
function showActTime(){
	//活动时间
	if($('#isUsed').val() == null || $('#isUsed').val() == 'false'){
		$('#time').hide();
		$('#msg').text("活动无效");
		$('#msg').show();
	}else{
		if($('#beginTime').val() != null && $('#beginTime').val() != '' && $('#endTime').val() != null && $('#endTime').val() != ''){
			getTime();
			setInterval("getTime()", 1000);
		}
	}
}

function NewDate(str) { 
	var time = str.split(" ");
	var timeStr = time[0].replace(/-/g, '/') + " " + time[1].substring(0, time[1].indexOf("."));
//	console.log(timeStr);
	var date = new Date(timeStr); 
	return date; 
} 

//倒计时
function getTime(){
	var newTime = new Date();
//	var beginTimeStr = document.getElementById("beginTime").value.replace("/-/g", "/");//时间转换
//	console.log(document.getElementById("beginTime").value);
	var beginTime = NewDate(document.getElementById("beginTime").value);
//	console.log(beginTime);
//	var endTimeStr = document.getElementById("endTime").value.replace("/-/g", "/");//时间转换
	var endTime = NewDate(document.getElementById("endTime").value);
	var dateDiff;
	var text = '';
	if(newTime >= beginTime){
		if(newTime <= endTime){
			text = "距离结束还有";
			dateDiff = endTime.getTime() - newTime.getTime();//时间差的毫秒数
		}else{
			dateDiff = 0;
			text = "活动已结束";
			document.getElementById("msg").innerHTML = text;
			document.getElementById("msg").style.display = "block";
			document.getElementById("time").style.display = "none";
			return;
		}
	}else{
		dateDiff = beginTime.getTime() - newTime.getTime(); //时间差的毫秒数
	    text = "距离开始还有";
	}
	var dayDiff = Math.floor(dateDiff / (24 * 3600 * 1000));//计算出相差天数
    var leave1 = dateDiff % (24 * 3600 * 1000)    //计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000))//计算出小时数
    //计算相差分钟数
    var leave2 = leave1 % (3600 * 1000)    //计算小时数后剩余的毫秒数
    var minutes = Math.floor(leave2 / (60 * 1000))//计算相差分钟数
    //计算相差秒数
    var leave3 = leave2 % (60 * 1000)      //计算分钟数后剩余的毫秒数
    var seconds = Math.round(leave3 / 1000);
    document.getElementById('timeText').innerHTML = text;
    document.getElementById('day').innerHTML = dayDiff > 999 ? "999+" : dayDiff;
    document.getElementById('hour').innerHTML = hours < 10 ? "0" + hours : hours;
    document.getElementById('minutes').innerHTML = minutes < 10 ? "0" + minutes : minutes;
    document.getElementById('seconds').innerHTML = seconds < 10 ? "0" + seconds : seconds;
}

//关闭弹窗
function closePopup(value){
	if(value != null && value.length > 0){
		$('.close').on('click', function(){
			for(var i = 0;i <  value.length;i++){
				$(value[i]).hide();
			}
		});
	}
}


//登录
function login(){
	$.ajax({ 
		url: "/ruleVerify/memberLogin", 
		data: {actId: $('#actId').val(), memberName: $('#memberName').val(), inviter: $('#inviter').val()},
		dataType: 'json',
		success: function(result){
			if(result.code == "000000"){
				/*$('#memberId').val(result.data.id);
				getMemberAct();
				$('.close').click();*/
				var url = window.location.href;
				if(url.indexOf("&inviter=") > -1){
					var index = url.indexOf("&inviter=");
					var str = url.substring(index + 1);
					var index2 = str.indexOf("&");
					if(index2 == -1){
						url = url.substring(0, index);
					}else{
						url = url.substring(0, index) + url.substring(index + 1).substring(index2);
					}
				}
				window.location.href = url + '&inviter=' + result.data.id;
			}else{
				layer.msg(result.msg);
				$('#memberId').val('');
			}
		}
	});
}

//获取活动会员信息
function getMemberAct(){
	$.ajax({ 
		url: "/ruleVerify/getMemberAct", 
		data: {memberId: $('#memberId').val(), actId: $('#actId').val()},
		dataType: 'json',
		async: false,
		success: function(result){
			if(result.code == "000000"){
				memberAct = result.data;
				if(memberAct == null){
					layer.msg("您没有参与此活动");
				}else{
					if(memberAct.allTimes != null && memberAct.allTimes != ''){
						$('#times').text(memberAct.allTimes);
					}else{
						$('#times').text("0");
					}
				}
			}else{
				layer.msg("获取活动会员信息失败");
			}
		}
	});
}

//获取该活动中奖纪录
function getRewardRecordByActId(actId){
	var content = '';
	$.ajax({ 
		url: "/ruleVerify/getRewardRecordByActId", 
		data: {actId: actId},
		dataType: 'json',
		async: false,
		success: function(result){
			if(result.code == "000000"){
				var data = result.data;
				if(data != null){
					
					for(var i = 0;i < data.length;i++){
						content += '<div class="msg" id="msg_' + i +'">';
						content += ' 恭喜，<span class="name">';
						content += data[i].remName;
						content += '</span>';
						content += '获得<span class="price">';
						content += data[i].rewardCon;
						content += '</span>大奖';
						content += '</div>';
					}
				}
			}
		}
	});
	return content;
}

//中奖纪录滚动
function rewardFlash(){
	var count = $('.msg').length;
	var i = 0;
	setInterval(function(){
		if(i + 1 == count){
			i = 0;
			$('.msg').show();
		}else{
			$('#msg_' + i).slideUp();
			i++;
		}
		
	}, 3000);
}

//获取该活动奖品
function getRewards(){
	var reward = [];
	$.ajax({ 
		url: "/ruleVerify/getRewardList", 
		data: {actId: $('#actId').val()},
		dataType: 'json',
		async: false,
		success: function(result){
			if(result.code == '000000'){
				if(result.data != null){
					reward = result.data;
				}
			}
		}
	});
	return reward;
}

//获取我的中奖纪录
function getMyReward(){
	var content = '';
	$.ajax({ 
		url: "/ruleVerify/getRewardRecord", 
		data: {actId: $('#actId').val(), memberId: $('#memberId').val(), centralActId: $('#mainActId').val()},
		dataType: 'json',
		async: false,
		success: function(result){
			if(result.code == '000000'){
				if(result.data != null){
					var data = result.data;
					for(var i = 0;i < data.length;i++){
						content += '<div class="row">';
						content += '<div class="time">' + createTime(data[i].createTime) + '</div>';
						if(data[i].isGit != 1){
							content += '<div class="reward">谢谢参与</div>';
						}else{
							content += '<div class="reward">' + data[i].rewardCon + '</div>';
						}
						if(data[i].isSend == 1){
							content += '<div class="isSend">已派奖</div>';
						}else{
							content += '<div class="isSend">未派奖</div>';
						}
						content += '</div>';
					}
				}
			}
		}
	});
	return content;
}

//奖品分页显示
function checkRewardByPage(rewards, page, pageSize){
	var content = '';
	if(rewards.length > page * pageSize && 0 <= page * pageSize){
		for(var i = page * pageSize;i <  (page + 1) * pageSize;i++){
			if(rewards[i] == null){
				break;
			}
			content += '<div class="reward">';
			content += '<div class="rewardImg">';
			content += '<img src="' + rewards[i].rewardImg + '" onerror="this.src=\'/activityPage/turntable/image/gift.png\'">';
			content += '</div>';
			content += '<div class="title">' + rewards[i].rewardOption + '</div>';
			content += '</div>';
		}
		
	}
	return content;
}

//获取站点域名配置
function getDomainKv(value){
	var content = '';
    $.ajax({ 
		url: "/ruleVerify/getDomainKv", 
		data: {domainEnName: value},
		dataType: 'json',
		async: false,
		success: function(result){
			if(result.code == '000000'){
				var data = result.data;
				if(data != null){
					for(var i = 0;i < data.length;i++){
						content += '<div class="row">';
						if(data[i].keyEnName == 'homepage'){
							content += '<img target="_blank" src="/activityPage/goldenEgg/image/index_page.png" />';
						}else if(data[i].keyEnName == 'login'){
							content += '<img target="_blank" src="/activityPage/goldenEgg/image/member.png" />';
						}else if(data[i].keyEnName == 'customer_service'){
							content += '<img target="_blank" src="/activityPage/goldenEgg/image/customer.png" />';
						}else if(data[i].keyEnName == 'logout'){
							content += '<img target="_blank" src="/activityPage/goldenEgg/image/logout.png" />';
						}else{
							content += '<img target="_blank" src="/activityPage/goldenEgg/image/index_page.png" />';
						}
						if(data[i].keyEnName == 'logout'){
							content += '<a target="_blank" href="' + data[i].siteKeyValue 
							+ '&actId=' + $('#actId').val() 
							+ '&mainActId=' + $('#mainActId').val() 
							+ '&inviter=' + $('#inviter').val()
							+ '">' + data[i].keyCnName + '</a>';
						}else{
							content += '<a target="_blank" href="' + data[i].siteKeyValue + '">' + data[i].keyCnName + '</a>';
						}
						content += '</div>';
					}
				}
			}
		}
	});
    return content;
}