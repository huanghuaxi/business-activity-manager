var $, layer, flow;
layui.use(['jquery', 'layer', 'flow'], function(){
	$ = layui.jquery
	,layer = layui.layer
	,flow = layui.flow;
	
	if($('body').width() < 960){
		tabPageSize = 4;
		scrollElem = '#activityList';
		$('.class').css("min-height", document.documentElement.clientHeight - $('.footer').height() - 30 + "px");
//		$('#activityList').height($('.class').height() - $('#actCategory').height() - $('.tabBox').height() - 100);
	}
	
	//获取活动类型
	$.ajax({ 
		url: "/ruleVerify/getActivityCategory", 
		data: {},
		dataType: 'json',
		success: function(result){
			if(result.code == "000000"){
				var data = result.data;
				if(data != null){
					category = data;
					for(var i = 0;i < category.length;i++){
						activityImg["log" + category[i].id] = category[i].activityImg;
					}
					getNewAct();
				}
			}
		}
	});
	
	//后退按钮监听
	$('.back').on('click', function(){
		$('#actBox1').show();
		$('#actBox2').hide();
	});
	
	//获取优惠券
	getCoupon();
	
	//监听弹窗关闭按钮
	$('.close').on('click', function(){
		$('#shade').hide();
		$('.apply').hide();
		$('.couponContentBox').hide();
		$('.couponApplyBox').hide();
	});
	
	//监听弹窗关闭按钮
	$('.closeBtn').on('click', function(){
		$('#shade').hide();
		$('.apply').hide();
		$('.couponContentBox').hide();
		$('.couponApplyBox').hide();
	});
	
	//监听搜索按钮
	$('.searchBtn').on('click', function(){
		if($('#searchName').val() == ''){
			getCoupon();
			return;
		}
		$.ajax({ 
			url: "/ruleVerify/searchCoupon", 
			data: {name: $('#searchName').val()},
			dataType: 'json',
			success: function(result){
				var data = result.data;
				if(data != null){
					var content = '';
					for(var i = 0;i < data.length;i++){
						if(i == 0){
							content += '<div class="myApply" style="font-weight: bold;">';
							content += '<div class="title" style="text-align:center;">优惠主题</div>';
							content += '<div class="time">申请时间</div>';
							content += '<div class="examineState">审核状态</div>';
							content += '</div>';
						}
						content += '<div class="myApply">';
						content += '<div class="title">' + data[i].coupon.title + '</div>';
						content += '<div class="time">' + data[i].applyTime.substring(0, 10) + '</div>';
						content += '<div class="examineState">';
						if(data[i].status == 0){
							content += '未审核';
						}else if(data[i].status == 1){
							content += '审核通过';
						}else if(data[i].status == 2){
							content += '审核不通过';
						}
						content += '</div>';
						content += '</div>';
					}
					$('#myApplys').html(content);
				}
				$('.couponApplyBox').show();
				$('#shade').show();
			}
		});
	});
	
	//获取活动大厅图片
	$.ajax({ 
		url: "/ruleVerify/getItemByName", 
		data: {itemName: "activity_bg_img"},
		dataType: 'json',
		success: function(result){
			if(result.code == '000000'){
				var data = result.data;
				if(data != null){
					activityBgImg = data.itemValue;
					if(activityBgImg != null && activityBgImg != ''){
						$('.banner').css("background", "url(" + activityBgImg + ") no-repeat center top");
						$('.banner').css("background-size", "cover");
						$('.banner').css("-webkit-background-size", "cover");
						$('.banner').css("-moz-background-size", "cover");
						$('.banner').css("-o-background-size", "cover");
						$('.banner').css("-ms-background-size", "cover");
						$('.bnr-img').hide();
					}
				}
			}
		}
	});
	
	//获取优惠大厅图片
	$.ajax({ 
		url: "/ruleVerify/getItemByName", 
		data: {itemName: "coupon_bg_img"},
		dataType: 'json',
		success: function(result){
			if(result.code == '000000'){
				var data = result.data;
				if(data != null){
					couponBgImg = data.itemValue;
					if(couponBgImg != null && couponBgImg != ''){
						$('.banner').css("background", "url(" + couponBgImg + ") no-repeat center top");
						$('.banner').css("background-size", "cover");
						$('.banner').css("-webkit-background-size", "cover");
						$('.banner').css("-moz-background-size", "cover");
						$('.banner').css("-o-background-size", "cover");
						$('.banner').css("-ms-background-size", "cover");
						$('.bnr-img').hide();
					}
				}
			}
		}
	});
	
	
});

var activityBgImg = '';
var couponBgImg = '';

var category = [];
var activityImg = [];
var tabPageSize = 6;
var tabPageNo = 1;

//显示活动类型列表
function showCategory(){
	if(category != null){
		var page = tabPageSize * (tabPageNo - 1);
		if(page >= category.length){
			return ;
		}
		var content = '<tr>';
		content += '<th class="border_right tabPage" onclick="tabLeft()">';
		content += "<<";
		content += '</th>';
		for(var i = page;i < (page + tabPageSize);i++){
			if(i < category.length){
				content += '<th class="border_right" id="tab_' + category[i].id + '" onclick="getActivity(' + category[i].id + ')">' 
				content += category[i].actName;
				content += '</th>';
			}

		}
		content += '<th class="tabPage" onclick="tabRight()">';
		content += ">>";
		content += '</th>';
		content += '</tr>';
		$('#actCategory').html(content);
		
	}
}

//活动列表上一页
function tabLeft(){
	if(tabPageNo > 1){
		tabPageNo--;
		showCategory();
	}
}

//活动列表下一页
function tabRight(){
	var page = tabPageSize * tabPageNo;
	if(page < category.length){
		tabPageNo++;
		showCategory();
	}
}

var pageNo = 1;
var pageSize = 10;
var categoryId = 0;
var flag = true;
var scrollElem = 'document';
//获取活动
function getActivity(actId){
	$('.border_right').removeClass("selected");
	$('#tab_' + actId).addClass("selected");
	if(categoryId != actId){
		categoryId = actId;
		$('#activityList').html("");
	}	
	var url = '/ruleVerify/getActivityList?actId=' + actId + '&pageSize=' + pageSize;
	flow.load({
		elem: '#activityList' //指定列表容器
		,scrollElem: scrollElem
	    ,done: function(page, next){ //到达临界点（默认滚动触发），触发下一页
	    	
	    	var lis = [];
	    	//以jQuery的Ajax请求为例，请求下一页数据（注意：page是从1开始返回）
	    	$.get(url + '&pageNo=' + page, function(res){
				//假设你的列表返回在data集合中
				layui.each(res.data, function(index, item){
					var content = '';
					content += '<a href="/ruleVerify/toActivityPage?actId=' + item.id + '">';
					content += '<div class="col-md-4 class-left animated wow fadeInLeft animated" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-name: fadeInLeft;">';
					content += '<figure>';
					content += '<img src="' + activityImg["log" + actId] + '" alt="" class="img_responsive">';
					content += '</figure>';
					content += '<h6>' + item.beginTime.substring(0, 10) + ' - ' + item.endTime.substring(0, 10) + '</h6>';
					content += '<div class="c-btm">';
					content += '<h4>' + item.actName + '</h4>';
					content += '</div>';
					content += '</div>';
					content += '</a>';
					lis.push(content);
				}); 
				
				//执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
				//pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
				/*if(page == 1){
					$('#activityList').html("");
				}*/
				next(lis.join(''), pageSize <= res.data.length);    
	    	});
	    }
	});
}

//获取最新活动
function getNewAct(){
	$.ajax({
		url: '/ruleVerify/getNewActivity',
		type: 'POST',
		dataType: 'json',
		success: function(result){
			if(result.code == '000000'){
				var data = result.data;
				if(data != null){
					var content = '';
					console.log(data);
					for(var i = 0;i < data.length;i++){
						var item = data[i];
						content += '<a onclick="toActivity(\'/ruleVerify/toActivityPage?actId=' + item.id + '\')" >';
						content += '<div class="col-md-4 class-left animated wow fadeInLeft animated" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-name: fadeInLeft;">';
						content += '<figure>';
						content += '<img src="' + activityImg["log" + item.actId] + '" alt="" class="img_responsive">';
						content += '</figure>';
//						content += '<h6>' + item.beginTime.substring(0, 10) + ' - ' + item.endTime.substring(0, 10) + '</h6>';
						content += '<div class="c-btm">';
						content += '<h4>' + item.actName + '</h4>';
						content += '</div>';
						content += '<div class="checkBtm" onclick="checkAct(' + item.actId + ', event)">';
						content += '<span>查看更多</span>';
						content += '</div>';
						content += '</div>';
						content += '</a>';
					}
					$('#activityList').html(content);
				}
			}else{
				layer.msg("获取最新活动失败");
			}
		}
	});
}

//跳转到活动页
function toActivity(url){
	window.location.href = url;
}

//查看更多活动
function checkAct(actId, event){
	$('#actBox1').hide();
	$('#actBox2').show();
	$('#actList').html('');
	var url = '/ruleVerify/getActivityList?actId=' + actId + '&pageSize=' + pageSize;
	flow.load({
		elem: '#actList' //指定列表容器
		,scrollElem: scrollElem
	    ,done: function(page, next){ //到达临界点（默认滚动触发），触发下一页
	    	
	    	var lis = [];
	    	//以jQuery的Ajax请求为例，请求下一页数据（注意：page是从1开始返回）
	    	$.get(url + '&pageNo=' + page, function(res){
				//假设你的列表返回在data集合中
				layui.each(res.data, function(index, item){
					var content = '';
					content += '<a onclick="toActivity(\'/ruleVerify/toActivityPage?actId=' + item.id + '\')" >';
					content += '<div class="col-md-4 class-left animated wow fadeInLeft animated" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-name: fadeInLeft;">';
					content += '<figure>';
					content += '<img src="' + activityImg["log" + actId] + '" alt="" class="img_responsive">';
					content += '</figure>';
//					content += '<h6>' + item.beginTime.substring(0, 10) + ' - ' + item.endTime.substring(0, 10) + '</h6>';
					content += '<div class="c-btm">';
					content += '<h4>' + item.actName + '</h4>';
					content += '</div>';
					content += '</div>';
					content += '</a>';
					lis.push(content);
				}); 
				
				//执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
				//pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
				/*if(page == 1){
					$('#activityList').html("");
				}*/
				next(lis.join(''), pageSize <= res.data.length);    
	    	});
	    }
	});
	event.stopPropagation();
}

var img_show;
//图片预览
function showImg(rewardImg, id){
	var img = new Image();
	img.src = rewardImg;
	var width = img.width;
	if(width > document.documentElement.clientWidth * 0.3){
		width = document.documentElement.clientWidth * 0.3
	}
	var img = "<img class='img_msg' src='"+ rewardImg +"' style='max-width: " + width + "px' />";
    img_show = layer.tips(img, '.actImg_' + id,{
        tips:2
        ,area: [width + 30 + 'px']
    });
}

//隐藏图片
function hiddenImg(){
	layer.close(img_show);
}

//切换tab
function changTab(type){
	if(type == 0){
		//活动中心
		$(".tab").css("text-align", "left");
		$(".activityTab").css("background-color", "#a70001");
		$(".couponTab").css("background-color", "transparent");
		$(".activityBox").show();
		$(".couponBox").hide();
		if(activityBgImg != null && activityBgImg != ''){
			$('.banner').css("background", "url(" + activityBgImg + ") no-repeat center top");
			$('.banner').css("background-size", "cover");
			$('.banner').css("-webkit-background-size", "cover");
			$('.banner').css("-moz-background-size", "cover");
			$('.banner').css("-o-background-size", "cover");
			$('.banner').css("-ms-background-size", "cover");
			$('.bnr-img').hide();
		}else{
			$('.banner').css("background", "");
			$('.bnr-img').show();
		}
	}else{
		//优惠大厅
		$(".tab").css("text-align", "right");
		$(".activityTab").css("background-color", "transparent");
		$(".couponTab").css("background-color", "#a70001");
		$(".activityBox").hide();
		$(".couponBox").show();
		checkApplyPass(0);
		if(couponBgImg != null && couponBgImg != ''){
			$('.banner').css("background", "url(" + couponBgImg + ") no-repeat center top");
			$('.banner').css("background-size", "cover");
			$('.banner').css("-webkit-background-size", "cover");
			$('.banner').css("-moz-background-size", "cover");
			$('.banner').css("-o-background-size", "cover");
			$('.banner').css("-ms-background-size", "cover");
			$('.bnr-img').hide();
		}else{
			$('.banner').css("background", "");
			$('.bnr-img').show();
		}
	}
}

var conpons = [];
//获取优惠券
function getCoupon(){
	$.ajax({ 
		url: "/ruleVerify/getCouponList", 
		data: {},
		dataType: 'json',
		success: function(result){
			if(result.code == "000000"){
				var data = result.data;
				if(data != null){
					coupons = data;
					var img = '';
					for(var i = 0;i < data.length;i++){
						img += '<div class="coupon">';
						img += '<div class="couponImg" onclick="checkApplyPass(' + data[i].id + ')">';
						if(data[i].couponImg != null && data[i].couponImg != ''){
							img += '<img src="' + data[i].couponImg + '" onerror="this.src=\'/activityPage/portal/image/coupon_img.jpg\'">';
						}else{
							img += '<img src="/activityPage/portal/image/coupon_img.jpg">';
						}
						img += '</div>';
						img += '<div class="couponTitle">' + data[i].title + '</div>';
						img += '<div class="couponTime">';
						if(data[i].isTimeLimit){
							if(data[i].startTime != null){
								img += "<div>开始时间：" + strToDate(data[i].startTime) + "</div>";
							}
							if(data[i].endTime != null){
								img += "<div>结束时间：" + strToDate(data[i].endTime) + "</div>";
							}
						}else{
							img += "<div>开始时间：无时间限制 </div>";
							img += "<div>结束时间：无时间限制 </div>";
						}
						img += '</div>';
						img += '<div class="couponBtnBox">';
						if(data[i].isUse){
							img += '<div class="drawBtn" onclick="drawCoupon(' + data[i].id +')">立即申领</div>';
						}
						img += '<div class="contentBtn" onclick="checkCoupon(' + data[i].id + ')">优惠活动规则</div>';
						img += '</div>';
						img += '</div>';
						/*img += '<div class="coupon">'
						img += '<div class="stamp ' + data[i].style + '">';
						img += '<div class="bg">'
						img += '<div class="par">';
						img += '<p>' + (data[i].title == null ? '' : data[i].title) + '&nbsp</p>';
						img += '<sub class="sign"></sub>';
						img += '<span>' + data[i].money + '</span>';
						if(data[i].type == 0){
							img += '<sub>元</sub>';
						}else{
							img += '<sub>折</sub>';
						}
						img += '<p>' + (data[i].useCondition == null ? '' : data[i].useCondition) + '&nbsp</p>';
						img += '</div>';
						img += '<i></i>'
						img += '<div class="copy1">副券'
						img += '<p>' + data[i].startTime.substring(0, 10) + '<br>' + data[i].endTime.substring(0, 10) + '</p>';
						img += '<div class="draw" onclick="drawCoupon(' + data[i].id +')">立即申领</div>'
						img += '</div>';
						img += '</div>';
						
						img += '</div>';
						img += '</div>';*/
					}
					$('.coupons').html(img);
					
				}
			}
		}
	});
}

//领取优惠券
function drawCoupon(id){
	$('#shade').show();
	$('.apply').show();
	$('#couponId').val(id);
}
//优惠券申请
function couponApply(){
	var name = $('#name').val();
	if(name == null || name == ''){
		layer.msg("请填写账号");
		return false;
	}
	$.ajax({
		url: '/ruleVerify/couponApply',
		data: {couponId: $("#couponId").val(), name: name},
		type: 'POST',
		dataType: 'json',
		success: function(result){
			layer.msg(result.msg);
			if(result.code == "000000"){
				$('#shade').hide();
				$('.apply').hide();
				$('#couponId').val("");
				$('#name').val("");
			}
		}
	});
}
//查看优惠券规则
function checkCoupon(id){
	$('#couponContent').html("");
	for(var i = 0;i < coupons.length;i++){
		if(coupons[i].id == id){
			$('#couponContent').html(coupons[i].content);
			break;
		}
	}
	$('.couponContentBox').show();
	$('#shade').show();
}

//查看申请通过账号
function checkApplyPass(couponId){
	$('#applyPassMsg').html("");
	$.ajax({
		url: '/ruleVerify/getCouponPass',
		data: {couponId: couponId},
		type: 'POST',
		dataType: 'json',
		success: function(result){
			if(result.code == "000000"){
				var data = result.data;
				var title = '';
				/*for(var i = 0;i < coupons.length;i++){
					if(coupons[i].id == couponId){
						title = coupons[i].title;
						break;
					}
				}*/
				var content = '<div class="msg">&#9;</div>';
				if(data != null && data.length > 0){
					for(var i = 0;i < data.length;i++){
						console.log(data[i]);
						if(i + 1 < data.length){
							content += '<div class="msg">' + data[i] + ',</div>';
						}else{
							content += '<div class="msg">' + data[i] + '</div>';
						}
						
					}
				}else{
					content += '<div class="msg">没有审核通过账号</div>';
				}
			}else{
				var content = '<div class="msg">获取优惠活动申请通过账号失败 </div>';
			}
			$('#applyPassMsg').html(content);
			$('.info').stop(true, true);
			animate();
			
		}
	});
}

//中奖纪录滚动
function animate() {
    var i = 0;
    i++;
    var width = 0;
    var s = 0;
    $('.msg').each(function(index){
    	width += $(this).width() + 30;
    	s += 30 * $(this).width();
    });
    console.log(width);
    $('.info').width(width + "px");
    if (i < $('.msg').length) {
        $('.info').animate({
            left: -width + 'px'
        }, s, 'linear', function () {
            $('.info').css({ 'left': '0px' });
            i = 0;
            animate();
        })
    }
}

//时间格式转换
function strToDate(str){
	var date = new Date(str);
	var date2 = date.getFullYear() + "年" + (date.getMonth() + 1) + "月" + date.getDate() + "日 ";
	date2 += date.getHours() + "时" + date.getMinutes() + "分" + date.getSeconds() + "秒";
	return date2;
}

function CheckImgExists(imgurl) {
    var imgObj = new Image(); //判断图片是否存在  
    imgObj.src = imgurl;  
    //存在图片
    console.log(imgObj);
    console.log("imgurl:" + imgurl);
    console.log("size:" + imgObj.fileSize);
    console.log("width:" + imgObj.width);
    console.log("height:" + imgObj.height);
    if (imgObj.fileSize > 0 || (imgObj.width > 0 && imgObj.height > 0)) {  
         return true;
    } else {  
         return false;
     }   
}