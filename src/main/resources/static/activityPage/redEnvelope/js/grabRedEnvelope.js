layui.use(['jquery', 'layer'], function(){
	$ = layui.jquery
	,layer = layui.layer;
	
	//获取站点域名配置
	$('.menu').html(getDomainKv('red_envelope'));
	
	//显示活动时间
	showActTime();
	
	//我的奖品按钮监听
	$('#myReward').on('click', function(){
		if($('#memberId').val() == null || $('#memberId').val() == ''){
			$('#shade').show();
			$('.login').show();
		}else{
			$('#shade').show();
			$('.recordBox').show();
			var content = getMyReward();
			$('#record').html(content);
		}
	});
	
	//点击关闭弹窗按钮隐藏弹窗
	var closeValue = ['.login', '.gitReward', '.ruleBox', '.rewardBox', '.recordBox', '#shade'];
	closePopup(closeValue);
	
	$('.closeBtn').on('click', function(){
		$('.gitReward').hide();
		$('#shade').hide();
	});
	
	var rewards = [];
	var page = 0;
	var pageSize = 3;
	//查看奖品监听
	$('.checkReward').on('click', function(){
		page = 0;
		if(rewards == null || rewards.length == 0){
			rewards = getRewards();
		}
		var content = checkRewardByPage(rewards, page, pageSize);
		$('.rewards').html(content);
		$('#shade').show();
		$('.rewardBox').show();
	});

	//查看奖品左翻页监听
	$('.leftBtn').on('click', function(){
		if((page - 1) * pageSize >= 0){
			page--;
			var content = checkRewardByPage(rewards, page, pageSize);
			$('.rewards').html(content);
		}
	});
	
	//查看奖品右翻页监听
	$('.rightBtn').on('click', function(){
		if((page + 1) * pageSize < rewards.length){
			page++;
			var content = checkRewardByPage(rewards, page, pageSize);
			$('.rewards').html(content);
		}
	});
	
	//拆红包监听
	$('.redBag').on('click', function(){
		//判断是否登录
		if($('#memberId').val() == null || $('#memberId').val() == ''){
			$('#shade').show();
			$('.login').show();
		}else{
			//获取奖品
			$.ajax({ 
				url: "/ruleVerify/redEnvelopeRuleVerify", 
				data: {actId: $('#actId').val(), centralActId: $('#mainActId').val()},
				dataType: 'json',
				success: function(result){
					if(result.code == '000000'){
						if(result.data != null && result.data != ''){
							var reward = result.data.length > 4 ? result.data.substring(0,4) + "..." : result.data;
							$('.rewardCon').html("恭喜你获得<br>" + reward + "大奖");
							$('#recordInfo').html(getRewardRecordByActId($('#actId').val()));
						}else{
							$('.rewardCon').html("谢谢参与");
						}
						$('#shade').show();
						$('.gitReward').show();
						getMemberAct();
					}else{
						layer.msg(result.msg);
					}
				}
			});
		}
	});
	
	//中奖记录滚动
	var rewardRecords = getRewardRecordByActId($('#actId').val());
	$('#recordInfo').html(rewardRecords);
	//中奖记录动画
	rewardFlash();
	//获取奖品
    rewards = getRewards();
    
});

