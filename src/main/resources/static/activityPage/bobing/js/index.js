
var jquery, layer;
layui.use(['jquery', 'layer'], function(){
	jquery = layui.jquery
	,layer = layui.layer;
	
	//获取站点域名配置
	jquery('.menu').html(getDomainKv('bobing'));
	
	$('.menuBtn').css("left", ($("body").width() - $("#root").width()) / 2 + "px");
	$('.menu').css("left", ($("body").width() - $("#root").width()) / 2 + "px");
	
	//显示活动时间
	showActTime();
	
	//活动规格按钮监听
	/*$('#ruleBtn').on('click', function(){
		$('#rule').popover('toggle');
	});*/
	//中奖记录动画
	rewardFlash();
	
	function rewardFlash(){
		var count = jquery('.msg').length;
		var i = 0;
		setInterval(function(){
			if(i + 1 == count){
				i = 0;
				jquery('.msg').show();
			}else{
				jquery('#msg_' + i).slideUp();
				i++;
			}
			
		}, 3000);
	}
	
});

(function($) {
	var times = 0;
	var flag = 0;
	function getMemberAct(){
		$.ajax({ 
			url: "/ruleVerify/getMemberAct", 
			data: {memberId: document.getElementById("memberId").value, actId: document.getElementById("actId").value},
			dataType: 'json',
			async: false,
			success: function(result){
				if(result.code == "000000"){
					memberAct = result.data;
					if(memberAct == null){
						layer.msg("您没有参与此活动");
					}else{
						if(memberAct.allTimes != null && memberAct.allTimes != ''){
							times = memberAct.allTimes;
						}else{
							times = 0;
						}
					}
				}else{
					layer.msg("获取活动会员信息失败");
				}
			}
		});
	}
	
	if(document.getElementById("memberId").value != null && document.getElementById("memberId").value != ''){
		//获取会员信息
		getMemberAct();
	}
	
	//获取中奖记录
	function getRewardRecordByActId(actId){
		var content = '';
		$.ajax({ 
			url: "/ruleVerify/getRewardRecordByActId", 
			data: {actId: actId},
			dataType: 'json',
			async: false,
			success: function(result){
				if(result.code == "000000"){
					var data = result.data;
					if(data != null){
						for(var i = 0;i < data.length;i++){
							content += '<div class="msg" id="msg_' + i +'">';
							content += ' 恭喜';
							content += data[i].remName;
							content += '博到';
							content += data[i].rewardCon;
							content += '</div>';
						}
					}
				}
			}
		});
		return content;
	}
	var allRecord = getRewardRecordByActId(document.getElementById("actId").value);
	
	
	var showBgLightTime,
		t; //计时器
	var vm = new Vue({
		el: '#root',
		data: {
			showBgLight: false, //显示背景光圈
			showWinPopup: false, //显示获奖弹窗
			remainingNumber: times, //剩余次数
			points: [1, 2, 4, 3, 6, 5], //随机点数集合,初始化为1
			phone: '', //手机号码
			winUserTip: allRecord, //系统中奖用户提示
			ranking: '三', //中奖等级
			prize: '洗发水', //奖品
			isShake: false, //骰子是否在摇动
			timeStr: '', //倒计时
			recordList: [{
				time: '2019/08/12 16:18',
				goods: '海尔空调1.5P＊1',
				isSend: false,
				sendStatus: '未派奖'
			}, {
				time: '2019/08/12 16:18',
				goods: '海尔空调1.5P＊1',
				isSend: false,
				sendStatus: '未派奖'
			}, {
				time: '2019/08/12 16:18',
				goods: '海尔空调1.5P＊1',
				isSend: true,
				sendStatus: '已派奖'
			}], //中奖查询列表
			isShowPhoneErrorTip: false,
			prizeImg: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566487686&di=6d738036129e0ca0d18d96315db74308&imgtype=jpg&er=1&src=http%3A%2F%2Fimg005.hc360.cn%2Fhb%2FMTQ3MzE1NDA0MDMyMTEwMTk1MzU5NjU%3D.jpg', //奖品图片

		},
		mounted: function() {
			this.$nextTick(function() {
				this.init();
			});
			

		},
		methods: {
			init: function() {
				$('.mui-scroll-wrapper').scroll()
				/*vm.getDate();
				t = setInterval(function() {
					vm.getDate();
				}, 1000);*/
			},
			checkPhone: function() {
				if (!(/^1[3456789]\d{9}$/.test(vm.phone))) {
					vm.isShowPhoneErrorTip = true;
				} else {
					vm.isShowPhoneErrorTip = false;
				}
			},
			showRule: function() {
				$('#rule').popover('toggle');
			},
			showLogin: function() {
				if(document.getElementById("memberName").value == ""){
					$.toast('请输入账号，非会员请输入手机号');
					return;
				}
				login();
			},
			hideLogin: function() {
				$('#loginBox').popover('toggle');
			},
			showRecord: function() {
				//判断是否登录
				if(jquery("#memberId").val() == null || jquery("#memberId").val() == ''){
					$('#loginBox').popover('toggle');
				}else{
					$.ajax({ 
						url: "/ruleVerify/getRewardRecord", 
						data: {actId: jquery('#actId').val(), memberId: jquery('#memberId').val(), centralActId: jquery('#mainActId').val()},
						dataType: 'json',
						async: false,
						success: function(result){
							console.log(result);
							if(result.code == "000000"){
								var records = [];
								var record = {};
								if(result.data != null){
									var data = result.data;
									for(var i = 0;i < data.length;i++){
										record = {};
										record.time = createTime(data[i].createTime);
										console.log(data[i].rewardCon);
										if(data[i].isGit){
											record.goods = data[i].rewardCon;
										}else{
											record.goods = "谢谢参与";
										}
										record.isSend = data[i].isSend;
										if(data[i].isSend == 1){
											record.sendStatus = "已派奖";
										}else{
											record.sendStatus = "未派奖";
										}
										records.push(record);
									}
								}
								console.log(records);
								vm.recordList = records;
								$('#record').popover('toggle');
							}else{
								layer.msg("获取中奖记录失败");
							}
						}
					});
					
				}
				
			},
			focusout: function() {
				document.body.scrollTop = 0;
			},
			shake: function() {
				if(flag != 0){
					return;
				}
				flag = 1;
				if(jquery("#memberId").val() == null || jquery("#memberId").val() == ''){
					$('#loginBox').popover('toggle');
				}else{
					if (vm.remainingNumber < 1) {
						$.toast('次数已经用完');
						return;
					}
					
					//博饼
					$.ajax({ 
						url: "/ruleVerify/bobingRuleVerify", 
						data: {actId: jquery('#actId').val(), centralActId: jquery('#mainActId').val()},
						dataType: 'json',
						success: function(result){
							if(result.code == '000000'){
								console.log(result.data);
								if(result.data.reward != null && result.data.reward != ''){
									var reward = result.data;
									vm.ranking = reward.rewardConf.rewardOption;
									vm.prize = reward.reward;
									vm.prizeImg = reward.rewardConf.rewardImg;
								}else{
									vm.ranking = "无";
									vm.prize = "谢谢参与";
									vm.prizeImg = "";
								}
								getMemberAct();
								vm.isShake = true;
								vm.remainingNumber = times;
								vm.points = result.data.dices.split(",");
								setTimeout(function() {
									vm.isShake = false;
									vm.showWin();
								}, 1500);
								
								vm.winUserTip = getRewardRecordByActId(document.getElementById("actId").value);
							}else{
								layer.msg(result.msg);
							}
						}
					});
					
					
				}
			},
			showWin: function() {
				vm.showWinPopup = true;
				showBgLightTime = setTimeout(function() {
					vm.showBgLight = true;
				}, 1600);
			},
			closeWin: function() {
				flag = 0;
				vm.showWinPopup = false;
				vm.showBgLight = false;
				if (showBgLightTime) {
					clearTimeout(showBgLightTime);
					showBgLightTime = null;
				}
			},
			tow: function(n) {
				return n >= 0 && n < 10 ? '0' + n : '' + n;
			}
			/*getDate: function() {
				var oDate = new Date(); //获取日期对象
				var oldTime = oDate.getTime(); //现在距离1970年的毫秒数
				var newDate = new Date('2019/9/13 00:00:00');
				var newTime = newDate.getTime(); //2019年距离1970年的毫秒数
				var second = Math.floor((newTime - oldTime) / 1000); //未来时间距离现在的秒数
				var day = Math.floor(second / 86400); //整数部分代表的是天；一天有24*60*60=86400秒 ；
				second = second % 86400; //余数代表剩下的秒数；
				var hour = Math.floor(second / 3600); //整数部分代表小时；
				second %= 3600; //余数代表 剩下的秒数；
				var minute = Math.floor(second / 60);
				second %= 60;
				vm.timeStr = vm.tow(day) + '天 ' +
					vm.tow(hour) + ':' +
					vm.tow(minute) + ':' +
					vm.tow(second);
				if (day < 1 && hour < 1 && minute < 1 && second < 1) {
					clearTimeout(t);
				}
			}*/

		}
	});

})(mui);


