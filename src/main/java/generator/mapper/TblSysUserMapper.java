package generator.mapper;

import generator.entity.TblSysUser;
import java.util.List;

public interface TblSysUserMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_sys_user
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_sys_user
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    int insert(TblSysUser record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_sys_user
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    TblSysUser selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_sys_user
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    List<TblSysUser> selectAll();

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_sys_user
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    int updateByPrimaryKey(TblSysUser record);
}