package generator.entity;

import java.util.Date;

public class TblSysUser {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.id
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.user_name
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private String userName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.pwd
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private String pwd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.is_lock
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private Integer isLock;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.descr
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private String descr;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.create_time
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.create_person
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private Integer createPerson;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.mail
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private String mail;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.is_valide
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private Integer isValide;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_sys_user.fail_count
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    private Integer failCount;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.id
     *
     * @return the value of tbl_sys_user.id
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.id
     *
     * @param id the value for tbl_sys_user.id
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.user_name
     *
     * @return the value of tbl_sys_user.user_name
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public String getUserName() {
        return userName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.user_name
     *
     * @param userName the value for tbl_sys_user.user_name
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.pwd
     *
     * @return the value of tbl_sys_user.pwd
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.pwd
     *
     * @param pwd the value for tbl_sys_user.pwd
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.is_lock
     *
     * @return the value of tbl_sys_user.is_lock
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public Integer getIsLock() {
        return isLock;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.is_lock
     *
     * @param isLock the value for tbl_sys_user.is_lock
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setIsLock(Integer isLock) {
        this.isLock = isLock;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.descr
     *
     * @return the value of tbl_sys_user.descr
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public String getDescr() {
        return descr;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.descr
     *
     * @param descr the value for tbl_sys_user.descr
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.create_time
     *
     * @return the value of tbl_sys_user.create_time
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.create_time
     *
     * @param createTime the value for tbl_sys_user.create_time
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.create_person
     *
     * @return the value of tbl_sys_user.create_person
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public Integer getCreatePerson() {
        return createPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.create_person
     *
     * @param createPerson the value for tbl_sys_user.create_person
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.mail
     *
     * @return the value of tbl_sys_user.mail
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public String getMail() {
        return mail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.mail
     *
     * @param mail the value for tbl_sys_user.mail
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.is_valide
     *
     * @return the value of tbl_sys_user.is_valide
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public Integer getIsValide() {
        return isValide;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.is_valide
     *
     * @param isValide the value for tbl_sys_user.is_valide
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setIsValide(Integer isValide) {
        this.isValide = isValide;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_sys_user.fail_count
     *
     * @return the value of tbl_sys_user.fail_count
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public Integer getFailCount() {
        return failCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_sys_user.fail_count
     *
     * @param failCount the value for tbl_sys_user.fail_count
     *
     * @mbg.generated Sun Apr 07 16:15:54 CST 2019
     */
    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }
}