package generator.entity;

import java.util.Date;

public class TblSiteKv {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_kv.id
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_kv.key_cn_name
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    private String keyCnName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_kv.key_en_name
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    private String keyEnName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_kv.descr
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    private String descr;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_kv.create_time
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_kv.create_persion
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    private Integer createPersion;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_kv.id
     *
     * @return the value of tbl_site_kv.id
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_kv.id
     *
     * @param id the value for tbl_site_kv.id
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_kv.key_cn_name
     *
     * @return the value of tbl_site_kv.key_cn_name
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public String getKeyCnName() {
        return keyCnName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_kv.key_cn_name
     *
     * @param keyCnName the value for tbl_site_kv.key_cn_name
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public void setKeyCnName(String keyCnName) {
        this.keyCnName = keyCnName == null ? null : keyCnName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_kv.key_en_name
     *
     * @return the value of tbl_site_kv.key_en_name
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public String getKeyEnName() {
        return keyEnName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_kv.key_en_name
     *
     * @param keyEnName the value for tbl_site_kv.key_en_name
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public void setKeyEnName(String keyEnName) {
        this.keyEnName = keyEnName == null ? null : keyEnName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_kv.descr
     *
     * @return the value of tbl_site_kv.descr
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public String getDescr() {
        return descr;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_kv.descr
     *
     * @param descr the value for tbl_site_kv.descr
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_kv.create_time
     *
     * @return the value of tbl_site_kv.create_time
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_kv.create_time
     *
     * @param createTime the value for tbl_site_kv.create_time
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_kv.create_persion
     *
     * @return the value of tbl_site_kv.create_persion
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public Integer getCreatePersion() {
        return createPersion;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_kv.create_persion
     *
     * @param createPersion the value for tbl_site_kv.create_persion
     *
     * @mbg.generated Wed Apr 17 09:37:03 CST 2019
     */
    public void setCreatePersion(Integer createPersion) {
        this.createPersion = createPersion;
    }
}