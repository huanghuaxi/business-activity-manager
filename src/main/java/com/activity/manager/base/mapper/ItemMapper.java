package com.activity.manager.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.activity.manager.base.entity.Item;

/**
 * 
 * <p>Description: 加载键值类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月7日   
 * @version 1.0
 */
@Mapper
public interface ItemMapper {
    
	int deleteByPrimaryKey(Integer id);

    int insert(Item record);

    Item selectByPrimaryKey(Integer id);

    List<Item> selectAll();

    int updateByPrimaryKey(Item record);
    
    /**
     * 
     * Description: 根据名称获取键值 
     * @param itemName
     * @return 
     * date 2019年8月7日
     */
    Item selectByName(String itemName);
}