package com.activity.manager.base.service;

import java.util.List;

import com.activity.manager.base.entity.Item;

/**
 * 
 * <p>Description: 加载键值类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月7日   
 * @version 1.0
 */
public interface ItemService {
	
	/**
	 * 
	 * Description: 获取全部键值 
	 * @return 
	 * date 2019年8月7日
	 */
	public List<Item> selectAll();
	
	/**
	 * 
	 * Description: 根据名称获取键值 
	 * @param itemName
	 * @return 
	 * date 2019年8月7日
	 */
	public Item selectByName(String itemName);
	
	/**
	 * 
	 * Description: 添加键值 
	 * @param item 
	 * date 2019年8月7日
	 */
	public void addItem(Item item);
	
	/**
	 * 
	 * Description: 修改键值 
	 * @param item 
	 * date 2019年8月7日
	 */
	public void editItem(Item item);
	
}
