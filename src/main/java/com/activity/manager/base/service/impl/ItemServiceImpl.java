package com.activity.manager.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.base.entity.Item;
import com.activity.manager.base.mapper.ItemMapper;
import com.activity.manager.base.service.ItemService;

/**
 * 
 * <p>Description: 更新键值类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月7日   
 * @version 1.0
 */

@Service
public class ItemServiceImpl implements ItemService{

	@Autowired
	private ItemMapper itemMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectAll</p>   
	 * <p>Description: 获取全部键值</p>   
	 * @return   
	 * @see com.activity.manager.base.service.ItemService#selectAll()
	 */
	@Override
	public List<Item> selectAll() {
		return itemMapper.selectAll();
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByName</p>   
	 * <p>Description: 根据名称查询键值</p>   
	 * @param itemName
	 * @return   
	 * @see com.activity.manager.base.service.ItemService#selectByName(java.lang.String)
	 */
	@Override
	public Item selectByName(String itemName) {
		Assert.hasText(itemName, "键值名称数据不存在");
		return itemMapper.selectByName(itemName);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: addItem</p>   
	 * <p>Description: 添加键值</p>   
	 * @param item   
	 * @see com.activity.manager.base.service.ItemService#addItem(com.activity.manager.base.entity.Item)
	 */
	@Override
	public void addItem(Item item) {
		Assert.hasText(item.getItemName(), "键值名数据不存在");
		Item item1 = itemMapper.selectByName(item.getItemName());
		if(item1 == null) {
			itemMapper.insert(item);
		}else {
			item1.setItemValue(item.getItemValue());
			itemMapper.updateByPrimaryKey(item1);
		}
		
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editItem</p>   
	 * <p>Description: 修改键值</p>   
	 * @param item   
	 * @see com.activity.manager.base.service.ItemService#editItem(com.activity.manager.base.entity.Item)
	 */
	@Override
	public void editItem(Item item) {
		Assert.notNull(item.getId(), "键值ID数据不存在");
		Assert.hasText(item.getItemName(), "键值名数据不存在");
		Assert.hasText(item.getItemValue(), "键值参数数据不存在");
		Item item1 = itemMapper.selectByPrimaryKey(item.getId());
		Assert.notNull(item1, "键值数据不存在");
		itemMapper.updateByPrimaryKey(item);
	}

}
