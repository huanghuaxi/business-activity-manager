package com.activity.manager.base.entity;

/**
 * 
 * <p>Description: 键值类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月7日   
 * @version 1.0
 */
public class Item {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	键
     */
    private String itemName;

    /**
     *	值
     */
    private String itemValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName == null ? null : itemName.trim();
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue == null ? null : itemValue.trim();
    }
}