package com.activity.manager.base.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.base.entity.Item;
import com.activity.manager.base.mapper.ItemMapper;
import com.activity.manager.base.service.ItemService;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;

/**
 * 
 * <p>Description: 键值控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月7日   
 * @version 1.0
 */

@Controller
@RequestMapping("/item")
public class ItemController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ItemService itemService;
	
	/**
	 * 
	 * Description: 获取所有键值 
	 * @return 
	 * date 2019年8月7日
	 */
	@ResponseBody
	@RequestMapping("/getItemAll")
	public ResultMessage getItemAll() {
		try {
			logger.info("获取所有键值");
			List<Item> items = itemService.selectAll();
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, items);
		}catch (Exception e) {
			logger.error("获取所有键值异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 根据名称查询键值 
	 * @param itemName
	 * @return 
	 * date 2019年8月7日
	 */
	@ResponseBody
	@RequestMapping("/getItemByName")
	public ResultMessage getItemByName(String itemName) {
		try {
			logger.info("根据名称查询键值");
			Item item = itemService.selectByName(itemName);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, item);
		}catch (Exception e) {
			logger.error("根据名称查询键值异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 新增或修改键值 
	 * @param item
	 * @return 
	 * date 2019年8月7日
	 */
	@ResponseBody
	@RequestMapping("/saveOrEdit")
	public ResultMessage saveOrEdit(Item item) {
		try {
			logger.info("新增或修改键值");
			itemService.addItem(item);
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("新增或修改键值异常", e);
		}
		return ResultMessage.getFail();
	}
	
}
