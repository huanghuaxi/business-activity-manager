package com.activity.manager.security.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import com.activity.manager.security.entity.SysPermission;
import com.activity.manager.security.entity.SysRole;
import com.activity.manager.security.mapper.PermissionMapper;

@Service
public class MyInvocationSecurityMetadataSourceService implements FilterInvocationSecurityMetadataSource {

	@SuppressWarnings("unused")
	private HashMap<String, Collection<ConfigAttribute>> map = null;

	private List<SysPermission> permissionList;

	/**
	 * 权限配置
	 */
	@Autowired
	private PermissionMapper permissionMapper;

	/**
	 * 加载权限表中所有权限
	 */
	public Collection<ConfigAttribute> loadResourceDefine(Integer id) {
		map = new HashMap<>();
		List<SysPermission> permissions = permissionMapper.selectIdAndUrl(id);
		Collection<ConfigAttribute> array = new ArrayList<>();
		for (SysPermission permission : permissions) {
			for (SysRole role : permission.getRoleList()) {
				ConfigAttribute cfg = new SecurityConfig(String.valueOf(role.getId()));
				array.add(cfg);
			}
		}
		return array;
	}

	public void loadPermission() {
		permissionList = permissionMapper.selectAll();
	}

	// 此方法是为了判定用户请求的url 是否在权限表中，如果在权限表中，则返回给 decide 方法，用来判定用户是否有此权限。如果不在权限表中则放行。
	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
//		if (permissionList == null)
			loadPermission();
		// object 中包含用户请求的request 信息
		HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
		AntPathRequestMatcher matcher;
		String resUrl;
		for(SysPermission permission: permissionList) {
			resUrl = permission.getUrl();
			String resUri = resUrl;
			boolean b = false;
			//判断是否有参数
			if (resUrl!=null && resUrl.indexOf("?") > -1) {
				String[] data = resUrl.split("[?]");
				String[] params = data[1].split("&");
				int a = 0;
				for (int i = 0; i < params.length; i++) {
					String param = request.getParameter(params[i].split("=")[0]);
					//参数判断
					if (param != null && !"".equals(param) && param.equals(params[i].split("=")[1])) {
						a++;
					}
				}

				if (a == params.length) {
					b = true;
					resUri = data[0];
				}
			} else {
				b = true;
			}
			matcher = new AntPathRequestMatcher(resUri);
//			String uri = request.getRequestURI();
			if (matcher.matches(request) && b) {
				Collection<ConfigAttribute>  collection =  loadResourceDefine(permission.getId());
				if(collection!=null && collection.isEmpty()) {
					ConfigAttribute cfg = new SecurityConfig("-1");
					collection.add(cfg);
				}
				return collection;	
			}
		}
		// 没有匹配上的资源，都是登录访问
		return null;

	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		return null;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

}
