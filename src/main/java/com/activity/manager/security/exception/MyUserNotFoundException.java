package com.activity.manager.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * <p>Description: 自定义定义用户没发现异常 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月28日   
 * @version 1.0
 */
public class MyUserNotFoundException extends AuthenticationException  {
	
  /** 
	*serialVersionUID
	*/ 
  private static final long serialVersionUID = 1L;
  
  public MyUserNotFoundException(String msg) {
        super(msg);
    }

    public MyUserNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

}
