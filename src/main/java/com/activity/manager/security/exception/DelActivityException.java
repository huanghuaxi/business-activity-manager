package com.activity.manager.security.exception;

public class DelActivityException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DelActivityException(String msg) {
		super(msg);
	}

	public DelActivityException(String msg, Throwable t) {
		super(msg, t);
	}

}
