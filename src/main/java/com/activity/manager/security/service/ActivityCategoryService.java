package com.activity.manager.security.service;

import java.util.List;

import com.activity.manager.activity.entity.ActivityButton;
import com.activity.manager.activity.entity.ActivityVersion;
import com.activity.manager.security.entity.ActivityCategory;

/**
 * <p>Description: 活动类型配置类 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月15日   
 * @version 1.0
 */
public interface ActivityCategoryService {
	
	 /**
	 * Description: 活动类型配置
	 * @date 2019年4月6日    
	 * @param actName
	 * @return
	 */
	List<ActivityCategory> queryActivity(String actName,int start ,int pageSize);
	
	/**
	 * Description: 活动类型配置
	 * @date 2019年4月6日    
	 * @param actName
	 * @return
	 */
	Integer activityCount(String actName);

	/**
	 * Description: 插入活动类型表
	 * @param activityCate 
	 * date 2019年4月15日
	 */
	void insertActivityCategory(ActivityCategory activityCate);
	
	/**
	 * Description: 查询所有版本    
	 * @return 
	 * date 2019年4月15日
	 */
	List<ActivityVersion> selectAll();
	
	/**
	 * Description:插入活动分类  
	 * @param record
	 * @return 
	 * date 2019年4月16日
	 */
	int insert(ActivityVersion record);
	
	/**
	 * 
	 * Description:    
	 * @param record
	 * @return 
	 * date 2019年4月16日
	 */
	int insert(ActivityButton record);  
	
	/**
	 * Description:插入活动版本    
	 * @param vers 
	 * date 2019年4月16日
	 */
	void insertData(ActivityVersion vers);
	
	/**
	 * Description: 活动按钮 
	 * @parma id
	 * @return 
	 * date 2019年4月17日
	 */
	List<ActivityButton> getButtons(Integer id);
	
	/**
	 * Description: 修改版本信息 
	 * @param vers 版本数据
	 * date 2019年4月17日
	 */
	void updateVersion(ActivityVersion vers);
	
	 /**
     * Description: 删除版本按钮    
     * @param id 
     * date 2019年4月17日
     */
    void delBtnsByVerId(Integer id);
    
    /**
     * Description: 修改版本按钮   
     * @param record 
     * date 2019年4月17日
     */
    void updateActivityBtnById(ActivityButton record);
    
    /**
     * Description: 删除主键
     * @param id 
     * date 2019年4月17日
     */
    void deleteByPrimaryKey(Integer id);
    
    /**
     * Description: 更新版本信息 
     * @param record 
     * date 2019年4月17日
     */
    void updateVerById(ActivityVersion record);
    
    /**
     * Description:  查询该版本下的该按钮  
     * @param verId
     * @param id
     * @return 
     * date 2019年4月17日
     */
    ActivityButton  selectByVerAndId(Integer verId,Integer id);
    
    /**
     * Description: 根据id 删除版本 
     * @param verId 
     * date 2019年4月17日
     */
    void deleteVerByPrimaryKey(Integer verId);
    
    /**
     * 
     * Description:    
     * @param verId 
     * date 2019年4月17日
     */
    void delVersion(String verIds);
    
    /**
	 * Description: 更新活动类型    
	 * @param record 
	 * date 2019年4月17日
	 */
	void updateCateById(ActivityCategory record);
	
	/**
	 * Description: 删除活动类型 
	 * @param ids 
	 * date 2019年4月17日
	 */
	void delCategory(String ids);
	
	/**
	 * Description: 获取活动类型    
	 * @param id 活动类型id
	 * @return 
	 * date 2019年4月17日
	 */
	ActivityCategory selectActByPrimaryKey(Integer id);
	
	/**
     * 
     * Description: 根据活动类型ID获取版本    
     * @param actId
     * @return 
     * date 2019年4月15日
     */
    List<ActivityVersion> selectVerByActId(Integer actId);
    
    /**
     * Description: 删除活动类型主键  
     * @param actId 
     * date 2019年4月17日
     */
   void deleteActByPrimaryKey(Integer actId);
   
   /**
    * Description: 查询版本    
    * @param verName
    * @param actId
    * @return 
    * date 2019年4月18日
    */
   List<ActivityVersion> selectVerByActIdAndVerName(String verName,String actId);
   
	/**
	 * 
	 * Description: 修改是否在活动演示页展示    
	 * @param record 
	 * date 2019年6月3日
	 */
	void updateActivityShow(ActivityCategory record);
	
	/**
	 * 
	 * Description: 修改活动图片
	 * @param id
	 * @param activityImg 
	 * date 2019年6月4日
	 */
	void editActivityImg(Integer id, String activityImg);
}
