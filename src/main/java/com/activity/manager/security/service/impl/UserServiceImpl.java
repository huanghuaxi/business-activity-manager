package com.activity.manager.security.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.security.entity.Menu;
import com.activity.manager.security.entity.SysRole;
import com.activity.manager.security.entity.SysUser;
import com.activity.manager.security.mapper.MenuMapper;
import com.activity.manager.security.mapper.SysRoleMapper;
import com.activity.manager.security.mapper.UserMapper;
import com.activity.manager.security.service.UserService;
import com.alibaba.druid.util.StringUtils;

/**
 * 
 * Description: 用户更新类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月1日   
 * @version 1.0
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private MenuMapper menuMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;
	
	public SysUser getUserByName(String userName) {
		return userMapper.selectByUserName(userName);
	}

	@Override
	public List<Menu> loadMenu(List<SysRole> roleLst) {
		return loadMenu(0,roleLst);
	}
	
	private List<Menu> loadMenu(Integer parentId,List<SysRole> roleLst){
		List<Menu> list = menuMapper.selectMenu(parentId,roleLst);
	 	for(Menu menu:list) {
	 	   if(!menu.leaf) {
	 		 List<Menu> chillst = loadMenu(menu.getId(),roleLst);
	 		 menu.setChildren(chillst);
	 	   }
	 	}
		return list;
	}

	@Override
	public void updateFailCount(Integer uid, int failCount) {
		userMapper.updateFailCount(uid, failCount);
	}

	/**
	 * 查询用户
	 */
	@Override
	public List<SysUser> queryUser(String userName,int start ,int pageSize) {
		return userMapper.queryUser(userName,start,pageSize);
	}

	/**
	 * 查询用户总数
	 */
	@Override
	public Integer userCount(String userName) {
		return userMapper.userCount(userName);
	}

	@Override
	public void delUser(List<String> ids) {
		userMapper.delUser(ids);		
	}

	@Override
	public void delUserRoleRef(List<String> ids) {
		userMapper.delUserRoleRef(ids);
	}
	
	/**
	 * 删除用户数据
	 * @param ids
	 */
 	@Transactional
	public void delUserRefData(List<String> ids) {
		delUser(ids);
		delUserRoleRef(ids);
	}

	@Override
	public List<SysRole> selectAllBySate(String userId) {
		 List<SysRole> roleLst =StringUtils.isEmpty(userId)?null:sysRoleMapper.selectAllByUserId(userId);
		 List<SysRole> allRoleLst = sysRoleMapper.selectAllBySate();
		 if(roleLst!=null && roleLst.size()!=0) {
			 roleLst.forEach(role->{
				 int roleId = role.getId();
				 allRoleLst.forEach(allRole->{
					 if(roleId==allRole.getId()) 
						 allRole.setHave(true);
				 });
			 });
		 }
		return allRoleLst;
	}

	@Override
	@Transactional
	public void addUser(SysUser sysuser) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		sysuser.getPwd();
		sysuser.setPwd(encoder.encode(sysuser.getPwd().trim()));
		 userMapper.addUser(sysuser);
		if((sysuser.getRoleStr()) != null && !"".equals(sysuser.getRoleStr())) {
			for(String roleId:sysuser.getRoleStr().split(",")) {
				insertUserRoleRef(sysuser.getId(),roleId);
			}
		}
		
	}

	@Override
	public void insertUserRoleRef(Integer userId, String roleId) {
		userMapper.insertUserRoleRef(userId, roleId);
	}

	@Override
	public SysUser queryUserById(String uid) {
		return userMapper.queryUserById(uid);
	}

	@Override
	public void delUserRoleRef(Integer uid, String rid) {
        sysRoleMapper.delUserRoleRef(uid, rid);		
	}

	@Transactional
	@Override
	public void updateUserConf(SysUser sysUser) {
		String[] roleIdArr = sysUser.getRoleStr().split(";");
		//添加角色关联
		if(roleIdArr.length!=0&&!StringUtils.isEmpty(roleIdArr[0])) {
			String[] roleAdd = roleIdArr[0].split(",");
			for(String rid:roleAdd) {
				if(!StringUtils.isEmpty(rid)) {
					insertUserRoleRef(sysUser.getId(),rid);
				}
			}
		}
		
		//删除角色关联
		if(roleIdArr.length>1&&!StringUtils.isEmpty(roleIdArr[1])) {
			String[] roleDel = roleIdArr[1].split(",");
			for(String rid:roleDel) {
				if(!StringUtils.isEmpty(rid)) {
					delUserRoleRef(sysUser.getId(),rid);
				}
			}
		}
		//更新用户信息
		updateUserById(sysUser);
	}

	@Override
	public void updateUserById(SysUser sysUser) {
		userMapper.updateUserById(sysUser);		
	}
	
	/**
	 * Description: 更改状态
	 * @param uid 用户id
	 * @param value 值
	 * @param type 修改类型
	 * date 2019年4月12日
	 */
	public void updateStateBusiness(String uid, boolean value, String type) {
		updateStateById(uid, value, type);
	}

	
	@Override
	public void updateStateById(String uid, boolean value, String type) {
		userMapper.updateStateById(uid, value, type);
	}

	@Override
	public Integer userUnique(String userName) {
		return userMapper.userUnique(userName);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: changePassword</p>   
	 * <p>Description: 修改密码</p>   
	 * @param oldPasswrod
	 * @param newPasswrod
	 * @param confirmPasswrod
	 * @return   
	 * @see com.activity.manager.security.service.UserService#changePassword(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Integer changePassword(String oldPassword, String newPassword, String confirmPassword) {
		Assert.hasText(oldPassword, "旧密码数据不存在");
		Assert.hasText(newPassword, "新密码数据不存在");
		Assert.hasText(confirmPassword, "确认密码数据不存在");
		//判断新密码是否正确
		if(!newPassword.equals(confirmPassword)) {
			return 1;
		}
		//获取当前用户
		SysUser user = CurrentParamUtil.getCurrentUser();
		MyAuthenticationProvider provider = new MyAuthenticationProvider();
		provider.setPasswordEncoder(new BCryptPasswordEncoder());
		if(provider.getPasswordEncoder().matches(oldPassword, user.getPassword())) {
			//密码正确，进行修改密码
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			String password = encoder.encode(newPassword);
			userMapper.updatePwdById(user.getId(), password);
			return 2;
		}
		return 0;
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateLastTime</p>   
	 * <p>Description: 修改最后登录时间和IP</p>   
	 * @param id
	 * @param lastLoginTime
	 * @param lastLoginIp   
	 * @see com.activity.manager.security.service.UserService#updateLastTime(java.lang.Integer, java.util.Date, java.lang.String)
	 */
	@Override
	public void updateLastTime(Integer id, Date lastLoginTime, String lastLoginIp) {
		userMapper.updateLastTime(id, lastLoginTime, lastLoginIp);
	}
}
