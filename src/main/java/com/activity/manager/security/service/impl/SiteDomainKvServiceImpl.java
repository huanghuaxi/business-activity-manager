/**
 * 
 */
package com.activity.manager.security.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.security.entity.SiteDomain;
import com.activity.manager.security.entity.SiteDomainKv;
import com.activity.manager.security.entity.SiteKv;
import com.activity.manager.security.entity.SysUser;
import com.activity.manager.security.mapper.SiteDomainKvMapper;
import com.activity.manager.security.mapper.SiteDomainMapper;
import com.activity.manager.security.mapper.SiteKvMapper;
import com.activity.manager.security.service.SiteDomainKvService;

/**    
 * <p>Description: 域名键值绑定更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Service
public class SiteDomainKvServiceImpl implements SiteDomainKvService{
	
	@Autowired
	private SiteDomainKvMapper siteDomainKvMapper;
	
	@Autowired
	private SiteKvMapper siteKvMapper;
	
	@Autowired
	private SiteDomainMapper siteDomainMapper;

	/* 
	 *(non-Javadoc)   
	 * <p>Title: querySiteDomainKv</p>   
	 * <p>Description: 查询所有域名键值绑定</p>   
	 * @param siteDomainKv
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.SiteDomainKvService#querySiteDomainKv(com.activity.manager.security.entity.SiteDomainKv, java.lang.Integer, java.lang.Integer)   
	 */
	@Override
	public List<SiteDomainKv> querySiteDomainKv(SiteDomainKv siteDomainKv, Integer start, Integer pageSize) {
		return siteDomainKvMapper.querySiteDomainKv(siteDomainKv, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: siteDomainKvCount</p>   
	 * <p>Description: 统计所有域名键值绑定</p>   
	 * @param siteDomainKv
	 * @return   
	 * @see com.activity.manager.security.service.SiteDomainKvService#siteDomainKvCount(com.activity.manager.security.entity.SiteDomainKv)   
	 */
	@Override
	public Integer siteDomainKvCount(SiteDomainKv siteDomainKv) {
		return siteDomainKvMapper.siteDomainKvCount(siteDomainKv);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveSiteDomainKv</p>   
	 * <p>Description: 新增域名键值绑定</p>   
	 * @param siteDomainKv   
	 * @see com.activity.manager.security.service.SiteDomainKvService#saveSiteDomainKv(com.activity.manager.security.entity.SiteDomainKv)   
	 */
	@Override
	public void saveSiteDomainKv(SiteDomainKv siteDomainKv) {
		Assert.notNull(siteDomainKv.getKeyId(), "站点键值ID数据不存在");
		Assert.notNull(siteDomainKv.getDomainId(), "站点域名ID数据不存在");
		Assert.hasText(siteDomainKv.getSiteKeyValue(), "站点键对应值数据不存在");
		//判断该站点键值配置是否存在
		SiteKv siteKv = siteKvMapper.selectByPrimaryKey(siteDomainKv.getKeyId());
		Assert.notNull(siteKv, "站点键值配置不存在");
		//判断该站点域名配置是否存在
		SiteDomain siteDomain = siteDomainMapper.selectByPrimaryKey(siteDomainKv.getDomainId());
		Assert.notNull(siteDomain, "站点域名配置不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		siteDomainKv.setDomainEnName(siteDomain.getDomainEnName());
		siteDomainKv.setDomainCnName(siteDomain.getDomainCnName());
		siteDomainKv.setKeyEnName(siteKv.getKeyEnName());
		siteDomainKv.setKeyCnName(siteKv.getKeyCnName());
		siteDomainKv.setCreatePerson(user.getId());
		siteDomainKv.setCreateTime(format.format(new Date()));
		siteDomainKvMapper.insert(siteDomainKv);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: editSiteDomainKv</p>   
	 * <p>Description: 编辑域名键值绑定</p>   
	 * @param siteDomainKv   
	 * @see com.activity.manager.security.service.SiteDomainKvService#editSiteDomainKv(com.activity.manager.security.entity.SiteDomainKv)   
	 */
	@Override
	public void editSiteDomainKv(SiteDomainKv siteDomainKv) {
		Assert.notNull(siteDomainKv.getId(), "站点域名键值绑定ID数据不存在");
		Assert.notNull(siteDomainKv.getKeyId(), "站点键值ID数据不存在");
		Assert.notNull(siteDomainKv.getDomainId(), "站点域名ID数据不存在");
		Assert.hasText(siteDomainKv.getSiteKeyValue(), "站点键对应值数据不存在");
		//判断该站点域名键值绑定是否存在
		SiteDomainKv domainKv = siteDomainKvMapper.selectByPrimaryKey(siteDomainKv.getId());
		Assert.notNull(domainKv, "站点域名键值绑定数据不存在");
		//判断该站点键值配置是否存在
		SiteKv siteKv = siteKvMapper.selectByPrimaryKey(siteDomainKv.getKeyId());
		Assert.notNull(siteKv, "站点键值配置不存在");
		//判断该站点域名配置是否存在
		SiteDomain siteDomain = siteDomainMapper.selectByPrimaryKey(siteDomainKv.getDomainId());
		Assert.notNull(siteDomain, "站点域名配置不存在");
		domainKv.setDomainId(siteDomainKv.getDomainId());
		domainKv.setKeyId(siteDomainKv.getKeyId());
		domainKv.setSiteKeyValue(siteDomainKv.getSiteKeyValue());
		domainKv.setDomainEnName(siteDomain.getDomainEnName());
		domainKv.setDomainCnName(siteDomain.getDomainCnName());
		domainKv.setKeyCnName(siteKv.getKeyCnName());
		domainKv.setKeyEnName(siteKv.getKeyEnName());
		siteDomainKvMapper.updateByPrimaryKey(domainKv);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: delSiteDomainKv</p>   
	 * <p>Description: 删除域名键值绑定</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.SiteDomainKvService#delSiteDomainKv(java.util.List)   
	 */
	@Override
	public void delSiteDomainKv(List<String> ids) {
		siteDomainKvMapper.delSiteDomainKv(ids);
	}
	
}
