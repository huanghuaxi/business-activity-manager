package com.activity.manager.security.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.activity.manager.security.entity.SysUser;
import com.activity.manager.security.exception.MyUserNotFoundException;
import com.activity.manager.security.service.UserService;

/**
 * Description: 重写userDetailService
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月3日   
 * @version 1.0
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserService userService;
	 
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SysUser user = userService.getUserByName(username);
		if(user==null) {
		   logger.info("该用户："+username+"不存在！");
		   throw new MyUserNotFoundException("user not exist");
		}
		user.setAccountNonExpired(true);
		user.setCredentialsNonExpired(true);
		return user;
	}
}
