/**
 * 
 */
package com.activity.manager.security.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.SiteDomain;

/**    
 * <p>Description: 加载站点域名配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */
public interface SiteDomainService {
	
	/**
     * 
     * Description: 查询所有站点域名配置   
     * @param siteDomain
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月17日
     */
    public List<SiteDomain> querySiteDomain(SiteDomain siteDomain, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有站点域名配置    
     * @param siteDomain
     * @return 
     * date 2019年4月17日
     */
    public Integer siteDomainCount(SiteDomain siteDomain);
    
    /**
     * 
     * Description: 新增站点域名配置    
     * @param siteDomain 
     * date 2019年4月17日
     */
    public void saveSiteDomain(SiteDomain siteDomain);
    
    /**
     * 
     * Description: 编辑站点域名配置    
     * @param siteDomain 
     * date 2019年4月17日
     */
    public void editSiteDomain(SiteDomain siteDomain);
    
    /**
     * 
     * Description: 删除站点域名配置   
     * @param ids 
     * date 2019年4月17日
     */
    public Boolean delSiteDomain(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 获取所有站点域名    
     * @return 
     * date 2019年4月17日
     */
    public List<SiteDomain> selectAll();
	
}
