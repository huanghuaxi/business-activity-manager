package com.activity.manager.security.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.activity.entity.ActivityButton;
import com.activity.manager.activity.entity.ActivityVersion;
import com.activity.manager.activity.mapper.ActivityButtonMapper;
import com.activity.manager.activity.mapper.ActivityVersionMapper;
import com.activity.manager.security.entity.ActivityCategory;
import com.activity.manager.security.mapper.ActivityCategoryMapper;
import com.activity.manager.security.service.ActivityCategoryService;

/**
 * <p>Description: 活动类型配置  
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月15日   
 * @version 1.0
 */
@Service
public class ActivityCategoryServiceImpl implements ActivityCategoryService {
	
	@Autowired
	private ActivityCategoryMapper activityCategoryMapper;

	@Autowired
	private ActivityVersionMapper activityVersionmapper;
	@Autowired
	private ActivityButtonMapper activityButtonMapper;
	
	@Override
	public List<ActivityCategory> queryActivity(String actName, int start, int pageSize) {
		return activityCategoryMapper.queryActivity(actName, start, pageSize);
	}

	@Override
	public Integer activityCount(String actName) {
		return activityCategoryMapper.activityCount(actName);
	}

	@Override
	public void insertActivityCategory(ActivityCategory activityCate) {
		activityCategoryMapper.insert(activityCate);
		
	}

	@Override
	public List<ActivityVersion> selectAll() {
		return activityVersionmapper.selectAll();
	}

	@Override
	public int insert(ActivityVersion record) {
		return activityVersionmapper.insert(record);
	}

	@Override
	public int insert(ActivityButton record) {
		return activityButtonMapper.insert(record);
	}

	@Transactional
	@Override
	public void insertData(ActivityVersion vers) {
		insert(vers);
	  vers.getButtons().forEach(btn->{
		  btn.setVerId(vers.getId());
		  insert(btn);
	  });
	}

	@Override
	public List<ActivityButton> getButtons(Integer id) {
		return activityButtonMapper.selectByVersion(id);
	}
	
	/*
	 *(non-Javadoc)   
	 * <p>Title: updateVersion</p>   
	 * <p>Description: 此方法逻辑较多，包含事务，有可能出现长事务的情况，后期不可再往里加逻辑，不然就进行方法拆解 </p>   
	 * @param vers   
	 * @see com.activity.manager.security.service.ActivityCategoryService#updateVersion(com.activity.manager.activity.entity.ActivityVersion)
	 */
	@Transactional
	@Override
	public void updateVersion(ActivityVersion vers) {
		List<ActivityButton> btnLst = getButtons(vers.getId());
		
		updateVerById(vers);
		
		if(vers.getButtons().size()==0 && btnLst.size()!=0) {
			delBtnsByVerId(vers.getId());
		}
		if(vers.getButtons().size()>0) {
			vers.getButtons().forEach(btn->{
				ActivityButton actBtn = activityButtonMapper.selectByVerAndId(vers.getId(),btn.getId());
				btn.setVerId(vers.getId());
				if(actBtn!=null) {
					updateActivityBtnById(btn);
				}else {
					insert(btn);
				}
			});	
			
			
			//判断有不存在的id 则进行删除
			for(ActivityButton  qactBtn:btnLst) {
				boolean isExist = false;
				for(ActivityButton btn :vers.getButtons()){
					if(btn.getId() == qactBtn.getId()) {
						isExist = true;
						break;
					}
				}
				if(!isExist) {
					deleteByPrimaryKey(qactBtn.getId());
				}
		}
	   }
	}

	@Override
	public void delBtnsByVerId(Integer id) {
		activityButtonMapper.delBtnsByVerId(id);		
	}

	@Override
	public void updateActivityBtnById(ActivityButton record) {
		activityButtonMapper.updateActivityBtnById(record);
	}
	
	@Override
	public void deleteByPrimaryKey(Integer id) {
		activityButtonMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateVerById(ActivityVersion record) {
		activityVersionmapper.updateVerById(record);
	}

	@Override
	public ActivityButton selectByVerAndId(Integer verId, Integer id) {
		return activityButtonMapper.selectByVerAndId(verId, id);
	}

	@Override
	public void deleteVerByPrimaryKey(Integer verId) {
		activityVersionmapper.deleteByPrimaryKey(verId);		
	}

	@Transactional
	@Override
	public void delVersion(String verIds) {
	  List<String> list = 	Arrays.asList(verIds.split(","));
	  for(String str:list) {
		  deleteVerByPrimaryKey(Integer.valueOf(str));
		  delBtnsByVerId(Integer.valueOf(str));
	  }
	}

	@Override
	public void updateCateById(ActivityCategory record) {
		activityCategoryMapper.updateCateById(record);		
	}

	@Transactional
	@Override
	public void delCategory(String ids) {
		List<String> list = 	Arrays.asList(ids.split(","));
		for(String str :list) {
			ActivityCategory actCate = selectActByPrimaryKey(Integer.valueOf(str));
			if(actCate==null) {
				continue;
			}
			deleteActByPrimaryKey(actCate.getId());
			List<ActivityVersion> verLst = selectVerByActId(actCate.getId());
			for(ActivityVersion actVer:verLst) {
				deleteVerByPrimaryKey(actVer.getId());
			    delBtnsByVerId(actVer.getId());
			}
		}
	}

	@Override
	public ActivityCategory selectActByPrimaryKey(Integer id) {
		return activityCategoryMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<ActivityVersion> selectVerByActId(Integer actId) {
		return activityVersionmapper.selectByActId(actId);
	}

	@Override
	public void deleteActByPrimaryKey(Integer actId) {
		activityCategoryMapper.deleteByPrimaryKey(actId);		
	}

	@Override
	public List<ActivityVersion> selectVerByActIdAndVerName(String verName, String actId) {
		return activityVersionmapper.selectVerByActIdAndVerName(verName, actId);
	}

	@Override
	public void updateActivityShow(ActivityCategory record) {
		activityCategoryMapper.updateActivityShow(record);
	}

	@Override
	public void editActivityImg(Integer id, String activityImg) {
		Assert.notNull(id, "活动类型ID数据不存在");
		Assert.hasText(activityImg, "活动图片数据不存在");
		ActivityCategory category = activityCategoryMapper.selectByPrimaryKey(id);
		Assert.notNull(category, "活动类型数据不存在");
		category.setActivityImg(activityImg);
		activityCategoryMapper.updateByPrimaryKey(category);
	}
}