package com.activity.manager.security.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.Menu;
import com.activity.manager.security.entity.SysRole;

/**
 * Description: 角色业务类
 * Copyright: Copyright (c) 2019    
 * @author huanghx   
 * @date 2019年4月12日   
 * @version 1.0
 */
public interface RoleService {
	
	/**
	 * Description: 查询所有角色
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	List<SysRole> queryRole(String roleName,int start ,int pageSize);
	
	/**
	 * Description: 统计所有角色数
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	Integer roleCount(String roleName);
	
	/**
	 * Description: 删除角色
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delRole(List<String> ids);
	
	/**
	 * Description: 删除用户与角色的关联关系
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delRoleUserRef(List<String> ids);
	
	/**
	 * Description: 删除角色权限关联关系
	 * @param ids 
	 * date 2019年4月12日
	 */
	void delRolePermRef(List<String> ids);
	
	/**
	 * 删除用户数据
	 * @param ids
	 */
	public void delRoleRefData(List<String> ids);
	
	/**
	 * Description:根据状态改值    
	 * @param uid 用户id
	 * @param value 值
	 * @param type 操作类型
	 * date 2019年4月12日
	 */
	void updateRoleStateById(String uid,boolean value,String type);
	
	/**
	 * Description: 增加用户信息
	 * @param user 
	 * date 2019年4月10日
	 */
	Integer addRole(SysRole role);
	
	/**
	 * Description: 获取所有有效菜单  
	 * @param parentId 父节点
	 * @return 
	 * date 2019年4月13日
	 */
	List<Menu> selectAllMenu(Integer parentId);
	
	/**
	 * Description: 加载所有权限
	 * @return 
	 * date 2019年4月13日
	 */
	public List<Menu> loadPerm(String id) ;
	
	/**
	 * Description: 根据父节点，角色id查询权限    
	 * @param parentId
	 * @param roleId
	 * @return 
	 * date 2019年4月13日
	 */
	List<Menu> selectAllMenuByRoleId(Integer parentId,String roleId);
	
	/**
	 * Description: 保存角色与权限的关联关系   
	 * @param permId
	 * @param roleId 
	 * date 2019年4月13日
	 */
	void addRolePermRef(String permId,Integer roleId);
	
	/**
	 * Description: 保存角色
	 * @param sysRole 角色对象
	 * @param ids id对象
	 * date 2019年4月13日
	 */
	void saveRole(SysRole sysRole,String ids);
	
	/**
	 * Description:  更新角色数据  
	 * @param role 
	 * date 2019年4月13日
	 */
	void updateRole(SysRole role);
	
	/**
	 * Description: 删除角色权限关系 
	 * @param permId 权限id
	 * @param roleId 角色id
	 * date 2019年4月13日
	 */
	void delRolePermRefByRidAndPid(@Param("permId") String permId,@Param("roleId") Integer roleId);
}
