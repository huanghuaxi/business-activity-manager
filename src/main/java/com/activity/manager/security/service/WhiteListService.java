/**
 * 
 */
package com.activity.manager.security.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.SysWhiteList;

/**    
 * <p>Description: 加载白名单类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */
public interface WhiteListService {
	
	/**
     * 
     * Description: 查询所有白名单   
     * @param whiteList
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月16日
     */
    public List<SysWhiteList> queryWhiteList(SysWhiteList whiteList, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有白名单   
     * @param whiteList
     * @return 
     * date 2019年4月16日
     */
    public Integer whiteListCount(SysWhiteList whiteList);
    
    /**
     * 
     * Description: 新增白名单   
     * @param whiteList 
     * date 2019年4月16日
     */
    public void saveWhiteList(SysWhiteList whiteList);
    
    /**
     * 
     * Description: 编辑白名单   
     * @param whiteList 
     * date 2019年4月16日
     */
    public void editWhiteList(SysWhiteList whiteList);
    
    /**
     * 
     * Description: 删除白名单   
     * @param ids 
     * date 2019年4月16日
     */
    public void delWhiteList(@Param("ids") List<String> ids);
	
}
