package com.activity.manager.security.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.activity.manager.security.entity.Menu;
import com.activity.manager.security.entity.SysRole;
import com.activity.manager.security.mapper.MenuMapper;
import com.activity.manager.security.mapper.SysRoleMapper;
import com.activity.manager.security.service.RoleService;
import com.alibaba.druid.util.StringUtils;
/**
 * 
 * Description: 角色业务类
 * Copyright: Copyright (c) 2019   
 * @author binghe  
 * @date 2019年4月12日   
 * @version 1.0
 */
@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
	private MenuMapper menuMapper;
	
	
	@Override
	public List<SysRole> queryRole(String roleName, int start, int pageSize) {
		return sysRoleMapper.queryRole(roleName, start, pageSize);
	}

	@Override
	public Integer roleCount(String roleName) {
		return sysRoleMapper.roleCount(roleName);
	}

	@Override
	public void delRole(List<String> ids) {
		sysRoleMapper.delRole(ids);
	}

	@Override
	public void delRoleUserRef(List<String> ids) {
		sysRoleMapper.delRoleUserRef(ids);
	}

	@Override
	public void delRolePermRef(List<String> ids) {
		sysRoleMapper.delRolePermRef(ids);
	}

	/**
	 * 删除角色数据
	 * @param ids
	 */
 	@Transactional
	public void delRoleRefData(List<String> ids) {
		delRole(ids);
		delRoleUserRef(ids);
		delRolePermRef(ids);
	}

 	/**
 	 * 更新角色数据
 	 */
	@Override
	public void updateRoleStateById(String uid, boolean value, String type) {
		sysRoleMapper.updateRoleStateById(uid, value, type);
	}

	@Override
	public Integer addRole(SysRole role) {
		return sysRoleMapper.addRole(role);
	}
	
	

	@Override
	public List<Menu> selectAllMenu(Integer parentId) {
		return menuMapper.selectAllMenu(parentId);
	}
	
	
	@Override
	public List<Menu> loadPerm(String id) {
		return loadMenu(0,id);
	}
	
	private List<Menu> loadMenu(Integer parentId,String id){
		  List<Menu> lst= new ArrayList<>();
			if(StringUtils.isEmpty(id)) {
				lst = selectAllMenuByRoleId(parentId,"0");
			}else {
				lst = selectAllMenuByRoleId(parentId,id);
			}
		
	 	for(Menu menu:lst) {
	 		List<Menu> chillst = loadMenu(menu.getId(),id);
	 		menu.setChildren(chillst);
	 	}
		return lst;
	}

	@Override
	public List<Menu> selectAllMenuByRoleId(Integer parentId, String roleId) {
		return menuMapper.selectAllMenuByRoleId(parentId, roleId);
	}

	@Override
	public void addRolePermRef(String permId, Integer roleId) {
		sysRoleMapper.addRolePermRef(permId, roleId);
	}

	@Transactional
	@Override
	public void saveRole(SysRole sysRole, String ids) {
         String[] idArr = ids.split(";");   
         if(sysRole.getId()==0) 
            addRole(sysRole);
          else 
        	updateRole(sysRole);
         
         if(idArr.length!=0) {
        	 String[] addId = StringUtils.isEmpty(idArr[0])? null:idArr[0].split(",");
        	 if(addId!=null && addId.length!=0) {
        		  for(int i=0;i<addId.length;i++) {
        			  if(!StringUtils.isEmpty(addId[i])) {
        				  addRolePermRef(addId[i],sysRole.getId()); 
        			  }
        		  }
        	 }
        	 
        	 String[] delId = idArr.length>1 && !StringUtils.isEmpty(idArr[1])? idArr[1].split(","):null;
        	 if(delId!=null && delId.length!=0) {
        		  for(int i=0;i<delId.length;i++) {
        			  if(!StringUtils.isEmpty(delId[i])) {
        				  delRolePermRefByRidAndPid(delId[i],sysRole.getId()); 
        			  }
        		  }
        	 }
         }
	}

	@Override
	public void updateRole(SysRole role) {
		sysRoleMapper.updateRole(role);
	}

	@Override
	public void delRolePermRefByRidAndPid(String permId, Integer roleId) {
		sysRoleMapper.delRolePermRefByRidAndPid(permId, roleId);
	}
}
