/**
 * 
 */
package com.activity.manager.security.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.security.entity.SiteDomain;
import com.activity.manager.security.entity.SysUser;
import com.activity.manager.security.mapper.SiteDomainMapper;
import com.activity.manager.security.service.SiteDomainService;

/**    
 * <p>Description: 站点域名配置更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Service
public class SiteDomainServiceImpl implements SiteDomainService{

	@Autowired
	private SiteDomainMapper siteDomainMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: querySiteDomain</p>   
	 * <p>Description: 查询所有站点域名配置</p>   
	 * @param siteDomain
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.SiteDomainService#querySiteDomain(com.activity.manager.security.entity.SiteDomain, java.lang.Integer, java.lang.Integer)   
	 */
	@Override
	public List<SiteDomain> querySiteDomain(SiteDomain siteDomain, Integer start, Integer pageSize) {
		return siteDomainMapper.querySiteDomain(siteDomain, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: siteDomainCount</p>   
	 * <p>Description: 统计所有站点域名配置 </p>   
	 * @param siteDomain
	 * @return   
	 * @see com.activity.manager.security.service.SiteDomainService#siteDomainCount(com.activity.manager.security.entity.SiteDomain)   
	 */
	@Override
	public Integer siteDomainCount(SiteDomain siteDomain) {
		return siteDomainMapper.siteDomainCount(siteDomain);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveSiteDomain</p>   
	 * <p>Description: 新增站点域名配置</p>   
	 * @param siteDomain   
	 * @see com.activity.manager.security.service.SiteDomainService#saveSiteDomain(com.activity.manager.security.entity.SiteDomain)   
	 */
	@Override
	public void saveSiteDomain(SiteDomain siteDomain) {
		Assert.hasText(siteDomain.getDomainCnName(), "站点域名中文名称数据不存在");
		Assert.hasText(siteDomain.getDomainEnName(), "站点域名英文名称数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		siteDomain.setCreatePerson(user.getId());
		siteDomain.setCreateTime(format.format(new Date()));
		siteDomainMapper.insert(siteDomain);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: editSiteDomain</p>   
	 * <p>Description: 编辑站点域名配置</p>   
	 * @param siteDomain   
	 * @see com.activity.manager.security.service.SiteDomainService#editSiteDomain(com.activity.manager.security.entity.SiteDomain)   
	 */
	@Override
	public void editSiteDomain(SiteDomain siteDomain) {
		Assert.notNull(siteDomain.getId(), "站点域名ID数据不存在");
		Assert.hasText(siteDomain.getDomainCnName(), "站点域名中文名称数据不存在");
		Assert.hasText(siteDomain.getDomainEnName(), "站点域名英文名称数据不存在");
		SiteDomain domain = siteDomainMapper.selectByPrimaryKey(siteDomain.getId());
		Assert.notNull(domain, "站点域名数据不存在");
		domain.setDomainCnName(siteDomain.getDomainCnName());
		domain.setDomainEnName(siteDomain.getDomainEnName());
		domain.setDescr(siteDomain.getDescr());
		siteDomainMapper.updateByPrimaryKey(domain);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: delSiteDomain</p>   
	 * <p>Description: 删除站点域名配置</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.SiteDomainService#delSiteDomain(java.util.List)   
	 */
	@Override
	public Boolean delSiteDomain(List<String> ids) {
		//查询要删除的域名是否已经绑定域名
		Integer count = siteDomainMapper.siteDomainKvByIdsCount(ids);
		if(count == 0) {
			siteDomainMapper.delSiteDomain(ids);
			return true;
		}else {
			return false;
		}
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: selectAll</p>   
	 * <p>Description: 获取所有站点域名</p>   
	 * @return   
	 * @see com.activity.manager.security.service.SiteDomainService#selectAll()   
	 */
	@Override
	public List<SiteDomain> selectAll() {
		return siteDomainMapper.selectAll();
	}

}
