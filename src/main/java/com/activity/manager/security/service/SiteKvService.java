/**
 * 
 */
package com.activity.manager.security.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.SiteKv;

/**    
 * <p>Description: 加载站点键值配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */
public interface SiteKvService {
	
	/**
     * 
     * Description: 查询所有站点键值配置    
     * @param siteKv
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月17日
     */
    public List<SiteKv> querySiteKv(SiteKv siteKv, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有站点键值配置
     * @param siteKv
     * @return 
     * date 2019年4月17日
     */
    public Integer siteKvCount(SiteKv siteKv);
    
    /**
     * 
     * Description: 新增站点键值配置    
     * @param siteKv 
     * date 2019年4月17日
     */
    public void saveSiteKv(SiteKv siteKv);
    
    /**
     * 
     * Description: 编辑站点键值配置    
     * @param siteKv 
     * date 2019年4月17日
     */
    public void editSiteKv(SiteKv siteKv);
    
    /**
     * 
     * Description: 删除站点键值配置   
     * @param ids 
     * date 2019年4月17日
     */
    public Boolean delSiteKv(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 查询所有站点键值配置    
     * @return 
     * date 2019年4月17日
     */
    public List<SiteKv> selectAll();
	
}
