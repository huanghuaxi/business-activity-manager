package com.activity.manager.security.entity;

/**
 * 
 * <p>Description: 白名单类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0
 */
public class SysWhiteList {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	IP地址
     */
    private String ipAddr;

    /**
     *	类型 0：白名单 1：黑名单
     */
    private Integer type;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPersion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr == null ? null : ipAddr.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getCreatePersion() {
        return createPersion;
    }

    public void setCreatePersion(Integer createPersion) {
        this.createPersion = createPersion;
    }
}