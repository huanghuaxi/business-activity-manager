package com.activity.manager.security.entity;

import java.util.Date;

/**
 * 
 * <p>Description: 活动类型实体类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月15日   
 * @version 1.0
 */
public class ActivityCategory {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 版本名称
     */
    private String actName;

    /**
     *版本描述
     */
    private String descr;

    /**
     * 创建人
     */
    private Integer createPerson;

    /**
     *	创建时间
     */
    private Date createTime;
    
    /**
     * 	活动演示页展示
     */
    private Boolean activityShow;
    
    /**
     * 	活动图片
     */
    private String activityImg;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName == null ? null : actName.trim();
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public Boolean getActivityShow() {
		return activityShow;
	}

	public void setActivityShow(Boolean activityShow) {
		this.activityShow = activityShow;
	}

	public String getActivityImg() {
		return activityImg;
	}

	public void setActivityImg(String activityImg) {
		this.activityImg = activityImg;
	}
}