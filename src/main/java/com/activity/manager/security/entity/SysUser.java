package com.activity.manager.security.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * Description:  用户类 
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年3月31日   
 * @version 1.0
 */
public class SysUser  implements UserDetails   {
	
	/**
	 * 序列号
	 */
	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;;

	public SysUser(String userName,String pwd) {
		this.userName = userName;
		this.pwd = pwd;
	}
	
	public SysUser() {
		
	}
	
	private int id;

	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 密码
	 */
	private String pwd;
	
	/**
	 * 用户是否被锁
	 */
	private boolean isLock;
	
	/**
	 * 用户是否有效
	 */
	private boolean isValide;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 邮件
	 */
	private String mail;
	
	/**
	 * 密码是否过期
	 */
	private boolean credentialsNonExpired;
	
	/**
	 * 用户是否过期
	 */
	private boolean isAccountNonExpired;

	/**
	 * 角色名称
	 */
	private List<SysRole> roleList;
	
	/**
	 * 描述
	 */
	private String descr;
	
	/**
	 * 失败次数
	 */
	private int failCount;
	
	/**
	 * 	最后登录时间
	 */
	private Date lastLoginTime;
	
	/**
	 * 	最后登录IP
	 */
	private String lastLoginIp;
	
	/**
	 * 角色id
	 */
	private String roleStr;
	
	public String getRoleStr() {
		return roleStr;
	}

	public void setRoleStr(String roleStr) {
		this.roleStr = roleStr;
	}

	public int getFailCount() {
		return failCount;
	}

	public void setFailCount(int failCount) {
		this.failCount = failCount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public List<SysRole> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<SysRole> roleList) {
		this.roleList = roleList;
	}

	/**
	 * 角色控制
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		if(roleList != null) {
			for (SysRole role : roleList) {
			     authorities.add(new SimpleGrantedAuthority(String.valueOf(role.getId())));
			}
		}
		return authorities;
	}

	@Override
	public String getPassword() {
		return pwd;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return isAccountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return isLock;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return isValide ;
	}
	
	public boolean getIsLock() {
		return isLock;
	}

	public void setIsLock(boolean isLock) {
		this.isLock = isLock;
	}

	public boolean getIsValide() {
		return isValide;
	}

	public void setIsValide(boolean isValide) {
		this.isValide = isValide;
	}
	
	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
	
	public void setAccountNonExpired(boolean isAccountNonExpired) {
		this.isAccountNonExpired = isAccountNonExpired;
	}
	
	@Override
	public int hashCode() {
		return userName.hashCode();
	}
	
	@Override
	public boolean equals(Object rhs) {
		if (rhs instanceof SysUser) {
			return userName.equals(((SysUser) rhs).userName);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.userName;
	}
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}
	
}
