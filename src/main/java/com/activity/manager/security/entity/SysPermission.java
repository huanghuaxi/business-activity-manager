package com.activity.manager.security.entity;

import java.util.Date;
import java.util.List;

/**
 * 
 * <p>Description: 权限实体类 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月14日   
 * @version 1.0
 */
public class SysPermission {
	
	/**
	 * 权限id
	 */
	private int id;
	
	/**
	 * 权限名称
	 */
	private String permName;
	
    /**
     * 权限code
     */
	private String permisCode;
	
	/**
	 * 访问url
	 */
	private String url;
	
	/**
	 * 权限描述
	 */
	private String desc;
	
	/**
	 * 是否父子节点
	 */
	private boolean  isLeaf;
	
	/**
	 * 是否操作项
	 */
	private boolean isAction;
	
	/**
	 * 父节点
	 */
	private int parentId;
	
	/**
	 * 菜单排序
	 */
	private String sequence;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 有效状态
	 */
	private int State;
	
	/**
	 * 图标
	 */
	private String icon;
	
	/**
	 * 角色
	 */
	private List<SysRole> roleList;
	
	/**
	 * 权限集合（菜单）
	 */
	private List<SysPermission> menu;
	
	/**
	 * 父节点名称
	 */
	private String parentName;
	
	/**
	 * 0 隐藏，1显示
	 */
	private boolean hidden;
	
	/**
	 * 用户id
	 */
	private int uid;
	
	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<SysPermission> getMenu() {
		return menu;
	}

	public void setMenu(List<SysPermission> menu) {
		this.menu = menu;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<SysRole> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<SysRole> roleList) {
		this.roleList = roleList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPermName() {
		return permName;
	}

	public void setPermName(String permName) {
		this.permName = permName;
	}

	public String getPermisCode() {
		return permisCode;
	}

	public void setPermisCode(String permisCode) {
		this.permisCode = permisCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public void setAction(boolean isAction) {
		this.isAction = isAction;
	}

	public boolean getIsAction() {
		return isAction;
	}

	public void setIsAction(boolean isAction) {
		this.isAction = isAction;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getState() {
		return State;
	}

	public void setState(int state) {
		State = state;
	}
	public boolean getHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	public String toString() {
		return "pername:【"+this.permName+"】,desc:【"+this.desc+"】,url:【"+this.url+"】";
	}

}
