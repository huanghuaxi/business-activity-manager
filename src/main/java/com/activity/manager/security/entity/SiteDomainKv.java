package com.activity.manager.security.entity;

/**
 * 
 * <p>Description: 域名键值绑定类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0
 */
public class SiteDomainKv {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	域名英文名
     */
    private String domainEnName;

    /**
     *	站点键英文名称
     */
    private String keyEnName;
    
    /**
     *	域名中文名
     */
    private String domainCnName;
    
    /**
     *	站点键中文名称
     */
    private String keyCnName;

    /**
     *	站点键对应值
     */
    private String siteKeyValue;

    /**
     *	域名id
     */
    private Integer domainId;

    /**
     *	键id
     */
    private Integer keyId;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPerson;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDomainEnName() {
		return domainEnName;
	}

	public void setDomainEnName(String domainEnName) {
		this.domainEnName = domainEnName == null ? null : domainEnName.trim();
	}

	public String getKeyEnName() {
		return keyEnName;
	}

	public void setKeyEnName(String keyEnName) {
		this.keyEnName = keyEnName == null ? null : keyEnName.trim();
	}

	public String getDomainCnName() {
		return domainCnName;
	}

	public void setDomainCnName(String domainCnName) {
		this.domainCnName = domainCnName == null ? null : domainCnName.trim();
	}

	public String getKeyCnName() {
		return keyCnName;
	}

	public void setKeyCnName(String keyCnName) {
		this.keyCnName = keyCnName == null ? null : keyCnName.trim();
	}

	public String getSiteKeyValue() {
		return siteKeyValue;
	}

	public void setSiteKeyValue(String siteKeyValue) {
		this.siteKeyValue = siteKeyValue == null ? null : siteKeyValue.trim();
	}

	public Integer getDomainId() {
		return domainId;
	}

	public void setDomainId(Integer domainId) {
		this.domainId = domainId;
	}

	public Integer getKeyId() {
		return keyId;
	}

	public void setKeyId(Integer keyId) {
		this.keyId = keyId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(Integer createPerson) {
		this.createPerson = createPerson;
	}
    
}