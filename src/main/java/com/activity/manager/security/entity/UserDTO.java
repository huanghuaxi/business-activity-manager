package com.activity.manager.security.entity;

import java.util.Date;

import com.activity.manager.common.annotation.Table;

/**
 * Description:   
 * Copyright: Copyright (c) 2019    
 * @author binghe   
 * @date 2019年4月6日   
 * @version 1.0   
 */
@Table(value="tbl_sys_user")
public class UserDTO {
	
	/**
	 * 主键id
	 */
	private int id;
	
	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 密码
	 */
	private String pwd;
	
	/**
	 * 用户是否被锁
	 */
	private boolean isLock;
	
	/**
	 * 用户是否有效
	 */
	private boolean isValide;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 邮件
	 */
	private String mail;
	
	/**
	 * 密码是否过期
	 */
	private boolean credentialsNonExpired;
	
	/**
	 * 用户是否过期
	 */
	private boolean isAccountNonExpired;
	
	/**
	 * 描述
	 */
	private String descr;
	
	/**
	 * 失败次数
	 */
	private int failCount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public boolean getIsLock() {
		return isLock;
	}

	public void setIsLock(boolean isLock) {
		this.isLock = isLock;
	}

	public boolean getIsValide() {
		return isValide;
	}

	public void setIsValide(boolean isValide) {
		this.isValide = isValide;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public boolean isAccountNonExpired() {
		return isAccountNonExpired;
	}

	public void setAccountNonExpired(boolean isAccountNonExpired) {
		this.isAccountNonExpired = isAccountNonExpired;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public int getFailCount() {
		return failCount;
	}

	public void setFailCount(int failCount) {
		this.failCount = failCount;
	}
}
