package com.activity.manager.security.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.activity.entity.ActivityButton;
import com.activity.manager.activity.entity.ActivityVersion;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.Constants;
import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.security.entity.ActivityCategory;
import com.activity.manager.security.enums.ButtonColorEnum;
import com.activity.manager.security.service.ActivityCategoryService;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;

/**
 * <p>Description: 商家活动类型配置   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author    
 * @date 2019年4月15日   
 * @version 1.0
 */
@RequestMapping("/actCategory")
@Controller
public class ActivityCategoryController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ActivityCategoryService activityCategoryService;
	
	@Value("${web.upload.path}")
    private String uploadPath;
	
	
	/**
	 * Description: 页面跳转    
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/list")
	public String activityCateList() {
		return "sysmanage/activityconf/activityCategoryConf.html";
	}
	
	/**
	 * Description: 版本页面跳转    
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/ver/list")
	public String verList(String actId,Model model) {
		model.addAttribute("actId", actId);
		return "sysmanage/activityconf/vermanage.html";
	}
	
	/**
	 * Description: 版本页面跳转    
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/ver/saveOrEdit")
	public String verSaveOrEdit(String actId,String vid,String version,String verUrl,Model model) {
		model.addAttribute("verName", version);
		model.addAttribute("url", verUrl);
		model.addAttribute("actId", actId);
		model.addAttribute("id", vid);
		return "sysmanage/activityconf/verSaveOrEdit.html";
	}
	
	@RequestMapping("/grid")
	@ResponseBody
	public  PageUtil getList(PageUtil pageUtil,String actName){
	    List<ActivityCategory> ulist = activityCategoryService.queryActivity(actName,pageUtil.getCurrIndex(),pageUtil.getLimit());
	    List<Object> uLst =  formate(ulist);
	    pageUtil.setData(uLst);
		Integer count = activityCategoryService.activityCount(actName);
		pageUtil.setCode("0");
		pageUtil.setCount(count);
		pageUtil.setMsg("成功");
		pageUtil.setData(uLst);
		return pageUtil;
	}
	
	@RequestMapping("/ver/grid")
	@ResponseBody
	public  PageUtil getVerList(PageUtil pageUtil,String verName,String actId){
	    List<ActivityVersion> ulist = activityCategoryService.selectVerByActIdAndVerName(verName,actId);
	    List<Object> uLst =  formateToVer(ulist);
	    pageUtil.setData(uLst);
		pageUtil.setCode("0");
		pageUtil.setMsg("成功");
		pageUtil.setData(uLst);
		return pageUtil;
	}
	
	/**
	 * 将数据转化成dto形式
	 * @param list
	 * @return
	 */
	private List<Object> formate(List<ActivityCategory> list){
		List<Object> lst = new ArrayList<Object>();
		list.forEach(actCate -> {
			lst.add(actCate);
		});
		return lst;
	}
	
	/**
	 * 将数据转化成dto形式
	 * @param list
	 * @return
	 */
	private List<Object> formateToVer(List<ActivityVersion> list){
		List<Object> lst = new ArrayList<Object>();
		list.forEach(actCate -> {
			lst.add(actCate);
		});
		return lst;
	}
	
	/**
	 * Description: 增加活动类型 
	 * @param actName 活动名称
	 * @param descr
	 * @return 
	 * date 2019年4月15日
	 */
	@RequestMapping("/addAct")
	@ResponseBody
	public ResultMessage addAct(String actName,String descr) {
		try {
			Assert.hasText(actName, "actName 参数为空");
			ActivityCategory actCate = new ActivityCategory();
			actCate.setActName(actName);
			actCate.setDescr(descr);
			actCate.setCreateTime(new Date());
			actCate.setCreatePerson(CurrentParamUtil.getCurrentUser().getId());
			activityCategoryService.insertActivityCategory(actCate);
		}catch(Exception e) {
			logger.error("保存活动配置出错",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 修改活动演示页展示    
	 * @param category
	 * @return 
	 * date 2019年6月3日
	 */
	@RequestMapping("/editActvityShow")
	@ResponseBody
	public ResultMessage editActvityShow(ActivityCategory category) {
		try {
			activityCategoryService.updateActivityShow(category);
			logger.info("修改活动演示页展示");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("修改活动演示页展示异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * Description: 增加活动类型 
	 * @param actName 活动名称
	 * @param descr
	 * @return 
	 * date 2019年4月15日
	 */
	@RequestMapping("/ver/saveVer")
	@ResponseBody
	public ResultMessage addVer(String actId, String verName,String url, String actBtns) {
		try {
			Assert.hasText(actBtns, "actBtns 参数为空");
			Assert.hasText(url, "url参数为空");
			Assert.hasText(verName, "verName参数为空");
			Assert.hasText(actId, "actId参数为空");
			ActivityVersion actVerson = new ActivityVersion();
			actVerson.setActivityId(Integer.valueOf(actId));
			actVerson.setVerUrl(url);
			actVerson.setVersion(verName);
			List<ActivityButton> btnLst = JSON.parseArray(actBtns, ActivityButton.class);
			actVerson.setButtons(btnLst);
			activityCategoryService.insertData(actVerson);
			return ResultMessage.getSuccess();
		}catch(Exception e) {
	       return ResultMessage.getFail();
		}
	}
	
	/**
	 * Description: 增加活动类型 
	 * @param actName 活动名称
	 * @param descr
	 * @return 
	 * date 2019年4月15日
	 */
	@RequestMapping("/ver/getButtons")
	@ResponseBody
	public ResultMessage getButtons(String id){
		try {	
			List<ActivityButton> list = new ArrayList<>(); 
			if(StringUtils.isEmpty(id)) {
				list = getDefaultButton();
			}else {
			    list = activityCategoryService.getButtons(Integer.valueOf(id));
			}
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, list.size() == 0?getDefaultButton():list);
		}catch(Exception e) {
			logger.error("查询按钮失败", e);
			return ResultMessage.getFail();
		}
   }
	
	@RequestMapping("/ver/updateVer")
	@ResponseBody
	public ResultMessage  updateVersion(String verId, String verName,String url, String actBtns) {
		try {
			Assert.hasText(actBtns, "actBtns 参数为空");
			Assert.hasText(url, "url参数为空");
			Assert.hasText(verName, "verName参数为空");
			Assert.hasText(verId, "verId参数为空");
			ActivityVersion actVerson = new ActivityVersion();
			actVerson.setId(Integer.valueOf(verId));
			actVerson.setVerUrl(url);
			actVerson.setVersion(verName);
			List<ActivityButton> btnLst = JSON.parseArray(actBtns, ActivityButton.class);
			actVerson.setButtons(btnLst);
			activityCategoryService.updateVersion(actVerson);
			return ResultMessage.getSuccess();
		}catch(Exception e) {
	       return ResultMessage.getFail();
		}
	}
	
	
	/**
	 * Description: 获取默认按钮  
	 * @return 
	 * date 2019年4月17日
	 */
   private List<ActivityButton> getDefaultButton(){
	   
	   List<ActivityButton> lst = new ArrayList<>();
	   
	   ActivityButton memberButton = new ActivityButton();
	   memberButton.setId(1);
	   memberButton.setBtnStyle(ButtonColorEnum.NORMAL_COLOR.getBtnStyle());
	   memberButton.setBtnName(Constants.BUTTON_NAME_MEMBER);
	   memberButton.setBtnUrl("/memberAct/list?actId=");
	   
	   ActivityButton rewardButton = new ActivityButton();
	   rewardButton.setId(2);
	   rewardButton.setBtnStyle(ButtonColorEnum.DEFAULT_COLOR.getBtnStyle());
	   rewardButton.setBtnName(Constants.BUTTON_NAME_REWARD_CONF);
	   rewardButton.setBtnUrl("/rewardConf/list?actId=");
	   
	   ActivityButton innerRewardButton = new ActivityButton();
	   innerRewardButton.setId(3);
	   innerRewardButton.setBtnStyle(ButtonColorEnum.DANGER_COLOR.getBtnStyle());
	   innerRewardButton.setBtnName(Constants.BUTTON_NAME_INNER_REWARD);
	   innerRewardButton.setBtnUrl("/innerReward/list?actId=");
	   
	   ActivityButton recordButton = new ActivityButton();
	   recordButton.setId(4);
	   recordButton.setBtnStyle(ButtonColorEnum.WARM_COLOR.getBtnStyle());
	   recordButton.setBtnName(Constants.BUTTON_NAME_RECORD);
	   recordButton.setBtnUrl("/rewardRecord/list?actId=");
	   
	   lst.add(memberButton);
	   lst.add(rewardButton);
	   lst.add(innerRewardButton);
	   lst.add(recordButton);
	   return lst;
   }
   
   /**
	 * 删除版本
	 * @return
	 */
	@RequestMapping("/ver/del")
	@ResponseBody
	public ResultMessage delVer (String ids){
       try {
        Assert.hasText(ids, "活动版本ids数据不存在");
        logger.info("用户：【"+CurrentParamUtil.getCurrentUser().getUsername()+"】，删除数据，ids【"+ids+"】");	
		 String str = ids.substring(0, ids.length()-1);
		 activityCategoryService.delVersion(str);
       }catch(Exception e) {
       	logger.error("删除权限数据异常：",e);
       	return  ResultMessage.getFail();
		}
		return  ResultMessage.getSuccess();
	}
	
	/**
	 * Description: 增加活动类型 
	 * @param actName 活动名称
	 * @param descr
	 * @return 
	 * date 2019年4月15日
	 */
	@RequestMapping("/updateAct")
	@ResponseBody
	public ResultMessage updateAct(String id,String actName,String descr) {
		try {
			Assert.hasText(actName, "actName 参数为空");
			Assert.hasText(id, "id 参数为空");
			ActivityCategory actCate = new ActivityCategory();
			actCate.setId(Integer.valueOf(id));
			actCate.setActName(actName);
			actCate.setDescr(descr);
			activityCategoryService.updateCateById(actCate);
		}catch(Exception e) {
			logger.error("修改活动配置出错",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
   /**
	 * 删除版本
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public ResultMessage del (String ids){
       try {
        Assert.hasText(ids, "活动版本ids数据不存在");
        logger.info("用户：【"+CurrentParamUtil.getCurrentUser().getUsername()+"】，删除数据，ids【"+ids+"】");	
		 String str = ids.substring(0, ids.length()-1);
		 activityCategoryService.delCategory(str);
       }catch(Exception e) {
       	logger.error("删除权限数据异常：",e);
       	return  ResultMessage.getFail();
		}
		return  ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 获取按钮枚举   
	 * @return 
	 * date 2019年4月19日
	 */
	@ResponseBody
	@RequestMapping("/ver/getButtonEnum")
	public ResultMessage getButtonEnum() {
		try {
			logger.info("获取按钮枚举");
			List<Map<String, String>> list = new ArrayList<Map<String, String>>();
			Map<String, String> map = new HashMap<String, String>();
			map.put("btnName", ButtonColorEnum.NORMAL_COLOR.getColorName());
			map.put("btnStyle", ButtonColorEnum.NORMAL_COLOR.getBtnStyle());
			list.add(map);
			map = new HashMap<String, String>();
			map.put("btnName", ButtonColorEnum.WARM_COLOR.getColorName());
			map.put("btnStyle", ButtonColorEnum.WARM_COLOR.getBtnStyle());
			list.add(map);
			map = new HashMap<String, String>();
			map.put("btnName", ButtonColorEnum.DANGER_COLOR.getColorName());
			map.put("btnStyle", ButtonColorEnum.DANGER_COLOR.getBtnStyle());
			list.add(map);
			map = new HashMap<String, String>();
			map.put("btnName", ButtonColorEnum.DISABLE_COLOR.getColorName());
			map.put("btnStyle", ButtonColorEnum.DISABLE_COLOR.getBtnStyle());
			list.add(map);
			map = new HashMap<String, String>();
			map.put("btnName", ButtonColorEnum.DEFAULT_COLOR.getColorName());
			map.put("btnStyle", ButtonColorEnum.DEFAULT_COLOR.getBtnStyle());
			list.add(map);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, list);
		}catch(Exception e) {
			logger.error("获取按钮枚举异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 上传图片   
	 * @param file
	 * @param id
	 * @param request
	 * @return 
	 * date 2019年6月4日
	 */
	@ResponseBody
	@RequestMapping("/upload")
	public ResultMessage upload(@RequestParam("file") MultipartFile file, Integer id, 
			HttpServletRequest request) {
		try {
			if(file.isEmpty()) {
				return ResultMessage.getFail();
			}
			String fileName = file.getOriginalFilename();
			//文件保存地址
			String path = uploadPath;
			path += "activityImg/";
			//新文件名
			String newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf("."));
			File dest = new File(path + newFileName);
			logger.info("文件上传路径："+path);
			//判断文件父目录是否存在
			if(!dest.getParentFile().exists()){ 
				logger.info(path + "targetPath is not exist");  
				dest.getParentFile().mkdirs(); 
			}
			file.transferTo(dest);
			logger.info("文件上传成功");
			//文件访问地址
			String url = "http://" + request.getServerName() + ":" 
					+ request.getServerPort()  + "/uploads/activityImg/" + newFileName;
			Map<String, Object> map = new HashMap<String, Object>();
			logger.info("前端页面访问路径："+url);
			map.put("imgUrl", url);
			map.put("id", id);
			//保存图片地址
			activityCategoryService.editActivityImg(id, url);
			logger.info("奖品图片上传结束");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, map);
		}catch(Exception e) {
			logger.error("文件上传异常", e);
			return ResultMessage.getFail();
		}
	}
}
