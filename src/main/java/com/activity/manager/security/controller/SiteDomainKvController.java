/**
 * 
 */
package com.activity.manager.security.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.security.entity.SiteDomainKv;
import com.activity.manager.security.service.SiteDomainKvService;

/**    
 * <p>Description: 域名键值绑定控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/siteDomainKv")
public class SiteDomainKvController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SiteDomainKvService siteDomainKvService;
	
	/**
	 * 
	 * Description: 跳转到站点域名键值绑定页面    
	 * @return 
	 * date 2019年4月17日
	 */
	@RequestMapping("/list")
	public String toSiteDomainKvList() {
		logger.info("跳转到站点域名键值绑定页面");
		return "sysmanage/site/siteDomainKv.html";
	}
	
	/**
	 * 
	 * Description: 获取站点域名键值绑定列表   
	 * @param pageUtil
	 * @param SiteDomainKv
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, SiteDomainKv siteDomainKv) {
		try {
			List<SiteDomainKv> kList = siteDomainKvService.querySiteDomainKv(siteDomainKv, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> kLst = formate(kList);
			Integer count = siteDomainKvService.siteDomainKvCount(siteDomainKv);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(kLst);
			logger.info("获取站点域名键值绑定列表");
		}catch(Exception e) {
			logger.error("获取站点域名键值绑定列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增站点域名键值绑定   
	 * @param SiteDomainKv
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(SiteDomainKv SiteDomainKv) {
		try {
			siteDomainKvService.saveSiteDomainKv(SiteDomainKv);
			logger.info("新增站点域名键值绑定");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增站点域名键值绑定异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑站点域名键值绑定   
	 * @param SiteDomainKv
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(SiteDomainKv SiteDomainKv) {
		try {
			siteDomainKvService.editSiteDomainKv(SiteDomainKv);
			logger.info("编辑站点域名键值绑定");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑站点域名键值绑定异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除站点域名键值绑定   
	 * @param ids
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delete(String ids) {
		try {
			Assert.hasText(ids, "站点域名键值绑定ID数据不存在");
			siteDomainKvService.delSiteDomainKv(Arrays.asList(ids.split(",")));
			logger.info("删除站点域名键值绑定");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("删除站点域名键值绑定异常", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<SiteDomainKv> list){
		List<Object> kList = new ArrayList<Object>();
		list.forEach(SiteDomainKv -> {
			kList.add(SiteDomainKv);
		});
		return kList;
	}
	
}
