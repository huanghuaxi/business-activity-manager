package com.activity.manager.security.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.security.entity.SysRole;
import com.activity.manager.security.service.RoleService;

@Controller
@RequestMapping("/role")
public class RoleController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RoleService roleService;
	
	/**
	 * Description: 页面跳转    
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/list")
	public String toUserList() {
		return "sysmanage/role/rolemanage.html";
	}

	@RequestMapping("/grid")
	@ResponseBody
	public  PageUtil getList(PageUtil pageUtil,String roleName){
		  List<SysRole> rlist = roleService.queryRole(roleName,pageUtil.getCurrIndex(),pageUtil.getLimit());
		  List<Object> rLst = formate(rlist); 
		  pageUtil.setData(rLst); 
		  Integer count = roleService.roleCount(roleName);
		  pageUtil.setCode("0");
		  pageUtil.setCount(count); 
		  pageUtil.setMsg("成功"); 
		  pageUtil.setData(rLst);
		  return pageUtil;
	}
	
	/**
	 * 将数据转化成dto形式
	 * @param list
	 * @return
	 */
	private List<Object> formate(List<SysRole> list){
		List<Object> lst = new ArrayList<Object>();
		list.forEach(sysUser -> {
			lst.add(sysUser);
		});
		return lst;
	}
	
	/**
	 * 用户删除
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public ResultMessage del (String ids){
        try {
		 Assert.hasText(ids, "角色id不存在");
		 String str = ids.substring(0, ids.length()-1);
		 roleService.delRoleRefData(Arrays.asList(str.split(",")));
        }catch(Exception e) {
        	logger.error("删除异常",e);
        	return  ResultMessage.getFail();
		}
		return  ResultMessage.getSuccess();
	}
	

	/**
	 * Description:更改用户状态    
	 * @param type 类型
	 * @param id 用户id
	 * @param value 值
	 * @return 结果对象
	 * date 2019年4月12日
	 */
	@RequestMapping("/updateState")
	@ResponseBody
	public ResultMessage updateState(String type, String id, boolean value) {
		try {
		   roleService.updateRoleStateById(id, value, type);
		}catch(Exception e) {
		   logger.error("修改状态失败", e);
	       return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 重定向新增页面
	 * @return
	 */
	@GetMapping("/redirect")
	public String addRedirect(String roleId,String roleName,String descr,Model model) {
		model.addAttribute("id", roleId);
		model.addAttribute("roleName", roleName);
		model.addAttribute("descr",descr);
		return "sysmanage/role/saveOrEdit.html";
	}
	
	/**
	 * 新增角色
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public ResultMessage addRole(SysRole sysRole) {
		roleService.addRole(sysRole);
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 获取权限内容
	 * @return
	 */
	@RequestMapping("/getPerm")
	@ResponseBody
	public ResultMessage getPerm(String id) {
	    return ResultMessage.getRtMsg(CodeUtil.LAYUI_SUCCESS_CODE, CodeUtil.LAYUI_SUCCESS_CODE,roleService.loadPerm(id));
	} 
	
	/**
	 * Description: 修改角色
	 * @return 
	 * date 2019年4月13日
	 */
	@RequestMapping("/roleSaveAndUpdate")
	@ResponseBody
	public ResultMessage roleSaveAndUpdate(SysRole role) {
	   try {	 
		   logger.info("用户修改角色数据"+role);
		   roleService.saveRole(role,role.getPermIds());
	   }catch(Exception e) {
		   logger.error("修改角色信息失败", e);
	       return ResultMessage.getFail();
	   }
		return ResultMessage.getSuccess();
	}
}