/**
 * 
 */
package com.activity.manager.security.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.security.entity.SiteKv;
import com.activity.manager.security.service.SiteKvService;

/**    
 * <p>Description: 站点键值配置控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/siteKv")
public class SiteKvController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SiteKvService siteKvService;
	
	/**
	 * 
	 * Description: 跳转到站点键值配置页面    
	 * @return 
	 * date 2019年4月17日
	 */
	@RequestMapping("/list")
	public String toSiteKvList() {
		logger.info("跳转到站点键值配置页面");
		return "sysmanage/site/siteKv.html";
	}
	
	/**
	 * 
	 * Description: 获取站点键值配置列表   
	 * @param pageUtil
	 * @param siteKv
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, SiteKv siteKv) {
		try {
			List<SiteKv> kList = siteKvService.querySiteKv(siteKv, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> kLst = formate(kList);
			Integer count = siteKvService.siteKvCount(siteKv);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(kLst);
			logger.info("获取站点键值配置列表");
		}catch(Exception e) {
			logger.error("获取站点键值配置列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增站点键值配置    
	 * @param siteKv
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(SiteKv siteKv) {
		try {
			siteKvService.saveSiteKv(siteKv);
			logger.info("新增站点键值配置");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增站点键值配置异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑站点键值配置   
	 * @param siteKv
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(SiteKv siteKv) {
		try {
			siteKvService.editSiteKv(siteKv);
			logger.info("编辑站点键值配置");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑站点键值配置异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除站点键值配置   
	 * @param ids
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delete(String ids) {
		try {
			Assert.hasText(ids, "站点键值配置ID数据不存在");
			Boolean b =siteKvService.delSiteKv(Arrays.asList(ids.split(",")));
			logger.info("删除站点键值配置");
			if(b) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "要删除的键值已经绑定域名", null);
			}
		}catch(Exception e) {
			logger.error("删除站点键值配置异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取站点键值列表    
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/getSiteKvList")
	public ResultMessage getSiteKvList() {
		try {
			List<SiteKv> siteKvList = siteKvService.selectAll();
			logger.info("获取站点键值列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, siteKvList);
		}catch(Exception e) {
			logger.error("获取站点键值列表异常", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<SiteKv> list){
		List<Object> kList = new ArrayList<Object>();
		list.forEach(siteKv -> {
			kList.add(siteKv);
		});
		return kList;
	}
}
