package com.activity.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.SysPermission;

/**
 * <p>Description: 权限mapper </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月14日   
 * @version 1.0
 */
@Mapper
public interface PermissionMapper {

	 List<SysPermission> selectIdAndUrl(@Param("pid") Integer pid);
	 
	 /**
	  * 
	  * Description: 查询所有叶子节点    
	  * @return 
	  * date 2019年4月22日
	  */
	 
	 List<SysPermission> selectAll();
	 
	 /**
		 * Description: 查询所有权限
		 * @date 2019年4月6日    
		 * @param permName
		 * @return
		 */
		List<SysPermission> queryPermission(@Param("permName") String permName,int start ,int pageSize);
		
		/**
		 * Description: 统计所有权限树
		 * @date 2019年4月6日    
		 * @param permName
		 * @return
		 */
		Integer permCount(@Param("permName") String permName);
		
		/**
		 * Description: 删除权限
		 * @date 2019年4月6日    
		 * @param ids
		 * @return
		 */
		void delPerm(@Param("ids") List<String> ids);
		
		/**
		 * Description: 删除角色跟权限的关联关系    
		 * @param ids 
		 * date 2019年4月14日
		 */
		void delPermAndRoleRef(@Param("ids") List<String> ids);
		
		/**
		 * Description: 插入权限数据
		 * @param perm 权限对象
		 * @param uid 用户id
		 * date 2019年4月14日
		 */
		void insertPerm(SysPermission perm);
		
		/**
		 * Description: 更新菜单叶子节点字段
		 * @param isLeaf
		 * @param id 
		 * date 2019年4月14日
		 */
		void updatePermLeafState(@Param("isLeaf") boolean isLeaf,@Param("id") int id);
		
		/**
		 * Description: 查询权限 
		 * @param id
		 * @return 
		 * date 2019年4月14日
		 */
		SysPermission queryPermissionById(@Param("id") String id);
		
		/**
		 * Description: 更新权限   
		 * @param perm 权限对象
		 * date 2019年4月15日
		 */
		void updatePerm(SysPermission perm);
		
		/**
		 * Description:根据状态改值    
		 * @param uid 用户id
		 * @param value 值
		 * @param type 类型
		 * date 2019年4月12日
		 */
		void updateStateById(@Param("id") String id, @Param("value") boolean value,@Param("type") String type);
		
		/**
		 * Description:  计算子节点  
		 * @param parentId 父节点
		 * @return 
		 * date 2019年5月1日
		 */
		Integer countChild(@Param("pid") String parentId,@Param("isAction") Boolean isAction);
		
}