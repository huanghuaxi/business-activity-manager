package com.activity.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.SysWhiteList;

/**
 * 
 * <p>Description: 加载白名单类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0
 */

@Mapper
public interface SysWhiteListMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(SysWhiteList record);

    SysWhiteList selectByPrimaryKey(Integer id);

    List<SysWhiteList> selectAll();

    int updateByPrimaryKey(SysWhiteList record);
    
    /**
     * 
     * Description: 查询所有白名单   
     * @param whiteList
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月16日
     */
    List<SysWhiteList> queryWhiteList(SysWhiteList whiteList, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有白名单   
     * @param whiteList
     * @return 
     * date 2019年4月16日
     */
    Integer whiteListCount(SysWhiteList whiteList);
    
    /**
     * 
     * Description: 删除白名单   
     * @param ids 
     * date 2019年4月16日
     */
    void delWhiteList(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 根据IP获取白名单    
     * @param ip
     * @return 
     * date 2019年4月23日
     */
    List<SysWhiteList> selectByType(@Param("type") Integer type);
}