package com.activity.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.ActivityCategory;

/**
 * 
 * <p>Description: 活动类型mapper </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月15日   
 * @version 1.0
 */
@Mapper
public interface ActivityCategoryMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(ActivityCategory record);

    ActivityCategory selectByPrimaryKey(Integer id);

    List<ActivityCategory> selectAll();

    int updateByPrimaryKey(ActivityCategory record);
    
	/**
	 * Description: 活动类型配置
	 * @date 2019年4月6日    
	 * @param actName
	 * @return
	 */
	List<ActivityCategory> queryActivity(@Param("actName") String actName,int start ,int pageSize);
	
	/**
	 * Description: 活动类型配置
	 * @date 2019年4月6日    
	 * @param actName
	 * @return
	 */
	Integer activityCount(@Param("actName") String actName);
	
	/**
	 * Description: 更新活动类型    
	 * @param record 
	 * date 2019年4月17日
	 */
	void updateCateById(ActivityCategory record);
	
	/**
	 * 
	 * Description: 修改是否在活动演示页展示    
	 * @param record 
	 * date 2019年6月3日
	 */
	void updateActivityShow(ActivityCategory record);
	
	/**
	 * 
	 * Description: 获取展示活动类型   
	 * @return 
	 * date 2019年6月3日
	 */
	List<ActivityCategory> selectByActivityShow();
}