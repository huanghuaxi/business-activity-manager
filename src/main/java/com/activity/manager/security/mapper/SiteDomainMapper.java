package com.activity.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.SiteDomain;

/**
 * 
 * <p>Description: 加载站点域名配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0
 */
@Mapper
public interface SiteDomainMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(SiteDomain record);

    SiteDomain selectByPrimaryKey(Integer id);

    List<SiteDomain> selectAll();

    int updateByPrimaryKey(SiteDomain record);
    
    /**
     * 
     * Description: 查询所有站点域名配置   
     * @param siteDomain
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月17日
     */
    List<SiteDomain> querySiteDomain(SiteDomain siteDomain, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有站点域名配置    
     * @param siteDomain
     * @return 
     * date 2019年4月17日
     */
    Integer siteDomainCount(SiteDomain siteDomain);
    
    /**
     * 
     * Description: 删除站点域名配置   
     * @param ids 
     * date 2019年4月17日
     */
    void delSiteDomain(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 根据域名ID统计域名键值绑定   
     * @param ids
     * @return 
     * date 2019年4月17日
     */
    Integer siteDomainKvByIdsCount(@Param("ids") List<String> ids);
}