package com.activity.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.security.entity.SiteDomainKv;

/**
 * 
 * <p>Description: 加载域名键值绑定类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0
 */
@Mapper
public interface SiteDomainKvMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(SiteDomainKv record);

    SiteDomainKv selectByPrimaryKey(Integer id);

    List<SiteDomainKv> selectAll();

    int updateByPrimaryKey(SiteDomainKv record);
    
    /**
     * 
     * Description: 查询所有域名键值绑定  
     * @param siteDomainKv
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月17日
     */
    List<SiteDomainKv> querySiteDomainKv(SiteDomainKv siteDomainKv, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有域名键值绑定    
     * @param siteDomainKv
     * @return 
     * date 2019年4月17日
     */
    Integer siteDomainKvCount(SiteDomainKv siteDomainKv);
    
    /**
     * 
     * Description: 删除域名键值绑定 
     * @param ids 
     * date 2019年4月17日
     */
    void delSiteDomainKv(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 根据域名获取数据    
     * @param keyEnName
     * @return 
     * date 2019年5月13日
     */
    List<SiteDomainKv> selectByDomainEnName(String domainEnName);
}