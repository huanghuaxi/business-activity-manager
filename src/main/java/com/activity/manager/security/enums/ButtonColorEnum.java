package com.activity.manager.security.enums;

/**
 * <p>Description: 按钮颜色枚举 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月17日   
 * @version 1.0
 */
public enum ButtonColorEnum {

	  NORMAL_COLOR("百搭色","layui-btn layui-btn-normal layui-btn-xs" ,"NORMAL"), 
	  WARM_COLOR("暖色", "layui-btn layui-btn-warm layui-btn-xs","WARM"), 
	  DANGER_COLOR("警告色", "layui-btn layui-btn-danger layui-btn-xs","DANGER"),
	  DISABLE_COLOR("禁用色", "layui-btn layui-btn-disabled layui-btn-xs","DISABLED"),
	  DEFAULT_COLOR("默认色", "layui-btn layui-btn-xs","DEFAULT");  
	
      /**
       * 颜色名称
       */
	  private String colorName;
	  
	  /**
	   * 按钮样式 
	   */
	  private String btnStyle;
	  
	  /**
	   * 编码 
	   */
	  private String code;
    
    private ButtonColorEnum(String colorName,String btnStyle,String code){
        this.code = code;
        this.btnStyle = btnStyle;
        this.colorName = colorName;
    }
    
    public String getColorName() {
		return colorName;
	}



	public void setColorName(String colorName) {
		this.colorName = colorName;
	}



	public String getBtnStyle() {
		return btnStyle;
	}



	public void setBtnStyle(String btnStyle) {
		this.btnStyle = btnStyle;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}
    
    
	/**
	 * Description:    根据code 获取按钮style
	 * @param code
	 * @return 
	 * date 2019年4月17日
	 */
    public static String getBtnStyle(String code) {  
    	String btnStyle = "";
    	switch(code) {
    	   case "NORMAL":
    		   btnStyle = ButtonColorEnum.NORMAL_COLOR.getBtnStyle();
    		   break;
    	   case "WARM":
    		   btnStyle = ButtonColorEnum.WARM_COLOR.getBtnStyle();
    	       break;
    	   case "DANGER":
    		   btnStyle = ButtonColorEnum.DANGER_COLOR.getBtnStyle();
    		   break;
    	   case "DISABLE":
    		   btnStyle = ButtonColorEnum.DISABLE_COLOR.getBtnStyle();
    		   break;
    	   case "DEFAULT":
    		   btnStyle = ButtonColorEnum.DISABLE_COLOR.getBtnStyle();
    		   break;
    	}
        return btnStyle;  
    }  

}
