package com.activity.manager.security.interceptor;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;


/**
 * 
 * <p>Description: 域名拦截器</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年5月14日   
 * @version 1.0
 */

@Service
public class DomainInterceptor implements Filter{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BusiActivityService activityService;

	@Value("${service.ip}")
    private String serviceIp;
	
	@Value("${server.port}")
    private String port;
	
	@Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String http = request.getScheme() + "://";
		String bangDomain = http + request.getServerName();
		HttpServletRequest re = (HttpServletRequest) request;
		HttpServletResponse rs = (HttpServletResponse) response;
		if(request.getServerPort() != 80) {
			bangDomain += ":" + request.getServerPort();
		}
		if(re.getRequestURI() != null && !"".equals(re.getRequestURI()) && !"/".equals(re.getRequestURI())) {
			bangDomain += re.getRequestURI();
			if(re.getQueryString() != null && !"".equals(re.getQueryString())) {
				bangDomain += "?" + re.getQueryString();
			}
		}

		Assert.hasText(bangDomain, "域名数据不存在");
		logger.info("域名：" + bangDomain);
		String portStr = port;
		if("80".equals(port)) {
			portStr = "";
		}else {
			portStr = ":" + port;
		}
		if((http + "localhost" + portStr).equals(bangDomain) || (http + "127.0.0.1" + portStr).equals(bangDomain) || (http + serviceIp  + portStr).equals(bangDomain)) {
			re.getRequestDispatcher("ruleVerify/toPortal").forward(re, response);
		}else {
			BusiActivity activity = activityService.selectByBangDomain(bangDomain);
			if(activity != null) {
				re.getRequestDispatcher("ruleVerify/toActivityPage?actId=" + activity.getId()) .forward(re, response);
			}else {
				chain.doFilter(re, rs);
			}
		}
	}
	
	@Override
	public void destroy() {
		
	}
}
