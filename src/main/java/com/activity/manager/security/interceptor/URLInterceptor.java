/**
 * 
 */
package com.activity.manager.security.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.activity.manager.common.IPUtils;
import com.activity.manager.security.entity.SysWhiteList;
import com.activity.manager.security.mapper.SysWhiteListMapper;

/**    
 * <p>Description: 白名单拦截器</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月23日   
 * @version 1.0   
 */
public class URLInterceptor implements HandlerInterceptor{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SysWhiteListMapper whiteListMapper;
	@Value("${service.ip}")
    private String serviceIp;
	
	//在请求处理之前进行调用（Controller方法调用之前）
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
    	String ip = IPUtils.getRealIP(request);
    	if("0:0:0:0:0:0:0:1".equals(ip) || "127.0.0.1".equals(ip) || serviceIp.equals(ip)) {
    		 return true;
    	}
    	//判断是否是黑名单
    	List<SysWhiteList> whiteLists = whiteListMapper.selectByType(1);
    	if(whiteLists != null && whiteLists.size() > 0) {
    		for(SysWhiteList whiteList : whiteLists) {
    			if(ip.equals(whiteList.getIpAddr())) {
    				logger.info("拦截黑名单IP:" + ip + "的请求");
    				return false;
    			}
    		}
    	}
    	//判断白名单
    	whiteLists = whiteListMapper.selectByType(0);
    	if(whiteLists == null || whiteLists.size() == 0) {
    		return true;
    	}else {
    		//判断访问路径
    		String url = request.getRequestURI();
    		if(url.indexOf("/ruleVerify/") == 0 || url.indexOf("/activityPage/") == 0) {
    			return true;
    		}else {
    			for(SysWhiteList whiteList : whiteLists) {
        			if(ip.equals(whiteList.getIpAddr())) {
        				return true;
        			}
        		}
    		}
    	}
//         request.getRequestDispatcher("error/403.html").forward(request,response);
    	logger.info("拦截IP:" + ip + "的请求");
    	return false;
    }

    //请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {
       
    }

    //在整个请求结束之后被调用
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
	
}
