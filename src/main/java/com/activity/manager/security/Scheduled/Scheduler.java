package com.activity.manager.security.Scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.activity.manager.member.mapper.MemberActivityMapper;

/**
 * 
 * <p>Description: 定时器类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年5月20日   
 * @version 1.0
 */
@Component
public class Scheduler {
	
	@Autowired
	private MemberActivityMapper memberActMapper;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 
	 * Description: 每天0点定时清零配套活动活动会员抽奖次数
	 * date 2019年5月20日
	 */
    @Scheduled(cron = "0 0 0 ? * *")
    public void testTasks() {
    	try {
        	memberActMapper.updateByIsSecondary();
        	logger.info("定时清零配套活动活动会员抽奖次数");
    	}catch(Exception e) {
    		logger.error("定时清零配套活动活动会员抽奖次数异常", e);
    	}
    	
    }
	
}
