package com.activity.manager.business.controller;

import java.util.List;

/**
 * Description:   
 * Copyright: Copyright (c) 2019    
 * @author binghe   
 * @date 2019年4月4日   
 * @version 1.0   
 */
public class PageUtil {
	
	/**
	 * 编码
	 */
	private String code;
	
	/**
	 * 消息
	 */
	private String msg;
	
	/**
	 * 总数
	 */
	private int count;
	
	/**
	 * 数据
	 */
	private List<Object> data;
	
	/**
	 * 当前页
	 */
	private int page;

	/**
	 * 每页页数
	 */
	private int limit;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	/**
	 * Description: 获取当前条数
	 * @date 2019年4月7日    
	 * @return
	 */
	public Integer getCurrIndex() {
	  return ( page -1 ) * limit;
	}
}
