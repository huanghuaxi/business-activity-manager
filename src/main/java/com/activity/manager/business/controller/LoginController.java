package com.activity.manager.business.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.common.IPUtils;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.security.entity.Menu;
import com.activity.manager.security.entity.SysUser;
import com.activity.manager.security.service.UserService;

/**
 * 登陆页、首页跳转控制类
 * Description:   
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月3日   
 * @version 1.0
 */
@Controller
public class LoginController {
	
	@Autowired
	private SessionRegistry sessionRegistry;
	@Autowired
	private UserService userService;
	
	 /**
	  * 登陆页
	  * @return
	  */
     @RequestMapping("/login")
     public String login() {
         return "login.html";
     }
     
     /**
	  * 登陆成功后跳转首页
	  * @return
	  */
     @RequestMapping("/index")
     public String index(ModelAndView model,HttpServletRequest request, HttpServletResponse response) {
         try {
        	 HttpSession session = request.getSession();
        	 if(session.getAttribute("isLogin") == null) {
        		 String ip = IPUtils.getRealIP(request);
        		 //修改用户最后登录时间
        		 SysUser user = CurrentParamUtil.getCurrentUser();
        		 userService.updateLastTime(user.getId(), new Date(), ip);
        		 session.setAttribute("isLogin", true);
        	 }
         }catch(Exception e) {
        	 e.printStackTrace();
         }
    	 return "index.html";
     }
     
     /**
	  * 登陆成功后跳转首页
	  * @return
	  */
     @RequestMapping("/menu/load")
     public @ResponseBody  List<Menu> loadMenu() {
    	 SysUser user = CurrentParamUtil.getCurrentUser();
         List<Menu> mlst = userService.loadMenu(user.getRoleList());
    	 return mlst;
     }
     
     /**
      * 根据用户名将其session置为无效
      * @param username
      * @return
      */
     @ResponseBody
     @GetMapping("/removeUserSessionByUsername")
     public ResultMessage removeUserSessionByUsername(@RequestParam String username) {
    	 // 获取session中所有的用户信息
    	 try {
    		 List<Object> users = sessionRegistry.getAllPrincipals(); 
             for (Object principal : users) {
                 if (principal instanceof SysUser) {
                     final SysUser loggedUser = (SysUser) principal;
                     if (username.equals(loggedUser.getUsername())) {
                    	 // false代表不包含过期session
                         List<SessionInformation> sessionsInfo = sessionRegistry.getAllSessions(principal, false);
                         if (null != sessionsInfo && sessionsInfo.size() > 0) {
                             for (SessionInformation sessionInformation : sessionsInfo) {
                                 sessionInformation.expireNow();
                             }
                         }
                     }
                 }
             }
             return ResultMessage.getSuccess();
    	 }catch(Exception e) {
    		 e.printStackTrace();
    		 return ResultMessage.getFail();
    	 }
         
     }
	 
     /**
      * 失败登陆返回登陆页
      * @param model
      * @return
      */
     @RequestMapping("/login-error")
     public String loginError(Model model) {
         model.addAttribute("loginError", true);
         return "login.html";
     }
     
     /**
      * 
      * Description: 用户退出系统
      * @date 2019年4月1日    
      * @param request
      * @param response
      * @return
      */
     @RequestMapping(value="/logout")
     public String logout (HttpServletRequest request, HttpServletResponse response) {
    	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	 new SecurityContextLogoutHandler().logout(request, response, auth);
    	 return "login.html";
     } 

}
