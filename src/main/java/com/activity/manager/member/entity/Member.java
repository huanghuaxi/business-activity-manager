package com.activity.manager.member.entity;

/**
 * 
 * <p>Description: 会员类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
public class Member {
    /**
     * ID
     */
    private Integer id;

    /**
     * 	会员账号
     */
    private String memberName;

    /**
     *	 姓名
     */
    private String realName;

    /**
     * 	电话
     */
    private String cel;

    /**
     *	 注册IP
     */
    private String registIp;

    /**
     * 	注册时间
     */
    private String registTime;

    /**
     * 	邀请码
     */
    private String invitationCode;
    
    /**
     *	 等级
     */
    private String level;

    /**
     * 	创建时间
     */
    private String createTime;

    /**
     *	 创建人
     */
    private Integer createPerson;
    
    /**
     * 	会员类型 0：会员 1：游客
     */
    private Integer type;
    
    /**
     * 	活动会员
     */
    private MemberActivity memberAct;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel == null ? null : cel.trim();
    }

    public String getRegistIp() {
        return registIp;
    }

    public void setRegistIp(String registIp) {
        this.registIp = registIp == null ? null : registIp.trim();
    }

    public String getRegistTime() {
        return registTime;
    }

    public void setRegistTime(String registTime) {
        this.registTime = registTime;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode == null ? null : invitationCode.trim();
    }
    
    public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level == null ? null : level.trim();
	}

	public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public MemberActivity getMemberAct() {
		return memberAct;
	}

	public void setMemberAct(MemberActivity memberAct) {
		this.memberAct = memberAct;
	}
}