package com.activity.manager.member.entity;

/**
 * 
 * <p>Description: 签到纪录类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0
 */
public class SignRecord {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	会员ID
     */
    private Integer memberId;

    /**
     *	活动ID
     */
    private Integer actId;

    /**
     *	签到类型
     */
    private Integer signType;

    /**
     *	签到时间
     */
    private String signTime;

    /**
     *	创建时间
     */
    private String createTime;
    
    /**
     * 	会员
     */
    private Member member;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getActId() {
        return actId;
    }

    public void setActId(Integer actId) {
        this.actId = actId;
    }

    public Integer getSignType() {
        return signType;
    }

    public void setSignType(Integer signType) {
        this.signType = signType;
    }

    public String getSignTime() {
        return signTime;
    }

    public void setSignTime(String signTime) {
        this.signTime = signTime == null ? null : signTime.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
}