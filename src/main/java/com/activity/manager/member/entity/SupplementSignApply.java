package com.activity.manager.member.entity;

import java.util.Date;

/**
 * 
 * <p>Description: 补签申请类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年5月17日   
 * @version 1.0
 */
public class SupplementSignApply {

	private Integer id;

    /**
     *  活动Id
     */
    private Integer actId;

    /**
     *  会员ID
     */
    private Integer memberId;

    /**
     *  申请补签时间
     */
    private String signTime;

    /**
     *  状态
     */
    private Integer start;

    /**
     *  审核人
     */
    private Integer examiner;

    /**
     *  审核时间
     */
    private Date examineTime;

    /**
     *  创建时间
     */
    private Date createTime;
    
    /**
     *  会员
     */
    private Member member;
    
    /**
     *  会员账号
     */
    private String memberName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActId() {
		return actId;
	}

	public void setActId(Integer actId) {
		this.actId = actId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getSignTime() {
		return signTime;
	}

	public void setSignTime(String signTime) {
		this.signTime = signTime == null ? "" : signTime.trim();
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getExaminer() {
		return examiner;
	}

	public void setExaminer(Integer examiner) {
		this.examiner = examiner;
	}

	public Date getExamineTime() {
		return examineTime;
	}

	public void setExamineTime(Date examineTime) {
		this.examineTime = examineTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
    
}