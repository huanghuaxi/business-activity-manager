package com.activity.manager.member.entity;

/**
 * 
 * <p>Description: 会员活动类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
public class MemberActivity {
    /**
     * ID
     */
    private Integer id;

    /**
     * 	 会员ID
     */
    private Integer memberId;

    /**
     * 	 活动ID
     */
    private Integer actId;

    /**
     * 	 创建时间
     */
    private String createTime;

    /**
     * 	 创建人
     */
    private Integer createPerson;

    /**
     * 	可参加次数
     */
    private Integer allTimes;

    /**
     * 	已参加次数
     */
    private Integer participatedTimes;
    
    /**
     * 	等级
     */
    private String level;
    
    /**
     * 	会员
     */
    private Member member;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getActId() {
        return actId;
    }

    public void setActId(Integer actId) {
        this.actId = actId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    public Integer getAllTimes() {
        return allTimes;
    }

    public void setAllTimes(Integer allTimes) {
        this.allTimes = allTimes;
    }

    public Integer getParticipatedTimes() {
        return participatedTimes;
    }

    public void setParticipatedTimes(Integer participatedTimes) {
        this.participatedTimes = participatedTimes;
    }

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level == null ? null : level.trim();
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
    
}