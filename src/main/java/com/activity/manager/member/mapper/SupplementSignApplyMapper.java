package com.activity.manager.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.member.entity.SupplementSignApply;

/**
 * 
 * <p>Description: 加载补签申请类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年5月17日   
 * @version 1.0
 */
@Mapper
public interface SupplementSignApplyMapper {
    
	int deleteByPrimaryKey(Integer id);
    
	int insert(SupplementSignApply record);

    SupplementSignApply selectByPrimaryKey(Integer id);

    List<SupplementSignApply> selectAll();

    int updateByPrimaryKey(SupplementSignApply record);
    
    /**
     * 
     * Description: 获取签到申请列表     
     * @param apply
     * @param start
     * @param pageSize
     * @return 
     * date 2019年5月17日
     */
    List<SupplementSignApply> querySupplementSignApply(SupplementSignApply apply, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计签到申请    
     * @param apply
     * @return 
     * date 2019年5月17日
     */
    Integer supplementSignApplyCount(SupplementSignApply apply);
    
    /**
     * 
     * Description: 根据补签时间查询    
     * @param actId
     * @param memberId
     * @param signTimes
     * @return 
     * date 2019年5月17日
     */
    List<SupplementSignApply> selectBySignTimes(Integer actId, Integer memberId, List<String> signTimes);
    
    /**
     * 
     * Description: 根据会员查询补签申请    
     * @param actId
     * @param memberId
     * @return 
     * date 2019年5月17日
     */
    List<SupplementSignApply> selectByMember(Integer actId, Integer memberId);
    
    /**
     * 
     * Description: 审核通过    
     * @param ids 
     * date 2019年5月23日
     */
    void applyPass(@Param("ids")List<String> ids, @Param("userId")Integer userId);
    
    /**
     * 
     * Description: 审核不通过    
     * @param ids 
     * date 2019年5月23日
     */
    void applyFail(@Param("ids")List<String> ids, @Param("userId")Integer userId);
    
    /**
     * 
     * Description: 根据ID获取补签申请    
     * @param ids
     * @return 
     * date 2019年5月23日
     */
    List<SupplementSignApply> selectByIds(@Param("ids")List<String> ids);
}