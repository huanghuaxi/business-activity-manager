package com.activity.manager.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.member.entity.Member;

/**
 * 
 * <p>Description: 获取会员类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
@Mapper
public interface MemberMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(Member record);

    Member selectByPrimaryKey(Integer id);

    List<Member> selectAll();

    int updateByPrimaryKey(Member record);
    
    /**
     * 
     * Description: 查询所有会员   
     * @param member
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月18日
     */
    List<Member> queryMember(Member member,int start ,int pageSize);
    
    /**
     * 
     * Description: 统计所有会员    
     * @param member
     * @return 
     * date 2019年4月18日
     */
	Integer memberCount(Member member);
	
	/**
	 * 
	 * Description: 删除会员    
	 * @param ids 
	 * date 2019年4月18日
	 */
	void delMember(@Param("ids") List<String> ids);
    
    /**
     * Description:根据会员账号查找会员
     * @param memberName
     * @return 
     * date 2019年4月10日
     */
    Member selectByName(String memberName);
    
}