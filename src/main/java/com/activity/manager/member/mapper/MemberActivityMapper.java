package com.activity.manager.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.MemberActivityDTO;

/**
 * 
 * <p>Description: 获取活动会员类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
@Mapper
public interface MemberActivityMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(MemberActivity record);

    MemberActivity selectByPrimaryKey(Integer id);

    List<MemberActivity> selectAll();

    int updateByPrimaryKey(MemberActivity record);
    
    /**
     * Description：查询所有参加活动会员
     * @date 2019年4月10日 
     * @param member
     * @param start
     * @param pageSize
     * @return
     */
    List<MemberActivity> queryMember(MemberActivityDTO member,int start ,int pageSize);
    
    /**
	 * Description：统计所有参加活动会员
	 * @date 2019年4月10日 
	 * @param member
	 * @return
	 */
	Integer memberCount(MemberActivityDTO member);
	
	/**
	 * Description: 删除参加活动会员
	 * @date 2019年4月10日    
	 * @param ids
	 * @return
	 */
	void delMember(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 根据会员ID删除活动会员    
	 * @param ids 
	 * date 2019年4月28日
	 */
	void delByMemberId(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 根据会员账号查找数据   
	 * @param memberId
	 * @return 
	 * date 2019年4月12日
	 */
	MemberActivity selectByMemberId(Integer memberId);
	
	/**
	 * 
	 * Description: 根据活动ID和会员ID统计数据   
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年4月17日
	 */
	MemberActivity selectByActId(Integer actId, Integer memberId);
	
	/**
	 * 
	 * Description: 删除配套活动活动会员     
	 * date 2019年5月20日
	 */
	void updateByIsSecondary();
	
	/**
	 * 
	 * Description: 根据活动ID删除活动会员    
	 * @param actId 
	 * date 2019年5月28日
	 */
	void delByActId(Integer actId);
    
}