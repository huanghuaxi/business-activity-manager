package com.activity.manager.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.activity.manager.member.entity.SignRecord;

/**
 * 
 * <p>Description: 加载签到纪录类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0
 */

@Mapper
public interface SignRecordMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(SignRecord record);

    SignRecord selectByPrimaryKey(Integer id);

    List<SignRecord> selectAll();

    int updateByPrimaryKey(SignRecord record);
    
    /**
     * 
     * Description: 查询所有签到纪录   
     * @param signRecord
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月16日
     */
    List<SignRecord> querySignRecord(SignRecord signRecord, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有签到纪录   
     * @param signRecord
     * @return 
     * date 2019年4月16日
     */
    Integer signRecordCount(SignRecord signRecord);
    
    /**
     * 
     * Description: 根据会员获取签到记录    
     * @param memberId
     * @param actId
     * @return 
     * date 2019年5月16日
     */
    List<SignRecord> selectByMember(Integer memberId, Integer actId);
    
    /**
     * 
     * Description: 统计签到天数    
     * @param memberId
     * @param actId
     * @return 
     * date 2019年5月16日
     */
    Integer countByYear(Integer memberId, Integer actId);
    
    /**
     * 
     * Description: 查询是否签到    
     * @param memberId
     * @param actId
     * @return 
     * date 2019年5月16日
     */
    SignRecord selectByDate(Integer memberId, Integer actId);
    
    /**
     * 
     * Description: 查询是否签到  
     * @param memberId
     * @param actId
     * @param signTime
     * @return 
     * date 2019年5月17日
     */
    SignRecord selectBySignTime(Integer memberId, Integer actId, String signTime);
    
    /**
     * 
     * Description: 查询是否签到    
     * @param memberId
     * @param actId
     * @param dates
     * @return 
     * date 2019年5月24日
     */
    List<SignRecord> selectBySignTimes(Integer memberId, Integer actId, List<String> dates);
    
    /**
     * 
     * Description: 根据活动ID删除签到记录    
     * @param actId 
     * date 2019年5月28日
     */
    void delByActId(Integer actId);
}