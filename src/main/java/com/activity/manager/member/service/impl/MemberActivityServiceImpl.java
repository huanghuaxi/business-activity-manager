package com.activity.manager.member.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.MemberActivityDTO;
import com.activity.manager.member.mapper.MemberActivityMapper;
import com.activity.manager.member.mapper.MemberMapper;
import com.activity.manager.member.service.MemberActivityService;
import com.activity.manager.member.service.MemberTransactionalService;
import com.activity.manager.security.entity.SysUser;

/**
 * 
 * Description: 活动会员更新类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author zhangxin   
 * @date 2019年4月10日   
 * @version 1.0
 */

@Service
public class MemberActivityServiceImpl implements MemberActivityService {

	@Autowired
	private MemberActivityMapper memberActMapper;

	@Autowired
	private MemberTransactionalService memberService;

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: queryMember</p>   
	 * <p>Description: 查询所有参加活动的会员</p>   
	 * @param member
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.MemberActivityService#queryMember(com.activity.manager.security.entity.MemberActivityDTO, int, int)
	 */
	@Override
	public List<MemberActivity> queryMember(MemberActivityDTO member, int start, int pageSize) {
		return memberActMapper.queryMember(member, start, pageSize);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: memberCount</p>   
	 * <p>Description: 统计所有参加活动的会员</p>   
	 * @param member
	 * @return   
	 * @see com.activity.manager.security.service.MemberActivityService#memberCount(com.activity.manager.security.entity.MemberActivityDTO)
	 */
	@Override
	public Integer memberCount(MemberActivityDTO member) {
		return memberActMapper.memberCount(member);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getmemberById</p>   
	 * <p>Description: 根据ID获取参加活动的会员</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.security.service.MemberActivityService#getmemberById(java.lang.Integer)
	 */
	@Override
	public MemberActivity getmemberById(Integer id) {
		return memberActMapper.selectByPrimaryKey(id);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveMember</p>   
	 * <p>Description: 添加参加活动会员</p>   
	 * @param memberActDTO   
	 * @see com.activity.manager.security.service.MemberActivityService#saveMember(com.activity.manager.security.entity.MemberActivityDTO)
	 */
	@Override
	public void saveMember(MemberActivityDTO memberActDTO) {
		List<MemberActivityDTO> memberActDTOs = new ArrayList<MemberActivityDTO>();
		memberActDTO.setType(0);
		memberActDTOs.add(memberActDTO);
		memberService.saveMemberActTra(memberActDTOs);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editMember</p>   
	 * <p>Description: 编辑参加活动会员</p>   
	 * @param memberAct   
	 * @see com.activity.manager.security.service.MemberActivityService#editMember(com.activity.manager.security.entity.MemberActivity)
	 */
	@Override
	public void editMember(MemberActivity memberAct) {
		Assert.notNull(memberAct.getId(), "活动会员ID数据不存在");
		Assert.notNull(memberAct.getAllTimes(), "可参加次数数据不存在");
		MemberActivity member = memberActMapper.selectByPrimaryKey(memberAct.getId());
		member.setAllTimes(memberAct.getAllTimes());
		member.setLevel(memberAct.getLevel());
		memberActMapper.updateByPrimaryKey(member);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: deleteMember</p>   
	 * <p>Description: 删除参加活动会员</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.MemberActivityService#deleteMember(java.util.List)
	 */
	@Override
	public void deleteMember(List<String> ids) {
		memberActMapper.delMember(ids);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: impExcel</p>   
	 * <p>Description: 批量导入活动会员</p>   
	 * @param list
	 * @return   
	 * @see com.activity.manager.security.service.MemberActivityService#impExcel(java.util.List)   
	 */
	@Override
	public String impExcel(Integer actId, String flag, Workbook workbook) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(actId, "活动类型数据不存在");
		MemberActivityDTO activity = new MemberActivityDTO();
		Pattern pattern = Pattern.compile("[0-9]*");
		if(workbook != null) {
			for(int sheetNum = 0;sheetNum < workbook.getNumberOfSheets();sheetNum++){
				//获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(sheetNum);
                if(sheet == null){
                    continue;
                }
                //获得当前sheet的开始行
                int firstRowNum  = sheet.getFirstRowNum();
                //获得当前sheet的结束行
                int lastRowNum = sheet.getLastRowNum();
                List<MemberActivityDTO> memberActDTOs = new ArrayList<MemberActivityDTO>();
                Map<String, String> memberActMap = new HashMap<String, String>();
                //循环除了第一行的所有行
                for(int rowNum = firstRowNum+1;rowNum <= lastRowNum;rowNum++){ //为了过滤到第一行因为我的第一行是数据库的列
                    //获得当前行
                    Row row = sheet.getRow(rowNum);
                    if(row == null){
                        continue;
                    }
                    //获取每列数据
                    Cell cell = row.getCell(0);
                    String memberName = ExcelUtil.getCellValue(cell).trim();
                    cell = row.getCell(1);
                    String allTimes = ExcelUtil.getCellValue(cell).trim();
                    cell = row.getCell(2);
                    String level = ExcelUtil.getCellValue(cell).trim();
                    Matcher isNum = pattern.matcher(allTimes);
                    if(flag != null && "sign".equals(flag)) {
        				//签到活动抽奖次数默认为0
        				allTimes = "0";
                    }
                    if(memberName == null || "".equals(memberName)) {
        				//会员账号为空不做插入
        				return "导入失败，会员账号不能为空";
        			}else if(allTimes == null || "".equals(allTimes) || !isNum.matches()) {
        				//抽奖次数为空不做插入，返回会员账号
        				return "导入失败，抽奖次数不能为空且只能为正整数";
        			}else if(flag != null && "box".equals(flag) && (level == null || "".equals(level))) {
        				//宝箱活动一定要输入等级
        				return "导入失败，宝箱不能为空";
        			
        			}else if(memberActMap.get(memberName) != null) {
        				return "导入失败，活动会员不能重复";
        			}else {
        				activity = new MemberActivityDTO();
        				activity.setActId(actId);
        				activity.setMemberName(memberName);
        				activity.setLevel(level);
        				activity.setAllTimes(Integer.parseInt(allTimes));
        				activity.setType(0);
        				memberActDTOs.add(activity);
        				memberActMap.put(memberName, allTimes);
        			}
                }
                //新增活动会员
                return memberService.saveMemberActTra(memberActDTOs);
			}
		}
		return "";
	}
	
}
