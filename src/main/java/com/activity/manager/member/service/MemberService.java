/**
 * 
 */
package com.activity.manager.member.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.usermodel.Workbook;

import com.activity.manager.member.entity.Member;

/**    
 * <p>Description: 加载会员类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月18日   
 * @version 1.0   
 */
public interface MemberService {
	
	/**
     * 
     * Description: 查询所有会员   
     * @param member
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月18日
     */
    public List<Member> queryMember(Member member,int start ,int pageSize);
    
    /**
     * 
     * Description: 统计所有会员    
     * @param member
     * @return 
     * date 2019年4月18日
     */
	public Integer memberCount(Member member);
	
	/**
	 * 
	 * Description: 新增会员    
	 * @param member 
	 * date 2019年4月18日
	 */
	public Integer saveMember(Member member);
	
	/**
	 * 
	 * Description: 编辑会员    
	 * @param member 
	 * date 2019年4月18日
	 */
	public void editMember(Member member);
	
	/**
	 * 
	 * Description: 删除会员    
	 * @param ids 
	 * date 2019年4月18日
	 */
	public void deleteMember(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 根据ID获取会员   
	 * @param id
	 * @return 
	 * date 2019年4月18日
	 */
	public Member selectById(Integer id);
	
	/**
	 * 
	 * Description: 批量导入会员   
	 * @param actId
	 * @param workbook
	 * @return 
	 * date 2019年4月18日
	 */
	public List<String> impExcel(Workbook workbook);
	
}
