package com.activity.manager.member.service;

import java.util.List;

import com.activity.manager.member.entity.MemberActivityDTO;
import com.activity.manager.reward.entity.InnerRewardDTO;

/**
 * 
 * <p>Description: 加载会员事务类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年4月29日   
 * @version 1.0
 */
public interface MemberTransactionalService {
	
	/**
	 * 
	 * Description: 新增内定会员   
	 * @param innerDTOs
	 * @return 
	 * date 2019年4月29日
	 */
	public String saveInnerTra(List<InnerRewardDTO> innerDTOs, String flag);
	
	/**
	 * 
	 * Description: 新增活动会员   
	 * @param memberActDTOs
	 * @return 
	 * date 2019年4月29日
	 */
	public String saveMemberActTra(List<MemberActivityDTO> memberActDTOs);
	
	/**
	 * 
	 * Description: 新增会员    
	 * @param memberName 
	 * date 2019年4月29日
	 */
	public void saveMember(String memberName, Integer type);
	
	/**
	 * 
	 * Description: 新增活动会员    
	 * @param actId
	 * @param memberId
	 * @param level 
	 * date 2019年4月29日
	 */
	public void saveMemberAct(Integer actId, Integer memberId, String level, Integer allTimes);
	
	/**
	 * 
	 * Description: 新增内定会员   
	 * @param innerDTO 
	 * date 2019年4月29日
	 */
	public void saveInnerReward(InnerRewardDTO innerDTO);
	
}
