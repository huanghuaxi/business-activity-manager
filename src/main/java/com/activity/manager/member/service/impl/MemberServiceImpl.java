/**
 * 
 */
package com.activity.manager.member.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.mapper.MemberActivityMapper;
import com.activity.manager.member.mapper.MemberMapper;
import com.activity.manager.member.service.MemberService;
import com.activity.manager.reward.mapper.InnerRewardMapper;
import com.activity.manager.security.entity.SysUser;

/**    
 * <p>Description: 会员更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月18日   
 * @version 1.0   
 */

@Service
public class MemberServiceImpl implements MemberService{
	
	@Autowired
	private MemberMapper memberMapper;
	
	@Autowired
	private MemberActivityMapper memberActMapper;
	
	@Autowired
	private InnerRewardMapper innerRewardMapper;

	/* 
	 *(non-Javadoc)   
	 * <p>Title: queryMember</p>   
	 * <p>Description: 查询所有会员</p>   
	 * @param member
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.member.service.MemberService#queryMember(com.activity.manager.member.entity.Member, int, int)   
	 */
	@Override
	public List<Member> queryMember(Member member, int start, int pageSize) {
		return memberMapper.queryMember(member, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: memberCount</p>   
	 * <p>Description: 统计所有会员</p>   
	 * @param member
	 * @return   
	 * @see com.activity.manager.member.service.MemberService#memberCount(com.activity.manager.member.entity.Member)   
	 */
	@Override
	public Integer memberCount(Member member) {
		return memberMapper.memberCount(member);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveMember</p>   
	 * <p>Description: 新增会员</p>   
	 * @param member   
	 * @see com.activity.manager.member.service.MemberService#saveMember(com.activity.manager.member.entity.Member)   
	 */
	@Override
	public Integer saveMember(Member member) {
		Assert.hasText(member.getMemberName(), "会员账号数据不存在");
		Member m = memberMapper.selectByName(member.getMemberName());
		if(m != null) {
			return 0;
		}
		SysUser user = CurrentParamUtil.getCurrentUser();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = format.format(new Date());
		member.setCreatePerson(user.getId());
		member.setCreateTime(time);
		member.setRegistTime(time);
		member.setType(0);
		memberMapper.insert(member);
		return 1;
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: edtiMember</p>   
	 * <p>Description: 编辑会员</p>   
	 * @param member   
	 * @see com.activity.manager.member.service.MemberService#edtiMember(com.activity.manager.member.entity.Member)   
	 */
	@Override
	public void editMember(Member member) {
		Assert.notNull(member.getId(), "会员ID数据不存在");
		Assert.hasText(member.getMemberName(), "会员账号数据不存在");
		Member m = memberMapper.selectByPrimaryKey(member.getId());
		Assert.notNull(m, "会员数据不存在");
		m.setMemberName(member.getMemberName());
		m.setCel(member.getCel());
		m.setInvitationCode(member.getInvitationCode());
//		m.setLevel(member.getLevel());
		m.setRealName(member.getRealName());
		m.setRegistIp(member.getRegistIp());
		memberMapper.updateByPrimaryKey(m);
	};
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: delMember</p>   
	 * <p>Description: 删除会员</p>   
	 * @param ids   
	 * @see com.activity.manager.member.service.MemberService#delMember(java.util.List)   
	 */
	@Override
	@Transactional
	public void deleteMember(List<String> ids) {
		//删除活动会员
		memberActMapper.delByMemberId(ids);
		//删除内定会员
		innerRewardMapper.delByMemberId(ids);
		//删除会员
		memberMapper.delMember(ids);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: selectById</p>   
	 * <p>Description: 根据ID获取会员</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.member.service.MemberService#selectById(java.lang.Integer)   
	 */
	@Override
	public Member selectById(Integer id) {
		Assert.notNull(id, "会员ID数据不存在");
		return memberMapper.selectByPrimaryKey(id);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: impExcel</p>   
	 * <p>Description: 批量导入会员</p>   
	 * @param actId
	 * @param workbook
	 * @return   
	 * @see com.activity.manager.member.service.MemberService#impExcel(java.lang.Integer, org.apache.poi.ss.usermodel.Workbook)   
	 */
	@Override
	public List<String> impExcel(Workbook workbook) {
		List<String> impError = new ArrayList<String>();
		Member member = new Member();
		if(workbook != null) {
			for(int sheetNum = 0;sheetNum < workbook.getNumberOfSheets();sheetNum++){
				//获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(sheetNum);
                if(sheet == null){
                    continue;
                }
                //获得当前sheet的开始行
                int firstRowNum  = sheet.getFirstRowNum();
                //获得当前sheet的结束行
                int lastRowNum = sheet.getLastRowNum();
                //循环除了第一行的所有行
                for(int rowNum = firstRowNum+1;rowNum <= lastRowNum;rowNum++){ //为了过滤到第一行因为我的第一行是数据库的列
                    //获得当前行
                    Row row = sheet.getRow(rowNum);
                    if(row == null){
                        continue;
                    }
                    //获取每列数据
                    Cell cell = row.getCell(0);
                    String memberName = ExcelUtil.getCellValue(cell).trim();
                    cell = row.getCell(1);
                    String realName = ExcelUtil.getCellValue(cell).trim();
                    cell = row.getCell(2);
                    String cel = ExcelUtil.getCellValue(cell).trim();
                    cell = row.getCell(3);
                    String invitationCode = ExcelUtil.getCellValue(cell).trim();
                    if(memberName == null || "".equals(memberName)) {
        				//会员账号为空不做插入
        				continue;
        			}else {
        				//插入数据
        				member = new Member();
        				member.setMemberName(memberName);
        				member.setRealName(realName);
        				member.setCel(cel);
        				member.setType(0);
        				member.setInvitationCode(invitationCode);
        				try { 
        					saveMember(member); 
        				}catch(Exception e) { 
        					e.printStackTrace(); 
        					//新增数据失败
        					impError.add(memberName);
        				}
        			}
                }
			}
		}
		
		return impError;
	}
}
