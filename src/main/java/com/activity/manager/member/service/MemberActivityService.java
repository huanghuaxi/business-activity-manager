package com.activity.manager.member.service;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.MemberActivityDTO;

/**
 * 
 * Description: 加载活动会员类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author zhangxin   
 * @date 2019年4月10日   
 * @version 1.0
 */

public interface MemberActivityService {
	
	/**
	 * 
	 * Description: 查询所有参加活动的会员   
	 * @param member
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年4月13日
	 */
	public List<MemberActivity> queryMember(MemberActivityDTO member, int start, int pageSize);
	
	/**
	 * 
	 * Description: 统计所有参加活动的会员   
	 * @param member
	 * @return 
	 * date 2019年4月13日
	 */
	public Integer memberCount(MemberActivityDTO member);
	
	/**
	 * 
	 * Description: 根据ID获取参加活动的会员   
	 * @param id
	 * @return 
	 * date 2019年4月13日
	 */
	public MemberActivity getmemberById(Integer id);
	
	/**
	 * 
	 * Description: 添加参加活动会员   
	 * @param memberActDTO 
	 * date 2019年4月13日
	 */
	public void saveMember(MemberActivityDTO memberActDTO);
	
	/**
	 * 
	 * Description: 编辑参加活动会员   
	 * @param memberAct 
	 * date 2019年4月13日
	 */
	public void editMember(MemberActivity memberAct);
	
	/**
	 * 
	 * Description: 删除参加活动会员  
	 * @param ids 
	 * date 2019年4月13日
	 */
	public void deleteMember(List<String> ids);
	
	/**
	 * 
	 * Description: 批量导入活动会员   
	 * @param actId
	 * @param workbook
	 * @return 
	 * date 2019年4月13日
	 */
	public String impExcel(Integer actId, String flag, Workbook workbook);
	
	
}
