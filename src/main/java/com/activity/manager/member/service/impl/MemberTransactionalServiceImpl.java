package com.activity.manager.member.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.MemberActivityDTO;
import com.activity.manager.member.mapper.MemberActivityMapper;
import com.activity.manager.member.mapper.MemberMapper;
import com.activity.manager.member.service.MemberTransactionalService;
import com.activity.manager.reward.entity.InnerReward;
import com.activity.manager.reward.entity.InnerRewardDTO;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.mapper.InnerRewardMapper;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.security.entity.SysUser;

/**
 * 
 * <p>Description: 更新会员事务类 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月29日   
 * @version 1.0
 */
@Service
public class MemberTransactionalServiceImpl implements MemberTransactionalService {

	@Autowired
	private InnerRewardMapper innerMapper;
	@Autowired
	private MemberMapper memberMapper;
	@Autowired
	private MemberActivityMapper memberActMapper;
	@Autowired
	private RewardConfMapper rewardMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveInnerTra</p>   
	 * <p>Description: 新增内定会员 </p>   
	 * @param innerDTOs
	 * @return   
	 * @see com.activity.manager.member.service.MemberTransactionalService#saveInnerTra(java.util.List)
	 */
	@Override
	@Transactional
	public String saveInnerTra(List<InnerRewardDTO> innerDTOs, String flag) {
		Assert.notNull(innerDTOs, "奖品内定数据不存在");
		if(innerDTOs.size() <= 0) {
			return "失败，数据不能为空";
		}
		Member member = new Member();
		//查看奖品数量是否足够
		List<String> rewardIds = new ArrayList<String>();
		Map<String, Integer> rewardNumMap = new HashMap<String, Integer>();
		Integer number = 0;
		for(InnerRewardDTO innerDTO : innerDTOs) {
			Assert.notNull(innerDTO.getRewardId(), "奖品ID数据不存在");
			number = rewardNumMap.get(innerDTO.getRewardId().toString());
			if(number != null) {
				rewardNumMap.put(innerDTO.getRewardId().toString(), (number + 1));
			}else {
				rewardNumMap.put(innerDTO.getRewardId().toString().toString(), 1);
				rewardIds.add(innerDTO.getRewardId().toString());
			}
		}
		//获取该活动已有的内定会员
		List<InnerReward> oldInner = innerMapper.selectByActId(innerDTOs.get(0).getActId());
		if(oldInner != null && oldInner.size() > 0) {
			for(InnerReward inner : oldInner) {
				number = rewardNumMap.get(inner.getRewardId().toString());
				if(number != null) {
					rewardNumMap.put(inner.getRewardId().toString(), (number));
				}else {
					rewardNumMap.put(inner.getRewardId().toString().toString(), 0);
					rewardIds.add(inner.getRewardId().toString());
				}
			}
		}
		List<RewardConf> rewards = rewardMapper.selectByIds(rewardIds, innerDTOs.get(0).getActId());
		Map<String, Integer> rewardCountMap = new HashMap<String, Integer>();
		Map<String, String> rewardLevelMap = new HashMap<String, String>();
		for(RewardConf rewardCon : rewards) {
			rewardCountMap.put(rewardCon.getId().toString(), rewardCon.getRewardNum());
			rewardLevelMap.put(rewardCon.getId().toString(), rewardCon.getRewardOption());
		}
		for(String rewardId : rewardIds) {
			if(rewardCountMap.get(rewardId) == null) {
				return "失败，奖品ID为" + rewardId + "的奖品不存在";
			}
			if(rewardCountMap.get(rewardId) < rewardNumMap.get(rewardId)) {
				return "失败，奖品ID为" + rewardId + "的数量不足";
			}
		}
		//判断宝箱等级是否一致
		for(InnerRewardDTO innerDTO : innerDTOs) {
			Assert.hasText(innerDTO.getMemberName(), "会员账号数据不存在");
			member = memberMapper.selectByName(innerDTO.getMemberName());
			if(member != null) {
				//查询该会员是否有参加活动，如果没有新增会员到该活动
				MemberActivity memberAct = memberActMapper.selectByActId(innerDTO.getActId(), member.getId());
				if(memberAct != null) {
					if(flag != null && "box".equals(flag)) {
						if(!memberAct.getLevel().equals(rewardLevelMap.get(innerDTO.getRewardId().toString()))) {
							return "失败，奖品ID为" + innerDTO.getRewardId() + "的宝箱等级与会员账号为" 
									+ innerDTO.getMemberName() + "的宝箱等级不一致";
						}
					}
					
				}
			}
			
		}
		//新增内定会员
		for(InnerRewardDTO innerDTO : innerDTOs) {
			Assert.hasText(innerDTO.getMemberName(), "会员账号数据不存在");
			member = memberMapper.selectByName(innerDTO.getMemberName());
			if(member == null) {
				//新增会员
				saveMember(innerDTO.getMemberName(), innerDTO.getType());
				member = memberMapper.selectByName(innerDTO.getMemberName());
			}
			//查询该会员是否有参加活动，如果没有新增会员到该活动
			MemberActivity memberAct = memberActMapper.selectByActId(innerDTO.getActId(), member.getId());
			if(memberAct == null) {
				saveMemberAct(innerDTO.getActId(), member.getId(), rewardLevelMap.get(innerDTO.getRewardId().toString()), 0);
			}else {
				//增加该会员抽奖次数
				memberAct.setAllTimes(memberAct.getAllTimes());
				memberActMapper.updateByPrimaryKey(memberAct);
			}
			//新增内定会员
			innerDTO.setRemId(member.getId());
			saveInnerReward(innerDTO);
		}
		return "";
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveMemberActTra</p>   
	 * <p>Description: 新增活动会员</p>   
	 * @param memberActDTOs
	 * @return   
	 * @see com.activity.manager.member.service.MemberTransactionalService#saveMemberActTra(java.util.List)
	 */
	@Override
	@Transactional
	public String saveMemberActTra(List<MemberActivityDTO> memberActDTOs) {
		Assert.notNull(memberActDTOs, "活动会员数据不存在");
		if(memberActDTOs.size() <= 0) {
			return "失败，数据不能为空";
		}
		for(MemberActivityDTO memberActDTO : memberActDTOs) {
			Assert.hasText(memberActDTO.getMemberName(), "会员账号数据不存在");
			Assert.notNull(memberActDTO.getAllTimes(), "会员抽奖次数数据不存在");
			Member member = memberMapper.selectByName(memberActDTO.getMemberName());
			if(member == null) {
				saveMember(memberActDTO.getMemberName(), memberActDTO.getType());
				member = memberMapper.selectByName(memberActDTO.getMemberName());
			}
			//给活动添加会员
			MemberActivity memberAct = memberActMapper.selectByActId(memberActDTO.getActId(), member.getId());
			if(memberAct == null) {
				saveMemberAct(memberActDTO.getActId(), member.getId(), memberActDTO.getLevel(), memberActDTO.getAllTimes());
			}else {
				memberAct.setMemberId(member.getId());
				memberAct.setActId(memberActDTO.getActId());
				memberAct.setAllTimes(memberActDTO.getAllTimes());
				memberAct.setLevel(memberActDTO.getLevel());
				memberActMapper.updateByPrimaryKey(memberAct);
			}
		}
		return "";
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveMember</p>   
	 * <p>Description: 新增会员 </p>   
	 * @param memberName   
	 * @see com.activity.manager.member.service.MemberTransactionalService#saveMember(java.lang.String)
	 */
	@Override
	public void saveMember(String memberName, Integer type) {
		Assert.hasText(memberName, "会员账号数据不存在");
		Member member = new Member();
		SysUser user = null;
		try {
			user = CurrentParamUtil.getCurrentUser();
		}catch(Exception e) {
			
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = format.format(new Date());
		member.setMemberName(memberName);
		member.setCreatePerson(user != null ? user.getId() : null);
		member.setCreateTime(time);
		member.setRegistTime(time);
		member.setType(type);
		memberMapper.insert(member);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveMemberAct</p>   
	 * <p>Description: 新增活动会员 </p>   
	 * @param actId
	 * @param memberId
	 * @param level   
	 * @see com.activity.manager.member.service.MemberTransactionalService#saveMemberAct(java.lang.Integer, java.lang.Integer, java.lang.String)
	 */
	@Override
	public void saveMemberAct(Integer actId, Integer memberId, String level, Integer allTimes) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(memberId, "会员ID数据不存在");
		Assert.notNull(memberId, "活动会员等级数据不存在");
		MemberActivity memberAct = new MemberActivity();
		SysUser user = null;
		try {
			user = CurrentParamUtil.getCurrentUser();
		}catch(Exception e) {
			
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = format.format(new Date());
		memberAct.setActId(actId);
		memberAct.setMemberId(memberId);
		memberAct.setAllTimes(allTimes == null ? 0 : allTimes);
		memberAct.setParticipatedTimes(0);
		memberAct.setCreateTime(time);
		memberAct.setCreatePerson(user != null ? user.getId() : null);
		memberAct.setLevel(level);
		memberActMapper.insert(memberAct);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveInnerReward</p>   
	 * <p>Description: 新增内定会员</p>   
	 * @param innerDTO   
	 * @see com.activity.manager.member.service.MemberTransactionalService#saveInnerReward(com.activity.manager.reward.entity.InnerRewardDTO)
	 */
	@Override
	public void saveInnerReward(InnerRewardDTO innerDTO) {
		InnerReward inner = new InnerReward();
		SysUser user = CurrentParamUtil.getCurrentUser();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = format.format(new Date());
		inner.setActId(innerDTO.getActId());
		inner.setRemId(innerDTO.getRemId());
		inner.setRewardId(innerDTO.getRewardId());
		inner.setIsUsed(0);
		inner.setCreatePerson(user != null ? user.getId() : null);
		inner.setCreateTime(time);
		innerMapper.insert(inner);
	}

	

}
