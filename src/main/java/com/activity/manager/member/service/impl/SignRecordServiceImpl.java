/**
 * 
 */
package com.activity.manager.member.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.SignRecord;
import com.activity.manager.member.entity.SupplementSignApply;
import com.activity.manager.member.mapper.MemberMapper;
import com.activity.manager.member.mapper.SignRecordMapper;
import com.activity.manager.member.mapper.SupplementSignApplyMapper;
import com.activity.manager.member.service.SignRecordService;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.entity.RewardRecord;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.reward.mapper.RewardRecordMapper;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.rule.mapper.SignRuleMapper;
import com.activity.manager.security.entity.SysUser;

/**    
 * <p>Description: 签到纪录更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */

@Service
public class SignRecordServiceImpl implements SignRecordService{

	@Autowired
	private SignRecordMapper signRecordMapper;
	
	@Autowired
	private MemberMapper memberMapper;
	
	@Autowired
	private RewardConfMapper rewardConfMapper;
	
	@Autowired
	private RewardRecordMapper rewardRecordMapper;
	
	@Autowired
	private SignRuleMapper signRuleMapper;
	
	@Autowired
	private SupplementSignApplyMapper applyMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: querySignRecord</p>   
	 * <p>Description: 查询所有签到纪录</p>   
	 * @param signRecord
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.member.service.SignRecordService#querySignRecord(com.activity.manager.member.entity.SignRecord, java.lang.Integer, java.lang.Integer)   
	 */
	@Override
	public List<SignRecord> querySignRecord(SignRecord signRecord, Integer start, Integer pageSize) {
		return signRecordMapper.querySignRecord(signRecord, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: SignRecordCount</p>   
	 * <p>Description: 统计所有签到纪录</p>   
	 * @param signRecord
	 * @return   
	 * @see com.activity.manager.member.service.SignRecordService#SignRecordCount(com.activity.manager.member.entity.SignRecord)   
	 */
	@Override
	public Integer signRecordCount(SignRecord signRecord) {
		return signRecordMapper.signRecordCount(signRecord);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByMember</p>   
	 * <p>Description: 根据会员获取签到记录</p>   
	 * @param actId
	 * @param memberName
	 * @return   
	 * @see com.activity.manager.member.service.SignRecordService#selectByMember(java.lang.Integer, java.lang.String)
	 */
	@Override
	public List<SignRecord> selectByMember(Integer actId, String memberName) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.hasText(memberName, "会员账号数据不存在");
		//根据会员账号获取会员
		Member member = memberMapper.selectByName(memberName);
		Assert.notNull(member, "会员数据不存在");
		List<SignRecord> record = signRecordMapper.selectByMember(member.getId(), actId);
		return record;
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveSignRecord</p>   
	 * <p>Description: 补签</p>   
	 * @param actId
	 * @param memberName
	 * @param dayStr   
	 * @see com.activity.manager.member.service.SignRecordService#saveSignRecord(java.lang.Integer, java.lang.String)
	 */
	@Override
	@Transactional
	public String saveSignRecord(Integer actId, String memberName, String dayStr) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.hasText(memberName, "会员账号数据不存在");
		Assert.hasText(dayStr, "补签日期数据不存在");
		//根据会员账号获取会员
		Member member = memberMapper.selectByName(memberName);
		Assert.notNull(member, "会员数据不存在");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		String newDate = format.format(new Date());
		List<String> days = Arrays.asList(dayStr.trim().split(","));
		List<String> dates = new ArrayList<String>();
		String date = "";
		for(int i = 0;i < days.size();i++) {
			date = newDate + "-" + days.get(i);
			dates.add(date);
		}
		//判断是否已经签到
		List<SignRecord> records = signRecordMapper.selectBySignTimes(member.getId(), actId, dates);
		if(records != null && records.size() > 0) {
			return "选择的日期已做补签，请重新选择";
		}
		//查询补签申请
		List<SupplementSignApply> applys = applyMapper.selectBySignTimes(actId, member.getId(), dates);
		if(applys != null && applys.size() > 0) {
			//通过补签申请
			List<String> applyIds = new ArrayList<String>();
			for(SupplementSignApply apply : applys) {
				applyIds.add(apply.getId().toString());
			}
			SysUser user = CurrentParamUtil.getCurrentUser();
			applyMapper.applyPass(applyIds, user.getId());
		}
		//获取签到规则
		List<SignRule> rules = signRuleMapper.selectByActId(actId);
		SignRule signRule = null;
		if(rules != null) {
			for(SignRule rule: rules) {
				if(rule.getSignType() == 0) {
					signRule = rule;
					break;
				}
			}
		}
		//新增补签记录
		format = new SimpleDateFormat("yyyy-MM-dd");
		newDate = format.format(new Date());
		SignRecord record = new SignRecord();
		for(int i = 0;i < dates.size();i++) {
			record = new SignRecord();
			record.setActId(actId);
			record.setMemberId(member.getId());
			record.setSignType(1);
			record.setSignTime(dates.get(i));
			record.setCreateTime(newDate);
			signRecordMapper.insert(record);
			//发放奖品
			sendReward(signRule, member);
		}
		return "";
	}
	
	/**
	 * 
	 * Description: 补发奖品    
	 * @param apply 
	 * date 2019年5月23日
	 */
	private void sendReward(SignRule signRule, Member member) {
		if(signRule != null) {
			List<RewardConf> rewards = rewardConfMapper.selectByIds(
					Arrays.asList(signRule.getRewardId().split(",")), signRule.getActId());
			RewardConf rc = new RewardConf();
			Map<String, String> reward = new HashMap<String, String>();
			if(rewards != null && rewards.size() > 0) {
				//判读是否有奖品
				int rewardNum = 0;
				Float ratioCount = 0.0f;
				for(int j = 0;j < rewards.size();j++) {
					rewardNum += rewards.get(j).getRewardNum();
					ratioCount += rewards.get(j).getRatio();
				}
				if(rewardNum > 0) {
					//抽奖
					Integer ratioNum = 10000;
					if(ratioCount > 100) {
						ratioNum = (int)(ratioCount * 100);
					}
					int k = 0;
					rc.setRewardNum(0);
					Random r = new Random();
					Float ratio = (float)r.nextInt(ratioNum) / 100;
					Float min = 0f;
					Float max = rewards.get(0).getRatio();
					for(k = 0;k < rewards.size();k++) {
						if(ratio >= min && ratio <= max) {
							if(rewards.get(k).getRewardNum() > 0) {
								reward.put("reward", createReward(rewards.get(k)));
							}
							rc = rewards.get(k);
							break;
						}else {
							if(k + 1 == rewards.size()) {
								if(ratioCount >= 100) {
									if(rewards.get(k).getRewardNum() > 0) {
										reward.put("reward", createReward(rewards.get(k)));
									}
									rc = rewards.get(k);
								}
								break;
							}else {
								min = max;
								max += rewards.get(k + 1).getRatio();
							}
						}
					}
				}
			}
			if(reward.get("reward") != null) {
				//减少奖品数
				reduceReward(rc);
				//生成中奖纪录
				saveRewardRecord(signRule.getActId(), rc, reward.get("reward"), member);
			}
		}
	}
	
	/**
	 * 
	 * Description: 生成奖品   
	 * @param rewardConf
	 * @return 
	 * date 2019年4月17日
	 */
	private String createReward(RewardConf rewardConf) {
		String reward = new String();
		String rewardCon = rewardConf.getRewardCon();
		//根据概率查看是否中奖
		Random r = new Random();
		if(rewardCon.indexOf("~") != -1) {
			//范围
			String[] numbers = rewardCon.split("~");
			Integer min = Integer.parseInt(numbers[0]);
			Integer max = Integer.parseInt(numbers[1]) - min;
			int number = r.nextInt(max) + min;
			reward = number + "";
		}else if(rewardCon.indexOf(",") != -1) {
			//多值
			String[] numbers = rewardCon.split(",");
			int number = r.nextInt(numbers.length - 1);
			reward = numbers[number] + (rewardConf.getUnit() == null ? "" : rewardConf.getUnit());
		}else {
			//单值
			reward = rewardCon;
		}
		
		return reward;
	}
	
	/**
	 * 
	 * Description: 减少奖品数   
	 * @param rewardConf
	 * @return 
	 * date 2019年4月17日
	 */
	private void reduceReward(RewardConf rewardConf) {
		RewardConf reward = rewardConfMapper.selectByPrimaryKey(rewardConf.getId());
		reward.setRewardNum(reward.getRewardNum() - 1);
		reward.setWinNum(reward.getWinNum() + 1);
		rewardConfMapper.updateByPrimaryKey(reward);
	}
	
	/**
	 * 
	 * Description: 添加中奖纪录   
	 * @param actId
	 * @param rewardConf
	 * @param member
	 * @return 
	 * date 2019年4月17日
	 */
	private void saveRewardRecord(Integer actId, RewardConf rewardConf, String reward, Member member) {
		RewardRecord record = new RewardRecord();
		record.setActId(actId);
		record.setRemId(member.getId());
		record.setRemName(member.getMemberName());
		record.setRewardCon(reward);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		record.setCreatePersion(member.getCreatePerson());
		record.setCreateTime(format.format(new Date()));
		record.setIsSend(0);
		//是否中奖
		if(rewardConf == null) {
			record.setIsGit(false);
		}else {
			record.setRewardId(rewardConf.getId());
			record.setRewardOption(rewardConf.getRewardOption() + "：" + rewardConf.getRewardCon());
			record.setIsGit(true);
		}
		rewardRecordMapper.insert(record);
	}
	
}
