package com.activity.manager.member.service.impl;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.SignRecord;
import com.activity.manager.member.entity.SupplementSignApply;
import com.activity.manager.member.mapper.SignRecordMapper;
import com.activity.manager.member.mapper.SupplementSignApplyMapper;
import com.activity.manager.member.service.SupplementSignApplyService;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.entity.RewardRecord;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.reward.mapper.RewardRecordMapper;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.rule.mapper.SignRuleMapper;
import com.activity.manager.security.entity.SysUser;

/**
 * 
 * <p>Description: 补签申请更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年5月17日   
 * @version 1.0
 */
@Service
public class SupplementSignApplyServiceImpl implements SupplementSignApplyService{

	@Autowired
	private SupplementSignApplyMapper applyMapper;
	
	@Autowired
	private SignRuleMapper signRuleMapper;
	
	@Autowired
	private RewardConfMapper rewardConfMapper;
	
	@Autowired
	private RewardRecordMapper rewardRecordMapper;
	
	@Autowired
	private SignRecordMapper signRecordMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: querySupplementSignApply</p>   
	 * <p>Description: 获取补签申请列表</p>   
	 * @param apply
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.member.service.SupplementSignApplyService#querySupplementSignApply(com.activity.manager.member.entity.SupplementSignApply, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<SupplementSignApply> querySupplementSignApply(SupplementSignApply apply, Integer start,
			Integer pageSize) {
		return applyMapper.querySupplementSignApply(apply, start, pageSize);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: supplementSignApplyCount</p>   
	 * <p>Description: 统计补签申请</p>   
	 * @param apply
	 * @return   
	 * @see com.activity.manager.member.service.SupplementSignApplyService#supplementSignApplyCount(com.activity.manager.member.entity.SupplementSignApply)
	 */
	@Override
	public Integer supplementSignApplyCount(SupplementSignApply apply) {
		return applyMapper.supplementSignApplyCount(apply);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: examine</p>   
	 * <p>Description: 审核补签申请</p>   
	 * @param apply   
	 * @see com.activity.manager.member.service.SupplementSignApplyService#examine(com.activity.manager.member.entity.SupplementSignApply)
	 */
	@Override
	@Transactional
	public void examine(SupplementSignApply apply) {
		Assert.notNull(apply.getId(), "补签申请ID数据不存在");
		Assert.notNull(apply.getStart(), "补签申请状态数据不存在");
		SupplementSignApply apply2 = applyMapper.selectByPrimaryKey(apply.getId());
		Assert.notNull(apply2, "补签申请数据不存在");
		if(apply2.getStart() == 0) {
			apply2.setStart(apply.getStart());
			SysUser user = CurrentParamUtil.getCurrentUser();
			apply2.setExaminer(user.getId());
			apply2.setExamineTime(new Date());
			applyMapper.updateByPrimaryKey(apply2);
			if(apply.getStart() == 1) {
				//查看此日期是否已经签到,补签
				boolean isSign = saveSignRecord(apply2);
				if(!isSign) {
					//通过发放奖品
					sendReward(apply2);
				}
			}
		}
	}
	
	/**
	 * 
	 * Description: 生成奖品   
	 * @param rewardConf
	 * @return 
	 * date 2019年4月17日
	 */
	private String createReward(RewardConf rewardConf) {
		String reward = new String();
		String rewardCon = rewardConf.getRewardCon();
		//根据概率查看是否中奖
		Random r = new Random();
		if(rewardCon.indexOf("~") != -1) {
			//范围
			String[] numbers = rewardCon.split("~");
			Integer min = Integer.parseInt(numbers[0]);
			Integer max = Integer.parseInt(numbers[1]) - min;
			int number = r.nextInt(max) + min;
			reward = number + "";
		}else if(rewardCon.indexOf(",") != -1) {
			//多值
			String[] numbers = rewardCon.split(",");
			int number = r.nextInt(numbers.length - 1);
			reward = numbers[number] + (rewardConf.getUnit() == null ? "" : rewardConf.getUnit());
		}else {
			//单值
			reward = rewardCon;
		}
		
		return reward;
	}
	
	/**
	 * 
	 * Description: 减少奖品数   
	 * @param rewardConf
	 * @return 
	 * date 2019年4月17日
	 */
	private void reduceReward(RewardConf rewardConf) {
		RewardConf reward = rewardConfMapper.selectByPrimaryKey(rewardConf.getId());
		reward.setRewardNum(reward.getRewardNum() - 1);
		reward.setWinNum(reward.getWinNum() + 1);
		rewardConfMapper.updateByPrimaryKey(reward);
	}
	
	/**
	 * 
	 * Description: 添加中奖纪录   
	 * @param actId
	 * @param rewardConf
	 * @param member
	 * @return 
	 * date 2019年4月17日
	 */
	private void saveRewardRecord(Integer actId, RewardConf rewardConf, String reward, Member member) {
		RewardRecord record = new RewardRecord();
		record.setActId(actId);
		record.setRemId(member.getId());
		record.setRemName(member.getMemberName());
		record.setRewardCon(reward);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SysUser user = CurrentParamUtil.getCurrentUser();
		record.setCreatePersion(user.getId());
		record.setCreateTime(format.format(new Date()));
		record.setIsSend(0);
		//是否中奖
		if(rewardConf == null) {
			record.setIsGit(false);
		}else {
			record.setRewardId(rewardConf.getId());
			record.setRewardOption(rewardConf.getRewardOption() + "：" + rewardConf.getRewardCon());
			record.setIsGit(true);
		}
		rewardRecordMapper.insert(record);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: pass</p>   
	 * <p>Description: 审核通过</p>   
	 * @param ids
	 * @return   
	 * @see com.activity.manager.member.service.SupplementSignApplyService#pass(java.lang.String)
	 */
	@Override
	@Transactional
	public void pass(String ids) {
		Assert.hasText(ids, "补签申请ID数据不存在");
		List<String> applyIds = Arrays.asList(ids.trim().split(","));
		//通过申请
		SysUser user = CurrentParamUtil.getCurrentUser();
		applyMapper.applyPass(applyIds, user.getId());
		//获取补签申请
		List<SupplementSignApply> applys = applyMapper.selectByIds(applyIds);
		if(applys != null) {
			for(SupplementSignApply apply : applys) {
				//添加补签记录
				boolean isSign = saveSignRecord(apply);
				if(!isSign) {
					//发放奖品
					sendReward(apply);
				}
			}
		}
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: fail</p>   
	 * <p>Description: 审核未通过</p>   
	 * @param ids
	 * @return   
	 * @see com.activity.manager.member.service.SupplementSignApplyService#fail(java.lang.String)
	 */
	@Override
	public void fail(String ids) {
		Assert.hasText(ids, "补签申请ID数据不存在");
		List<String> applyIds = Arrays.asList(ids.trim().split(","));
		//不通过申请
		SysUser user = CurrentParamUtil.getCurrentUser();
		applyMapper.applyFail(applyIds, user.getId());
	}
	
	/**
	 * 
	 * Description: 添加签到记录    
	 * @param apply 
	 * date 2019年5月23日
	 */
	private boolean saveSignRecord(SupplementSignApply apply) {
		//查看此日期是否已经签到
		SignRecord record = signRecordMapper.selectBySignTime(apply.getMemberId(),apply.getActId(), apply.getSignTime());
		if(record != null) {
			return true;
		}
		//添加签到记录
		SignRecord signRecord = new SignRecord();
		signRecord.setActId(apply.getActId());
		signRecord.setMemberId(apply.getMemberId());
		signRecord.setSignTime(apply.getSignTime());
		signRecord.setSignType(1);
		signRecord.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		signRecordMapper.insert(signRecord);
		return false;
	}
	
	/**
	 * 
	 * Description: 补发奖品    
	 * @param apply 
	 * date 2019年5月23日
	 */
	private void sendReward(SupplementSignApply apply) {
		List<SignRule> signRules = signRuleMapper.selectByActId(apply.getActId());
		if(signRules != null) {
			for(int i = 0;i < signRules.size();i++) {
				if(signRules.get(i).getSignType() == 0) {
					//每日签到奖品
					if(signRules.get(i).getRewardId() != null && !"".equals(signRules.get(i).getRewardId())) {
						List<RewardConf> rewards = rewardConfMapper.selectByIds(
								Arrays.asList(signRules.get(i).getRewardId().split(",")), apply.getActId());
						RewardConf rc = new RewardConf();
						Map<String, String> reward = new HashMap<String, String>();
						if(rewards != null && rewards.size() > 0) {
							//判读是否有奖品
							int rewardNum = 0;
							Float ratioCount = 0.0f;
							for(int j = 0;j < rewards.size();j++) {
								rewardNum += rewards.get(j).getRewardNum();
								ratioCount += rewards.get(j).getRatio();
							}
							if(rewardNum > 0) {
								//抽奖
								Integer ratioNum = 10000;
								if(ratioCount > 100) {
									ratioNum = (int)(ratioCount * 100);
								}
								int k = 0;
								rc.setRewardNum(0);
								Random r = new Random();
								Float ratio = (float)r.nextInt(ratioNum) / 100;
								Float min = 0f;
								Float max = rewards.get(0).getRatio();
								for(k = 0;k < rewards.size();k++) {
									if(ratio >= min && ratio <= max) {
										if(rewards.get(k).getRewardNum() > 0) {
											reward.put("reward", createReward(rewards.get(k)));
										}
										rc = rewards.get(k);
										break;
									}else {
										if(k + 1 == rewards.size()) {
											if(ratioCount >= 100) {
												if(rewards.get(k).getRewardNum() > 0) {
													reward.put("reward", createReward(rewards.get(k)));
												}
												rc = rewards.get(k);
											}
											break;
										}else {
											min = max;
											max += rewards.get(k + 1).getRatio();
										}
									}
								}
							}
						}
						if(reward.get("reward") != null) {
							//减少奖品数
							reduceReward(rc);
							//生成中奖纪录
							saveRewardRecord(apply.getActId(), rc, reward.get("reward"), apply.getMember());
						}
					}
				}
			}
		}
	}
}
