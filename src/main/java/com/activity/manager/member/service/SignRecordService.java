/**
 * 
 */
package com.activity.manager.member.service;

import java.util.List;

import com.activity.manager.member.entity.SignRecord;

/**    
 * <p>Description: 加载签到纪录类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */
public interface SignRecordService {
	
	/**
     * 
     * Description: 查询所有签到纪录   
     * @param signRecord
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月16日
     */
    public List<SignRecord> querySignRecord(SignRecord signRecord, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有签到纪录   
     * @param signRecord
     * @return 
     * date 2019年4月16日
     */
    public Integer signRecordCount(SignRecord signRecord);
	
    /**
     * 
     * Description: 根据会员获取签到记录    
     * @param actId
     * @param memberName
     * @return 
     * date 2019年5月24日
     */
    public List<SignRecord> selectByMember(Integer actId, String memberName);
    
    /**
     * 
     * Description: 补签    
     * @param actId
     * @param memberName
     * @param dayStr 
     * date 2019年5月24日
     */
    public String saveSignRecord(Integer actId, String memberName, String dayStr);
    
}
