package com.activity.manager.member.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.MemberActivityDTO;
import com.activity.manager.member.service.MemberActivityService;

import lombok.extern.slf4j.Slf4j;

/**
 * Description: 活动会员控制类
 * Copyright: Copyright (c) 2019    
 * @author zhangxin   
 * @date 2019年4月10日   
 * @version 1.0   
 */

@Slf4j
@Controller
@RequestMapping("/memberAct")
public class MemberActivityController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MemberActivityService memberActService;
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到活动会员管理页面   
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月13日
	 */
	@RequestMapping("/list")
	public String toMemberActList(Integer actId, String flag, Model model) {
		String url = "activitymanage/activity/memberAct.html";
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			model.addAttribute("flag", flag);
			if(flag != null && "sign".equals(flag)) {
				url = "activitymanage/activity/signMemberAct.html";
			}
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到活动会员管理页面");
		}catch(Exception e) {
			logger.error("活动会员管理页面跳转异常", e);
		}
		return url;
	}
	
	/**
	 * 
	 * Description: 获取活动会员列表   
	 * @param pageUtil
	 * @param memberAct
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, MemberActivityDTO memberAct) {
		try {
			Assert.notNull(memberAct.getActId(), "活动ID数据不存在");
			List<MemberActivity> mList = memberActService.queryMember(memberAct, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> aLst = formate(mList);
			Integer count = memberActService.memberCount(memberAct);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(aLst);
			logger.info("获取活动会员列表");
		}catch(Exception e) {
			logger.error("获取活动会员列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 添加活动会员  
	 * @param memberAct
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage saveMember(MemberActivityDTO memberAct) {
		try {
			memberActService.saveMember(memberAct);
			logger.info("添加活动会员");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("添加活动会员异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * Description: 编辑活动会员
	 * @param memberAct
	 * @return 
	 * date 2019年4月10日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage editMember(MemberActivity memberAct) {
		try {
			memberActService.editMember(memberAct);
			logger.info("编辑活动会员");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑活动会员异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * Description: 删除参加活动会员
	 * @param ids
	 * @return 
	 * date 2019年4月10日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delMember(String ids) {
		try {
			Assert.hasText(ids, "活动会员ID数据不存在");
			logger.info("删除参加活动会员");
			memberActService.deleteMember(Arrays.asList(ids.split(",")));
		}catch(Exception e) {
			logger.error("删除活动会员异常",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 活动会员批量导入    
	 * @param file
	 * @param actId
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/impExcel")
	public ResultMessage impExcel(@RequestParam("file") MultipartFile file, Integer actId, String flag) {
		try {
			//读取Excel数据内容
			Workbook workbook = ExcelUtil.getWorkBook(file);
			String msg = memberActService.impExcel(actId, flag, workbook);
			logger.info("活动会员批量导入 ");
			if("".equals(msg)) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("活动会员批量导入失败", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<MemberActivity> list){
		List<Object> mLst = new ArrayList<Object>();
		list.forEach(memberAct -> {
			MemberActivityDTO memberActDTO = new MemberActivityDTO();
			memberActDTO.setId(memberAct.getId());
			memberActDTO.setActId(memberAct.getActId());
			memberActDTO.setMemberId(memberAct.getMemberId());
			memberActDTO.setCreatePersion(memberAct.getCreatePerson());
			memberActDTO.setCreateTime(memberAct.getCreateTime());
			memberActDTO.setAllTimes(memberAct.getAllTimes());
			memberActDTO.setParticipatedTimes(memberAct.getParticipatedTimes());
			memberActDTO.setMemberName(memberAct.getMember().getMemberName());
			memberActDTO.setLevel(memberAct.getLevel());
			memberActDTO.setType(memberAct.getMember().getType());
			mLst.add(memberActDTO);
		});
		return mLst;
	}
	
}
