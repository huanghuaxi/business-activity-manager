/**
 * 
 */
package com.activity.manager.member.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.service.MemberService;

/**    
 * <p>Description: 会员控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月18日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/member")
public class MemberController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MemberService memberService;
	
	/**
	 * 
	 * Description: 跳转到会员管理页面    
	 * @return 
	 * date 2019年4月18日
	 */
	@RequestMapping("/list")
	public String toMemberList() {
		logger.info("跳转到会员管理页面");
		return "membermanage/member/member.html";
	}
	
	/**
	 * 
	 * Description: 跳转到会员新增编辑页面   
	 * @param id
	 * @param model
	 * @return 
	 * date 2019年4月18日
	 */
	@RequestMapping("/toSaveOrEdit")
	public String toSaveOrEdit(Integer id, Model model) {
		try {
			model.addAttribute("id", id);
			logger.info("跳转到会员新增编辑页面");
		}catch(Exception e) {
			logger.error("会员新增编辑页面跳转失败", e);
		}
		return "membermanage/member/editMember.html";
	}
	
	/**
	 * 
	 * Description: 获取会员列表   
	 * @param pageUtil
	 * @param member
	 * @return 
	 * date 2019年4月18日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, Member member) {
		try {
			List<Member> mList = memberService.queryMember(member, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> mLst = formate(mList);
			Integer count = memberService.memberCount(member);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(mLst);
			logger.info("获取会员列表");
		}catch(Exception e) {
			logger.error("获取会员列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增会员   
	 * @param member
	 * @return 
	 * date 2019年4月18日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage saveMember(Member member) {
		try {
			Integer isSuccess = memberService.saveMember(member);
			logger.info("新增会员");
			if(isSuccess == 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该会员账号已存在", null);
			}
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增会员异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑会员   
	 * @param member
	 * @return 
	 * date 2019年4月18日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage editMember(Member member) {
		try {
			memberService.editMember(member);
			logger.info("编辑会员");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑会员异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除会员   
	 * @param ids
	 * @return 
	 * date 2019年4月18日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delMember(String ids) {
		try {
			Assert.hasText(ids, "会员ID数据不存在");
			logger.info("删除会员");
			memberService.deleteMember(Arrays.asList(ids.split(",")));
		}catch(Exception e) {
			logger.error("删除会员异常",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 根据ID获取会员   
	 * @param id
	 * @return 
	 * date 2019年4月18日
	 */
	@ResponseBody
	@RequestMapping("getMemberById")
	public ResultMessage getMemberById(Integer id) {
		try {
			Member member = memberService.selectById(id);
			logger.info("根据ID获取会员");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, member);
		}catch(Exception e) {
			logger.error("根据ID获取会员异常");
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 批量导入会员   
	 * @param file
	 * @return 
	 * date 2019年4月18日
	 */
	@ResponseBody
	@RequestMapping("/impExcel")
	public ResultMessage impExcel(@RequestParam("file") MultipartFile file) {
		try {
			//读取Excel数据内容
			Workbook workbook = ExcelUtil.getWorkBook(file);
			List<String> impError = memberService.impExcel(workbook);
			logger.info("会员批量导入 ");
			if(impError != null && impError.size() > 0) {
				String msg = "会员账号";
				for(String str : impError) {
					msg += str + "、"; 
				}
				msg = msg.substring(0, msg.length() - 1);
				msg += "导入失败";
				return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, msg, null);
			}else {
				return ResultMessage.getSuccess();
			}
		}catch(Exception e) {
			logger.error("会员批量导入失败", e);
			return ResultMessage.getFail();
		}
	}
	
	
	private List<Object> formate(List<Member> list){
		List<Object> mLst = new ArrayList<Object>();
		list.forEach(member -> {
			mLst.add(member);
		});
		return mLst;
	}
	
}
