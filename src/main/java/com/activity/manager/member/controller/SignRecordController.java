/**
 * 
 */
package com.activity.manager.member.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.SignRecord;
import com.activity.manager.member.service.MemberService;
import com.activity.manager.member.service.SignRecordService;

/**    
 * <p>Description: 签到记录控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/signRecord")
public class SignRecordController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SignRecordService signRecordService;
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到签到记录页面   
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月16日
	 */
	@RequestMapping("/list")
	public String toMemberActList(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到签到记录页面");
		}catch(Exception e) {
			logger.error("签到纪录页面跳转异常", e);
		}
		return "activitymanage/activity/signRecord.html";
	}
	
	/**
	 * 
	 * Description: 获取签到纪录列表   
	 * @param pageUtil
	 * @param signRecord
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, SignRecord signRecord, String memberName) {
		try {
			Assert.notNull(signRecord.getActId(), "数据不存在");
			signRecord.setMember(new Member());
			signRecord.getMember().setMemberName(memberName);
			List<SignRecord> sList = signRecordService.querySignRecord(signRecord, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> sLst = formate(sList);
			Integer count = signRecordService.signRecordCount(signRecord);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(sLst);
			logger.info("获取签到记录列表");
		}catch(Exception e) {
			logger.error("获取列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 获取当月签到记录    
	 * @param actId
	 * @param memberName
	 * @return 
	 * date 2019年5月24日
	 */
	@ResponseBody
	@RequestMapping("getListByMonth")
	public ResultMessage getListByMonth(Integer actId, String memberName) {
		try {
			List<SignRecord> record = signRecordService.selectByMember(actId, memberName);
			logger.info("获取当月签到记录");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, record);
		}catch(Exception e) {
			logger.error("获取当月签到记录异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 补签    
	 * @param actId
	 * @param memberName
	 * @param days
	 * @return 
	 * date 2019年5月24日
	 */
	@ResponseBody
	@RequestMapping("supplementSign")
	public ResultMessage supplementSign(Integer actId, String memberName, String days) {
		try {
			String msg = signRecordService.saveSignRecord(actId, memberName, days);
			logger.info("补签，活动ID：" + actId + "，补签日期：" + days);
			if("".equals(msg)) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("补签异常", e);
			return ResultMessage.getFail();
		}
	}
	
	
	private List<Object> formate(List<SignRecord> list){
		List<Object> sLst = new ArrayList<Object>();
		list.forEach(signRecord -> {
			sLst.add(signRecord);
		});
		return sLst;
	}
}
