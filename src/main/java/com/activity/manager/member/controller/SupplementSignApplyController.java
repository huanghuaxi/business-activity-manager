package com.activity.manager.member.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.SupplementSignApply;
import com.activity.manager.member.service.SupplementSignApplyService;

/**
 * 
 * <p>Description: 补签申请控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年5月17日   
 * @version 1.0
 */

@Controller
@RequestMapping("/supplementSignApply")
public class SupplementSignApplyController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SupplementSignApplyService applyService;
	
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到补签申请管理    
	 * @return 
	 * date 2019年5月17日
	 */
	@RequestMapping("/list")
	public String toApplyList(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到补签申请管理页面");
		}catch(Exception e) {
			logger.error("跳转到补签申请管理页面异常", e);
		}
		
		return "activitymanage/activity/supplymentSignApply.html";
	}
	
	/**
	 * 
	 * Description: 获取补签申请列表    
	 * @param pageUtil
	 * @param apply
	 * @return 
	 * date 2019年5月17日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, SupplementSignApply apply) {
		try {
			List<SupplementSignApply> sList = applyService.querySupplementSignApply(apply, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> sLst = formate(sList);
			Integer count = applyService.supplementSignApplyCount(apply);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(sLst);
			logger.info("获取补签申请列表");
		}catch(Exception e) {
			logger.error("获取补签申请异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 补签申请审核    
	 * @param apply
	 * @return 
	 * date 2019年5月17日
	 */
	@ResponseBody
	@RequestMapping("/examine")
	public ResultMessage examine(SupplementSignApply apply) {
		try {
			applyService.examine(apply);
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("补签申请审核异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 补签申请审核通过    
	 * @param ids
	 * @return 
	 * date 2019年5月23日
	 */
	@ResponseBody
	@RequestMapping("/pass")
	public ResultMessage pass(String ids) {
		try {
			applyService.pass(ids);
			logger.info("补签申请审核通过");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("补签申请审核通过异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 补签申请审核未通过    
	 * @param ids
	 * @return 
	 * date 2019年5月23日
	 */
	@ResponseBody
	@RequestMapping("/fail")
	public ResultMessage fail(String ids) {
		try {
			applyService.fail(ids);
			logger.info("补签申请审核未通过");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("补签申请审核未通过异常", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<SupplementSignApply> list){
		List<Object> sLst = new ArrayList<Object>();
		list.forEach(apply -> {
			sLst.add(apply);
		});
		return sLst;
	}
}
