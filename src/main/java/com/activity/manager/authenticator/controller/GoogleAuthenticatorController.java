/**
 * 
 */
package com.activity.manager.authenticator.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.common.GoogleAuthenticator;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.security.entity.SysUser;


/**    
 * <p>Description: 谷歌身份验证控制器</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月24日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/googleAuthenticator")
public class GoogleAuthenticatorController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 
	 * Description: 跳转到谷歌身份验证页面   
	 * @return 
	 * date 2019年4月24日
	 */
	@RequestMapping("toGoogleAut")
	public String toGoogleAut() {
		logger.info("跳转到谷歌身份验证页面");
		return "authenticator/googleAuthenticator.html";
	}
	
	/**
	 * 
	 * Description: 生成验证码    
	 * @param request
	 * @return 
	 * date 2019年4月24日
	 */
	@ResponseBody
	@RequestMapping("/genSecret")
	public ResultMessage genSecret(HttpServletRequest request) {
		try {
			//获取当前用户
			SysUser user = CurrentParamUtil.getCurrentUser();
			//获取key
			String secret = GoogleAuthenticator.genSecret(user.getUserName());
			// 把这个qrcode生成二维码，用google身份验证器扫描二维码就能添加成功 
			String qrcode = GoogleAuthenticator.getQRBarcodeURL(user.getUserName(), "testhost", secret);
			System.out.println("qrcode:" + qrcode + ",key:" + secret); 
			HttpSession session = request.getSession();
			session.setAttribute(user.getUserName() + "_secret", secret);
			logger.info("谷歌身份验证生成验证码");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, qrcode);
		}catch(Exception e) {
			logger.error("谷歌身份验证密匙生成异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 密匙验证    
	 * @param key
	 * @param request
	 * @return 
	 * date 2019年4月24日
	 */
	@ResponseBody
	@RequestMapping("/checkSecret")
	public ResultMessage checkSecret(String key, HttpServletRequest request) {
		try {
			Assert.hasText(key, "密匙数据不存在");
			//获取当前用户
			SysUser user = CurrentParamUtil.getCurrentUser();
			HttpSession session =request.getSession();
			String secret  = session.getAttribute(user.getUserName() + "_secret") == null ? "" 
						: session.getAttribute(user.getUserName() + "_secret").toString();
			//验证
			Boolean authcode = GoogleAuthenticator.authcode(key, secret);
			logger.info("谷歌身份验证");
			if(authcode) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getFail();
			}
		}catch(Exception e) {
			logger.error("谷歌身份验证异常", e);
			return ResultMessage.getFail();
		}
	}
	
}
