package com.activity.manager.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.activity.manager.common.Constants;
import com.activity.manager.security.interceptor.DomainInterceptor;
import com.activity.manager.security.interceptor.URLInterceptor;
import com.activity.manager.security.validatecode.filter.ValidateCodeFilter;

/**
 * Description: 静态资源路径访问映射
 * Copyright: Copyright (c) 2019    
 * @author binghe  
 * @date 2019年4月11日   
 * @version 1.0
 */
@SuppressWarnings("deprecation")
@Configuration  
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {  
	
	@Value("${web.upload.path}")
    private String uploadPath;

	@Value("${web.upload.urlvisit.path}")
	private String urlVisitPath;
	
	@Bean
	public HandlerInterceptor getIPInterceptor() {
		return new URLInterceptor();
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        registry.addResourceHandler(urlVisitPath).addResourceLocations(
                Constants.SOURCE_FILE+uploadPath);
    }
	
	@Override 
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(getIPInterceptor()).addPathPatterns("/**")
			.excludePathPatterns("/error/**", "/layui/**"); 
		super.addInterceptors(registry); 
	}
	 
	 
}