package com.activity.manager.rule.entity;

import com.activity.manager.reward.entity.RewardConf;

/**
 * 
 * <p>Description: 宝箱规则类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0
 */
public class BoxRule {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	宝箱等级
     */
    private String level;

    /**
     *	奖品ID
     */
    private Integer rewardId;

    /**
     *	活动ID
     */
    private Integer actId;

    /**
     *	创建人
     */
    private Integer createPerson;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     * 	奖品
     */
    private RewardConf rewardConf;
    
    /**
     *	宝箱图片
     */
    private String boxImg;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level == null ? null : level.trim();
    }

    public Integer getRewardId() {
        return rewardId;
    }

    public void setRewardId(Integer rewardId) {
        this.rewardId = rewardId;
    }

    public Integer getActId() {
        return actId;
    }

    public void setActId(Integer actId) {
        this.actId = actId;
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getBoxImg() {
        return boxImg;
    }

    public void setBoxImg(String boxImg) {
        this.boxImg = boxImg == null ? null : boxImg.trim();
    }

	public RewardConf getRewardConf() {
		return rewardConf;
	}

	public void setRewardConf(RewardConf rewardConf) {
		this.rewardConf = rewardConf;
	}
    
}