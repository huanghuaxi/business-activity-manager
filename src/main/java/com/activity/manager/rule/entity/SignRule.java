package com.activity.manager.rule.entity;

import java.util.List;

import com.activity.manager.reward.entity.RewardConf;

/**
 * 
 * <p>Description: 签到规则类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0
 */

public class SignRule {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	签到天数
     */
    private Integer statisticsTimes;

    /**
     *	获得几次抽奖机会
     */
    private Integer lotteryTimes;

    /**
     *	彩金奖品id
     */
    private Integer moneyRewardId;

    /**
     *	优惠券奖品ID
     */
    private Integer couponRewardId;

    /**
     *	抽奖奖品ID
     */
    private String rewardId;

    /**
     *	统计签到天数类型(0:连续 1:累计 2:每天)
     */
    private Integer signType;

    /**
     *	抽奖活动ID
     */
    private Integer deputyActId;

    /**
     *	活动ID
     */
    private Integer actId;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPerson;
    
    /**
     * 	 奖品翻倍数
     */
    private Integer rewardTimes;
    
    /**
     *	奖品列表
     */
    private List<RewardConf> rewards;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatisticsTimes() {
        return statisticsTimes;
    }

    public void setStatisticsTimes(Integer statisticsTimes) {
        this.statisticsTimes = statisticsTimes;
    }

    public Integer getLotteryTimes() {
        return lotteryTimes;
    }

    public void setLotteryTimes(Integer lotteryTimes) {
        this.lotteryTimes = lotteryTimes;
    }

    public Integer getMoneyRewardId() {
        return moneyRewardId;
    }

    public void setMoneyRewardId(Integer moneyRewardId) {
        this.moneyRewardId = moneyRewardId;
    }

    public Integer getCouponRewardId() {
        return couponRewardId;
    }

    public void setCouponRewardId(Integer couponRewardId) {
        this.couponRewardId = couponRewardId;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId == null ? "" : rewardId;
    }

    public Integer getSignType() {
		return signType;
	}

	public void setSignType(Integer signType) {
		this.signType = signType;
	}

    public Integer getDeputyActId() {
		return deputyActId;
	}

	public void setDeputyActId(Integer deputyActId) {
		this.deputyActId = deputyActId;
	}

	public Integer getActId() {
        return actId;
    }

    public void setActId(Integer actId) {
        this.actId = actId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

	public Integer getRewardTimes() {
		return rewardTimes;
	}

	public void setRewardTimes(Integer rewardTimes) {
		this.rewardTimes = rewardTimes;
	}

	public List<RewardConf> getRewards() {
		return rewards;
	}

	public void setRewards(List<RewardConf> rewards) {
		this.rewards = rewards;
	}
}