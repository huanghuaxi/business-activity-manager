/**
 * 
 */
package com.activity.manager.rule.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;

/**    
 * <p>Description: 大转盘规则控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/turntableRule")
public class TurntableRuleController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${web.upload.path}")
    private String uploadPath;
	
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到大转盘规则设置页面    
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月15日
	 */
	@RequestMapping("list")
	public String list(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到大转盘规则设置页面");
		}catch(Exception e) {
			logger.error("大转盘规则设置页面跳转异常", e);
		}
		return "activitymanage/activity/turntableRule.html";
	}
	
	/**
	 * 
	 * Description: 文件上传    
	 * @param file
	 * @param id
	 * @param request
	 * @return 
	 * date 2019年4月15日
	 */
	@ResponseBody
	@RequestMapping("/upload")
	public ResultMessage upload(@RequestParam("file") MultipartFile file, Integer id, 
			HttpServletRequest request) {
		try {
			if(file.isEmpty()) {
				return ResultMessage.getFail();
			}
			String fileName = file.getOriginalFilename();
			//文件保存地址
			String path = uploadPath;
			path += "activityImg/";
			//新文件名
			String newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf("."));
			File dest = new File(path + newFileName);
			//判断文件父目录是否存在
			if(!dest.getParentFile().exists()){ 
				dest.getParentFile().mkdirs(); 
			}
			file.transferTo(dest);
			//文件访问地址
			String url = "http://" + request.getServerName() + ":" 
					+ request.getServerPort()  + "/uploads/activityImg/" + newFileName;
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("imgUrl", url);
			map.put("id", id);
			logger.info("上传活动背景图片");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, map);
		}catch(Exception e) {
			logger.error("文件上传异常", e);
			return ResultMessage.getFail();
		}
	}
	
}
