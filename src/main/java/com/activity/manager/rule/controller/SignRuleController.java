/**
 * 
 */
package com.activity.manager.rule.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.rule.service.SignRuleService;

/**    
 * <p>Description: 签到规则控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/signRule")
public class SignRuleController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SignRuleService signRuleService;
	
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到签到规则设置页面   
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月16日
	 */
	@RequestMapping("/list")
	public String list(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到签到规则设置页面");
		}catch(Exception e) {
			logger.error("签到规则设置页面跳转异常", e);
		}
		return "activitymanage/activity/signRule.html";
	}
	
	/**
	 * 
	 * Description: 跳转到新增编辑签到规则页面    
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月16日
	 */
	@RequestMapping("/toSaveOrEdit")
	public String toSaveOrEdit(Integer actId, Integer id, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			model.addAttribute("id", id);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到新增编辑签到规则页面");
		}catch(Exception e) {
			logger.error("签到规则设置页面跳转异常", e);
		}
		return "activitymanage/activity/editSignRule.html";
	}
	
	/**
	 * 
	 * Description: 获取签到规则列表    
	 * @param pageUtil
	 * @param signRule
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, SignRule signRule) {
		try {
			Assert.notNull(signRule.getActId(), "活动ID数据不存在");
			List<SignRule> sList = signRuleService.querySignRule(signRule, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> sLst = formate(sList);
			Integer count = signRuleService.signRuleCount(signRule);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(sLst);
			logger.info("获取签到规则列表");
		}catch(Exception e) {
			logger.error("签到规则列表获取异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增签到规则   
	 * @param signRule
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(SignRule signRule) {
		try {
			String msg = signRuleService.saveSignRule(signRule);
			logger.info("新增签到规则");
			if(msg != null && !"".equals(msg)) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增签到规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑签到规则    
	 * @param signRule
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(SignRule signRule) {
		try {
			String msg = signRuleService.editSignRule(signRule);
			logger.info("编辑签到规则");
			if(msg != null && !"".equals(msg)) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑签到规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除签到规则    
	 * @param ids
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delete(String ids) {
		try {
			signRuleService.delSignRule(Arrays.asList(ids.trim().split(",")));
			logger.info("删除签到规则");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("删除签到规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 根据ID获取签到规则   
	 * @param id
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/getSignRuleById")
	public ResultMessage getSignRuleById(Integer id) {
		try {
			Assert.notNull(id, "签到规则ID数据不存在");
			SignRule signRule = signRuleService.getSignRuleById(id);
			logger.info("根据ID获取签到规则");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, signRule);
		}catch(Exception e) {
			logger.error("删除签到规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 新增奖品并添加到签到规则    
	 * @param reward
	 * @param ruleId
	 * @return 
	 * date 2019年5月7日
	 */
	@ResponseBody
	@RequestMapping("/saveReward")
	public ResultMessage saveReward(RewardConf reward, Integer ruleId) {
		try {
			Integer rewardId = signRuleService.saveReward(reward, ruleId);
			logger.info("新增奖品并添加到签到规则");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rewardId);
		}catch(Exception e) {
			logger.error("新增奖品并添加到签到规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除奖品并修改规则   
	 * @param ids
	 * @param ruleId
	 * @return 
	 * date 2019年5月7日
	 */
	@ResponseBody
	@RequestMapping("/deleteReward")
	public ResultMessage deleteReward(String ids, Integer ruleId) {
		try {
			Assert.hasText(ids, "奖品ID数据不存在");
			String rewardId = signRuleService.deleteReward(Arrays.asList(ids.trim().split(",")), ruleId);
			logger.info("删除奖品并修改规则");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rewardId);
		}catch(Exception e) {
			logger.error("删除奖品并修改规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<SignRule> list){
		List<Object> sLst = new ArrayList<Object>();
		list.forEach(signRule -> {
			sLst.add(signRule);
		});
		return sLst;
	}
	
}
