/**
 * 
 */
package com.activity.manager.rule.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.rule.entity.BoxRule;
import com.activity.manager.rule.service.BoxRuleService;

/**    
 * <p>Description: 宝箱规则控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/boxRule")
public class BoxRuleController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BoxRuleService boxRuleService;
	
	@Value("${web.upload.path}")
    private String uploadPath;
	
	/**
	 * 
	 * Description: 跳转到宝箱规则配置页面    
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月15日
	 */
	@RequestMapping("/list")
	public String list(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			logger.info("跳转到宝箱规则配置页面");
		}catch(Exception e) {
			logger.error("宝箱规则设置页面跳转异常", e);
		}
		return "activitymanage/activity/boxRule.html";
	}
	
	/**
	 * 
	 * Description: 获取所有宝箱规则    
	 * @param pageUtil
	 * @param boxRule
	 * @return 
	 * date 2019年4月15日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, BoxRule boxRule) {
		try {
			Assert.notNull(boxRule.getActId(), "活动ID数据不存在");
			List<BoxRule> bList = boxRuleService.queryBoxRule(boxRule, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> bLst = formate(bList);
			Integer count = boxRuleService.boxRuleCount(boxRule);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(bLst);
			logger.info("获取宝箱规则列表");
		}catch(Exception e) {
			logger.error("宝箱规则列表获取异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增宝箱规则    
	 * @param boxRule
	 * @return 
	 * date 2019年4月15日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(BoxRule boxRule) {
		try {
			boxRuleService.saveBoxRule(boxRule);
			logger.info("新增宝箱规则");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增宝箱规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 修改宝箱规则    
	 * @param boxRule
	 * @return 
	 * date 2019年4月15日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(BoxRule boxRule) {
		try {
			boxRuleService.editBoxRule(boxRule);
			logger.info("修改宝箱规则");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("修改宝箱规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除宝箱规则    
	 * @param ids
	 * @return 
	 * date 2019年4月15日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delete(String ids) {
		try {
			boxRuleService.delBoxRule(Arrays.asList(ids.trim().split(",")));
			logger.info("删除宝箱规则");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增宝箱规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 文件上传    
	 * @param file
	 * @param id
	 * @param request
	 * @return 
	 * date 2019年4月15日
	 */
	@ResponseBody
	@RequestMapping("/upload")
	public ResultMessage upload(@RequestParam("file") MultipartFile file, Integer id, 
			HttpServletRequest request) {
		try {
			if(file.isEmpty()) {
				return ResultMessage.getFail();
			}
			String fileName = file.getOriginalFilename();
			//文件保存地址
			String path = uploadPath;
			path += "boxImg/";
			//新文件名
			String newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf("."));
			File dest = new File(path + newFileName);
			//判断文件父目录是否存在
			if(!dest.getParentFile().exists()){ 
				dest.getParentFile().mkdirs(); 
			}
			file.transferTo(dest);
			//文件访问地址
			String url = "http://" + request.getServerName() + ":" 
					+ request.getServerPort()  + "/uploads/boxImg/" + newFileName;
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("imgUrl", url);
			map.put("id", id);
			logger.info("上传宝箱图片");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, map);
		}catch(Exception e) {
			logger.error("文件上传异常", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<BoxRule> list){
		List<Object> bLst = new ArrayList<Object>();
		list.forEach(boxRule -> {
			bLst.add(boxRule);
		});
		return bLst;
	}
	
}
