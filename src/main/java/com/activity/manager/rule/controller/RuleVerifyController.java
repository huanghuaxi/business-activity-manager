/**
 * 
 */
package com.activity.manager.rule.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.base.entity.Item;
import com.activity.manager.base.service.ItemService;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.coupon.entity.Coupon;
import com.activity.manager.coupon.entity.CouponApply;
import com.activity.manager.coupon.service.CouponApplyService;
import com.activity.manager.coupon.service.CouponSerivce;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.SignRecord;
import com.activity.manager.member.entity.SupplementSignApply;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.entity.RewardRecord;
import com.activity.manager.rule.entity.BoxRule;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.rule.service.RuleVerifyService;
import com.activity.manager.security.entity.ActivityCategory;
import com.activity.manager.security.entity.SiteDomainKv;

/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/ruleVerify")
public class RuleVerifyController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RuleVerifyService ruleVerifyService;
	
	@Autowired
	private BusiActivityService activityService;
	
	@Autowired
	private CouponSerivce couponService;
	
	@Autowired
	private CouponApplyService couponApplyService;

	@Autowired
	private ItemService itemService;
	
	
	/**
	 * 
	 * Description: 跳转到活动页面   
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月15日
	 */
	@RequestMapping("/toActivityPage")
	public String toActivityPage(Integer actId, Integer mainActId, Integer inviter, 
			Model model, HttpServletRequest request) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			BusiActivity activity = activityService.getActivityById(actId);
			Assert.notNull(activity, "活动数据不存在");
			Assert.notNull(activity.getVersion(), "活动版本数据不存在");
			Assert.hasText(activity.getVersion().getVerUrl(), "活动页面地址数据不存");
			String url = activity.getVersion().getVerUrl();
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member != null) {
				Integer actId2 = (Integer) session.getAttribute("actId");
				if(actId != actId2) {
					if(mainActId == null) {
						session.removeAttribute("member");
						session.removeAttribute("actId");
					}else {
						model.addAttribute("memberId", member.getId());
					}
				}else {
					model.addAttribute("memberId", member.getId());
				}
			}
			model.addAttribute("activity", activity);
			model.addAttribute("mainActId", mainActId);
			model.addAttribute("inviter", inviter);
			return url;
		}catch(Exception e) {
			logger.error("活动页面跳转异常", e);
		}
		return "error";
	}
	
	/**
	 * 
	 * Description: 跳转到活动演示页
	 * @return 
	 * date 2019年6月3日
	 */
	@RequestMapping("/toPortal")
	public String toPortal() {
		return "activityPage/portal/portal.html";
	}
	
	/**
	 * 
	 * Description: 会员登录    
	 * @param memberName
	 * @param request
	 * @return 
	 * date 2019年4月18日
	 */
	@ResponseBody
	@RequestMapping("/memberLogin")
	public ResultMessage memberLogin(Integer actId, String memberName, Integer inviter, 
			HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = ruleVerifyService.memberLogin(memberName, actId, inviter);
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请输入会员账号，非会员可输入手机号参与", null);
			}
			MemberActivity memberAct = ruleVerifyService.getMemberAct(actId, member.getId());
			logger.error("会员登录");
			if(memberAct == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您不是该活动的会员，不能参加此活动", member);
			}
			session.setAttribute("member", member);
			session.setAttribute("actId", actId);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, member);
		}catch(Exception e) {
			logger.error("会员登录失败", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取活动会员信息   
	 * @param memberId
	 * @param actId
	 * @return 
	 * date 2019年4月19日
	 */
	@ResponseBody
	@RequestMapping("/getMemberAct")
	public ResultMessage getMemberAct(Integer memberId, Integer actId) {
		try {
			MemberActivity memberAct = ruleVerifyService.getMemberAct(actId, memberId);
			logger.info("获取活动会员");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, memberAct);
		}catch(Exception e) {
			logger.error("获取活动会员异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取会员中奖记录   
	 * @param actId
	 * @param memberId
	 * @param centralActId
	 * @return 
	 * date 2019年4月19日
	 */
	@ResponseBody
	@RequestMapping("/getRewardRecord")
	public ResultMessage getRewardRecord(Integer actId, Integer memberId, Integer centralActId, Integer pageNo, Integer pageSize) {
		try {
			List<RewardRecord> rewardRecord = ruleVerifyService.getRewardRecordByMemberId(actId, memberId, centralActId, pageNo, pageSize);
			logger.info("获取会员中奖记录");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rewardRecord);
		}catch (Exception e) {
			logger.error("获取会员中奖记录异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取该活动中奖记录   
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年4月19日
	 */
	@ResponseBody
	@RequestMapping("/getRewardRecordByActId")
	public ResultMessage getRewardRecordByActId(Integer actId) {
		try {
			List<RewardRecord> rewardRecord = ruleVerifyService.getRewardRecordByActId(actId);
			logger.info("获取活动中奖记录");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rewardRecord);
		}catch (Exception e) {
			logger.error("获取会员中奖记录异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取奖品列表    
	 * @param actId
	 * @return 
	 * date 2019年5月5日
	 */
	@ResponseBody
	@RequestMapping("/getRewardList")
	public ResultMessage getRewardList(Integer actId) {
		try {
			List<RewardConf> rewardList = ruleVerifyService.getRewardList(actId);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rewardList);
		}catch(Exception e) {
			e.printStackTrace();
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 宝箱规则验证   
	 * @param actId
	 * @param centralActId
	 * @param request
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/boxRuleVerify")
	public ResultMessage boxRuleVerify(Integer actId, Integer centralActId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			logger.info("宝箱抽奖");
			return ruleVerifyService.BoxRuleVerify(actId, member, centralActId);
		}catch(Exception e) {
			logger.error("宝箱抽奖异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取等级宝箱    
	 * @param actId
	 * @param level
	 * @return 
	 * date 2019年4月26日
	 */
	@ResponseBody
	@RequestMapping("/getBox")
	public ResultMessage getBox(Integer actId, String level) {
		try {
			BoxRule boxRule = ruleVerifyService.getBox(actId, level);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, boxRule);
		}catch(Exception e) {
			logger.error("获取等级宝箱异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 砸金蛋规则验证    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月5日
	 */
	@ResponseBody
	@RequestMapping("/eggRuleVerify")
	public ResultMessage eggRuleVerify(Integer actId, Integer centralActId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			logger.info("砸金蛋");
			return ruleVerifyService.EggRuleVerify(actId, member, centralActId);
		}catch(Exception e) {
			logger.error("砸金蛋异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 红包规则验证    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月8日
	 */
	@ResponseBody
	@RequestMapping("/redEnvelopeRuleVerify")
	public ResultMessage redEnvelopeRuleVerify(Integer actId, Integer centralActId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			logger.info("拆红包");
			return ruleVerifyService.redEnvelopeVerify(actId, member, centralActId);
		}catch(Exception e) {
			logger.error("拆红包异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 翻牌规则验证    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月8日
	 */
	@ResponseBody
	@RequestMapping("/cardRuleVerify")
	public ResultMessage cardRuleVerify(Integer actId, Integer centralActId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			logger.info("翻牌");
			return ruleVerifyService.cardVerify(actId, member, centralActId);
		}catch(Exception e) {
			logger.error("翻牌异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 转盘规则验证    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月9日
	 */
	@ResponseBody
	@RequestMapping("/turntableRuleVerify")
	public ResultMessage turntableRuleVerify(Integer actId, Integer centralActId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			logger.info("转盘");
			return ruleVerifyService.turntableVerify(actId, member, centralActId);
		}catch(Exception e) {
			logger.error("转盘异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 签到规则验证    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月16日
	 */
	@ResponseBody
	@RequestMapping("/signRuleVerify")
	public ResultMessage signRuleVerify(Integer actId, Integer centralActId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			logger.info("签到");
			return ruleVerifyService.signVerify(actId, member, centralActId);
		}catch(Exception e) {
			logger.error("签到异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 博饼规则验证 
	 * @param actId
	 * @param centralActId
	 * @param request
	 * @return 
	 * date 2019年8月15日
	 */
	@ResponseBody
	@RequestMapping("/bobingRuleVerify")
	public ResultMessage bobingRuleVerify(Integer actId, Integer centralActId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
//			Member member = ruleVerifyService.memberLogin("test", 67, null);
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			logger.info("博饼");
			return ruleVerifyService.bobingVerify(actId, member, centralActId);
		}catch(Exception e) {
			logger.error("博饼异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取站点域名绑定配置    
	 * @param keyEnName
	 * @return 
	 * date 2019年5月13日
	 */
	@ResponseBody
	@RequestMapping("/getDomainKv")
	public ResultMessage getDomainKv(String domainEnName) {
		try {
			List<SiteDomainKv> domainKvList = ruleVerifyService.getDomainKv(domainEnName);
			logger.info("获取站点域名绑定配置");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, domainKvList);
		}catch(Exception e) {
			logger.error("获取站点域名绑定配置失败", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取签到记录    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月16日
	 */
	@ResponseBody
	@RequestMapping("/getSignRecord")
	public ResultMessage getSignRecord(Integer actId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			List<SignRecord> signRecord = ruleVerifyService.getSignRecord(actId, member);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("signRecord", signRecord);
			Integer countSign = ruleVerifyService.getCountSign(actId, member);
			map.put("countSign", countSign);
			logger.info("获取签到记录");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, map);
		}catch(Exception e) {
			logger.error("获取签到记录异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取签到规则    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月17日
	 */
	@ResponseBody
	@RequestMapping("/getSignRule")
	public ResultMessage getSignRule(Integer actId) {
		try {
			List<SignRule> signRules = ruleVerifyService.getSignRecord(actId);
			logger.info("获取签到规则");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, signRules);
		}catch(Exception e) {
			logger.error("获取签到规则异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 补签申请    
	 * @param actId
	 * @param dates
	 * @param request
	 * @return 
	 * date 2019年5月17日
	 */
	@ResponseBody
	@RequestMapping("/supplementSignApply")
	public ResultMessage supplementSignApply(Integer actId, String dates, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			String msg = ruleVerifyService.supplementSignApply(dates, member, actId);
			if(msg != null && !"".equals(msg)) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("补签申请异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取补签记录    
	 * @param actId
	 * @param request
	 * @return 
	 * date 2019年5月17日
	 */
	@ResponseBody
	@RequestMapping("/getSupplementSignApply")
	public ResultMessage getSupplementSignApply(Integer actId, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Member member = (Member) session.getAttribute("member");
			if(member == null) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "请先登录", null);
			}
			List<SupplementSignApply> applyList = ruleVerifyService.getSupplementSignApply(member, actId);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, applyList);
		}catch(Exception e) {
			logger.error("补签申请异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 会员退出    
	 * @param actId
	 * @param mainActId
	 * @param model
	 * @param request
	 * @return 
	 * date 2019年5月22日
	 */
	@RequestMapping("/logoutMember")
	public ModelAndView logoutMember(Integer actId, Integer mainActId, Integer inviter, Model model, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			session.removeAttribute("member");
			Assert.notNull(actId, "活动ID数据不存在");
			BusiActivity activity = activityService.getActivityById(actId);
			Assert.notNull(activity, "活动数据不存在");
			Assert.notNull(activity.getVersion(), "活动版本数据不存在");
			Assert.hasText(activity.getVersion().getVerUrl(), "活动页面地址数据不存");
			String url = "/ruleVerify/toActivityPage";
			model.addAttribute("activity", activity);
			model.addAttribute("mainActId", mainActId);
			url += "?actId=" + actId;
			url += mainActId == null ? "" : "&mainActId=" + mainActId;
			url += inviter == null ? "" : "&inviter=" + inviter;
			return new ModelAndView("redirect:" + url);
		}catch(Exception e) {
			logger.error("活动页面跳转异常", e);
		}
		return new ModelAndView("error");
	}
	
	/**
	 * 
	 * Description: 获取活动类型
	 * @return 
	 * date 2019年6月3日
	 */
	@ResponseBody
	@RequestMapping("getActivityCategory")
	public ResultMessage getActivityCategory() {
		try {
			List<ActivityCategory> categorys = ruleVerifyService.selectByActivityShow();
			logger.info("获取活动类型");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, categorys);
		}catch(Exception e) {
			logger.error("获取活动类型异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取活动列表
	 * @param activity
	 * @param pageNo
	 * @param pageSize
	 * @return 
	 * date 2019年6月3日
	 */
	@ResponseBody
	@RequestMapping("getActivityList")
	public ResultMessage getActivityList(BusiActivity activity, Integer pageNo, Integer pageSize) {
		try {
			Assert.notNull(activity, "活动数据不存在");
			Assert.notNull(pageNo, "当前页码数据不存在");
			Assert.notNull(pageSize, "页数数据不存在");
			List<BusiActivity> activityList = ruleVerifyService.getActivityByActId(activity, (pageNo - 1) * pageSize, pageSize);
			logger.info("获取活动列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, activityList);
		}catch(Exception e) {
			logger.error("获取活动列表异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取最新活动列表   
	 * @return 
	 * date 2019年6月25日
	 */
	@ResponseBody
	@RequestMapping("/getNewActivity")
	public ResultMessage getNewActivity() {
		try {
			List<BusiActivity> list = ruleVerifyService.getNewActivity();
			logger.info("获取最新活动");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, list);
		}catch(Exception e) {
			logger.error("获取最新活动列表异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取有效优惠券    
	 * @return 
	 * date 2019年7月19日
	 */
	@ResponseBody
	@RequestMapping("/getCouponList")
	public ResultMessage getCouponList() {
		try {
			List<Coupon> couponList = couponService.selectByUse();
			logger.info("获取优惠券");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, couponList);
		}catch (Exception e) {
			logger.error("获取优惠券异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 优惠券申请    
	 * @param couponApply
	 * @return 
	 * date 2019年7月22日
	 */
	@ResponseBody
	@RequestMapping("/couponApply")
	public ResultMessage couponApply(CouponApply couponApply) {
		try {
			String msg = couponApplyService.addApply(couponApply);
			logger.info("优惠券申请");
			if(msg == null) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("优惠券申请异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 获取优惠券    
	 * @param couponApply
	 * @return 
	 * date 2019年7月22日
	 */
	@ResponseBody
	@RequestMapping("/searchCoupon")
	public ResultMessage searchCoupon(CouponApply couponApply) {
		try {
			List<CouponApply> list = couponApplyService.selectByName(couponApply);
			logger.info("查询优惠券");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, list);
		}catch (Exception e) {
			logger.error("查询优惠券异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 根据优惠ID获取通过申请 
	 * @param couponId
	 * @return 
	 * date 2019年8月8日
	 */
	@ResponseBody
	@RequestMapping("/getCouponPass")
	public ResultMessage getCouponPass(Integer couponId) {
		try {
			List<String> names = couponApplyService.selectPassByCouponId(couponId);
			logger.info("根据优惠ID获取通过申请");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, names);
		}catch (Exception e) {
			logger.error("根据优惠ID获取通过申请异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 根据名称查询键值 
	 * @param itemName
	 * @return 
	 * date 2019年8月7日
	 */
	@ResponseBody
	@RequestMapping("/getItemByName")
	public ResultMessage getItemByName(String itemName) {
		try {
			logger.info("根据名称查询键值");
			Item item = itemService.selectByName(itemName);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, item);
		}catch (Exception e) {
			logger.error("根据名称查询键值异常", e);
		}
		return ResultMessage.getFail();
	}
	
}
