package com.activity.manager.rule.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.rule.entity.BoxRule;

/**
 * 
 * <p>Description: 加载宝箱规则类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0
 */
@Mapper
public interface BoxRuleMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(BoxRule record);

    BoxRule selectByPrimaryKey(Integer id);

    List<BoxRule> selectAll();

    int updateByPrimaryKey(BoxRule record);
    
    /**
     * 
     * Description: 获取所有宝箱规则       
     * @param boxRule
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月15日
     */
    List<BoxRule> queryBoxRule(BoxRule boxRule, int start ,int pageSize);
    
    /**
     * 
     * Description: 统计所有宝箱规则   
     * @param boxRule
     * @return 
     * date 2019年4月15日
     */
    Integer boxRuleCount(BoxRule boxRule);
    
    /**
     * 
     * Description: 删除宝箱规则   
     * @param ids 
     * date 2019年4月15日
     */
    void delBoxRule(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 根据等级获取宝箱规则   
     * @param level
     * @return 
     * date 2019年4月17日
     */
    BoxRule selectByLevel(Integer actId, String level);
    
}