package com.activity.manager.rule.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.rule.entity.SignRule;

/**
 * 
 * <p>Description: 加载签到规则类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0
 */

@Mapper
public interface SignRuleMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(SignRule record);

    SignRule selectByPrimaryKey(Integer id);

    List<SignRule> selectAll();

    int updateByPrimaryKey(SignRule record);
    
    /**
     * 
     * Description: 查询所有签到规则
     * @param signRule
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月16日
     */
    List<SignRule> querySignRule(SignRule signRule, int start ,int pageSize);
    
    /**
     * 
     * Description: 统计所有签到规则    
     * @param signRule
     * @return 
     * date 2019年4月16日
     */
    Integer signRuleCount(SignRule signRule);
    
    /**
     * 
     * Description: 删除签到规则  
     * @param ids 
     * date 2019年4月16日
     */
    void delSignRule(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 根据活动ID获取签到规则    
     * @param actId
     * @return 
     * date 2019年5月16日
     */
    List<SignRule> selectByActId(Integer actId);
    
    
    /**
     * 
     * Description: 根据签到类型获取规则    
     * @param actId
     * @param signType
     * @param statisticsTimes
     * @return 
     * date 2019年5月17日
     */
    SignRule selectByType(Integer actId, Integer signType, Integer statisticsTimes);
    
    /**
     * 
     * Description: 根据配套活动查询    
     * @param deputyActId
     * @return 
     * date 2019年5月28日
     */
    List<SignRule> selectByDeputyActId(Integer deputyActId);
}