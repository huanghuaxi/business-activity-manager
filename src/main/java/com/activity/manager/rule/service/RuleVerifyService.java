/**
 * 
 */
package com.activity.manager.rule.service;

import java.util.List;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.SignRecord;
import com.activity.manager.member.entity.SupplementSignApply;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.entity.RewardRecord;
import com.activity.manager.rule.entity.BoxRule;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.security.entity.ActivityCategory;
import com.activity.manager.security.entity.SiteDomainKv;

/**    
 * <p>Description: 加载规则验证类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */
public interface RuleVerifyService {
	
	/**
	 * 
	 * Description: 会员登录    
	 * @param memberLogin 
	 * date 2019年4月18日
	 */
	public Member memberLogin(String memberName, Integer actId, Integer inviter);
	
	/**
	 * 
	 * Description: 获取活动会员   
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年4月19日
	 */
	public MemberActivity getMemberAct(Integer actId, Integer memberId);
	
	/**
	 * 
	 * Description: 获取会员中奖纪录   
	 * @param actId
	 * @param memberId
	 * @param centralActId
	 * @return 
	 * date 2019年4月19日
	 */
	public List<RewardRecord> getRewardRecordByMemberId(Integer actId, Integer memberId, Integer centralActId, Integer pageNo, Integer pageSize);
	
	/**
	 * 
	 * Description: 获取该活动中奖记录   
	 * @param actId
	 * @return 
	 * date 2019年4月19日
	 */
	public List<RewardRecord> getRewardRecordByActId(Integer actId);
	
	/**
	 * 
	 * Description: 获取奖品列表    
	 * @param actId
	 * @return 
	 * date 2019年5月5日
	 */
	public List<RewardConf> getRewardList(Integer actId);
	
	/**
	 * 
	 * Description: 获取展示活动类型
	 * @return 
	 * date 2019年6月3日
	 */
	public List<ActivityCategory> selectByActivityShow();
	
	/**
	 * 
	 * Description: 宝箱规则验证    
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年4月18日
	 */
	public ResultMessage BoxRuleVerify(Integer actId, Member member, Integer centralActId);
	
	/**
	 * 
	 * Description: 获取宝箱    
	 * @param actId
	 * @param level
	 * @return 
	 * date 2019年4月26日
	 */
	public BoxRule getBox(Integer actId, String level);
	
	/**
	 * 
	 * Description: 砸金蛋规则验证    
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年5月5日
	 */
	public ResultMessage EggRuleVerify(Integer actId, Member member, Integer centralActId);
	
	/**
	 * 
	 * Description: 红包规则验证    
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年5月8日
	 */
	public ResultMessage redEnvelopeVerify(Integer actId, Member member, Integer centralActId);
	
	/**
	 * 
	 * Description: 翻牌规则验证    
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年5月8日
	 */
	public ResultMessage cardVerify(Integer actId, Member member, Integer centralActId);
	
	/**
	 * 
	 * Description: 转盘规则验证    
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年5月9日
	 */
	public ResultMessage turntableVerify(Integer actId, Member member, Integer centralActId);
	
	/**
	 * 
	 * Description: 签到规则验证    
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年5月16日
	 */
	public ResultMessage signVerify(Integer actId, Member member, Integer centralActId);
	
	/**
	 * 
	 * Description: 博饼规则验证 
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年8月15日
	 */
	public ResultMessage bobingVerify(Integer actId, Member member, Integer centralActId);
	
	/**
	 * 
	 * Description: 获取站点域名绑定    
	 * @param keyEnName
	 * @return 
	 * date 2019年5月13日
	 */
	public List<SiteDomainKv> getDomainKv(String domainEnName);
	
	/**
	 * 
	 * Description: 获取签到纪录    
	 * @param actId
	 * @param member
	 * @return 
	 * date 2019年5月16日
	 */
	public List<SignRecord> getSignRecord(Integer actId, Member member);
	
	/**
	 * 
	 * Description: 获取累计签到    
	 * @param actId
	 * @param member
	 * @return 
	 * date 2019年5月16日
	 */
	public Integer getCountSign(Integer actId, Member member);
	
	/**
	 * 
	 * Description: 获取签到规则   
	 * @param actId
	 * @return 
	 * date 2019年5月17日
	 */
	public List<SignRule> getSignRecord(Integer actId);
	
	/**
	 * 
	 * Description: 补签申请    
	 * @param dates
	 * @param member
	 * @param actId 
	 * date 2019年5月17日
	 */
	public String supplementSignApply(String dates, Member member, Integer actId);
	
	/**
	 * 
	 * Description: 获取补签申请    
	 * @param member
	 * @param actId
	 * @return 
	 * date 2019年5月17日
	 */
	public List<SupplementSignApply> getSupplementSignApply(Member member, Integer actId);
	
	/**
	 * 
	 * Description: 根据活动类型获取活动   
	 * @param activity
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年6月3日
	 */
	public List<BusiActivity> getActivityByActId(BusiActivity activity, int start, int pageSize);
	
	/**
	 * 
	 * Description: 获取最新活动    
	 * @return 
	 * date 2019年6月25日
	 */
	public List<BusiActivity> getNewActivity();
}
