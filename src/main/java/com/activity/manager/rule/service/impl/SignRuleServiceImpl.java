/**
 * 
 */
package com.activity.manager.rule.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.reward.service.RewardConfService;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.rule.mapper.SignRuleMapper;
import com.activity.manager.rule.service.SignRuleService;
import com.activity.manager.security.entity.SysUser;

/**    
 * <p>Description: 签到规则更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */

@Service
public class SignRuleServiceImpl implements SignRuleService{

	@Autowired
	private SignRuleMapper signRuleMapper;
	@Autowired
	private RewardConfMapper rewardConfMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: querySignRule</p>   
	 * <p>Description: 查询所有签到规则</p>   
	 * @param signRule
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.rule.service.SignRuleService#querySignRule(com.activity.manager.rule.entity.SignRule, int, int)   
	 */
	@Override
	public List<SignRule> querySignRule(SignRule signRule, int start, int pageSize) {
		return signRuleMapper.querySignRule(signRule, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: signRuleCount</p>   
	 * <p>Description: 统计所有签到规则</p>   
	 * @param signRule
	 * @return   
	 * @see com.activity.manager.rule.service.SignRuleService#signRuleCount(com.activity.manager.rule.entity.SignRule)   
	 */
	@Override
	public Integer signRuleCount(SignRule signRule) {
		return signRuleMapper.signRuleCount(signRule);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveSignRule</p>   
	 * <p>Description: 新增签到规则</p>   
	 * @param signRule   
	 * @see com.activity.manager.rule.service.SignRuleService#saveSignRule(com.activity.manager.rule.entity.SignRule)   
	 */
	@Override
	public String saveSignRule(SignRule signRule) {
		Assert.notNull(signRule.getActId(), "活动ID数据不存在");
		Assert.notNull(signRule.getSignType(), "统计签到天数类型数据不存在");
		Assert.notNull(signRule.getRewardTimes(), "奖品翻倍数数据不存在");
		if(signRule.getSignType() == 0) {
			//每日签到
			signRule.setStatisticsTimes(1);
		}
		//查询规则是否重复
		SignRule rule = signRuleMapper.selectByType(signRule.getActId(), signRule.getSignType(), signRule.getStatisticsTimes());
		if(rule != null) {
			return "失败，该规则已存在不能重复配置";
		}
		signRule.setLotteryTimes(signRule.getLotteryTimes() == null ? 0 : signRule.getLotteryTimes());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SysUser user = CurrentParamUtil.getCurrentUser();
		signRule.setCreatePerson(user.getId());
		signRule.setCreateTime(format.format(new Date()));
		signRuleMapper.insert(signRule);
		return "";
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: editSignRule</p>   
	 * <p>Description: 编辑签到规则</p>   
	 * @param signRule   
	 * @see com.activity.manager.rule.service.SignRuleService#editSignRule(com.activity.manager.rule.entity.SignRule)   
	 */
	@Override
	public String editSignRule(SignRule signRule) {
		Assert.notNull(signRule.getId(), "签到规则ID数据不存在");
		Assert.notNull(signRule.getActId(), "活动ID数据不存在");
		Assert.notNull(signRule.getSignType(), "统计签到天数类型数据不存在");
		Assert.notNull(signRule.getRewardTimes(), "奖品翻倍数数据不存在");
		if(signRule.getSignType() == 0) {
			//每日签到
			signRule.setStatisticsTimes(1);
		}
		SignRule rule = signRuleMapper.selectByPrimaryKey(signRule.getId());
		SignRule rule2 = signRuleMapper.selectByType(signRule.getActId(), signRule.getSignType(), signRule.getStatisticsTimes());
		if(rule2 != null && rule.getId() != rule2.getId()) {
			return "失败，该规则已存在不能重复配置";
		}
		rule.setSignType(signRule.getSignType());
		rule.setDeputyActId(signRule.getDeputyActId());
		rule.setStatisticsTimes(signRule.getStatisticsTimes());
		rule.setLotteryTimes(signRule.getLotteryTimes() == null ? 0 : signRule.getLotteryTimes());
		rule.setMoneyRewardId(signRule.getMoneyRewardId());
		rule.setCouponRewardId(signRule.getCouponRewardId());
		rule.setRewardTimes(signRule.getRewardTimes());
		signRuleMapper.updateByPrimaryKey(rule);
		return "";
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: delSignRule</p>   
	 * <p>Description: 删除签到规则</p>   
	 * @param ids   
	 * @see com.activity.manager.rule.service.SignRuleService#delSignRule(java.util.List)   
	 */
	@Override
	public void delSignRule(List<String> ids) {
		signRuleMapper.delSignRule(ids);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: getSignRuleById</p>   
	 * <p>Description: 根据ID获取签到规则</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.rule.service.SignRuleService#getSignRuleById(java.lang.Integer)   
	 */
	@Override
	public SignRule getSignRuleById(Integer id) {
		return signRuleMapper.selectByPrimaryKey(id);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveReward</p>   
	 * <p>Description: 添加奖品</p>   
	 * @param reward
	 * @param ruleId
	 * @return   
	 * @see com.activity.manager.rule.service.SignRuleService#saveReward(com.activity.manager.reward.entity.RewardConf, java.lang.Integer)
	 */
	@Override
	@Transactional
	public Integer saveReward(RewardConf reward, Integer ruleId) {
		Assert.notNull(ruleId, "签到规则ID数据不存在");
		Assert.hasText(reward.getRewardOption(), "奖项数据不存在");
		Assert.hasText(reward.getRewardCon(), "奖品数据不存在");
		Assert.notNull(reward.getRatio(), "中奖概率数据不存在");
		Assert.notNull(reward.getRewardNum(), "奖品数量数据不存在");
		SignRule signRule = signRuleMapper.selectByPrimaryKey(ruleId);
		Assert.notNull(signRule, "签到规则数据不存在");
		reward.setWinNum(0);
		reward.setCreateTime(new Date());
		SysUser user = CurrentParamUtil.getCurrentUser();
		reward.setCreatePerson(user.getId());
		rewardConfMapper.insert(reward);
		//查询奖品ID
		Integer rewardId = rewardConfMapper.selectId(reward.getActId(), reward.getRewardOption());
		Assert.notNull(rewardId, "奖品ID数据不存在");
		String rewardIds = signRule.getRewardId();
		if(rewardIds == null || "".equals(rewardIds)) {
			rewardIds = rewardId + "";
		}else {
			rewardIds += "," + rewardId;
		}
		signRule.setRewardId(rewardIds);
		signRuleMapper.updateByPrimaryKey(signRule);
		return rewardId;
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: deleteReward</p>   
	 * <p>Description: 删除奖品</p>   
	 * @param ids
	 * @param ruleId   
	 * @see com.activity.manager.rule.service.SignRuleService#deleteReward(java.util.List, java.lang.Integer)
	 */
	@Override
	@Transactional
	public String deleteReward(List<String> ids, Integer ruleId) {
		Assert.notNull(ruleId, "签到规则ID数据不存在");
		SignRule signRule = signRuleMapper.selectByPrimaryKey(ruleId);
		Assert.notNull(signRule, "签到规则数据不存在");
		rewardConfMapper.delRewardConf(ids);
		List<String> rewardIds = signRule.getRewardId() == null ? new ArrayList<String>() 
								: new ArrayList<String>(Arrays.asList(signRule.getRewardId().split(",")));
		if(ids != null && ids.size() > 0) {
			for(String id : ids) {
				for(int i = 0;i < rewardIds.size();i++) {
					if(id.equals(rewardIds.get(i))) {
						rewardIds.remove(i);
						break;
					}
				}
			}
		}
		String rewardId = null;
		for(int i = 0;i < rewardIds.size();i++) {
			if(rewardId == null) {
				rewardId = rewardIds.get(i);
			}else {
				rewardId += "," + rewardIds.get(i);
			}
		}
		signRule.setRewardId(rewardId);
		signRuleMapper.updateByPrimaryKey(signRule);
		return rewardId;
	}
	
}
