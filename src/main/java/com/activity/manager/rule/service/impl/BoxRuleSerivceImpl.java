/**
 * 
 */
package com.activity.manager.rule.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.rule.entity.BoxRule;
import com.activity.manager.rule.mapper.BoxRuleMapper;
import com.activity.manager.rule.service.BoxRuleService;
import com.activity.manager.security.entity.SysUser;

/**    
 * <p>Description: 宝箱规则更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0   
 */

@Service
public class BoxRuleSerivceImpl implements BoxRuleService{

	@Autowired
	private BoxRuleMapper boxRuleMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: queryBoxRule</p>   
	 * <p>Description: 查询所有宝箱规则</p>   
	 * @param boxRule
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.rule.service.BoxRuleService#queryBoxRule(com.activity.manager.rule.entity.BoxRule, int, int)   
	 */
	@Override
	public List<BoxRule> queryBoxRule(BoxRule boxRule, int start, int pageSize) {
		return boxRuleMapper.queryBoxRule(boxRule, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: boxRuleCount</p>   
	 * <p>Description: 统计所有宝箱规则</p>   
	 * @param boxRule
	 * @return   
	 * @see com.activity.manager.rule.service.BoxRuleService#boxRuleCount(com.activity.manager.rule.entity.BoxRule)   
	 */
	@Override
	public Integer boxRuleCount(BoxRule boxRule) {
		return boxRuleMapper.boxRuleCount(boxRule);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveBoxRule</p>   
	 * <p>Description: 新增宝箱规则</p>   
	 * @param boxRule   
	 * @see com.activity.manager.rule.service.BoxRuleService#saveBoxRule(com.activity.manager.rule.entity.BoxRule)   
	 */
	@Override
	public void saveBoxRule(BoxRule boxRule) {
		Assert.hasText(boxRule.getLevel(), "宝箱等级数据不存在");
		Assert.notNull(boxRule.getActId(), "活动ID数据不存在");
		Assert.notNull(boxRule.getRewardId(), "奖品ID数据不存在");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SysUser user = CurrentParamUtil.getCurrentUser();
		boxRule.setCreatePerson(user.getId());
		boxRule.setCreateTime(format.format(new Date()));
		boxRuleMapper.insert(boxRule);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: editBoxRule</p>   
	 * <p>Description: 修改宝箱规则</p>   
	 * @param boxRule   
	 * @see com.activity.manager.rule.service.BoxRuleService#editBoxRule(com.activity.manager.rule.entity.BoxRule)   
	 */
	@Override
	public void editBoxRule(BoxRule boxRule) {
		Assert.notNull(boxRule.getId(), "宝箱规则ID数据不存在");
		Assert.hasText(boxRule.getLevel(), "宝箱等级数据不存在");
		Assert.notNull(boxRule.getRewardId(), "奖品ID数据不存在");
		BoxRule rule = boxRuleMapper.selectByPrimaryKey(boxRule.getId());
		rule.setRewardId(boxRule.getRewardId());
		rule.setLevel(boxRule.getLevel());
		rule.setBoxImg(boxRule.getBoxImg());
		boxRuleMapper.updateByPrimaryKey(rule);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: delBoxRule</p>   
	 * <p>Description: 删除宝箱规则</p>   
	 * @param ids   
	 * @see com.activity.manager.rule.service.BoxRuleService#delBoxRule(java.util.List)   
	 */
	@Override
	public void delBoxRule(List<String> ids) {
		boxRuleMapper.delBoxRule(ids);
	}

}
