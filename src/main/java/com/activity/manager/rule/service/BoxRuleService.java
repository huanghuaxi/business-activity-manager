/**
 * 
 */
package com.activity.manager.rule.service;

import java.util.List;

import com.activity.manager.rule.entity.BoxRule;

/**    
 * <p>Description: 加载宝箱规则类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0   
 */
public interface BoxRuleService {
	
	/**
	 * 
	 * Description: 获取所有宝箱规则   
	 * @param boxRule
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年4月15日
	 */
	public List<BoxRule> queryBoxRule(BoxRule boxRule, int start ,int pageSize);
	
	/**
	 * 
	 * Description: 统计所有宝箱规则   
	 * @param boxRule
	 * @return 
	 * date 2019年4月15日
	 */
	public Integer boxRuleCount(BoxRule boxRule);
	
	/**
	 * 
	 * Description: 添加宝箱规则   
	 * @param boxRule 
	 * date 2019年4月15日
	 */
	public void saveBoxRule(BoxRule boxRule);
	
	/**
	 * 
	 * Description: 修改宝箱规则   
	 * @param boxRule 
	 * date 2019年4月15日
	 */
	public void editBoxRule(BoxRule boxRule);
	
	/**
	 * 
	 * Description: 删除宝箱规则    
	 * @param ids 
	 * date 2019年4月15日
	 */
	public void delBoxRule(List<String> ids);
	
}
