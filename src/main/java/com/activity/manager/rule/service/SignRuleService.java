/**
 * 
 */
package com.activity.manager.rule.service;

import java.util.List;

import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.rule.entity.SignRule;

/**    
 * <p>Description: 加载签到规则类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */
public interface SignRuleService {
	
	/**
     * 
     * Description: 查询所有签到规则
     * @param signRule
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月16日
     */
    public List<SignRule> querySignRule(SignRule signRule, int start ,int pageSize);
    
    /**
     * 
     * Description: 统计所有签到规则    
     * @param signRule
     * @return 
     * date 2019年4月16日
     */
    public Integer signRuleCount(SignRule signRule);
    
    /**
     * 
     * Description: 新增签到规则    
     * @param signRule 
     * date 2019年4月16日
     */
    public String saveSignRule(SignRule signRule);
    
    /**
     * 
     * Description: 编辑签到规则    
     * @param signRule 
     * date 2019年4月16日
     */
    public String editSignRule(SignRule signRule);
    
    /**
     * 
     * Description: 删除签到规则  
     * @param ids 
     * date 2019年4月16日
     */
    public void delSignRule(List<String> ids);
    
    /**
     * 
     * Description: 根据ID获取签到规则    
     * @param id
     * @return 
     * date 2019年4月16日
     */
    public SignRule getSignRuleById(Integer id);
    
    /**
     * 
     * Description: 添加奖品    
     * @param reward
     * @param ruleId
     * @return 
     * date 2019年5月7日
     */
    public Integer saveReward(RewardConf reward, Integer ruleId);
    
    /**
     * 
     * Description: 删除奖品    
     * @param ids
     * @param ruleId 
     * date 2019年5月7日
     */
    public String deleteReward(List<String> ids, Integer ruleId);
    
}
