/**
 * 
 */
package com.activity.manager.rule.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.mapper.BusiActivityMapper;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.MemberActivityDTO;
import com.activity.manager.member.entity.SignRecord;
import com.activity.manager.member.entity.SupplementSignApply;
import com.activity.manager.member.mapper.MemberActivityMapper;
import com.activity.manager.member.mapper.MemberMapper;
import com.activity.manager.member.mapper.SignRecordMapper;
import com.activity.manager.member.mapper.SupplementSignApplyMapper;
import com.activity.manager.member.service.MemberTransactionalService;
import com.activity.manager.reward.entity.InnerReward;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.entity.RewardRecord;
import com.activity.manager.reward.mapper.InnerRewardMapper;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.reward.mapper.RewardRecordMapper;
import com.activity.manager.rule.entity.BoxRule;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.rule.mapper.BoxRuleMapper;
import com.activity.manager.rule.mapper.SignRuleMapper;
import com.activity.manager.rule.service.RuleVerifyService;
import com.activity.manager.security.entity.ActivityCategory;
import com.activity.manager.security.entity.SiteDomainKv;
import com.activity.manager.security.mapper.ActivityCategoryMapper;
import com.activity.manager.security.mapper.SiteDomainKvMapper;

/**    
 * <p>Description: 规则验证更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Service
public class RuleVerifyServiceImpl implements RuleVerifyService{
	
	@Autowired
	private BusiActivityMapper activityMapper;
	
	@Autowired
	private BoxRuleMapper boxRuleMapper;
	
	@Autowired
	private MemberMapper memberMapper;
	
	@Autowired
	private MemberActivityMapper memberActMapper;
	
	@Autowired
	private RewardConfMapper rewardConfMapper;
	
	@Autowired
	private RewardRecordMapper rewardRecordMapper;
	
	@Autowired
	private InnerRewardMapper innerRewardMapper;
	@Value("${activity.reward.ratio}")
	private Integer rewardRatio;
	
	@Autowired
	private SiteDomainKvMapper siteDomainKvMapper;
	
	@Autowired
	private SignRecordMapper signRecordMapper; 
	
	@Autowired
	private SignRuleMapper signRuleMapper;
	
	@Autowired
	private SupplementSignApplyMapper applyMapper;
	
	@Autowired
	private ActivityCategoryMapper activityCategoryMapper;
	
	@Autowired
	private MemberTransactionalService memberTraService;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: memberLogin</p>   
	 * <p>Description: 获取会员</p>   
	 * @param memberLogin   
	 * @see com.activity.manager.rule.service.RuleVerifyService#memberLogin(java.lang.String)   
	 */
	@Override
	public Member memberLogin(String memberName, Integer actId, Integer inviter) {
		Assert.hasText(memberName, "会员名称数据不存在");
		Assert.notNull(actId, "活动ID数据不存在");
		Member member = memberMapper.selectByName(memberName);
		BusiActivity activity = activityMapper.selectByPrimaryKey(actId);
		boolean isShare = false;
		if(member == null) {
			//验证手机号登录
			isShare = mobileVerify(memberName);
		}else {
			MemberActivity memberAct = memberActMapper.selectByActId(actId, member.getId());
			if(memberAct == null) {
				isShare = true;
			}
		}
		if(isShare) {
			//新增活动会员
			List<MemberActivityDTO> memberActDTOs = new ArrayList<MemberActivityDTO>();
			MemberActivityDTO memberActDTO = new MemberActivityDTO();
			memberActDTO.setActId(actId);
			memberActDTO.setMemberName(memberName);
			memberActDTO.setAllTimes(1);
			memberActDTO.setLevel("青铜");
			memberActDTO.setType(1);
			memberActDTOs.add(memberActDTO);
			memberTraService.saveMemberActTra(memberActDTOs);
			member = memberMapper.selectByName(memberName);
		}
		//是否为分享成功
		if(inviter != null && isShare && activity.getIsShare()) {
			MemberActivity memberAct = memberActMapper.selectByActId(actId, inviter);
			if(memberAct != null) {
				memberAct.setAllTimes(memberAct.getAllTimes() + 1);
				memberActMapper.updateByPrimaryKey(memberAct);
			}
		}
		return member;
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: getMemberAct</p>   
	 * <p>Description: 获取活动会员</p>   
	 * @param actId
	 * @param memberId
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getMemberAct(java.lang.Integer, java.lang.Integer)   
	 */
	@Override
	public MemberActivity getMemberAct(Integer actId, Integer memberId) {
		Assert.notNull(memberId, "会员Id数据不存在");
		Assert.notNull(actId, "活动ID数据不存在");
		MemberActivity memberAct = memberActMapper.selectByActId(actId, memberId);
		return memberAct;
	}


	/* 
	 *(non-Javadoc)   
	 * <p>Title: getRewardRecord</p>   
	 * <p>Description: 获取会员中奖记录</p>   
	 * @param actId
	 * @param memberId
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getRewardRecord(java.lang.Integer, java.lang.Integer)   
	 */
	@Override
	public List<RewardRecord> getRewardRecordByMemberId(Integer actId, Integer memberId, Integer centralActId, Integer pageNo, Integer pageSize) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(memberId, "会员ID数据不存在");
		List<RewardRecord> rewardRecord = new ArrayList<RewardRecord>();
		if(pageNo != null && pageSize != null) {
			rewardRecord = rewardRecordMapper.selectByMemberId(actId, memberId, centralActId, (pageNo - 1) * pageSize, pageSize);
		}else {
			rewardRecord = rewardRecordMapper.selectByMemberId(actId, memberId, centralActId, null, null);
		}
		return rewardRecord;
	}
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: getRewardRecordById</p>   
	 * <p>Description: 获取该活动中奖记录</p>   
	 * @param actId
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getRewardRecordById(java.lang.Integer)   
	 */
	@Override
	public List<RewardRecord> getRewardRecordByActId(Integer actId) {
		Assert.notNull(actId, "活动ID数据不存在");
		List<RewardRecord> rewardRecord = rewardRecordMapper.selectByActId(actId);
		List<RewardRecord> records = new ArrayList<RewardRecord>();
		if(rewardRecord != null) {
			RewardRecord record = new RewardRecord();
			String name = new String();
			for(int i = 0;i < rewardRecord.size();i++) {
				record = new RewardRecord();
				record.setId(rewardRecord.get(i).getId());
				record.setRewardOption(rewardRecord.get(i).getRewardOption());
				record.setRewardCon(rewardRecord.get(i).getRewardCon());
				name = rewardRecord.get(i).getRemName();
				record.setRemName(name.substring(0,name.length() / 2) + "***");
				records.add(record);
			}
		}
		return records;
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getRewardList</p>   
	 * <p>Description: 获取奖品列表</p>   
	 * @param actId
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getRewardList(java.lang.Integer)
	 */
	@Override
	public List<RewardConf> getRewardList(Integer actId) {
		Assert.notNull(actId, "活动ID数据不存在");
		return rewardConfMapper.selectRewardConfByActId(actId);
	}

	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: BoxRuleVerify</p>   
	 * <p>Description: 宝箱规则验证</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#BoxRuleVerify(java.lang.Integer)   
	 */
	@Override
	@Transactional
	public ResultMessage BoxRuleVerify(Integer actId, Member member, Integer centralActId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(member, "会员数据不存在");
		Assert.notNull(member.getId(), "会员ID数据不存在");
		//判断活动是否到期
		int success = isActivityExpire(actId);
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "活动无效", null);
		}
		//判断该会员是否有资格参加此项活动，是否有抽奖次数
		success = isJoin(actId, member.getId()); 
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您未参加此活动", null);
		}else if(success == 1) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您的抽奖次数已用完", null);
		}
		//获取活动会员
		MemberActivity memberAct = memberActMapper.selectByActId(actId, member.getId());
		//获取此等级宝箱所有奖品
		List<RewardConf> rewards = rewardConfMapper.selectByIdAndOption(actId,  memberAct.getLevel());
		//判断该宝箱是否有奖品
		Float ratioCount = 0.0f;
		if(rewards != null && rewards.size() > 0) {
			int rewardNum = 0;
			for(int i = 0;i < rewards.size();i++) {
				rewardNum += rewards.get(i).getRewardNum();
				ratioCount += rewards.get(i).getRatio();
			}
			if(rewardNum <= 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
			}
		}else {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
		}
		//查看是否为内定会员
		String reward = "";
		Map<String, Object> map = isInnerMember(actId, member.getId());
		reward = map.get("reward") == null ? "" : map.get("reward").toString();
		
		//抽奖
		RewardConf rc = new RewardConf(); 
		int i = 0;
		if(reward == null || reward == "") {
			Map<String, Object> rewardMap = ratioCompute(rewards);
			if(rewardMap != null && rewardMap.get("reward") != null) {
				reward = rewardMap.get("reward").toString();
				rc = (RewardConf) rewardMap.get("rewardConf");
				i = (int) (rewardMap.get("index") == null ? 0 : rewardMap.get("index"));
			}
		}
		//判断抽中的奖品数量是否已经被内定
		if(!(boolean) map.get("isInner") && rc.getId() != null) {
			Integer innerCount = innerRewardMapper.countByRewardId(rc.getId());
			if(innerCount >= rc.getRewardNum()) {
				reward = "";
			}
		}
		
		//减少会员抽奖次数
		reduceTimes(actId, member.getId());
		//中奖
		if(reward != null && reward != "") {
			//减少奖品数
			RewardConf rewardConf = map.get("rewardConf") == null ? rewards.get(i) : (RewardConf) map.get("rewardConf");
			reduceReward(rewardConf);
			//生成中奖纪录
			saveRewardRecord(actId, rewardConf, reward, member, centralActId);
		}else {
			//生成中奖纪录
			saveRewardRecord(actId, null, reward, member, centralActId);
		}
		return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, reward);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByActivityShow</p>   
	 * <p>Description: 获取展示活动类型</p>   
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#selectByActivityShow()
	 */
	@Override
	public List<ActivityCategory> selectByActivityShow() {
		return activityCategoryMapper.selectByActivityShow();
	}
	
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: EggRuleVerify</p>   
	 * <p>Description: 砸金蛋规则验证</p>   
	 * @param actId
	 * @param member
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#EggRuleVerify(java.lang.Integer, com.activity.manager.member.entity.Member)
	 */
	@Override
	public ResultMessage EggRuleVerify(Integer actId, Member member, Integer centralActId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(member, "会员数据不存在");
		Assert.notNull(member.getId(), "会员ID数据不存在");
		//判断活动是否到期
		int success = isActivityExpire(actId);
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "活动无效", null);
		}
		//判断该会员是否有资格参加此项活动，是否有抽奖次数
		success = isJoin(actId, member.getId()); 
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您未参加此活动", null);
		}else if(success == 1) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您的砸金蛋机会已用完", null);
		}
		//判读是否有奖品
		int rewardNum = 0;
		Float ratioCount = 0.0f;
		List<RewardConf> rewards = rewardConfMapper.selectRewardConfByActId(actId);
		if(rewards != null && rewards.size() > 0) {
			for(int i = 0;i < rewards.size();i++) {
				rewardNum += rewards.get(i).getRewardNum();
				ratioCount += rewards.get(i).getRatio();
			}
			if(rewardNum <= 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
			}
		}else {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
		}
		//查看是否为内定会员
		String reward = "";
		Map<String, Object> map = isInnerMember(actId, member.getId());
		reward = map.get("reward") == null ? "" : map.get("reward").toString();
		
		//抽奖
		RewardConf rc = new RewardConf(); 
		int i = 0;
		if(reward == null || reward == "") {
			Map<String, Object> rewardMap = ratioCompute(rewards);
			if(rewardMap != null && rewardMap.get("reward") != null) {
				reward = rewardMap.get("reward").toString();
				rc = (RewardConf) rewardMap.get("rewardConf");
				i = (int) (rewardMap.get("index") == null ? 0 : rewardMap.get("index"));
			}
		}
		//判断抽中的奖品数量是否已经被内定
		if(!(boolean) map.get("isInner") && rc.getId() != null) {
			Integer innerCount = innerRewardMapper.countByRewardId(rc.getId());
			if(innerCount >= rc.getRewardNum()) {
				reward = "";
			}
		}
		//减少会员抽奖次数
		reduceTimes(actId, member.getId());
		//中奖
		if(reward != null && reward != "") {
			//减少奖品数
			RewardConf rewardConf = map.get("rewardConf") == null ? rewards.get(i) : (RewardConf) map.get("rewardConf");
			reduceReward(rewardConf);
			//生成中奖纪录
			saveRewardRecord(actId, rewardConf, reward, member, centralActId);
		}else {
			//生成中奖纪录
			saveRewardRecord(actId, null, reward, member, centralActId);
		}
		return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, reward);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: redEnvelopeVerify</p>   
	 * <p>Description: 红包规则验证 </p>   
	 * @param actId
	 * @param member
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#redEnvelopeVerify(java.lang.Integer, com.activity.manager.member.entity.Member)
	 */
	@Override
	public ResultMessage redEnvelopeVerify(Integer actId, Member member, Integer centralActId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(member, "会员数据不存在");
		Assert.notNull(member.getId(), "会员ID数据不存在");
		//判断活动是否到期
		int success = isActivityExpire(actId);
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "活动无效", null);
		}
		//判断该会员是否有资格参加此项活动，是否有抽奖次数
		success = isJoin(actId, member.getId()); 
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您未参加此活动", null);
		}else if(success == 1) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您的拆红包机会已用完", null);
		}
		//判读是否有奖品
		int rewardNum = 0;
		Float ratioCount = 0.0f;
		List<RewardConf> rewards = rewardConfMapper.selectRewardConfByActId(actId);
		if(rewards != null && rewards.size() > 0) {
			for(int i = 0;i < rewards.size();i++) {
				rewardNum += rewards.get(i).getRewardNum();
				ratioCount += rewards.get(i).getRatio();
			}
			if(rewardNum <= 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
			}
		}else {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
		}
		//查看是否为内定会员
		String reward = "";
		Map<String, Object> map = isInnerMember(actId, member.getId());
		reward = map.get("reward") == null ? "" : map.get("reward").toString();
		
		//抽奖
		RewardConf rc = new RewardConf(); 
		int i = 0;
		if(reward == null || reward == "") {
			Map<String, Object> rewardMap = ratioCompute(rewards);
			if(rewardMap != null && rewardMap.get("reward") != null) {
				reward = rewardMap.get("reward").toString();
				rc = (RewardConf) rewardMap.get("rewardConf");
				i = (int) (rewardMap.get("index") == null ? 0 : rewardMap.get("index"));
			}
		}
		if(!(boolean) map.get("isInner") && rc.getId() != null) {
			Integer innerCount = innerRewardMapper.countByRewardId(rc.getId());
			if(innerCount >= rc.getRewardNum()) {
				reward = "";
			}
		}
		//减少会员抽奖次数
		reduceTimes(actId, member.getId());
		//中奖
		if(reward != null && reward != "") {
			//减少奖品数
			RewardConf rewardConf = map.get("rewardConf") == null ? rewards.get(i) : (RewardConf) map.get("rewardConf");
			reduceReward(rewardConf);
			//生成中奖纪录
			saveRewardRecord(actId, rewardConf, reward, member, centralActId);
		}else {
			//生成中奖纪录
			saveRewardRecord(actId, null, reward, member, centralActId);
		}
		return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, reward);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: cardVerify</p>   
	 * <p>Description: 翻牌规则验证</p>   
	 * @param actId
	 * @param member
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#cardVerify(java.lang.Integer, com.activity.manager.member.entity.Member)
	 */
	@Override
	public ResultMessage cardVerify(Integer actId, Member member, Integer centralActId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(member, "会员数据不存在");
		Assert.notNull(member.getId(), "会员ID数据不存在");
		//判断活动是否到期
		int success = isActivityExpire(actId);
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "活动无效", null);
		}
		//判断该会员是否有资格参加此项活动，是否有抽奖次数
		success = isJoin(actId, member.getId()); 
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您未参加此活动", null);
		}else if(success == 1) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您的翻牌机会已用完", null);
		}
		//判读是否有奖品
		int rewardNum = 0;
		Float ratioCount = 0.0f;
		List<RewardConf> rewards = rewardConfMapper.selectRewardConfByActId(actId);
		if(rewards != null && rewards.size() > 0) {
			for(int i = 0;i < rewards.size();i++) {
				rewardNum += rewards.get(i).getRewardNum();
				ratioCount += rewards.get(i).getRatio();
			}
			if(rewardNum <= 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
			}
		}else {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
		}
		//查看是否为内定会员
		String reward = "";
		Map<String, Object> map = isInnerMember(actId, member.getId());
		reward = map.get("reward") == null ? "" : map.get("reward").toString();
		
		//抽奖
		RewardConf rc = new RewardConf(); 
		int i = 0;
		if(reward == null || reward == "") {
			Map<String, Object> rewardMap = ratioCompute(rewards);
			if(rewardMap != null && rewardMap.get("reward") != null) {
				reward = rewardMap.get("reward").toString();
				rc = (RewardConf) rewardMap.get("rewardConf");
				i = (int) (rewardMap.get("index") == null ? 0 : rewardMap.get("index"));
			}
		}
		if(!(boolean) map.get("isInner") && rc.getId() != null) {
			Integer innerCount = innerRewardMapper.countByRewardId(rc.getId());
			if(innerCount >= rc.getRewardNum()) {
				reward = "";
			}
		}
		//减少会员抽奖次数
		reduceTimes(actId, member.getId());
		//中奖
		if(reward != null && reward != "") {
			//减少奖品数
			RewardConf rewardConf = map.get("rewardConf") == null ? rewards.get(i) : (RewardConf) map.get("rewardConf");
			reduceReward(rewardConf);
			//生成中奖纪录
			saveRewardRecord(actId, rewardConf, reward, member, centralActId);
		}else {
			//生成中奖纪录
			saveRewardRecord(actId, null, reward, member, centralActId);
		}
		return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, reward);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: turntableVerify</p>   
	 * <p>Description: 转盘规则验证</p>   
	 * @param actId
	 * @param member
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#turntableVerify(java.lang.Integer, com.activity.manager.member.entity.Member)
	 */
	@Override
	public ResultMessage turntableVerify(Integer actId, Member member, Integer centralActId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(member, "会员数据不存在");
		Assert.notNull(member.getId(), "会员ID数据不存在");
		//判断活动是否到期
		int success = isActivityExpire(actId);
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "活动无效", null);
		}
		//判断该会员是否有资格参加此项活动，是否有抽奖次数
		success = isJoin(actId, member.getId()); 
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您未参加此活动", null);
		}else if(success == 1) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您的转盘机会已用完", null);
		}
		//判读是否有奖品
		int rewardNum = 0;
		Float ratioCount = 0.0f;
		List<RewardConf> rewards = rewardConfMapper.selectRewardConfByActId(actId);
		if(rewards != null && rewards.size() > 0) {
			for(int i = 0;i < rewards.size();i++) {
				rewardNum += rewards.get(i).getRewardNum();
				ratioCount += rewards.get(i).getRatio();
			}
			if(rewardNum <= 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
			}
		}else {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
		}
		//查看是否为内定会员
		String reward = "";
		String rewardId = null;
		Map<String, Object> map = isInnerMember(actId, member.getId());
		reward = map.get("reward") == null ? "" : map.get("reward").toString();
		rewardId = map.get("rewardId") == null ? "" : map.get("rewardId").toString();
		
		//抽奖
		RewardConf rc = new RewardConf(); 
		int i = 0;
		if(reward == null || reward == "") {
			Map<String, Object> rewardMap = ratioCompute(rewards);
			if(rewardMap != null && rewardMap.get("reward") != null) {
				reward = rewardMap.get("reward").toString();
				rc = (RewardConf) rewardMap.get("rewardConf");
				i = (int) (rewardMap.get("index") == null ? 0 : rewardMap.get("index"));
				rewardId = rc != null ? rc.getId().toString() : null;
			}
		}
		if(!(boolean) map.get("isInner") && rc.getId() != null) {
			Integer innerCount = innerRewardMapper.countByRewardId(rc.getId());
			if(innerCount >= rc.getRewardNum()) {
				reward = "";
			}
		}
		//减少会员抽奖次数
		reduceTimes(actId, member.getId());
		//中奖
		if(reward != null && reward != "") {
			//减少奖品数
			RewardConf rewardConf = map.get("rewardConf") == null ? rewards.get(i) : (RewardConf) map.get("rewardConf");
			reduceReward(rewardConf);
			//生成中奖纪录
			saveRewardRecord(actId, rewardConf, reward, member, centralActId);
		}else {
			//生成中奖纪录
			saveRewardRecord(actId, null, reward, member, centralActId);
		}
		Map<String, String> result = new HashMap<String, String>();
		result.put("reward", reward);
		result.put("rewardId", rewardId);
		return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, result);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: signVerify</p>   
	 * <p>Description: 签到规则验证</p>   
	 * @param actId
	 * @param member
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#signVerify(java.lang.Integer, com.activity.manager.member.entity.Member)
	 */
	@Override
	@Transactional
	public ResultMessage signVerify(Integer actId, Member member, Integer centralActId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(member, "会员数据不存在");
		Assert.notNull(member.getId(), "会员ID数据不存在");
		//判断活动是否到期
		int success = isActivityExpire(actId);
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "活动无效", null);
		}
		//判断该会员是否有资格参加此项活动，是否有抽奖次数
		success = isJoin(actId, member.getId()); 
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您未参加此活动", null);
		}
		//获取活动规则
		List<SignRule> signRules = signRuleMapper.selectByActId(actId);
		if(signRules == null) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
		}
		//获取签到记录
		List<SignRecord> signRecords = signRecordMapper.selectByMember(member.getId(), actId);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		//计算连续签到
		int seriesDay = 0;
		if(signRecords != null) {
			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			int day1, day2;
			for(int i = signRecords.size() - 1;i >= 0;i--) {
				if(seriesDay == 0) {
					cal1.setTime(date);
					try {
						cal2.setTime(format.parse(signRecords.get(i).getSignTime()));
					} catch (ParseException e) {
						e.printStackTrace();
						break;
					}
					day1 = cal1.get(Calendar.DAY_OF_MONTH);
					day2 = cal2.get(Calendar.DAY_OF_MONTH);
					if(day1 == day2) {
						seriesDay++;
					}else {
						cal1.add(Calendar.DAY_OF_MONTH, -1);
						day1 = cal1.get(Calendar.DAY_OF_MONTH);
						if(day1 == day2) {
							seriesDay++;
						}
					}
				}else {
					try {
						cal2.setTime(format.parse(signRecords.get(i).getSignTime()));
					} catch (ParseException e) {
						e.printStackTrace();
						break;
					}
					cal1.add(Calendar.DAY_OF_MONTH, -1);
					day1 = cal1.get(Calendar.DAY_OF_MONTH);
					day2 = cal2.get(Calendar.DAY_OF_MONTH);
					if(day1 == day2) {
						seriesDay++;
					}else {
						break;
					}
				}
			}
		}
		//获取累计签到
		int countSign = signRecordMapper.countByYear(member.getId(), actId);
		//查询是否签到
		SignRecord signRecord = signRecordMapper.selectByDate(member.getId(), actId);
		if(signRecord != null) {
			//获取签到奖励记录
			Map<String, Integer> map = getSignSecondaryAct(signRules, member.getId(), countSign, seriesDay);
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "你已签到", map);
		}
		//签到
		signRecord = new SignRecord();
		signRecord.setActId(actId);
		signRecord.setMemberId(member.getId());
		signRecord.setSignType(0);
		signRecord.setSignTime(format.format(date));
		signRecord.setCreateTime(format.format(date));
		signRecordMapper.insert(signRecord);
		
		seriesDay++;
		countSign++;
		
		int days = 1;
		//获取奖品
		Map<String, String> reward = new HashMap<String, String>();
		RewardConf rc = new RewardConf();
		//判断签到类型获取奖品,0:每天 1:连续 2:累计
		SignRule signRule = null;
		int flag = 0;
		for(int num = 2;num >= 0;num--) {
			if(flag == 1) {
				break;
			}
			if(reward.get("reward") == null && reward.get("actId") == null) {
				for(int i = 0;i < signRules.size();i++) {
					if(signRules.get(i).getSignType() == num) {
						if(num == 1) {
							//连续签到
							days = seriesDay;
						}else if(num == 2) {
							//累计签到
							days = countSign;
						}
						if(num != 0 && days != signRules.get(i).getStatisticsTimes()) {
							continue;
						}
						signRule = signRules.get(i);
						//抽奖次数
						if(signRules.get(i).getDeputyActId() != null && signRules.get(i).getLotteryTimes() != null && signRules.get(i).getLotteryTimes() >= 0) {
							reward.put("actId", signRules.get(i).getDeputyActId().toString());
							reward.put("lotteryTimes", signRules.get(i).getLotteryTimes().toString());
						}
						//奖品
						if(signRules.get(i).getRewardId() != null && !"".equals(signRules.get(i).getRewardId())) {
							List<RewardConf> rewards = rewardConfMapper.selectByIds(
									Arrays.asList(signRules.get(i).getRewardId().split(",")), actId);
							Map<String, Object> map = ratioCompute(rewards);
							if(map != null && map.get("reward") != null) {
								reward.put("reward", map.get("reward").toString());
								rc = (RewardConf) map.get("rewardConf");
							}
						}
						flag = 1;
						break;
					}
				}
			}
		}
		
		//中奖
		if(reward.get("reward") != null) {
			if(signRule != null && signRule.getRewardTimes() != null) {
				//减少奖品数
				RewardConf reward2= rewardConfMapper.selectByPrimaryKey(rc.getId());
				if(signRule.getRewardTimes() > reward2.getRewardNum()) {
					signRule.setRewardTimes(reward2.getRewardNum());
				}
				reward2.setRewardNum(reward2.getRewardNum() - signRule.getRewardTimes());
				reward2.setWinNum(reward2.getWinNum() + signRule.getRewardTimes());
				rewardConfMapper.updateByPrimaryKey(reward2);
			}
			
			if(reward.get("reward") != null) {
				Integer rewardNum = 0;
				if(signRule != null && signRule.getRewardTimes() != null) {
					reward.put("rewardTimes", signRule.getRewardTimes().toString());
					if(rc != null && rc.getRewardCon().indexOf("~") > 0) {
						rewardNum = Integer.parseInt(reward.get("reward").trim()) * signRule.getRewardTimes();
						//生成中奖纪录
						saveRewardRecord(actId, rc, rewardNum.toString(), member, centralActId);
					}else {
						for(int i = 0;i < signRule.getRewardTimes();i++) {
							//生成中奖纪录
							saveRewardRecord(actId, rc, reward.get("reward"), member, centralActId);
						}
					}
					
				}else {
					//生成中奖纪录
					saveRewardRecord(actId, rc, reward.get("reward"), member, centralActId);
				}
			}
		}else {
			//生成中奖纪录
			saveRewardRecord(actId, null, "", member, centralActId);
		}
		if(reward.get("actId") != null) {
			//添加会员到活动
			MemberActivity memberAct = memberActMapper.selectByActId(Integer.parseInt(reward.get("actId")), member.getId());
			if(memberAct == null) {
				memberAct = new MemberActivity();
				memberAct.setActId(Integer.parseInt(reward.get("actId")));
				memberAct.setMemberId(member.getId());
				memberAct.setCreatePerson(member.getCreatePerson());
				memberAct.setAllTimes(Integer.parseInt(reward.get("lotteryTimes")));
				memberAct.setParticipatedTimes(0);
				memberAct.setCreateTime(format.format(date));
				memberAct.setLevel("青铜");
				memberActMapper.insert(memberAct);
			}else {
				memberAct.setAllTimes(memberAct.getAllTimes() + Integer.parseInt(reward.get("lotteryTimes")));
				memberActMapper.updateByPrimaryKey(memberAct);
			}
		}
		//添加签到次数
		MemberActivity memberAct = memberActMapper.selectByActId(actId, member.getId());
		memberAct.setParticipatedTimes(memberAct.getParticipatedTimes() + 1);
		memberActMapper.updateByPrimaryKey(memberAct);
		return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, reward);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: bobingVerify</p>   
	 * <p>Description: 博饼规则验证</p>   
	 * @param actId
	 * @param member
	 * @param centralActId
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#bobingVerify(java.lang.Integer, com.activity.manager.member.entity.Member, java.lang.Integer)
	 */
	@Override
	public ResultMessage bobingVerify(Integer actId, Member member, Integer centralActId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.notNull(member, "会员数据不存在");
		Assert.notNull(member.getId(), "会员ID数据不存在");
		//判断活动是否到期
		int success = isActivityExpire(actId);
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "活动无效", null);
		}
		//判断该会员是否有资格参加此项活动，是否有抽奖次数
		success = isJoin(actId, member.getId()); 
		if(success == 0) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您未参加此活动", null);
		}else if(success == 1) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "您的博饼机会已用完", null);
		}
		//判读是否有奖品
		int rewardNum = 0;
		Float ratioCount = 0.0f;
		List<RewardConf> rewards = rewardConfMapper.selectRewardConfByActId(actId);
		if(rewards != null && rewards.size() > 0) {
			for(int i = 0;i < rewards.size();i++) {
				rewardNum += rewards.get(i).getRewardNum();
				ratioCount += rewards.get(i).getRatio();
			}
			if(rewardNum <= 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
			}
		}else {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该活动没有奖品", null);
		}
		//查看是否为内定会员
		String reward = "";
		Map<String, Object> map = isInnerMember(actId, member.getId());
		reward = map.get("reward") == null ? "" : map.get("reward").toString();
		
		//抽奖
		RewardConf rc = new RewardConf(); 
		int i = 0;
		if(reward == null || reward == "") {
			Map<String, Object> rewardMap = ratioCompute(rewards);
			if(rewardMap != null && rewardMap.get("reward") != null) {
				reward = rewardMap.get("reward").toString();
				rc = (RewardConf) rewardMap.get("rewardConf");
				i = (int) (rewardMap.get("index") == null ? 0 : rewardMap.get("index"));
			}
		}
		if(!(boolean) map.get("isInner") && rc.getId() != null) {
			Integer innerCount = innerRewardMapper.countByRewardId(rc.getId());
			if(innerCount >= rc.getRewardNum()) {
				reward = "";
			}
		}
		
		//生成色子
		String dices = createDice(rewards, reward, rc.getId());
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("reward", reward);
		result.put("rewardConf", rc);
		result.put("dices", dices);
		
		//减少会员抽奖次数
		reduceTimes(actId, member.getId());
		//中奖
		if(reward != null && reward != "") {
			//减少奖品数
			RewardConf rewardConf = map.get("rewardConf") == null ? rewards.get(i) : (RewardConf) map.get("rewardConf");
			reduceReward(rewardConf);
			//生成中奖纪录
			saveRewardRecord(actId, rewardConf, reward, member, centralActId);
		}else {
			//生成中奖纪录
			saveRewardRecord(actId, null, reward, member, centralActId);
		}
		return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, result);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getBox</p>   
	 * <p>Description: 获取宝箱</p>   
	 * @param actId
	 * @param level
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getBox(java.lang.Integer, java.lang.String)
	 */
	@Override
	public BoxRule getBox(Integer actId, String level) {
		return boxRuleMapper.selectByLevel(actId, level);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getDomainKv</p>   
	 * <p>Description: 获取站点域名绑定</p>   
	 * @param keyEnName
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getDomainKv(java.lang.String)
	 */
	@Override
	public List<SiteDomainKv> getDomainKv(String domainEnName) {
		return siteDomainKvMapper.selectByDomainEnName(domainEnName);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getSignRecord</p>   
	 * <p>Description: 获取签到纪录</p>   
	 * @param actId
	 * @param member
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getSignRecord(java.lang.Integer, com.activity.manager.member.entity.Member)
	 */
	@Override
	public List<SignRecord> getSignRecord(Integer actId, Member member) {
		return signRecordMapper.selectByMember(member.getId(), actId);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getCountSign</p>   
	 * <p>Description: 获取累计签到</p>   
	 * @param actId
	 * @param member
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getCountSign(java.lang.Integer, com.activity.manager.member.entity.Member)
	 */
	@Override
	public Integer getCountSign(Integer actId, Member member) {
		return signRecordMapper.countByYear(member.getId(), actId);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getSignRecord</p>   
	 * <p>Description: 获取活动规则</p>   
	 * @param actId
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getSignRecord(java.lang.Integer)
	 */
	@Override
	public List<SignRule> getSignRecord(Integer actId) {
		Assert.notNull(actId, "活动ID数据不存在");
		List<SignRule> rules = signRuleMapper.selectByActId(actId);
		if(rules != null) {
			List<String> rewardIds = new ArrayList<String>();
			String[] ids = null;
			for(int i = 0;i < rules.size();i++) {
				if(rules.get(i).getRewardId() != null) {
					ids = rules.get(i).getRewardId().trim().split(",");
					for(int j = 0;j < ids.length;j++) {
						rewardIds.add(ids[j].trim());
					}
				}
			}
			if(rewardIds.size() > 0) {
				List<RewardConf> rewards = rewardConfMapper.selectByIds(rewardIds, actId);
				for(RewardConf reward : rewards) {
					for(int i = 0;i < rules.size();i++) {
						if(rules.get(i).getRewardId() != null && !"".equals(rules.get(i).getRewardId().trim())) {
							ids = rules.get(i).getRewardId().trim().split(",");
							for(int j = 0;j < ids.length;j++) {
								if(Integer.parseInt(ids[j].trim()) == reward.getId()) {
									if(rules.get(i).getRewards() == null) {
										rules.get(i).setRewards(new ArrayList<RewardConf>());
									}
									rules.get(i).getRewards().add(reward);
								}
							}
						}
					}
				}
			}
			
		}
		return rules;
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: supplementSignApply</p>   
	 * <p>Description: 补签申请</p>   
	 * @param dates
	 * @param member
	 * @param actId   
	 * @see com.activity.manager.rule.service.RuleVerifyService#supplementSignApply(java.lang.String, com.activity.manager.member.entity.Member, java.lang.Integer)
	 */
	@Override
	@Transactional
	public String supplementSignApply(String dates, Member member, Integer actId) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.hasText(dates, "补签日期数据不存在");
		String[] days = dates.trim().split(",");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		Date date = new Date();
		String newDate = format.format(date);
		List<String> signTimes = new ArrayList<String>();
		for(int i = 0;i < days.length;i++) {
			signTimes.add(newDate + "-" + (days[i].length() == 1 ? "0" + days[i] : days[i] ));
		}
		List<SignRecord> signRecord = signRecordMapper.selectBySignTimes(member.getId(), actId, signTimes);
		if(signRecord != null && signRecord.size() > 0) {
			return "提交失败，选择的日期已做补签，请重新选择";
		}
		List<SupplementSignApply> applys = applyMapper.selectBySignTimes(actId, member.getId(), signTimes);
		if(applys != null && applys.size() > 0) {
			return "提交失败，选择的日期已做补签申请，请重新选择";
		}
		SupplementSignApply apply = new SupplementSignApply();
		for(String signTime : signTimes) {
			apply = new SupplementSignApply();
			apply.setActId(actId);
			apply.setMemberId(member.getId());
			apply.setSignTime(signTime);
			apply.setStart(0);
			apply.setCreateTime(date);
			applyMapper.insert(apply);
		}
		return "";
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getSupplementSignApply</p>   
	 * <p>Description: 获取补签申请</p>   
	 * @param member
	 * @param actId
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getSupplementSignApply(com.activity.manager.member.entity.Member, java.lang.Integer)
	 */
	@Override
	public List<SupplementSignApply> getSupplementSignApply(Member member, Integer actId) {
		return applyMapper.selectByMember(actId, member.getId());
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getActivityByActId</p>   
	 * <p>Description: 根据活动类型获取活动</p>   
	 * @param activity
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getActivityByActId(com.activity.manager.activity.entity.BusiActivity, int, int)
	 */
	@Override
	public List<BusiActivity> getActivityByActId(BusiActivity activity, int start, int pageSize) {
		return activityMapper.selectByActId(activity, start, pageSize);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getNewActivity</p>   
	 * <p>Description: 获取最新活动</p>   
	 * @return   
	 * @see com.activity.manager.rule.service.RuleVerifyService#getNewActivity()
	 */
	@Override
	public List<BusiActivity> getNewActivity() {
		return activityMapper.selectNewAct();
	}
	
	
	/**
	 * 
	 * Description: 活动是否到期   
	 * @param actId
	 * @return 
	 * date 2019年4月17日
	 */
	private int isActivityExpire(Integer actId) {
		int isExpire = 0;
		try {
			BusiActivity activity = activityMapper.selectByPrimaryKey(actId);
			Assert.notNull(activity, "活动数据不存在");
			Assert.hasText(activity.getBeginTime(), "活动开始时间数据不存在");
			Assert.hasText(activity.getEndTime(), "活动结束时间数据不存在");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(activity.getIsUsed()) {
				Date newTime = new Date();
				Date beginTime = format.parse(activity.getBeginTime());
				Date endTime = format.parse(activity.getEndTime());
				if(beginTime.getTime() <= newTime.getTime() && endTime.getTime() >= newTime.getTime()) {
					isExpire = 1;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return isExpire;
	}
	
	/**
	 * 
	 * Description: 查询该会员是否参加此活动   
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年4月17日
	 */
	private int isJoin(Integer actId, Integer memberId) {
		int isJoin = 0;
		try {
			MemberActivity memberAct = memberActMapper.selectByActId(actId, memberId);
			if(memberAct != null) {
				if(memberAct.getAllTimes() > 0) {
					isJoin = 2;
				}else {
					isJoin = 1;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return isJoin;
	}
	
	/**
	 * 
	 * Description: 是否为内定会员   
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年4月19日
	 */
	private Map<String, Object> isInnerMember(Integer actId, Integer memberId) {
		Map<String, Object> map = new HashMap<String, Object>();
		String reward = null;
		InnerReward innerReward = innerRewardMapper.selectByMemberId(actId, memberId);
		if(innerReward != null) {
			map.put("isInner", true);
			RewardConf rewardConf = rewardConfMapper.selectByPrimaryKey(innerReward.getRewardId());
			if(rewardConf != null) {
				reward = createReward(rewardConf);
				//修改此内定会员信息为已使用
				innerReward.setIsUsed(1);
				innerRewardMapper.updateByPrimaryKey(innerReward);
				map.put("rewardConf", rewardConf);
				map.put("rewardId", innerReward.getRewardId());
			}
		}else {
			map.put("isInner", false);
		}
		map.put("reward", reward);
		return map;
	}
	
	/**
	 * 
	 * Description: 生成奖品   
	 * @param rewardConf
	 * @return 
	 * date 2019年4月17日
	 */
	private String createReward(RewardConf rewardConf) {
		String reward = new String();
		String rewardCon = rewardConf.getRewardCon();
		//根据概率查看是否中奖
		Random r = new Random();
		if(rewardCon.indexOf("~") != -1) {
			//范围
			String[] numbers = rewardCon.split("~");
//			int count = 0;
//			for(int i = Integer.parseInt(numbers[0]);i <= Integer.parseInt(numbers[1]);i++) {
//				count += i;
//			}
//			Integer min = Integer.parseInt(numbers[0]);
//			Integer max = Integer.parseInt(numbers[1]);
//			int minNum = max;
//			for(int j = 0; j < 3;j++) {
//				int number = r.nextInt(count - min) + min;
//				int leng = count;
//				for(int i = min;i <= max;i++) {
//					leng = leng - (max - i);
//					if(number >= leng) {
//						number = i;
//						break;
//					}
//				}
//				if(minNum > number) {
//					minNum = number;
//				}
//			}
			Integer min = Integer.parseInt(numbers[0]);
			Integer max = Integer.parseInt(numbers[1]) - min;
			int number = r.nextInt(max) + min;
//			reward = number + (rewardConf.getUnit() == null ? "" : rewardConf.getUnit());
			reward = number + "";
		}else if(rewardCon.indexOf(",") != -1) {
			//多值
			String[] numbers = rewardCon.split(",");
			int number = r.nextInt(numbers.length - 1);
			reward = numbers[number] + (rewardConf.getUnit() == null ? "" : rewardConf.getUnit());
		}else {
			//单值
			reward = rewardCon;
		}
		
		return reward;
	}
	
	/**
	 * 
	 * Description: 修改会员参与次数   
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年4月17日
	 */
	private void reduceTimes(Integer actId, Integer memberId) {
		MemberActivity memberAct = memberActMapper.selectByActId(actId, memberId);
		memberAct.setAllTimes(memberAct.getAllTimes() - 1);
		memberAct.setParticipatedTimes(memberAct.getParticipatedTimes() + 1);
		memberActMapper.updateByPrimaryKey(memberAct);
	}
	
	/**
	 * 
	 * Description: 减少奖品数   
	 * @param rewardConf
	 * @return 
	 * date 2019年4月17日
	 */
	private void reduceReward(RewardConf rewardConf) {
		RewardConf reward = rewardConfMapper.selectByPrimaryKey(rewardConf.getId());
		reward.setRewardNum(reward.getRewardNum() - 1);
		reward.setWinNum(reward.getWinNum() + 1);
		rewardConfMapper.updateByPrimaryKey(reward);
	}
	
	/**
	 * 
	 * Description: 添加中奖纪录   
	 * @param actId
	 * @param rewardConf
	 * @param reward
	 * @param member
	 * @param centralActId
	 * @return 
	 * date 2019年4月17日
	 */
	private void saveRewardRecord(Integer actId, RewardConf rewardConf, String reward, Member member, Integer centralActId) {
		RewardRecord record = new RewardRecord();
		record.setActId(actId);
		record.setRemId(member.getId());
		record.setRemName(member.getMemberName());
		record.setRewardCon(reward);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		record.setCreatePersion(member.getCreatePerson());
		record.setCreateTime(format.format(new Date()));
		record.setIsSend(0);
		record.setCentralActId(centralActId);
		//是否中奖
		if(rewardConf == null) {
			record.setIsGit(false);
		}else {
			record.setRewardId(rewardConf.getId());
			record.setRewardOption(rewardConf.getRewardOption() + "：" + rewardConf.getRewardCon());
			record.setIsGit(true);
		}
		rewardRecordMapper.insert(record);
	}

	/**
	 * 
	 * Description: 获取配套活动Id    
	 * @param signRules
	 * @param memberId
	 * @param signCount
	 * @param seriesDay 
	 * @return 
	 * date 2019年5月22日
	 */
	private Map<String, Integer> getSignSecondaryAct(List<SignRule> signRules, Integer memberId, Integer signCount, Integer seriesDay) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		if(signRules != null) {
			int flag = 0;
			int days = 1;
			for(int num = 2;num >= 0;num--) {
				if(flag == 1) {
					break;
				}
				for(int i = 0;i < signRules.size();i++) {
					if(signRules.get(i).getSignType() == num) {
						if(num == 1) {
							//连续签到
							days = seriesDay;
						}else if(num == 2) {
							//累计签到
							days = signCount;
						}
						if(num != 0 && days != signRules.get(i).getStatisticsTimes()) {
							continue;
						}
						if(signRules.get(i).getDeputyActId() != null) {
							map.put("actId", signRules.get(i).getDeputyActId());
							MemberActivity memberAct = memberActMapper.selectByActId(signRules.get(i).getDeputyActId(), memberId);
							if(memberAct != null) {
								map.put("lotteryTimes", memberAct.getAllTimes());
							}
						}
						flag = 1;
						break;
					}
				}
			}
		}
		return map;
	}
	
	/**
	 * 
	 * Description: 概率计算    
	 * @param rewards
	 * @return 
	 * date 2019年5月23日
	 */
	private Map<String, Object> ratioCompute(List<RewardConf> rewards){
		Map<String, Object> map = new HashMap<String, Object>();
		RewardConf rc = new RewardConf();
		if(rewards != null && rewards.size() > 0) {
			//判读是否有奖品
			int rewardNum = 0;
			Float ratioCount = 0.0f;
			for(int j = 0;j < rewards.size();j++) {
				rewardNum += rewards.get(j).getRewardNum();
				ratioCount += rewards.get(j).getRatio();
			}
			if(rewardNum > 0) {
				//抽奖
				Integer ratioNum = 10000;
				if(ratioCount > 100) {
					ratioNum = (int)(ratioCount * 100);
				}
				int i = 0;
				rc.setRewardNum(0);
				Random r = new Random();
				Float ratio = (float)r.nextInt(ratioNum) / 100;
				Float min = 0f;
				Float max = rewards.get(0).getRatio();
				for(i = 0;i < rewards.size();i++) {
					if(ratio >= min && ratio <= max) {
						if(rewards.get(i).getRewardNum() > 0) {
							map.put("reward", createReward(rewards.get(i)));
						}
						rc = rewards.get(i);
						map.put("rewardConf", rc);
						break;
					} else {
						if(i + 1 == rewards.size()) {
							if(ratioCount >= 100) {
								if(rewards.get(i).getRewardNum() > 0) {
									map.put("reward", createReward(rewards.get(i)));
								}
								rc = rewards.get(i);
								map.put("rewardConf", rc);
							}
							break;
						}else {
							min = max;
							max += rewards.get(i + 1).getRatio();
						}
					}
				}
				map.put("index", i);
			}
		}
		
		return map;
	}
	
	/**
	 * 
	 * Description: 手机号规则验证    
	 * @param mobile
	 * @return 
	 * date 2019年6月26日
	 */
	private boolean mobileVerify(String mobile) {
		String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
		if(mobile.matches(regex)) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 
	 * Description:  
	 * @param rewards
	 * @param reword
	 * @return 
	 * date 2019年8月15日
	 */
	private String createDice(List<RewardConf> rewards, String rewardStr, Integer rewardId) {
		//随机数
		Random random = new Random();
		List<Integer> dices = new ArrayList<Integer>();
		List<Integer> dices2 = new ArrayList<Integer>();
		String diceStr = "";
		if(rewardStr == null || "".equals(rewardStr)) {
			//没有中奖则随机生成一串数字
			boolean flag = true;
			while(flag) {
				dices2 = new ArrayList<Integer>();
				for(int i = 0;i < 6;i++) {
					dices2.add(random.nextInt(6) + 1);
				}
//				dices2.add(6);
//				dices2.add(6);
//				dices2.add(4);
//				dices2.add(6);
//				dices2.add(4);
//				dices2.add(6);
				//判断随机生成的数字有没对应的规则，如有重新生成
				if(rewards != null) {
					List<Integer> dices3 = new ArrayList<Integer>(dices2);
					int zeroNum = 0;
					int count = 0;
					for(RewardConf reward : rewards) {
						String[] expands = reward.getExpand().split("\\|");
						boolean isEqual = false;
						for(String expand : expands) {
							dices3 = new ArrayList<Integer>(dices2);
							zeroNum = 0;
							String[] diceRule = expand.split(",");
							for(int i = 0;i < diceRule.length;i++) {
								if(Integer.parseInt(diceRule[i]) == 0) {
									zeroNum++;
									continue;
								}else {
									for(int j = 0;j < dices3.size();j++) {
										if(dices3.get(j) == Integer.parseInt(diceRule[i])) {
											dices3.remove(j);
											break;
										}
									}
								}
							}
							if(dices3.size() == zeroNum) {
								isEqual = true;
								break;
							}
						}
						if(isEqual) {
							count++;
							break;
						}
					}
					if(count == 0) {
						flag = false;
					}
				}
			}
			//拼接字符串
			for(int i = 0;i < dices2.size();i++) {
				if(i == 0) {
					diceStr += dices2.get(i);
				}else {
					diceStr += "," + dices2.get(i);
				}
			}
		}else {
			//有中奖根据中奖规则生成
			RewardConf reward = new RewardConf();
			for(RewardConf r : rewards) {
				if(r.getId() == rewardId) {
					reward = r;
					break;
				}
			}
			String[] expands = reward.getExpand().split("\\|");
			String[] dices3 = expands[random.nextInt(expands.length)].split(",");
//			List<Integer> include = new ArrayList<Integer>();
			//生成1-6数组
			for(int i = 1;i <= 6;i++) {
				dices.add(i);
			}
			//去除规则存在的数字
			for(String d3 : dices3) {
				if(Integer.parseInt(d3) == 0) {
					continue;
				}
				for(int i = 0;i < dices.size();i++) {
					if(Integer.parseInt(d3) == dices.get(i)) {
						dices.remove(i);
						break;
					}
				}
			}
			//生成色子数
			for(String d3 : dices3) {
				if(Integer.parseInt(d3) != 0) {
					dices2.add(Integer.parseInt(d3));
				}else {
					dices2.add(dices.get(random.nextInt(dices.size())));
				}
			}
			//随机打乱顺序
			int r = 0;
			for(int i = 0;i < 6;i++) {
				r = random.nextInt(dices2.size());
				if(i == 0) {
					diceStr += dices2.get(r);
				}else {
					diceStr += "," + dices2.get(r);
				}
				dices2.remove(r);
			}
		}
		return diceStr;
	}


}
