package com.activity.manager.coupon.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.coupon.entity.Coupon;
import com.activity.manager.coupon.mapper.CouponMapper;
import com.activity.manager.coupon.service.CouponSerivce;
import com.activity.manager.security.entity.SysUser;

/**
 * 
 * <p>Description: 优惠券更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */
@Service
public class CouponServiceImpl implements CouponSerivce{

	@Autowired
	private CouponMapper couponMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: queryCoupon</p>   
	 * <p>Description: 分页查询</p>   
	 * @param coupon
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponSerivce#queryCoupon(com.activity.manager.coupon.entity.Coupon, int, int)
	 */
	@Override
	public List<Coupon> queryCoupon(Coupon coupon, int start, int pageSize) {
		return couponMapper.queryCoupon(coupon, start, pageSize);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: couponCount</p>   
	 * <p>Description: 查询总数</p>   
	 * @param coupon
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponSerivce#couponCount(com.activity.manager.coupon.entity.Coupon)
	 */
	@Override
	public Integer couponCount(Coupon coupon) {
		return couponMapper.couponCount(coupon);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: addCoupon</p>   
	 * <p>Description: 添加优惠券</p>   
	 * @param coupon   
	 * @see com.activity.manager.coupon.service.CouponSerivce#addCoupon(com.activity.manager.coupon.entity.Coupon)
	 */
	@Override
	public void addCoupon(Coupon coupon) {
		/*
		 * Assert.notNull(coupon.getMoney(), "优惠券金额数据不存在");
		 * Assert.notNull(coupon.getType(), "优惠券类型数据不存在");
		 * Assert.notNull(coupon.getStartTime(), "优惠券开始时间数据不存在");
		 * Assert.notNull(coupon.getEndTime(), "优惠券结束时间数据不存在");
		 */
		Assert.hasText(coupon.getTitle(), "优惠券主题数据不存在");
		Assert.hasText(coupon.getCouponImg(), "优惠券图片数据不存在");
		coupon.setCreateTime(new Date());
		SysUser user = CurrentParamUtil.getCurrentUser();
		coupon.setCreatePerson(user.getId());
		couponMapper.insert(coupon);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editCoupon</p>   
	 * <p>Description: 修改优惠券</p>   
	 * @param coupon   
	 * @see com.activity.manager.coupon.service.CouponSerivce#editCoupon(com.activity.manager.coupon.entity.Coupon)
	 */
	@Override
	public void editCoupon(Coupon coupon) {
		Assert.notNull(coupon.getId(), "优惠券ID数据不存在");
		Assert.hasText(coupon.getTitle(), "优惠券主题数据不存在");
		Assert.hasText(coupon.getCouponImg(), "优惠券图片数据不存在");
		Coupon cp = couponMapper.selectByPrimaryKey(coupon.getId());
		Assert.notNull(cp, "优惠券数据不存在");
		coupon.setCreateTime(cp.getCreateTime());
		coupon.setCreatePerson(cp.getCreatePerson());
		couponMapper.updateByPrimaryKey(coupon);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: delCoupon</p>   
	 * <p>Description: 批量删除</p>   
	 * @param ids   
	 * @see com.activity.manager.coupon.service.CouponSerivce#delCoupon(com.sun.tools.javac.util.List)
	 */
	@Override
	public void delCoupon(List<String> ids) {
		Assert.notNull(ids, "优惠券ID数据不存在");
		couponMapper.delCoupon(ids);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editIsUser</p>   
	 * <p>Description: 修改状态</p>   
	 * @param id
	 * @param isUser   
	 * @see com.activity.manager.coupon.service.CouponSerivce#editIsUser(java.lang.Integer, boolean)
	 */
	@Override
	public void editIsUse(Integer id, boolean isUse) {
		Assert.notNull(id, "优惠券ID数据不存在");
		couponMapper.updateStatus(id, isUse);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editIsShow</p>   
	 * <p>Description: 修改是否显示</p>   
	 * @param id
	 * @param isShow   
	 * @see com.activity.manager.coupon.service.CouponSerivce#editIsShow(java.lang.Integer, boolean)
	 */
	@Override
	public void editIsShow(Integer id, boolean isShow) {
		Assert.notNull(id, "优惠券ID数据不存在");
		couponMapper.updateIsShow(id, isShow);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editIsTime</p>   
	 * <p>Description: 修改是否时间限制</p>   
	 * @param id
	 * @param isTime   
	 * @see com.activity.manager.coupon.service.CouponSerivce#editIsTime(java.lang.Integer, boolean)
	 */
	@Override
	public void editIsTime(Integer id, boolean isTime) {
		Assert.notNull(id, "优惠券ID数据不存在");
		couponMapper.updateIsTime(id, isTime);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByUse</p>   
	 * <p>Description: 获取有效优惠券</p>   
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponSerivce#selectByUse()
	 */
	@Override
	public List<Coupon> selectByUse() {
		return couponMapper.selectByUse();
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectById</p>   
	 * <p>Description: 根据ID获取优惠券</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponSerivce#selectById(java.lang.Integer)
	 */
	@Override
	public Coupon selectById(Integer id) {
		return couponMapper.selectByPrimaryKey(id);
	}

	

	
}
