package com.activity.manager.coupon.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.coupon.entity.Coupon;
import com.activity.manager.coupon.entity.CouponApply;
import com.activity.manager.coupon.mapper.CouponApplyMapper;
import com.activity.manager.coupon.mapper.CouponMapper;
import com.activity.manager.coupon.service.CouponApplyService;
import com.activity.manager.security.entity.SysUser;

/**
 * 
 * <p>Description: 优惠券申请更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */
@Service
public class CouponApplyServiceImpl implements CouponApplyService {

	@Autowired
	private CouponApplyMapper couponApplyMapper;
	
	@Autowired
	private CouponMapper couponMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: queryCouponApply</p>   
	 * <p>Description: 分页查询</p>   
	 * @param couponApply
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponApplyService#queryCouponApply(com.activity.manager.coupon.entity.CouponApply, int, int)
	 */
	@Override
	public List<CouponApply> queryCouponApply(CouponApply couponApply, int start, int pageSize) {
		return couponApplyMapper.queryCouponApply(couponApply, start, pageSize);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: couponApplyCount</p>   
	 * <p>Description: 查询总数</p>   
	 * @param couponApply
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponApplyService#couponApplyCount(com.activity.manager.coupon.entity.CouponApply)
	 */
	@Override
	public Integer couponApplyCount(CouponApply couponApply) {
		return couponApplyMapper.couponApplyCount(couponApply);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateStatus</p>   
	 * <p>Description: 修改状态</p>   
	 * @param id
	 * @param status   
	 * @see com.activity.manager.coupon.service.CouponApplyService#updateStatus(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void updateStatus(CouponApply couponApply) {
		Assert.notNull(couponApply.getId(), "优惠券申请ID数据不存在");
		Assert.notNull(couponApply.getStatus(), "优惠券申请状态数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		couponApply.setExaminer(user.getId());
		couponApplyMapper.updateStatus(couponApply);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: addApply</p>   
	 * <p>Description: 添加优惠券</p>   
	 * @param couponApply   
	 * @see com.activity.manager.coupon.service.CouponApplyService#addApply(com.activity.manager.coupon.entity.CouponApply)
	 */
	@Override
	public String addApply(CouponApply couponApply) {
		Assert.notNull(couponApply.getCouponId(), "优惠券ID数据不存在");
		Assert.hasText(couponApply.getName(), "优惠券账号数据不存在");
		//判断是否有申领次数限制
		Coupon coupon = couponMapper.selectByPrimaryKey(couponApply.getCouponId());
		Assert.notNull(coupon, "优惠券数据不存在");
		if(coupon.getIsUse() == false) {
			return "申领失败，该优惠活动未启用不能申领";
		}
		if(coupon.getIsTriesLimit()) {
			//获取该账号申领次数
			Integer count = couponApplyMapper.countApplyByName(couponApply);
			if(count >= coupon.getTriesLimit()) {
				return "申领失败，今日该优惠活动申领已达次数";
			}
		}
		//查看是否有时间限制
		if(coupon.getIsTimeLimit()) {
			Date date = new Date();
			if(date.getTime() > coupon.getEndTime().getTime()) {
				return "申领失败，该优惠活动已结束";
			}
		}
		couponApply.setApplyTime(new Date());
		couponApply.setStatus(0);
		couponApplyMapper.insert(couponApply);
		return null;
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateAllStatus</p>   
	 * <p>Description: 修改全部未审核申请状态</p>   
	 * @param couponApply   
	 * @see com.activity.manager.coupon.service.CouponApplyService#updateAllStatus(com.activity.manager.coupon.entity.CouponApply)
	 */
	@Override
	public void updateAllStatus(CouponApply couponApply) {
		Assert.notNull(couponApply.getStatus(), "优惠券申请状态数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		couponApply.setExaminer(user.getId());
		couponApplyMapper.updateAllStatus(couponApply);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateStatusByIds</p>   
	 * <p>Description: 批量修改状态</p>   
	 * @param status
	 * @param ids   
	 * @see com.activity.manager.coupon.service.CouponApplyService#updateStatusByIds(java.lang.Integer, java.util.List)
	 */
	@Override
	public void updateStatusByIds(Integer status, List<String> ids) {
		Assert.notNull(status, "优惠券申请状态数据不存在");
		Assert.notNull(ids, "优惠券申请ID数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		couponApplyMapper.updateStatusByIds(status, user.getId(), ids);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: impExcel</p>   
	 * <p>Description: 批量导入</p>   
	 * @param workbook
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponApplyService#impExcel(org.apache.poi.ss.usermodel.Workbook)
	 */
	@Override
	@Transactional
	public String impExcel(Workbook workbook) {
		List<CouponApply> list = new ArrayList<CouponApply>();
		if(workbook != null) {
			//获得当前sheet工作表
            Sheet sheet = workbook.getSheetAt(0);
            //获得当前sheet的开始行
            int firstRowNum  = sheet.getFirstRowNum();
            //获得当前sheet的结束行
            int lastRowNum = sheet.getLastRowNum();
            SysUser user = CurrentParamUtil.getCurrentUser();
            Map<String, Integer> countMap = new HashMap<String, Integer>();
            Date date = new Date();
            //循环除了第一行的所有行
            for(int rowNum = firstRowNum+1;rowNum <= lastRowNum;rowNum++){ //为了过滤到第一行因为我的第一行是数据库的列
                //获得当前行
                Row row = sheet.getRow(rowNum);
                if(row == null){
                    continue;
                }
                //获取每列数据
                Cell cell = row.getCell(0);
                String name = ExcelUtil.getCellValue(cell).trim();
                cell = row.getCell(1);
                String couponId = ExcelUtil.getCellValue(cell).trim();
                if(name == null || "".equals(name)) {
    				//会员账号为空不做插入
    				return "导入失败，第" + (rowNum + 1) + "行，账号为空"; 
                }else if(couponId == null || "".equals(couponId)) {
    				return "导入失败，第" + (rowNum + 1) + "行，优惠券ID为空"; 
    			}else {
    				//查询优惠券是否存在
    				Coupon coupon = couponMapper.selectByPrimaryKey(Integer.parseInt(couponId));
    				if(coupon == null) {
    					return "导入失败，第" + (rowNum + 1) + "行，该优惠券不存在"; 
    				}
    				CouponApply couponApply = new CouponApply();
    				couponApply.setCouponId(Integer.parseInt(couponId));
    				couponApply.setName(name);
    				couponApply.setApplyTime(new Date());
    				couponApply.setStatus(1);
    				couponApply.setExaminer(user.getId());
    				couponApply.setExamineTime(date);
    				
    				//判断是否启用
    				if(!coupon.getIsUse()) {
    					return "导入失败，第" + (rowNum + 1) + "行优惠活动未启用不能申领";
    				}
    				//判断限制次数
    				if(coupon.getIsTriesLimit()) {
    					if(countMap.get(name + couponId) != null) {
        					countMap.put(name + couponId, countMap.get(name + couponId) + 1);
        				}else {
        					//查询已有申请次数
        					Integer count = couponApplyMapper.countApplyByName(couponApply);
        					countMap.put(name + couponId, count + 1);
        				}
        				if(countMap.get(name + couponId) > coupon.getTriesLimit()) {
        					return "导入失败，第" + (rowNum + 1) + "行申请次数超过限制次数";
        				}
    				}
    				//判断时间
    				if(coupon.getIsTimeLimit()) {
    					if(date.getTime() > coupon.getEndTime().getTime()) {
    						return "导入失败，第" + (rowNum + 1) + "行优惠活动已结束";
    					}
    				}
    			}
            }
		}
		for(CouponApply apply : list) {
			couponApplyMapper.insert(apply);
		}
		return "";
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: expordExcel</p>   
	 * <p>Description: 导出优惠券申请</p>   
	 * @param couponApply
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponApplyService#expordExcel(com.activity.manager.coupon.entity.CouponApply)
	 */
	@Override
	public HSSFWorkbook expordExcel(CouponApply couponApply) {
		//查询优惠券申请
		Integer count = couponApplyMapper.couponApplyCount(couponApply);
		List<CouponApply> list = couponApplyMapper.queryCouponApply(couponApply, 0, count);
		// 声明一个工作薄        
        HSSFWorkbook workbook = new HSSFWorkbook();
        //创建一个Excel表单,参数为sheet的名字
        HSSFSheet sheet = workbook.createSheet("中奖纪录");
        //创建表头
        setTitle(workbook, sheet);
        //新增数据行，并且设置单元格数据
        HSSFRow hssfRow = sheet.createRow(1);
        int rows = 1;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for(CouponApply apply : list) {
        	hssfRow = sheet.createRow(rows);
        	hssfRow.createCell(0).setCellValue(apply.getId());
        	hssfRow.createCell(1).setCellValue(apply.getName());
        	hssfRow.createCell(2).setCellValue(apply.getCouponId());
        	hssfRow.createCell(3).setCellValue(apply.getCoupon().getTitle());
//        	String util = "元";
//        	if(apply.getCoupon().getType() == 1) {
//        		util = "折";
//        	}
//        	hssfRow.createCell(4).setCellValue(apply.getCoupon().getMoney() + util);
//        	hssfRow.createCell(5).setCellValue(apply.getCoupon().getUseCondition());
        	hssfRow.createCell(4).setCellValue(format.format(apply.getApplyTime()));
        	if(apply.getStatus() == 0) {
        		hssfRow.createCell(5).setCellValue("未审核");
        	}else if(apply.getStatus() == 1) {
        		hssfRow.createCell(5).setCellValue("审核通过");
        	}else {
        		hssfRow.createCell(5).setCellValue("审核未通过");
        	}
        	if(apply.getExamineTime() != null) {
        		hssfRow.createCell(6).setCellValue(format.format(apply.getExamineTime()));
        	}
        	if(apply.getUser() != null) {
        		hssfRow.createCell(7).setCellValue(apply.getUser().getUserName());
        	}
        	rows++;
        }
		return workbook;
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByName</p>   
	 * <p>Description: 根据账号查询优惠券</p>   
	 * @param couponApply
	 * @return   
	 * @see com.activity.manager.coupon.service.CouponApplyService#selectByName(com.activity.manager.coupon.entity.CouponApply)
	 */
	@Override
	public List<CouponApply> selectByName(CouponApply couponApply) {
		return couponApplyMapper.selectByName(couponApply);
	}
	
	// 创建表头
    private static void setTitle(HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFRow row = sheet.createRow(0);
        // 设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度
        sheet.setColumnWidth(8, 60 * 256);
        // 设置为居中加粗
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        //导出的Excel头部
        String[] headers = { "编号", "申请账号", "优惠活动ID", "优惠活动","申请时间", "状态", "审核时间", "审核人" };
        // 设置表格默认列宽度为20个字节
        sheet.setDefaultColumnWidth((short) 20);
        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
            cell.setCellStyle(style);
        }
    }

    /*
     * 
     *(non-Javadoc)   
     * <p>Title: selectPassByCouponId</p>   
     * <p>Description: 根据优惠id获取审核通过申请 </p>   
     * @param couponId
     * @return   
     * @see com.activity.manager.coupon.service.CouponApplyService#selectPassByCouponId(java.lang.Integer)
     */
	@Override
	public List<String> selectPassByCouponId(Integer couponId) {
		Assert.notNull(couponId, "优惠ID数据不存在");
		if(couponId == 0) {
			//查询全部
			couponId = null;
		}
		List<CouponApply> list = couponApplyMapper.selectPassByCouponId(couponId);
		if(list == null) {
			return null;
		}
		List<String> names = new ArrayList<String>();
		String msg = "";
		for(CouponApply apply : list) {
			msg = apply.getName().substring(0,apply.getName().length() / 2) + "***";
			msg += "账号申领" + apply.getCoupon().getTitle() + "审核通过";
			names.add(msg);
		}
		return names;
	}

}
