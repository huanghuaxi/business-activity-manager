package com.activity.manager.coupon.service;

import java.util.List;

import com.activity.manager.coupon.entity.Coupon;

/**
 * 
 * <p>Description: 加载优惠券类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */
public interface CouponSerivce {
	
	/**
	 * 
	 * Description: 分页查询    
	 * @param coupon
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年7月19日
	 */
	public List<Coupon> queryCoupon(Coupon coupon, int start ,int pageSize);
	
	/**
	 * 
	 * Description: 查询总数    
	 * @param coupon
	 * @return 
	 * date 2019年7月19日
	 */
	public Integer couponCount(Coupon coupon);
	
	/**
	 * 
	 * Description: 添加优惠券    
	 * @param coupon 
	 * date 2019年7月19日
	 */
	public void addCoupon(Coupon coupon);
	
	/**
	 * 
	 * Description: 修改优惠券    
	 * @param coupon 
	 * date 2019年7月19日
	 */
	public void editCoupon(Coupon coupon);
	
	/**
	 * 
	 * Description: 批量删除    
	 * @param ids 
	 * date 2019年7月19日
	 */
	public void delCoupon(List<String> ids);
	
	/**
	 * 
	 * Description: 修改状态    
	 * @param id
	 * @param isUser 
	 * date 2019年7月19日
	 */
	public void editIsUse(Integer id, boolean isUse);
	
	/**
	 * 
	 * Description: 修改是否显示 
	 * @param id
	 * @param isShow 
	 * date 2019年8月7日
	 */
	public void editIsShow(Integer id, boolean isShow);

	/**
	 * 
	 * Description: 修改是否时间限制 
	 * @param id
	 * @param isTime 
	 * date 2019年8月7日
	 */
	public void editIsTime(Integer id, boolean isTime);
	
	/**
	 * 
	 * Description: 获取有效优惠券    
	 * @return 
	 * date 2019年7月19日
	 */
	public List<Coupon> selectByUse();
	
	/**
	 * 
	 * Description: 根据ID获取优惠券 
	 * @param id
	 * @return 
	 * date 2019年8月7日
	 */
	public Coupon selectById(Integer id);
}
