package com.activity.manager.coupon.service;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import com.activity.manager.coupon.entity.Coupon;
import com.activity.manager.coupon.entity.CouponApply;
import com.activity.manager.reward.entity.RewardRecord;

/**
 * 
 * <p>Description: 加载优惠券申请类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */
public interface CouponApplyService {
	
	/**
     * 
     * Description: 分页查询    
     * @param couponApply
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月19日
     */
    public List<CouponApply> queryCouponApply(CouponApply couponApply, int start ,int pageSize);
    
    /**
     * 
     * Description: 查询总数
     * @param couponApply
     * @return 
     * date 2019年7月19日
     */
    public Integer couponApplyCount(CouponApply couponApply);
    
    /**
     * 
     * Description: 修改状态    
     * @param id
     * @param status 
     * date 2019年7月19日
     */
    public void updateStatus(CouponApply couponApply);
    
    /**
     * 
     * Description: 添加优惠券申请    
     * @param couponApply 
     * date 2019年7月22日
     */
    public String addApply(CouponApply couponApply);
    
    /**
     * 
     * Description: 修改全部未审核申请状态   
     * @param couponApply 
     * date 2019年7月22日
     */
    public void updateAllStatus(CouponApply couponApply);
    
    /**
     * 
     * Description: 批量修改状态    
     * @param status
     * @param ids 
     * date 2019年7月22日
     */
    public void updateStatusByIds(Integer status, List<String> ids);
    
    
    /**
     * 
     * Description: 批量导入
     * @param workbook
     * @return 
     * date 2019年7月22日
     */
    public String impExcel(Workbook workbook);
    
    /**
     * 
     * Description: 导出优惠券申请    
     * @param couponApply
     * @return 
     * date 2019年7月22日
     */
    public HSSFWorkbook expordExcel(CouponApply couponApply); 
    
    /**
     * 
     * Description: 根据账号查询优惠券    
     * @param couponApply
     * @return 
     * date 2019年7月22日
     */
    public List<CouponApply> selectByName(CouponApply couponApply);
    
    /**
     * 
     * Description: 根据优惠id获取审核通过申请  
     * @param couponId
     * @return 
     * date 2019年8月8日
     */
    public List<String>selectPassByCouponId(Integer couponId);
	
}
