package com.activity.manager.coupon.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * <p>Description: 优惠券类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */
public class Coupon {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	优惠主题
     */
    private String title;

    /**
     *	类型  0：满减 0：折扣
     */
    private Integer type;

    /**
     *	金额
     */
    private Float money;

    /**
     *	使用条件
     */
    private String useCondition;

    /**
     *	样式
     */
    private String style;

    /**
     *	开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     *	结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     *	是否启用
     */
    private boolean isUse;
    
    /**
     * 	是否显示
     */
    private boolean isShow;

    /**
     *	是否时间限制
     */
    private boolean isTimeLimit;

    /**
     *	排序
     */
    private Integer sort;

    /**
     *	是否限制次数
     */
    private boolean isTriesLimit;

    /**
     *	限制次数
     */
    private Integer triesLimit;

    /**
     *	优惠内容
     */
    private String content;
    
    /**
     * 	优惠图片
     */
    private String couponImg;

    /**
     *	创建时间
     */
    private Date createTime;

    /**
     *	创建人
     */
    private Integer createPerson;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }

    public String getUseCondition() {
        return useCondition;
    }

    public void setUseCondition(String useCondition) {
        this.useCondition = useCondition == null ? null : useCondition.trim();
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style == null ? null : style.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean getIsUse() {
        return isUse;
    }

    public void setIsUse(boolean isUse) {
        this.isUse = isUse;
    }
    
    public boolean getIsShow() {
        return isShow;
    }

    public void setIsShow(boolean isShow) {
        this.isShow = isShow;
    }

    public boolean getIsTimeLimit() {
        return isTimeLimit;
    }

    public void setIsTimeLimit(boolean isTimeLimit) {
        this.isTimeLimit = isTimeLimit;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public boolean getIsTriesLimit() {
        return isTriesLimit;
    }

    public void setIsTriesLimit(boolean isTriesLimit) {
        this.isTriesLimit = isTriesLimit;
    }

    public Integer getTriesLimit() {
        return triesLimit;
    }

    public void setTriesLimit(Integer triesLimit) {
        this.triesLimit = triesLimit;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getCouponImg() {
		return couponImg;
	}

	public void setCouponImg(String couponImg) {
		this.couponImg = couponImg == null ? null : couponImg.trim();
	}

	public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }
}