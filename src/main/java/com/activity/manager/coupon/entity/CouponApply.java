package com.activity.manager.coupon.entity;

import java.util.Date;

import com.activity.manager.security.entity.SysUser;

/**
 * 
 * <p>Description: 优惠券申请类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */
public class CouponApply {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	优惠券ID
     */
    private Integer couponId;

    /**
     *	名称
     */
    private String name;

    /**
     *	手机号
     */
    private String mobile;

    /**
     *	申请时间
     */
    private Date applyTime;

    /**
     *	状态 0：未审核 1：通过 2：未通过
     */
    private Integer status;

    /**
     *	审核人
     */
    private Integer examiner;
    
    /**
     * 	审核时间
     */
    private Date examineTime;
    
    /**
     * 	优惠券
     */
    private Coupon coupon;
    
    /**
     * 	审核人
     */
    private SysUser user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Date getApplyTime() {
        return applyTime;
    }
 
    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getExaminer() {
        return examiner;
    }
 
    public void setExaminer(Integer examiner) {
        this.examiner = examiner;
    }

	public Date getExamineTime() {
		return examineTime;
	}

	public void setExamineTime(Date examineTime) {
		this.examineTime = examineTime;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}
	
}