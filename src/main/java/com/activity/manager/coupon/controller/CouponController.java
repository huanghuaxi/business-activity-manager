package com.activity.manager.coupon.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.coupon.entity.Coupon;
import com.activity.manager.coupon.service.CouponSerivce;

/**
 * 
 * <p>Description: 优惠券控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */

@Controller
@RequestMapping("/coupon")
public class CouponController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CouponSerivce couponService;
	
	@Value("${web.upload.path}")
    private String uploadPath;
	
	/**
	 * 
	 * Description: 跳转到优惠券列表页面    
	 * @return 
	 * date 2019年7月19日
	 */
	@RequestMapping("/toList")
	public String toList() {
		logger.info("跳转到优惠券页面");
		return "coupon/coupon.html";
	}
	
	/**
	 * 
	 * Description: 跳转到优惠券编辑页面
	 * @param id
	 * @param model
	 * @return 
	 * date 2019年8月7日
	 */
	@RequestMapping("/toEdit")
	public String toEdit(Integer id, Model model) {
		try {
			if(id != null) {
				model.addAttribute("id", id);
			}
			logger.info("跳转到优惠券编辑页面");
		}catch (Exception e) {
			logger.error("跳转到优惠券编辑页面异常", e);
		}
		return "coupon/editCoupon.html";
	}
	
	/**
	 * 
	 * Description: 跳转到优惠券图片设置页面
	 * @return 
	 * date 2019年8月7日
	 */
	@RequestMapping("/toCouponBg")
	public String toCouponBg() {
		logger.info("跳转到优惠券图片设置页面");
		return "coupon/couponBg.html";
	}
	
	/**
	 * 
	 * Description: 获取优惠券列表    
	 * @param pageUtil
	 * @param coupon
	 * @return 
	 * date 2019年7月19日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, Coupon coupon) {
		try {
			List<Coupon> cList = couponService.queryCoupon(coupon, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> cLst = formate(cList);
			Integer count = couponService.couponCount(coupon);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(cLst);
			logger.info("获取优惠券列表");
		}catch(Exception e) {
			logger.error("获取优惠券列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增优惠券    
	 * @param coupon
	 * @return 
	 * date 2019年7月19日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(Coupon coupon) {
		try {
			couponService.addCoupon(coupon);
			logger.info("新增优惠券");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增优惠券异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 修改优惠券    
	 * @param coupon
	 * @return 
	 * date 2019年7月19日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(Coupon coupon) {
		try {
			couponService.editCoupon(coupon);
			logger.info("修改优惠券");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("修改优惠券异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 删除优惠券    
	 * @param ids
	 * @return 
	 * date 2019年7月19日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delete(String ids) {
		try {
			Assert.hasText(ids, "优惠券ID数据不存在");
			couponService.delCoupon(Arrays.asList(ids.trim().split(",")));
			logger.info("删除优惠券");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("删除优惠券异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 修改状态    
	 * @param id
	 * @param isUse
	 * @return 
	 * date 2019年7月19日
	 */
	@ResponseBody
	@RequestMapping("/editStatus")
	public ResultMessage editStatus(Integer id, boolean isUse) {
		try {
			couponService.editIsUse(id, isUse);
			logger.info("修改状态");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("修改状态异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 修改是否显示 
	 * @param id
	 * @param isShow
	 * @return 
	 * date 2019年8月7日
	 */
	@ResponseBody
	@RequestMapping("/editIsShow")
	public ResultMessage editIsShow(Integer id, boolean isShow) {
		try {
			couponService.editIsShow(id, isShow);
			logger.info("修改是否显示");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("修改是否显示异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 修改是否时间限制 
	 * @param id
	 * @param isTime
	 * @return 
	 * date 2019年8月7日
	 */
	@ResponseBody
	@RequestMapping("/editisTimeLimit")
	public ResultMessage editisTimeLimit(Integer id, boolean isTimeLimit) {
		try {
			couponService.editIsTime(id, isTimeLimit);
			logger.info("修改是否时间限制");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("修改是否时间限制异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 根据ID获取优惠券 
	 * @param id
	 * @return 
	 * date 2019年8月7日
	 */
	@ResponseBody
	@RequestMapping("/getCouponById")
	public ResultMessage getCouponById(Integer id) {
		try {
			Coupon coupon = couponService.selectById(id);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, coupon);
		}catch (Exception e) {
			logger.error("获取优惠券异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 文件上传    
	 * @param file
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/upload")
	public ResultMessage upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		try {
			if(file.isEmpty()) {
				return ResultMessage.getFail();
			}
			String fileName = file.getOriginalFilename();
			//文件保存地址
			String path = uploadPath;
			path += "couponImg/";
			//新文件名
			String newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf("."));
			File dest = new File(path + newFileName);
			logger.info("文件上传路径："+path);
			//判断文件父目录是否存在
			if(!dest.getParentFile().exists()){ 
				logger.info(path + "targetPath is not exist");  
				dest.getParentFile().mkdirs(); 
			}
			file.transferTo(dest);
			logger.info("文件上传成功");
			//文件访问地址
			String url = "http://" + request.getServerName() + ":" 
					+ request.getServerPort()  + "/uploads/couponImg/" + newFileName;
			logger.info("前端页面访问路径："+url);
			logger.info("优惠券图片上传结束");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, url);
		}catch(Exception e) {
			logger.error("文件上传异常", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<Coupon> list){
		List<Object> aLst = new ArrayList<Object>();
		list.forEach(coupon -> {
			aLst.add(coupon);
		});
		return aLst;
	}
}
