package com.activity.manager.coupon.controller;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.coupon.entity.CouponApply;
import com.activity.manager.coupon.service.CouponApplyService;

/**
 * 
 * <p>Description: 优惠券申请控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月22日   
 * @version 1.0
 */

@Controller
@RequestMapping("/couponApply")
public class CouponApplyController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CouponApplyService couponApplyService;
	
	/**
	 * 
	 * Description: 跳转到优惠券申请页面   
	 * @return 
	 * date 2019年7月22日
	 */
	@RequestMapping("/toList")
	public String toList(Integer couponId, Model model) {
		logger.info("跳转到优惠券申请页面");
		model.addAttribute("couponId", couponId);
		return "coupon/couponApply.html";
	}
	
	/**
	 * 
	 * Description: 获取优惠券申请列表    
	 * @param pageUtil
	 * @param couponApply
	 * @return 
	 * date 2019年7月22日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, CouponApply couponApply) {
		try {
			List<CouponApply> cList = couponApplyService.queryCouponApply(couponApply, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> cLst = formate(cList);
			Integer count = couponApplyService.couponApplyCount(couponApply);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(cLst);
			logger.info("获取优惠券申请列表");
		}catch (Exception e) {
			logger.error("获取优惠券申请列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 修改优惠券申请状态    
	 * @param couponApply
	 * @return 
	 * date 2019年7月22日
	 */
	@ResponseBody
	@RequestMapping("/editStatus")
	public ResultMessage editStatus(CouponApply couponApply) {
		try {
			couponApplyService.updateStatus(couponApply);
			logger.info("修改优惠券申请状态");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("修改优惠券申请状态异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 修改全部未审核申请状态    
	 * @param couponApply
	 * @return 
	 * date 2019年7月22日
	 */
	@ResponseBody
	@RequestMapping("/editAllStatus")
	public ResultMessage editAllStatus(CouponApply couponApply, String ids) {
		try {
			if(ids != null && !"".equals(ids)) {
				couponApplyService.updateStatusByIds(couponApply.getStatus(), Arrays.asList(ids.trim().split(",")));
			}else {
				couponApplyService.updateAllStatus(couponApply);
				logger.info("修改全部未审核申请状态");
			}
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("修改全部未审核申请状态异常", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 优惠券申请批量导入    
	 * @param file
	 * @return 
	 * date 2019年7月22日
	 */
	@ResponseBody
	@RequestMapping("/impExcel")
	public ResultMessage impExcel(@RequestParam("file") MultipartFile file) {
		try {
			//读取Excel数据内容
			Workbook workbook = ExcelUtil.getWorkBook(file);
			String msg = couponApplyService.impExcel(workbook);
			logger.info("优惠券申请批量导入 ");
			if(!"".equals(msg)) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}else {
				return ResultMessage.getSuccess();
			}
		}catch(Exception e) {
			logger.error("优惠券申请批量导入失败", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 导出优惠券申请记录   
	 * @param couponApply
	 * @param response
	 * @param request 
	 * date 2019年7月22日
	 */
	@RequestMapping("/expordExcel")
	public void expordExcel(CouponApply couponApply, HttpServletResponse response, HttpServletRequest request) {
		try {
			HSSFWorkbook workbook = couponApplyService.expordExcel(couponApply);
			String fileName = "优惠活动申请记录_" + new Date().getTime() + ".xls";
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {    
				fileName = URLEncoder.encode(fileName, "UTF-8");// IE浏览器    
			}else{    
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			}
			//清空response  
            response.reset();  
            //设置response的Header  
            response.addHeader("Content-Disposition", "attachment;filename="+ fileName);  
            OutputStream os = new BufferedOutputStream(response.getOutputStream());  
            response.setContentType("application/vnd.ms-excel;charset=utf-8"); 
            //将excel写入到输出流中
            workbook.write(os);
            os.flush();
            os.close();
            logger.info("导出优惠券申请记录");
		}catch(Exception e) {
			logger.error("优惠券申请记录导出异常", e);
		}
	}
	
	private List<Object> formate(List<CouponApply> list){
		List<Object> aLst = new ArrayList<Object>();
		list.forEach(coupon -> {
			aLst.add(coupon);
		});
		return aLst;
	}
	
}
