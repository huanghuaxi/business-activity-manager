package com.activity.manager.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.usermodel.Workbook;

import com.activity.manager.coupon.entity.Coupon;
import com.activity.manager.coupon.entity.CouponApply;

/**
 * 
 * <p>Description: 加载优惠券类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月19日   
 * @version 1.0
 */
@Mapper
public interface CouponApplyMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(CouponApply record);

    CouponApply selectByPrimaryKey(Integer id);

    List<CouponApply> selectAll();

    int updateByPrimaryKey(CouponApply record);
    
    /**
     * 
     * Description: 分页查询    
     * @param couponApply
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月19日
     */
    List<CouponApply> queryCouponApply(CouponApply couponApply, int start ,int pageSize);
    
    /**
     * 
     * Description: 查询总数
     * @param couponApply
     * @return 
     * date 2019年7月19日
     */
    Integer couponApplyCount(CouponApply couponApply);
    
    /**
     * 
     * Description: 批量删除   
     * @param ids 
     * date 2019年7月19日
     */
    void delCouponApply(List<String> ids);
    
    /**
     * 
     * Description: 修改状态    
     * @param id
     * @param status 
     * date 2019年7月19日
     */
    void updateStatus(CouponApply couponApply);
    
    /**
     * 
     * Description: 修改全部未审核申请状态   
     * @param couponApply 
     * date 2019年7月22日
     */
    void updateAllStatus(CouponApply couponApply);
    
    /**
     * 
     * Description: 批量修改状态   
     * @param status
     * @param examiner
     * @param ids 
     * date 2019年7月22日
     */
    void updateStatusByIds(Integer status, Integer examiner, List<String> ids);

    /**
     * 
     * Description: 根据账号查询优惠券    
     * @param couponApply
     * @return 
     * date 2019年7月22日
     */
    List<CouponApply> selectByName(CouponApply couponApply);
    
    /**
     * 
     * Description: 统计申领次数 
     * @param couponApply
     * @return 
     * date 2019年8月8日
     */
    Integer countApplyByName(CouponApply couponApply);
    
    /**
     * 
     * Description: 根据优惠id获取审核通过申请 
     * @param couponId
     * @return 
     * date 2019年8月8日
     */
    List<CouponApply> selectPassByCouponId(@Param(value="couponId")Integer couponId);
    
}