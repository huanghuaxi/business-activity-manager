package com.activity.manager.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.activity.manager.coupon.entity.Coupon;

/**
 * 
 * <p>Description: 加载优惠券类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年7月19日   
 * @version 1.0
 */
@Mapper
public interface CouponMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(Coupon record);

    Coupon selectByPrimaryKey(Integer id);

    List<Coupon> selectAll();

    int updateByPrimaryKey(Coupon record);
    
    /**
     * 
     * Description: 分页查询    
     * @param coupon
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月19日
     */
    List<Coupon> queryCoupon(Coupon coupon, int start ,int pageSize);
    
    /**
     * 
     * Description: 查询总数
     * @param coupon
     * @return 
     * date 2019年7月19日
     */
    Integer couponCount(Coupon coupon);
    
    /**
     * 
     * Description: 批量删除   
     * @param ids 
     * date 2019年7月19日
     */
    void delCoupon(List<String> ids);
    
    /**
     * 
     * Description: 修改状态    
     * @param id
     * @param isUse 
     * date 2019年7月19日
     */
    void updateStatus(Integer id, boolean isUse);
    
    /**
     * 
     * Description: 修改是否显示 
     * @param id
     * @param isShow 
     * date 2019年8月7日
     */
    void updateIsShow(Integer id, boolean isShow);
    
    /**
     * 
     * Description: 修改是否时间限制 
     * @param id
     * @param isTime 
     * date 2019年8月7日
     */
    void updateIsTime(Integer id, boolean isTimeLimit);
    
    /**
     * 
     * Description: 获取有效优惠券    
     * @return 
     * date 2019年7月19日
     */
    List<Coupon> selectByUse();
}