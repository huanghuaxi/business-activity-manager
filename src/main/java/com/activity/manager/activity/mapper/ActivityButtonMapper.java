package com.activity.manager.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.activity.entity.ActivityButton;

/**
 * 
 * <p>Description: 加载活动按钮类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */

@Mapper
public interface ActivityButtonMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(ActivityButton record);

    ActivityButton selectByPrimaryKey(Integer id);

    List<ActivityButton> selectAll();

    int updateByPrimaryKey(ActivityButton record);
    

    /**
     * 
     * Description: 根据版本ID获取按钮    
     * @param verId
     * @return 
     * date 2019年4月13日
     */
    List<ActivityButton> selectByVersion(@Param("verId") Integer verId);
    
    /**
     * Description: 删除版本按钮    
     * @param id 
     * date 2019年4月17日
     */
    void delBtnsByVerId(Integer id);
    
    /**
     * Description: 修改版本按钮   
     * @param record 
     * date 2019年4月17日
     */
    void updateActivityBtnById(ActivityButton record);
    
    /**
     * Description:  查询该版本下的该按钮  
     * @param verId
     * @param id
     * @return 
     * date 2019年4月17日
     */
    ActivityButton  selectByVerAndId(@Param("verId") Integer verId,@Param("id") Integer id);
}