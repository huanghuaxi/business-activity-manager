package com.activity.manager.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.activity.entity.BusiActivity;

@Mapper
public interface BusiActivityMapper {

	Integer deleteByPrimaryKey(Integer id);

    Integer insert(BusiActivity record);

    BusiActivity selectByPrimaryKey(Integer id);

    List<BusiActivity> selectAll();

    Integer updateByPrimaryKey(BusiActivity record);
    
    /**
     * Description：查询所有活动
     * @date 2019年4月9日 
     * @param activityName
     * @param start
     * @param pageSize
     * @return
     */
    List<BusiActivity> queryActivity(BusiActivity activity,int start ,int pageSize);
    
    /**
	 * Description：统计所有活动
	 * @date 2019年4月9日 
	 * @param activityName
	 * @return
	 */
	Integer activityCount(BusiActivity activity);
	
	/**
	 * Description: 删除活动
	 * @date 2019年4月9日    
	 * @param ids
	 * @return
	 */
	void delActivity(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 修改状态    
	 * @param activity 
	 * date 2019年4月24日
	 */
	void updateState(BusiActivity activity);
    
	/**
	 * 
	 * Description: 根据域名查询活动    
	 * @param bangDomain
	 * @return 
	 * date 2019年5月7日
	 */
	BusiActivity selectByBangDomain(@Param("bangDomain") String bangDomain);
	
	/**
	 * 
	 * Description: 根据域名统计活动    
	 * @param bangDomain
	 * @return 
	 * date 2019年5月15日
	 */
	Integer countByBangDomain(@Param("bangDomain") String bangDomain);
	
	/**
	 * 
	 * Description: 查询配套活动列表    
	 * @param actId
	 * @return 
	 * date 2019年5月20日
	 */
	List<BusiActivity> selectByIsSecondary(Integer actId);
	
	/**
	 * 
	 * Description: 根据活动类型获取活动
	 * @param activity
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年6月3日
	 */
	List<BusiActivity> selectByActId(BusiActivity activity, int start, int pageSize);
	
	/**
	 * 
	 * Description: 获取最新活动    
	 * @return 
	 * date 2019年6月25日
	 */
	List<BusiActivity> selectNewAct();
	
	/**
	 * 
	 * Description: 修改分享        
	 * @param activity 
	 * date 2019年7月2日
	 */
	void updateShare(BusiActivity activity);
	
}