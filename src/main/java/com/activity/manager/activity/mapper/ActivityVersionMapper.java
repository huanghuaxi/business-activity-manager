package com.activity.manager.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.activity.entity.ActivityVersion;

/**
 * 
 * <p>Description: 加载活动版本类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */

@Mapper
public interface ActivityVersionMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(ActivityVersion record);

    ActivityVersion selectByPrimaryKey(Integer id);

    List<ActivityVersion> selectAll();

    int updateByPrimaryKey(ActivityVersion record);
    
    /**
     * 
     * Description: 根据活动类型ID获取版本    
     * @param actId
     * @return 
     * date 2019年4月15日
     */
    List<ActivityVersion> selectByActId(Integer actId);
    
    /**
     * Description: 更新版本信息 
     * @param record 
     * date 2019年4月17日
     */
    void updateVerById(ActivityVersion record);
    
    /**
     * Description: 查询版本    
     * @param verName
     * @param actId
     * @return 
     * date 2019年4月18日
     */
    List<ActivityVersion> selectVerByActIdAndVerName(@Param("verName") String verName,@Param("actId") String actId);
}