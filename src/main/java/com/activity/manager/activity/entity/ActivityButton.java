package com.activity.manager.activity.entity;

/**
 * 
 * <p>Description: 活动按钮类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
public class ActivityButton {
    /**
     * ID
     */
    private Integer id;

    /**
     * 按钮名称
     */
    private String btnName;
    
    /**
     * 按钮内容
     */
    private String btnValue;

    /**
     * 按钮样式
     */
    private String btnStyle;

    /**
     * 按钮地址
     */
    private String btnUrl;

    /**
     * 版本ID
     */
    private Integer verId;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private Integer createPersion;
    
    public int getLAY_TABLE_INDEX() {
		return LAY_TABLE_INDEX;
	}

	public void setLAY_TABLE_INDEX(int lAY_TABLE_INDEX) {
		LAY_TABLE_INDEX = lAY_TABLE_INDEX;
	}

	private int LAY_TABLE_INDEX;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBtnName() {
        return btnName;
    }

    public void setBtnName(String btnName) {
        this.btnName = btnName == null ? null : btnName.trim();
    }
    
    public String getBtnValue() {
		return btnValue;
	}

	public void setBtnValue(String btnValue) {
		this.btnValue = btnValue == null ? null : btnValue.trim();
	}

	public String getBtnStyle() {
        return btnStyle;
    }

    public void setBtnStyle(String btnStyle) {
        this.btnStyle = btnStyle == null ? null : btnStyle.trim();
    }

    public String getBtnUrl() {
        return btnUrl;
    }

    public void setBtnUrl(String btnUrl) {
        this.btnUrl = btnUrl == null ? null : btnUrl.trim();
    }

    public Integer getVerId() {
        return verId;
    }

    public void setVerId(Integer verId) {
        this.verId = verId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getCreatePersion() {
        return createPersion;
    }

    public void setCreatePersion(Integer createPersion) {
        this.createPersion = createPersion;
    }
}