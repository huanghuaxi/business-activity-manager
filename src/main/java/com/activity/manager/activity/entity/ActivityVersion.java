package com.activity.manager.activity.entity;

import java.util.List;

/**
 * 
 * <p>Description: 活动版本类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
public class ActivityVersion {
    /**
     * ID
     */
    private Integer id;

    /**
     * 版本
     */
    private String version;

    /**
     * 活动版本访问路径
     */
    private String verUrl;

    /**
     * 活动类型ID
     */
    private Integer activityId;
    
    /**
     * 活动按钮
     */
    private List<ActivityButton> buttons;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    public String getVerUrl() {
        return verUrl;
    }

    public void setVerUrl(String verUrl) {
        this.verUrl = verUrl == null ? null : verUrl.trim();
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

	public List<ActivityButton> getButtons() {
		return buttons;
	}

	public void setButtons(List<ActivityButton> buttons) {
		this.buttons = buttons;
	}

}