/**
 * 
 */
package com.activity.manager.activity.entity;

/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0   
 */
public class BusiActivityDTO {
	
	/**
	 * ID
	 */
	private Integer id;

    /**
     * 活动名称
     */
    private String actName;

    /**
     * 版本ID
     */
    private Integer verId;

    /**
     * 绑定域名
     */
    private String bangDomain;

    /**
     * 开始时间
     */
    private String beginTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 活动类型ID
     */
    private Integer actId;

    /**
     * 背景图片
     */
    private String backGroundImg;

    /**
     * 公告
     */
    private String announcement;

    /**
     * 活动规则
     */
    private String actRule;

    /**
     * 免责声明
     */
    private String statement;
    
    /**
     * 	是否启用
     */
    private Boolean isUsed;
    
    /**
     * 版本
     */
    private String version;
    
    /**
     * 
     */
    private String btnName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getActName() {
		return actName;
	}

	public void setActName(String actName) {
		this.actName = actName == null ? null : actName.trim();
	}

	public Integer getVerId() {
		return verId;
	}

	public void setVerId(Integer verId) {
		this.verId = verId;
	}

	public String getBangDomain() {
		return bangDomain;
	}

	public void setBangDomain(String bangDomain) {
		this.bangDomain = bangDomain == null ? null : bangDomain.trim();
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime == null ? null : beginTime.trim();
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime == null ? null : endTime.trim();
	}

	public Integer getActId() {
		return actId;
	}

	public void setActId(Integer actId) {
		this.actId = actId;
	}

	public String getBackGroundImg() {
		return backGroundImg;
	}

	public void setBackGroundImg(String backGroundImg) {
		this.backGroundImg = backGroundImg == null ? null : backGroundImg.trim();
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement == null ? null : announcement.trim();
	}

	public String getActRule() {
		return actRule;
	}

	public void setActRule(String actRule) {
		this.actRule = actRule == null ? null : actRule.trim();
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement == null ? null : statement.trim();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBtnName() {
		return btnName;
	}

	public void setBtnName(String btnName) {
		this.btnName = btnName == null ? null : btnName.trim();
	}

	public Boolean getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Boolean isUsed) {
		this.isUsed = isUsed;
	}
	
}
