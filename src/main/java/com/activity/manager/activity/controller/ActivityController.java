package com.activity.manager.activity.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.security.exception.DelActivityException;

import lombok.extern.slf4j.Slf4j;

/**
 * Description: 活动管理控制类
 * Copyright: Copyright (c) 2019    
 * @author zhangxin   
 * @date 2019年4月9日   
 * @version 1.0   
 */

@Controller
@Slf4j
@RequestMapping("/activity")
public class ActivityController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到活动管理页面   
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月13日
	 */
	@RequestMapping("/list")
	public String toActivityList(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动类型ID数据不存在");
			model.addAttribute("actId", actId);
			
		}catch(Exception e) {
			logger.error("活动管理页面跳转异常", e);
		}
		return "activitymanage/activity/list.html";
	}
	
	/**
	 * 
	 * Description: 跳转到新增编辑页面   
	 * @param id
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月13日
	 */
	@RequestMapping("/toSave")
	public String toSave(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动类型ID数据不存在");
			model.addAttribute("actId", actId);
		}catch(Exception e) {
			logger.error("活动新增编辑页面跳转异常", e);
		}
		return "activitymanage/activity/saveOrEdit.html";
	}
	
	/**
	 * 
	 * Description: 跳转到新增编辑页面    
	 * @param id
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月23日
	 */
	@RequestMapping("/toEdit")
	public String toEdit(Integer id, Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动类型ID数据不存在");
			Assert.notNull(id, "活动ID数据不存在");
			model.addAttribute("id", id);
			model.addAttribute("actId", actId);
		}catch(Exception e) {
			logger.error("活动新增编辑页面跳转异常", e);
		}
		return "activitymanage/activity/saveOrEdit.html";
	}
	
	
	/**
	 * 
	 * Description: 获取活动列表   
	 * @param pageUtil
	 * @param activity
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, BusiActivity activity) {
		try {
			Assert.notNull(activity.getActId(), "活动类型ID数据不存在");
			List<BusiActivity> aList = activityService.queryActivity(activity, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> aLst = formate(aList);
			Integer count = activityService.activityCount(activity);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(aLst);
		}catch(Exception e) {
			logger.error("活动列表获取异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 根据ID获取活动   
	 * @param id
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/getActivityById")
	public ResultMessage getActivityById(Integer id) {
		try {
			BusiActivity activity = activityService.getActivityById(id);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, activity);
		}catch(Exception e) {
			logger.error("获取活动数据异常", e);
			return ResultMessage.getFail();
		}
	}
	
	
	/**
	 * 
	 * Description: 新增活动   
	 * @param activity
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(BusiActivity activity) {
		try {
			String msg = "";
			msg = activityService.saveActivity(activity);
			if(msg != null && !"".equals(msg)) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("活动保存异常", e);
			ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 编辑活动    
	 * @param activity
	 * @return 
	 * date 2019年5月15日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(BusiActivity activity) {
		try {
			String msg = "";
			msg = activityService.editActivity(activity);
			if(msg != null && !"".equals(msg)) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("活动保存异常", e);
			ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 删除活动   
	 * @param ids
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delActivity(String ids) {
		try {
			Assert.hasText(ids, "活动ID数据不存在");
			activityService.deleteActivity(Arrays.asList(ids.split(",")));
		}catch(DelActivityException e) {
			logger.info(e.getMessage());
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, e.getMessage(), null);
		}catch(Exception e) {
			logger.error("活动删除异常",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 获取活动页面地址   
	 * @param id
	 * @param model
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/getActivityUrl")
	public ResultMessage getActivityUrl(Integer id, Model model) {
		try {
			Assert.notNull(id, "活动ID数据不存在");
			//生成活动地址
			String url = activityService.getActivityUrl(id);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, url);
		}catch(Exception e) {
			logger.error("获取活动地址失败", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 修改活动状态    
	 * @param id
	 * @param isUsed
	 * @return 
	 * date 2019年4月24日
	 */
	@ResponseBody
	@RequestMapping("/editState")
	public ResultMessage editState(Integer id, Boolean isUsed) {
		try {
			activityService.editState(id, isUsed);
			logger.info("修改活动状态");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("活动状态修改异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取活动列表   
	 * @return 
	 * date 2019年5月7日
	 */
	@ResponseBody
	@RequestMapping("getActivityList")
	public ResultMessage getActivityList() {
		try {
			List<BusiActivity> activityList = activityService.getActivityList();
			logger.info("获取活动列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, activityList);
		}catch(Exception e) {
			logger.error("获取活动列表异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取配套活动列表    
	 * @param actId
	 * @return 
	 * date 2019年5月20日
	 */
	@ResponseBody
	@RequestMapping("getSecondaryList")
	public ResultMessage getSecondaryList(Integer actId) {
		try {
			List<BusiActivity> activityList = activityService.getSecondaryList(actId);
			logger.info("获取配套活动列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, activityList);
		}catch(Exception e) {
			logger.error("获取配套活动列表异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 修改活动分享    
	 * @param id
	 * @param isShare
	 * @return 
	 * date 2019年7月2日
	 */
	@ResponseBody
	@RequestMapping("/editShare")
	public ResultMessage editShare(Integer id, boolean isShare) {
		try {
			activityService.editShare(id, isShare);
			logger.info("修改活动分享");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("活动状态分享异常", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<BusiActivity> list){
		List<Object> aLst = new ArrayList<Object>();
		list.forEach(activity -> {
			aLst.add(activity);
		});
		return aLst;
	}
	
}
