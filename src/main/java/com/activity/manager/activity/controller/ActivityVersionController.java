/**
 * 
 */
package com.activity.manager.activity.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.activity.entity.ActivityVersion;
import com.activity.manager.activity.service.ActivityVersionService;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;

/**    
 * <p>Description: 活动版本控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/activityVersion")
public class ActivityVersionController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ActivityVersionService versionService;
	
	/**
	 * 
	 * Description: 根据活动类型ID获取版本   
	 * @param actId
	 * @return 
	 * date 2019年4月15日
	 */
	@ResponseBody
	@RequestMapping("/getVersionList")
	public ResultMessage getVersionList(Integer actId) {
		try {
			Assert.notNull(actId, "活动类型ID数据不存在");
			List<ActivityVersion> versions = versionService.selectByActId(actId);
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, versions);
		}catch (Exception e) {
			logger.error("获取活动版本列表失败", e);
			return ResultMessage.getFail();
		}
	}
	
}
