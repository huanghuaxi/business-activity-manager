/**
 * 
 */
package com.activity.manager.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.activity.manager.activity.entity.ActivityVersion;
import com.activity.manager.activity.mapper.ActivityVersionMapper;
import com.activity.manager.activity.service.ActivityVersionService;

/**    
 * <p>Description: 活动版本更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0   
 */
@Service
public class ActivityVersionServiceImpl implements ActivityVersionService{

	@Autowired
	private ActivityVersionMapper versionMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: selectByActId</p>   
	 * <p>Description: 根据活动类型ID获取版本</p>   
	 * @param actId
	 * @return   
	 * @see com.activity.manager.activity.service.ActivityVersionService#selectByActId(java.lang.Integer)   
	 */
	@Override
	public List<ActivityVersion> selectByActId(Integer actId) {
		return versionMapper.selectByActId(actId);
	}

}
