package com.activity.manager.activity.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.mapper.BusiActivityMapper;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.member.mapper.MemberActivityMapper;
import com.activity.manager.member.mapper.SignRecordMapper;
import com.activity.manager.reward.mapper.InnerRewardMapper;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.reward.mapper.RewardRecordMapper;
import com.activity.manager.rule.entity.SignRule;
import com.activity.manager.rule.mapper.SignRuleMapper;
import com.activity.manager.security.entity.SysUser;
import com.activity.manager.security.exception.DelActivityException;

/**
 * 
 * Description: 商家活动更新类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author zhangxin   
 * @date 2019年4月9日   
 * @version 1.0
 */

@Service
public class BusiActivityServiceImpl implements BusiActivityService{

	
	@Autowired
	private BusiActivityMapper activityMapper;
	
	@Autowired
	private SignRuleMapper signRuleMapper;
	
	@Autowired
	private RewardRecordMapper rewardRecordMapper;
	
	@Autowired
	private InnerRewardMapper innerMapper;
	
	@Autowired
	private MemberActivityMapper memberActMapper;
	
	@Autowired
	private RewardConfMapper rewardMapper;
	
	@Autowired
	private SignRecordMapper signRecordMapper;
	
	@Value("${activity.page.ip}")
    private String activityPageIp;
	@Value("${activity.page.url}")
	private String activityPageUrl;
	@Value("${activity.page.path}")
	private String activityPagePath;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: queryActivity</p>   
	 * <p>Description: 查询所有活动</p>   
	 * @param activity
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.BusiActivityService#queryActivity(com.activity.manager.security.entity.BusiActivity, int, int)
	 */
	@Override
	public List<BusiActivity> queryActivity(BusiActivity activity, int start, int pageSize) {
		return activityMapper.queryActivity(activity, start, pageSize);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: activityCount</p>   
	 * <p>Description: 统计所有活动</p>   
	 * @param activity
	 * @return   
	 * @see com.activity.manager.security.service.BusiActivityService#activityCount(com.activity.manager.security.entity.BusiActivity)
	 */
	@Override
	public Integer activityCount(BusiActivity activity) {
		return activityMapper.activityCount(activity);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getActivityById</p>   
	 * <p>Description: 根据ID获取活动</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.security.service.BusiActivityService#getActivityById(java.lang.Integer)
	 */
	@Override
	public BusiActivity getActivityById(Integer id) {
		return activityMapper.selectByPrimaryKey(id);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveActivity</p>   
	 * <p>Description: 添加活动</p>   
	 * @param activity   
	 * @see com.activity.manager.security.service.BusiActivityService#saveActivity(com.activity.manager.security.entity.BusiActivity)
	 */
	@Override
	public String saveActivity(BusiActivity activity) {
		Assert.hasText(activity.getActName(), "活动名称数据不存在");
		Assert.notNull(activity.getVerId(), "活动版本数据不存在");
		Assert.notNull(activity.getActId(), "活动类型ID不存在");
		Assert.hasText(activity.getBeginTime(), "活动开始时间不存在");
		Assert.hasText(activity.getEndTime(), "活动结束时间不存在");
		//查看域名是否重复
		if(activity.getBangDomain() != null && !"".equals(activity.getBangDomain())) {
			int count = activityMapper.countByBangDomain(activity.getBangDomain());
			if(count > 0) {
				return "失败，域名不能重复";
			}
		}
		activity.setIsUsed(true);
		activity.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		SysUser user = CurrentParamUtil.getCurrentUser();
		activity.setCreatePerson(user.getId());
		activityMapper.insert(activity);
		return "";
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editActivity</p>   
	 * <p>Description: 修改活动</p>   
	 * @param activity   
	 * @see com.activity.manager.security.service.BusiActivityService#editActivity(com.activity.manager.security.entity.BusiActivity)
	 */
	@Override
	public String editActivity(BusiActivity activity) {
		Assert.notNull(activity.getId(), "活动ID数据不存在");
		Assert.hasText(activity.getActName(), "活动名称数据不存在");
		Assert.notNull(activity.getVerId(), "活动版本数据不存在");
		Assert.notNull(activity.getActId(), "活动类型ID不存在");
		Assert.hasText(activity.getBeginTime(), "活动开始时间不存在");
		Assert.hasText(activity.getEndTime(), "活动结束时间不存在");
		BusiActivity act = activityMapper.selectByPrimaryKey(activity.getId());
		Assert.notNull(act, "活动数据不存在");
		//查看域名是否重复
		if(activity.getBangDomain() != null && !"".equals(activity.getBangDomain())
				&& !activity.getBangDomain().equals(act.getBangDomain())) {
			int count = activityMapper.countByBangDomain(activity.getBangDomain());
			if(count > 0) {
				return "失败，域名不能重复";
			}
		}
		act.setActName(activity.getActName());
		act.setVerId(activity.getVerId());
		act.setBangDomain(activity.getBangDomain());
		act.setBeginTime(activity.getBeginTime());
		act.setEndTime(activity.getEndTime());
		act.setActRule(activity.getActRule());
		act.setAnnouncement(activity.getAnnouncement());
		act.setBackGroundImg(activity.getBackGroundImg());
		act.setIsShare(activity.getIsShare());
		activityMapper.updateByPrimaryKey(act);
		return "";
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: deleteActivity</p>   
	 * <p>Description: 删除活动</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.BusiActivityService#deleteActivity(java.util.List)
	 */
	@Override
	@Transactional
	public void deleteActivity(List<String> ids) {
		Assert.notNull(ids, "活动ID数据为空");
		String msg = "";
		BusiActivity activity = new BusiActivity();
		for(String id : ids) {
			//获取活动信息
			activity = activityMapper.selectByPrimaryKey(Integer.parseInt(id));
			if(activity == null) {
				continue;
			}
			//判读是否有被主活动绑定
			if(activity.getIsSecondary()) {
				List<SignRule> signRules = signRuleMapper.selectByDeputyActId(activity.getId());
				if(signRules != null && signRules.size() > 0) {
					msg = "删除失败，活动ID为" + id + "该配套活动已被绑定";
					throw new DelActivityException(msg);
				}
			}
			//删除签到规则
			List<SignRule> signRules = signRuleMapper.selectByActId(activity.getId());
			if(signRules != null && signRules.size() > 0) {
				List<String> signRuleIds = new ArrayList<String>();
				List<String> rewardIds = new ArrayList<String>();
				for(SignRule rule : signRules) {
					if(rule.getRewardId() != null && !"".equals(rule.getRewardId().trim())) {
						List<String> rIds = Arrays.asList(rule.getRewardId().trim().split(","));
						for(String rewardId : rIds) {
							rewardIds.add(rewardId);
						}
					}
					signRuleIds.add(rule.getId().toString());
				}
				//删除规则对应奖品
				rewardMapper.delRewardConf(rewardIds);
				//删除规则
				signRuleMapper.delSignRule(signRuleIds);
				//删除签到记录
				signRecordMapper.delByActId(activity.getId());
			}
			//删除中奖记录
			rewardRecordMapper.delByActId(activity.getId());
			//删除内定会员
			innerMapper.delByActId(activity.getId());
			//删除活动会员
			memberActMapper.delByActId(activity.getId());
			//删除奖品
			rewardMapper.delByActId(activity.getId());
			//删除活动
			activityMapper.deleteByPrimaryKey(activity.getId());
		}
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getActivityUrl</p>   
	 * <p>Description: 生成活动地址</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.security.service.BusiActivityService#getActivityUrl(java.lang.Integer)
	 */
	@Override
	public String getActivityUrl(Integer id) {
//		BusiActivity activity = activityMapper.selectByPrimaryKey(id);
		//根据获取到的版本地址和活动ID生成地址
		String url = activityPageIp + activityPageUrl + activityPagePath + id;
		return url;
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: updateState</p>   
	 * <p>Description: 修改状态</p>   
	 * @param activity   
	 * @see com.activity.manager.activity.service.BusiActivityService#updateState(com.activity.manager.activity.entity.BusiActivity)   
	 */
	@Override
	public void editState(Integer id, Boolean isUsed) {
		Assert.notNull(id, "活动ID数据不存在");
		Assert.notNull(isUsed, "活动状态数据不存在");
		BusiActivity activity = new BusiActivity();
		activity.setId(id);
		activity.setIsUsed(isUsed);
		activityMapper.updateState(activity);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getActivityList</p>   
	 * <p>Description: 获取活动列表</p>   
	 * @return   
	 * @see com.activity.manager.activity.service.BusiActivityService#getActivityList()
	 */
	@Override
	public List<BusiActivity> getActivityList() {
		return activityMapper.selectAll();
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByBangDomain</p>   
	 * <p>Description: 根据域名查询活动</p>   
	 * @param bangDomain
	 * @return   
	 * @see com.activity.manager.activity.service.BusiActivityService#selectByBangDomain(java.lang.String)
	 */
	@Override
	public BusiActivity selectByBangDomain(String bangDomain) {
		return activityMapper.selectByBangDomain(bangDomain);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getSecondaryList</p>   
	 * <p>Description: 获取配套活动列表</p>   
	 * @param actId
	 * @return   
	 * @see com.activity.manager.activity.service.BusiActivityService#getSecondaryList(java.lang.Integer)
	 */
	@Override
	public List<BusiActivity> getSecondaryList(Integer actId) {
		Assert.notNull(actId, "活动ID数据不存在");
		return activityMapper.selectByIsSecondary(actId);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editShare</p>   
	 * <p>Description: 修改分享</p>   
	 * @param id
	 * @param isUsed   
	 * @see com.activity.manager.activity.service.BusiActivityService#editShare(java.lang.Integer, boolean)
	 */
	@Override
	public void editShare(Integer id, boolean isShare) {
		Assert.notNull(id, "活动ID数据不存在");
		Assert.notNull(isShare, "活动分享数据不存在");
		BusiActivity activity = new BusiActivity();
		activity.setId(id);
		activity.setIsShare(isShare);
		activityMapper.updateShare(activity);
	}
	
}
