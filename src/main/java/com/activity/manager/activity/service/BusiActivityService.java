package com.activity.manager.activity.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.activity.manager.activity.entity.BusiActivity;

/**
 * 
 * Description: 加载商家活动类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author zhangxin   
 * @date 2019年4月9日   
 * @version 1.0
 */

public interface BusiActivityService {
	
	/**
	 * 
	 * Description: 查询所有活动    
	 * @param activity
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年4月13日
	 */
	public List<BusiActivity> queryActivity(BusiActivity activity, int start, int pageSize);
	
	/**
	 * 
	 * Description: 统计所有活动   
	 * @param activity
	 * @return 
	 * date 2019年4月13日
	 */
	public Integer activityCount(BusiActivity activity);
	
	/**
	 * 
	 * Description: 根据ID获取活动   
	 * @param id
	 * @return 
	 * date 2019年4月13日
	 */
	public BusiActivity getActivityById(Integer id);
	
	/**
	 * 
	 * Description: 添加活动   
	 * @param activity 
	 * date 2019年4月13日
	 */
	public String saveActivity(BusiActivity activity);
	
	/**
	 * 
	 * Description: 修改活动   
	 * @param activity 
	 * date 2019年4月13日
	 */
	public String editActivity(BusiActivity activity);
	
	/**
	 * 
	 * Description: 删除活动   
	 * @param ids 
	 * date 2019年4月13日
	 */
	public void deleteActivity(List<String> ids);
	
	/**
	 * 
	 * Description: 生成活动地址   
	 * @param id
	 * @return 
	 * date 2019年4月13日
	 */
	public String getActivityUrl(Integer id);
	
	/**
	 * 
	 * Description: 修改状态    
	 * @param activity 
	 * date 2019年4月24日
	 */
	public void editState(Integer id, Boolean isUsed);
	
	/**
	 * 
	 * Description: 获取活动列表    
	 * @return 
	 * date 2019年5月7日
	 */
	public List<BusiActivity> getActivityList();
	

	/**
	 * 
	 * Description: 根据域名查询活动    
	 * @param bangDomain
	 * @return 
	 * date 2019年5月7日
	 */
	public BusiActivity selectByBangDomain(@Param("bangDomain") String bangDomain);
	
	/**
	 * 
	 * Description: 获取配套活动列表    
	 * @param actId
	 * @return 
	 * date 2019年5月20日
	 */
	public List<BusiActivity> getSecondaryList(Integer actId);
	
	/**
	 * 
	 * Description: 修改分享    
	 * @param id
	 * @param isUsed 
	 * date 2019年7月2日
	 */
	public void editShare(Integer id, boolean isShare);
}
