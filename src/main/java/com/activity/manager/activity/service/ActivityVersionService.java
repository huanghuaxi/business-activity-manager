/**
 * 
 */
package com.activity.manager.activity.service;

import java.util.List;

import com.activity.manager.activity.entity.ActivityVersion;

/**    
 * <p>Description: 加载活动版本类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月15日   
 * @version 1.0   
 */
public interface ActivityVersionService {
	
	/**
	 * 
	 * Description: 根据活动类型获取版本    
	 * @param actId
	 * @return 
	 * date 2019年4月15日
	 */
	public List<ActivityVersion> selectByActId(Integer actId);
	
}
