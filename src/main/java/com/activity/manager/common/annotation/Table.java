package com.activity.manager.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: 方法上的注解
 * Copyright: Copyright (c) 2019    
 * @author binghe   
 * @date 2019年4月7日   
 * @version 1.0   
 */

@Target({ElementType.TYPE})  
@Retention(RetentionPolicy.RUNTIME) 
public @interface Table {
	public String value() default "";
}
