package com.activity.manager.common;

/**
 * Description:   编码常量类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月3日   
 * @version 1.0
 */
public interface CodeUtil {
	
	/**
	 * 成功编码
	 */
	public static final String 	SUCCESS_CODE ="000000";

	/**
	 * 成功
	 */
    public static final String SUCCESS_CODE_MSG ="成功"; 
    
    /**
	 * 失败编码
	 */
	public static final String 	FAIL_CODE ="999999";

	/**
	 * 用户存在失败编码
	 */
	public static final String 	FAIL_CODE_USER_ISEXIST ="000001";
	
	/**
	 * 失败
	 */
    public static final String FAIL_CODE_MSG ="失败"; 
    
    /**
     * layui规范-成功响应码
     */
    public static final String LAYUI_SUCCESS_CODE = "0";
   
    /**
     * layui规范-成功响信息
     */
    public static final String LAYUI_SUCCESS_CODE_MSG = "数据返回成功";
    
    /**
	 * 用户存在失败信息
	 */
	public static final String 	FAIL_CODE_USER_ISEXIST_MSG ="用户已存在,请重新填写";
}
