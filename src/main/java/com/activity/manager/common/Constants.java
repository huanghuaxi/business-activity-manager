package com.activity.manager.common;

/**
 * Description:   常量类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月3日   
 * @version 1.0
 */
public interface Constants {
	/**
	 * Times New Roman 字体样式
	 */
	public static final String 	FONT_TIMES_NEW_ROMAN ="Fixedsys";
	
	/**
	 * 图片格式
	 */
	public static final String 	IMAGE_FORMAT ="jpeg";
	
	/**
	 * 数值--200
	 */
	public static final int NUM_TYPE_200 = 200;
	
	/**
	 * 数值--250
	 */
	public static final int NUM_TYPE_250 = 250;
	
	/**
	 * 数值--160
	 */
	public static final int NUM_TYPE_160 = 160;
	
	/**
	 * 数值--160
	 */
	public static final int NUM_TYPE_155 = 155;
	
	/**
	 * 数值--20
	 */
	public static final int NUM_TYPE_20 = 20;
	
	/**
	 * 数值--12
	 */
	public static final int NUM_TYPE_12 = 12;
	
	/**
	 * 数值--110
	 */
	public static final int NUM_TYPE_110 = 110;
	
	/**
	 * 数值--13
	 */
	public static final int NUM_TYPE_13 = 13;
	
	/**
	 * 数值--6
	 */
	public static final int NUM_TYPE_6 = 6;
	
	/**
	 * 数值--6
	 */
	public static final int NUM_TYPE_16 = 16;
	
	/**
	 * 数值--0
	 */
	public static final int NUM_TYPE_0 = 0;
	
	/**
	 * 数值--255
	 */
	public static final int NUM_TYPE_255 = 255;
	
	/**
	 * 数值--1
	 */
	public static final int NUM_TYPE_1 = 1;
	
	/**
	 * 验证码缓存键名
	 */
	public final static String SESSION_KEY = "SESSION_KEY_IMAGE_CODE";
	
	/**
	 * 用户无效标识
	 */
	public final static String AUTH_MESSAGE_DISABLE = "auth.message.disable";
	
	/**
	 * 密码无效标识
	 */
	public final static String AUTH_MESSAGE_CREDENTIALS = "auth.message.credentials";
	
	/**
	 * 账户过期标识
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_EXPIRED = "auth.message.account.expired";
	
	/**
	 * 密码过期标识
	 */
	public final static String AUTH_MESSAGE_PWD_EXPIRED = "auth.message.pwd.expired";
	
	/**
	 * 用户被锁标识
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_LOCKED = "auth.message.account.locked";
	
	/**
	 * 验证码为空标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_NULL = "auth.message.validatecode.null";
	
	/**
	 * 验证码不存在标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_NOTEXIST = "auth.message.validatecode.notexist";
	
	/**
	 * 验证码过期标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_EXPIRED = "auth.message.validatecode.expired";
	
	/**
	 *  验证码不正确标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_INCORRECT = "auth.message.validatecode.incorrect";

	/**
	 * 用户被锁
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_IS_LOCKED = "auth.message.account.is.locked";
	
	/**
	 *  图片码
	 */
	public final static String IMAGE_CODE = "imageCode";
	
	/**
	 *  post 
	 */
	public final static String POST = "post";
	
	/**
	 * 密码错误标识
	 */
	public final static String BAD_CREDENTIALS = "Bad credentials";
	
	/**
	 * 用户无效标识
	 */
	public final static String USER_IS_DISABLED = "User is disabled";
	
	/**
	 * 用户过期标识
	 */
	public final static String USER_ACCOUNT_HAS_EXPIRED = "User account has expired";
	
	/**
	 * 密码过期标识
	 */
	public final static String USER_CREDENTIALS_HAVE_EXPIRED = "User credentials have expired";
	
	/**
	 * 用户被锁
	 */
	public final static String USER_ACCOUNT_IS_LOCKED = "User account is locked";
	
	/**
	 * 用户不存在
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_NOTEXIST = "auth.message.account.notexist";
	
	/**
	 * 用户不存在
	 */
	public final static String USER_NOT_EXIST = "user not exist";
	
	/**
	 * 资源访问路径
	 */
	public final static String  SOURCE_FILE = "file:";
	
	/**
	 * 活动会员按钮
	 */
	public final static String  BUTTON_NAME_MEMBER = "活动会员";
	
	/**
	 * 中奖记录
	 */
	public final static String BUTTON_NAME_RECORD = "中奖记录";
	
	/**
	 * 奖品配置
	 */
	public final static String BUTTON_NAME_REWARD_CONF = "奖品配置";
	
	/**
	 * 奖品内定
	 */
	public final static String BUTTON_NAME_INNER_REWARD = "奖品内定";
}
