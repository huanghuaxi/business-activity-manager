package com.activity.manager.common;

/**
 * 
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年5月23日   
 * @version 1.0
 */
public class CertificateUtil {
	
	//盐
	private static final String SALT = "activity_index"; 
	
	/**
	 * 
	 * Description: 生成证书    
	 * @return 
	 * date 2019年5月23日
	 */
	public static String createCertificate() {
		//证书
		String certificate = "";
		//MAC地址
		String macAddress = "";
		//硬盘序列号
		String Identifier = "";
		try {
			// 判断是Linux还是Windows
			if (ComputerInfoUtil.isLinux()) {
				// Linux操作系统
				macAddress = ComputerInfoUtil.getMACAddressByLinux();
//				System.out.println("Linux macAddress: " + macAddress);
				Identifier = ComputerInfoUtil.getIdentifierByLinux();
//				System.out.println("Linux Identifier: " + Identifier);
			} else {
				// Windows操作系统
				macAddress = ComputerInfoUtil.getMACAddressByWindows();
//				System.out.println("Windows macAddress: " + macAddress);
				Identifier = ComputerInfoUtil.getIdentifierByWindows();
//				System.out.println("Windows Identifier: " + Identifier);
			}
			//原数据
			String originalData = macAddress + Identifier + SALT;
			certificate = MD5Util.encode(originalData);
//			System.out.println("证书编码：" + certificate);
		}catch(Exception e) {
//			System.out.println("证书生成失败");
			e.printStackTrace();
		}
		return certificate;
	}
	
	/**
	 * 
	 * Description: 测试    
	 * @param a
	 * @throws Exception 
	 * date 2019年5月23日
	 */
	public static void main(String[] a) throws Exception {
		String certificate = createCertificate();
//		System.out.println("证书编码：" + certificate);
	}

}
