package com.activity.manager.common;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 
 * <p>Description: 获取计算机信息</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年5月23日   
 * @version 1.0
 */
public class ComputerInfoUtil {
	
	/**
	 * 
	 * Description: 判断操作系统是Windows还是Linux    
	 * @return 
	 * date 2019年5月23日
	 */
	public static Boolean isLinux() {
		String os = System.getProperty("os.name");
		System.out.println("os.name: " + os);
		return !os.toLowerCase().startsWith("win");
	}

	/**
	 * 
	 * Description: Linux获取MAC地址    
	 * @return
	 * @throws Exception 
	 * date 2019年5月23日
	 */
	public static String getMACAddressByLinux() throws Exception {
		String[] cmd = { "ifconfig" };
		Process process = Runtime.getRuntime().exec(cmd);
		process.waitFor();
		BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		String str1 = sb.toString();
		String str2 = str1.split("ether")[1].trim();
		String result = str2.split("txqueuelen")[0].trim();
//		System.out.println("Linux MacAddress is: " + result);
		br.close();

		return result;
	}
	
	/**
	 * 
	 * Description: Linux获取硬盘序列号    
	 * @return
	 * @throws Exception 
	 * date 2019年5月23日
	 */
	public static String getIdentifierByLinux() throws Exception {
		String[] cmd = { "fdisk", "-l" };

		Process process = Runtime.getRuntime().exec(cmd);
		process.waitFor();

		BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}

		String str1 = sb.toString();
		String str2 = str1.split("identifier:")[1].trim();
		String result = str2.split("Device Boot")[0].trim();
//		System.out.println("Linux Identifier is: " + result);
		br.close();

		return result;
	}
	
	/**
	 * 
	 * Description: windows获取MAC地址: (默认获取第一张网卡)   
	 * @return
	 * @throws Exception 
	 * date 2019年5月23日
	 */
	public static String getMACAddressByWindows() throws Exception {
		String result = "";
		Process process = Runtime.getRuntime().exec("ipconfig /all");
		BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"));

		String line;
		int index = -1;
		while ((line = br.readLine()) != null) {
			index = line.toLowerCase().indexOf("物理地址");
			if (index >= 0) {// 找到了
				index = line.indexOf(":");
				if (index >= 0) {
					result = line.substring(index + 1).trim();
				}
				break;
			}
		}
//		System.out.println("Windows MACAddress is: " + result);
		br.close();
		return result;
	}

	/**
	 * 
	 * Description: windows获取硬盘序列号: (默认获取C盘)   
	 * @return
	 * @throws Exception 
	 * date 2019年5月23日
	 */
	public static String getIdentifierByWindows() throws Exception {
		String result = "";
		Process process = Runtime.getRuntime().exec("cmd /c dir C:");
		BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"));

		String line;
		while ((line = br.readLine()) != null) {
			if (line.indexOf("卷的序列号是 ") != -1) {
				result = line.substring(line.indexOf("卷的序列号是 ") + "卷的序列号是 ".length(), line.length());
				break;
			}
		}
//		System.out.println("Windows Identifier is: " + result);
		br.close();
		return result;
	}
	
	/**
	 * 
	 * Description: 测试    
	 * @param a
	 * @throws Exception 
	 * date 2019年5月23日
	 */
	public static void main(String[] a) throws Exception {
		// 判断是Linux还是Windows
		if (isLinux()) {
			// Linux操作系统
			String macAddress = getMACAddressByLinux();
			System.out.println("Linux macAddress: " + macAddress);
			String Identifier = getIdentifierByLinux();
			System.out.println("Linux Identifier: " + Identifier);
		} else {
			// Windows操作系统
			String macAddress = getMACAddressByWindows();
			System.out.println("Windows macAddress: " + macAddress);
			String Identifier = getIdentifierByWindows();
			System.out.println("Windows Identifier: " + Identifier);
		}
	}
	
}
