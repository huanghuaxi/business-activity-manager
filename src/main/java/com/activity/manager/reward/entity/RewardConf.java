package com.activity.manager.reward.entity;

import java.util.Date;

/**
 * 
 * <p>Description: 奖品配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
public class RewardConf {

	/**
	 * ID
	 */
	private Integer id;
	
	/**
	 * 活动ID
	 */
	private Integer actId;
	
	/**
	 * 奖项
	 */
	private String rewardOption;
	
	/**
	 * 奖品
	 */
	private String rewardCon;
	
	/**
	 * 概率
	 */
	private Float ratio;
	/**
	 * 奖品数量
	 */
	private Integer rewardNum;
	/**
	 * 该奖品中奖数
	 */
	private Integer winNum;
	/**
	 * 奖品图片
	 */
	private String rewardImg;
	
	/**
	 * 	单位
	 */
	private String unit;
	
	/**
	 * 	创建时间
	 */
	private Date createTime;
	
	/**
	 * 	创建人
	 */
	private Integer createPerson;
	
	/**
	 * 	扩展字段
	 */
	private String expand;

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getActId() {
		return actId;
	}


	public void setActId(Integer actId) {
		this.actId = actId;
	}


	public String getRewardOption() {
		return rewardOption;
	}


	public void setRewardOption(String rewardOption) {
		this.rewardOption = rewardOption == null ? null : rewardOption.trim();
	}


	public String getRewardCon() {
		return rewardCon;
	}


	public void setRewardCon(String rewardCon) {
		this.rewardCon = rewardCon == null ? null : rewardCon.trim();
	}


	public Float getRatio() {
		return ratio;
	}


	public void setRatio(Float ratio) {
		this.ratio = ratio;
	}


	public Integer getRewardNum() {
		return rewardNum;
	}


	public void setRewardNum(Integer rewardNum) {
		this.rewardNum = rewardNum;
	}


	public Integer getWinNum() {
		return winNum;
	}


	public void setWinNum(Integer winNum) {
		this.winNum = winNum;
	}


	public String getRewardImg() {
		return rewardImg;
	}


	public void setRewardImg(String rewardImg) {
		this.rewardImg = rewardImg == null ? null : rewardImg.trim();
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit == null ? null : unit.trim();
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public Integer getCreatePerson() {
		return createPerson;
	}


	public void setCreatePerson(Integer createPerson) {
		this.createPerson = createPerson;
	}

	public String getExpand() {
		return expand;
	}

	public void setExpand(String expand) {
		this.expand = expand;
	}
	
}