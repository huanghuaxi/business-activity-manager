/**
 * 
 */
package com.activity.manager.reward.entity;


/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年4月11日   
 * @version 1.0   
 */
public class InnerRewardDTO {
	
	private Integer id;

    /**
     *	会员ID
     */
    private Integer remId;

    /**
     *	奖项ID
     */
    private Integer rewardId;

    /**
     *	活动ID
     */
    
    private Integer actId;
    
    /**
     * 	是否适用
     */
    private Integer isUsed;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPerson;
    
    /**
     * 	会员账号
     */
    private String memberName;
    
    /**
     * 	奖项名称
     */
    private String rewardOption;
    
    /**
     * 	奖品
     */
    private String rewardCon;
    
    /**
     * 	会员类型
     */
    private Integer type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRemId() {
		return remId;
	}

	public void setRemId(Integer remId) {
		this.remId = remId;
	}

	public Integer getRewardId() {
		return rewardId;
	}

	public void setRewardId(Integer rewardId) {
		this.rewardId = rewardId;
	}

	public Integer getActId() {
		return actId;
	}

	public void setActId(Integer actId) {
		this.actId = actId;
	}

	public Integer getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime == null ? null : createTime.trim();
	}

	public Integer getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(Integer createPerson) {
		this.createPerson = createPerson;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName == null ? null : memberName.trim();
	}

	public String getRewardOption() {
		return rewardOption;
	}

	public void setRewardOption(String rewardOption) {
		this.rewardOption = rewardOption == null ? null : rewardOption.trim();
	}

	public String getRewardCon() {
		return rewardCon;
	}

	public void setRewardCon(String rewardCon) {
		this.rewardCon = rewardCon == null ? null : rewardCon.trim();
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
