package com.activity.manager.reward.entity;

import com.activity.manager.activity.entity.BusiActivity;

/**
 * 
 * <p>Description: 中奖纪录类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月12日   
 * @version 1.0
 */
public class RewardRecord {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	活动ID
     */
    private Integer actId;

    /**
     *	奖品ID
     */
    private Integer rewardId;
    
    /**
     *	 奖项
     */
    private String rewardOption;
    
    /**
     * 	奖品
     */
    private String rewardCon;

    /**
     *	会员ID
     */
    private Integer remId;

    /**
     *	会员账号
     */
    private String remName;

    /**
     *	是否派奖
     */
    private Integer isSend;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPersion;
    
    /**
     * 	是否中奖
     */
    private Boolean isGit;
    
    /**
     *	活动
     */
    private BusiActivity activity;
    
    /**
     *	主活动ID
     */
    private Integer centralActId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActId() {
		return actId;
	}

	public void setActId(Integer actId) {
		this.actId = actId;
	}

	public Integer getRewardId() {
		return rewardId;
	}

	public void setRewardId(Integer rewardId) {
		this.rewardId = rewardId;
	}

	public String getRewardOption() {
		return rewardOption;
	}

	public void setRewardOption(String rewardOption) {
		this.rewardOption = rewardOption == null ? null : rewardOption.trim();
	}

	public String getRewardCon() {
		return rewardCon;
	}

	public void setRewardCon(String rewardCon) {
		this.rewardCon = rewardCon == null ? null : rewardCon.trim();
	}

	public Integer getRemId() {
		return remId;
	}

	public void setRemId(Integer remId) {
		this.remId = remId;
	}

	public String getRemName() {
		return remName;
	}

	public void setRemName(String remName) {
		this.remName = remName == null ? null : remName.trim();
	}

	public Integer getIsSend() {
		return isSend;
	}

	public void setIsSend(Integer isSend) {
		this.isSend = isSend;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime == null ? null : createTime.trim();
	}

	public Integer getCreatePersion() {
		return createPersion;
	}

	public void setCreatePersion(Integer createPersion) {
		this.createPersion = createPersion;
	}

	public Boolean getIsGit() {
		return isGit;
	}

	public void setIsGit(Boolean isGit) {
		this.isGit = isGit;
	}

	public Integer getCentralActId() {
		return centralActId;
	}

	public void setCentralActId(Integer centralActId) {
		this.centralActId = centralActId;
	}

	public BusiActivity getActivity() {
		return activity;
	}

	public void setActivity(BusiActivity activity) {
		this.activity = activity;
	}
    
}