package com.activity.manager.reward.entity;

import com.activity.manager.member.entity.Member;

/**
 * 
 * <p>Description: 内定会员类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */

public class InnerReward {
    
    private Integer id;

    /**
     *	会员ID
     */
    private Integer remId;

    /**
     *	奖项ID
     */
    private Integer rewardId;

    /**
     *	活动ID
     */
    
    private Integer actId;
    
    /**
     * 	是否适用
     */
    private Integer isUsed;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPerson;
    
    /**
     * 	会员
     */
    private Member member;
    
    /**
     * 	奖品
     */
    private RewardConf rewardConf;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRemId() {
		return remId;
	}

	public void setRemId(Integer remId) {
		this.remId = remId;
	}

	public Integer getRewardId() {
		return rewardId;
	}

	public void setRewardId(Integer rewardId) {
		this.rewardId = rewardId;
	}

	public Integer getActId() {
		return actId;
	}

	public void setActId(Integer actId) {
		this.actId = actId;
	}

	public Integer getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime == null ? null : createTime.trim();
	}

	public Integer getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(Integer createPerson) {
		this.createPerson = createPerson;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public RewardConf getRewardConf() {
		return rewardConf;
	}

	public void setRewardConf(RewardConf rewardConf) {
		this.rewardConf = rewardConf;
	}

}