package com.activity.manager.reward.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.reward.entity.RewardConf;

/**
 * 
 * <p>Description: 加载奖品配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
@Mapper
public interface RewardConfMapper {

	int deleteByPrimaryKey(Integer id);

	int insert(RewardConf record);

	RewardConf selectByPrimaryKey(Integer id);

	List<RewardConf> selectAll();

	int updateByPrimaryKey(RewardConf record);
	
	/**
     * Description：查询所有奖品
     * @date 2019年4月11日 
     * @param rewardConf
     * @param start
     * @param pageSize
     * @return
     */
    List<RewardConf> queryRewardConf(RewardConf rewardConf,int start ,int pageSize);
    
    /**
	 * Description：统计所有奖品
	 * @date 2019年4月11日 
	 * @param rewardConf
	 * @return
	 */
	Integer RewardConfCount(RewardConf rewardConf);
	
	/**
	 * Description: 删除奖品
	 * @date 2019年4月11日    
	 * @param ids
	 * @return
	 */
	void delRewardConf(@Param("ids") List<String> ids);
	
	/**
	 * Description: 根据活动ID获取奖品
	 * @date 2019年4月11日    
	 * @param actId
	 * @return
	 */
	List<RewardConf> selectRewardConfByActId(Integer actId);
	
	/**
	 * 
	 * Description: 根据活动ID和奖项查询     
	 * @param actId
	 * @param rewardOption
	 * @return 
	 * date 2019年4月26日
	 */
	List<RewardConf> selectByIdAndOption(Integer actId, String rewardOption);
	
	/**
	 * 
	 * Description: 根据ID获取奖品      
	 * @param ids
	 * @param actId
	 * @return 
	 * date 2019年4月29日
	 */
	List<RewardConf> selectByIds(@Param("ids") List<String> ids, @Param("actId") Integer actId);
	
	/**
	 * 
	 * Description: 查询新增奖品    
	 * @param actId
	 * @param rewardOption
	 * @return 
	 * date 2019年5月7日
	 */
	Integer selectId(Integer actId, String rewardOption);
	
	/**
	 * 
	 * Description: 根据活动ID删除奖品    
	 * @param actId 
	 * date 2019年5月28日
	 */
	void delByActId(Integer actId);
}