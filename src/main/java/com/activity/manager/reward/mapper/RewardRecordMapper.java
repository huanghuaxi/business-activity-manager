package com.activity.manager.reward.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.activity.manager.reward.entity.RewardRecord;

/**
 * 
 * <p>Description: 加载中奖纪录类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月12日   
 * @version 1.0
 */
@Mapper
public interface RewardRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RewardRecord record);

    RewardRecord selectByPrimaryKey(Integer id);

    List<RewardRecord> selectAll();

    int updateByPrimaryKey(RewardRecord record);
    
    /**
     * 
     * Description: 查询所有中奖纪录   
     * @param record
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月12日
     */
    List<RewardRecord> queryRewardRecord(RewardRecord record,int start ,int pageSize);
    
    /**
     * 
     * Description: 统计所有中奖纪录    
     * @param record
     * @return 
     * date 2019年4月12日
     */
	Integer rewardRecordCount(RewardRecord record);
	
	/**
	 * 
	 * Description: 全部派奖    
	 * @param actId 
	 * date 2019年4月12日
	 */
	void allSendReward(Integer actId);
	
	/**
	 * 
	 * Description: 查看会员中奖记录   
	 * @param actId
	 * @param memberId
	 * @param centralActId
	 * @return 
	 * date 2019年4月19日
	 */
	List<RewardRecord> selectByMemberId(Integer actId, Integer memberId, Integer centralActId, Integer pageNo, Integer pageSize);
	
	/**
	 * 
	 * Description: 查看该活动中奖记录   
	 * @param actId
	 * @return 
	 * date 2019年4月19日
	 */
	List<RewardRecord> selectByActId(Integer actId);
	
	/**
	 * 
	 * Description: 获取当天中奖记录    
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年5月16日
	 */
	RewardRecord selectByDay(Integer actId, Integer memberId);
	
	/**
	 * 
	 * Description: 根据活动ID删除中奖纪录   
	 * @param actId 
	 * date 2019年5月28日
	 */
	void delByActId(Integer actId);
}