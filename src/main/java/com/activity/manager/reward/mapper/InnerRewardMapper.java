package com.activity.manager.reward.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.activity.manager.reward.entity.InnerReward;
import com.activity.manager.reward.entity.InnerRewardDTO;

/**
 * 
 * <p>Description: 加载内定会员类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月13日   
 * @version 1.0
 */
@Mapper
public interface InnerRewardMapper {
    
    int deleteByPrimaryKey(Integer id);

    int insert(InnerReward record);

    InnerReward selectByPrimaryKey(Integer id);

    List<InnerReward> selectAll();

    int updateByPrimaryKey(InnerReward record);
    
    /**
     * Description：查询所有内定会员
     * @date 2019年4月11日 
     * @param inner
     * @param start
     * @param pageSize
     * @return
     */
    List<InnerReward> queryInnerReward(InnerRewardDTO inner,int start ,int pageSize);
    
    /**
	 * Description：统计所有内定会员
	 * @date 2019年4月11日 
	 * @param inner
	 * @return
	 */
	Integer innerRewardCount(InnerRewardDTO inner);
	
	/**
	 * Description: 删除内定会员
	 * @date 2019年4月10日    
	 * @param ids
	 * @return
	 */
	void delInnerReward(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 根据会员ID删除内定会员    
	 * @param ids 
	 * date 2019年4月28日
	 */
	void delByMemberId(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 是否为内定会员   
	 * @param actId
	 * @param memberId
	 * @return 
	 * date 2019年4月19日
	 */
	InnerReward selectByMemberId(Integer actId, Integer memberId);
	
	/**
	 * 
	 * Description: 查询指定活动的内定会员    
	 * @param actId
	 * @return 
	 * date 2019年5月13日
	 */
	List<InnerReward> selectByActId(Integer actId);
	
	/**
	 * 
	 * Description: 统计指定奖品内定会员    
	 * @param rewardId
	 * @return 
	 * date 2019年5月13日
	 */
	Integer countByRewardId(Integer rewardId);
	
	/**
	 * 
	 * Description: 根据活动ID删除内定会员    
	 * @param actId 
	 * date 2019年5月28日
	 */
	void delByActId(Integer actId);
}