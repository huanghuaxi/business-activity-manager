package com.activity.manager.reward.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.reward.service.RewardConfService;
import com.activity.manager.security.entity.SysUser;

/**
 * 
 * Description: 奖品配置更新类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author zhangxin   
 * @date 2019年4月11日   
 * @version 1.0
 */

@Service
public class RewardConfServiceImpl implements RewardConfService{

	@Autowired
	private RewardConfMapper rewardConfMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: queryRewardConf</p>   
	 * <p>Description: 查询所有奖品</p>   
	 * @param rewardConf
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.RewardConfService#queryRewardConf(com.activity.manager.security.entity.RewardConf, int, int)
	 */
	@Override
	public List<RewardConf> queryRewardConf(RewardConf rewardConf, int start, int pageSize) {
		return rewardConfMapper.queryRewardConf(rewardConf, start, pageSize);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: rewardConfCount</p>   
	 * <p>Description: 统计所有奖品</p>   
	 * @param rewardConf
	 * @return   
	 * @see com.activity.manager.security.service.RewardConfService#rewardConfCount(com.activity.manager.security.entity.RewardConf)
	 */
	@Override
	public Integer rewardConfCount(RewardConf rewardConf) {
		return rewardConfMapper.RewardConfCount(rewardConf);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getRewardConfById</p>   
	 * <p>Description: 根据ID获取奖品</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.security.service.RewardConfService#getRewardConfById(java.lang.Integer)
	 */
	@Override
	public RewardConf getRewardConfById(Integer id) {
		return rewardConfMapper.selectByPrimaryKey(id);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: getRewardConfListById</p>   
	 * <p>Description: 根据活动ID获取奖品</p>   
	 * @param actId
	 * @return   
	 * @see com.activity.manager.security.service.RewardConfService#getRewardConfListById(java.lang.Integer)
	 */
	@Override
	public List<RewardConf> getRewardConfListById(Integer actId) {
		return rewardConfMapper.selectRewardConfByActId(actId);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveRewardConf</p>   
	 * <p>Description: 添加奖品</p>   
	 * @param rewardConf   
	 * @see com.activity.manager.security.service.RewardConfService#saveRewardConf(com.activity.manager.security.entity.RewardConf)
	 */
	@Override
	public void saveRewardConf(RewardConf rewardConf) {
		Assert.hasText(rewardConf.getRewardOption(), "奖项数据不存在");
		Assert.hasText(rewardConf.getRewardCon(), "奖品数据不存在");
		Assert.notNull(rewardConf.getRatio(), "中奖概率数据不存在");
		Assert.notNull(rewardConf.getRewardNum(), "奖品数量数据不存在");
		rewardConf.setWinNum(0);
		rewardConf.setCreateTime(new Date());
		SysUser user = CurrentParamUtil.getCurrentUser();
		rewardConf.setCreatePerson(user.getId());
		rewardConfMapper.insert(rewardConf);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editRewardConf</p>   
	 * <p>Description: 修改奖品</p>   
	 * @param rewardConf   
	 * @see com.activity.manager.security.service.RewardConfService#editRewardConf(com.activity.manager.security.entity.RewardConf)
	 */
	@Override
	public void editRewardConf(RewardConf rewardConf) {
		Assert.notNull(rewardConf.getId(), "奖品ID数据不存在");
		Assert.hasText(rewardConf.getRewardOption(), "奖项数据不存在");
		Assert.hasText(rewardConf.getRewardCon(), "奖品数据不存在");
		Assert.notNull(rewardConf.getRatio(), "中奖概率数据不存在");
		Assert.notNull(rewardConf.getRewardNum(), "奖品数量数据不存在");
		RewardConf reward = rewardConfMapper.selectByPrimaryKey(rewardConf.getId());
		reward.setRatio(rewardConf.getRatio());
		reward.setRewardCon(rewardConf.getRewardCon());
		reward.setRewardImg(rewardConf.getRewardImg());
		reward.setRewardNum(rewardConf.getRewardNum());
		reward.setRewardOption(rewardConf.getRewardOption());
		reward.setUnit(rewardConf.getUnit());
		reward.setExpand(rewardConf.getExpand());
		rewardConfMapper.updateByPrimaryKey(reward);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: deleteRewardConf</p>   
	 * <p>Description: 删除奖品</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.RewardConfService#deleteRewardConf(java.util.List)
	 */
	@Override
	public void deleteRewardConf(List<String> ids) {
		rewardConfMapper.delRewardConf(ids);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByIdAndOption</p>   
	 * <p>Description: 根据活动ID和奖项查询</p>   
	 * @param actId
	 * @param rewardOption
	 * @return   
	 * @see com.activity.manager.reward.service.RewardConfService#selectByIdAndOption(java.lang.Integer, java.lang.String)
	 */
	@Override
	public List<RewardConf> selectByIdAndOption(Integer actId, String rewardOption) {
		Assert.notNull(actId, "活动ID数据不存在");
		Assert.hasText(rewardOption, "奖项数据不存在");
		return rewardConfMapper.selectByIdAndOption(actId, rewardOption);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByIds</p>   
	 * <p>Description: 根据ID获取奖品</p>   
	 * @param actId
	 * @param rewardIds
	 * @return   
	 * @see com.activity.manager.reward.service.RewardConfService#selectByIds(java.lang.Integer, java.util.List)
	 */
	@Override
	public List<RewardConf> selectByIds(Integer actId, List<String> rewardIds) {
		return rewardConfMapper.selectByIds(rewardIds, actId);
	}

}
