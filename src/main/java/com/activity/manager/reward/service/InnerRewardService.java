/**
 * 
 */
package com.activity.manager.reward.service;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.activity.manager.reward.entity.InnerReward;
import com.activity.manager.reward.entity.InnerRewardDTO;

/**    
 * <p>Description: 加载内定会员</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月11日   
 * @version 1.0   
 */
public interface InnerRewardService {
	
	/**
	 * 
	 * Description: 查询所有内定会员   
	 * @param inner
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年4月11日
	 */
	public List<InnerReward> queryInnerReward(InnerRewardDTO inner, int start, int pageSize);
	
	/**
	 * 
	 * Description: 统计所有内定会员   
	 * @param inner
	 * @return 
	 * date 2019年4月11日
	 */
	public Integer innerRewardCount(InnerRewardDTO inner);
	
	/**
	 * Description：根据ID获取内定会员
	 * @param id
	 * @return
	 */
	public InnerReward getmemberById(Integer id);
	
	/**
	 * Description：添加内定会员
	 * @param innerDTO
	 */
	public String saveInnerReward(InnerRewardDTO innerDTO, String flag);
	
	/**
	 * Description: 编辑内定会员
	 * @param inner 
	 * date 2019年4月10日
	 */
	public void editInnerReward(InnerReward inner);
	
	/**
	 * Description：删除内定会员
	 * @param Ids
	 */
	public void deleteInnerReward(List<String> ids);
	
	/**
	 * 
	 * Description: 内定会员批量导入   
	 * @param actId
	 * @param workbook
	 * @return 
	 * date 2019年4月29日
	 */
	public String impExcel(Integer actId, Workbook workbook, String flag);
	
}
