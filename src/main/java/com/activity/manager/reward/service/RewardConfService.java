package com.activity.manager.reward.service;

import java.util.List;

import com.activity.manager.reward.entity.RewardConf;

/**
 * Description: 加载奖品配置类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author zhangxin   
 * @date 2019年4月11日   
 * @version 1.0
 */

public interface RewardConfService {
	
	/**
	 * 
	 * Description: 查询所有奖品   
	 * @param rewardConf
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年4月13日
	 */
	public List<RewardConf> queryRewardConf(RewardConf rewardConf, int start, int pageSize);
	
	/**
	 * 
	 * Description: 统计所有奖品   
	 * @param rewardConf
	 * @return 
	 * date 2019年4月13日
	 */
	public Integer rewardConfCount(RewardConf rewardConf);
	
	/**
	 * 
	 * Description: 根据ID获取奖品   
	 * @param id
	 * @return 
	 * date 2019年4月13日
	 */
	public RewardConf getRewardConfById(Integer id);
	
	/**
	 * 
	 * Description: 根据活动ID获取奖品   
	 * @param actId
	 * @return 
	 * date 2019年4月13日
	 */
	public List<RewardConf> getRewardConfListById(Integer actId);
	
	/**
	 * 
	 * Description: 添加奖品   
	 * @param rewardConf 
	 * date 2019年4月13日
	 */
	public void saveRewardConf(RewardConf rewardConf);
	
	/**
	 * 
	 * Description: 修改奖品   
	 * @param rewardConf 
	 * date 2019年4月13日
	 */
	public void editRewardConf(RewardConf rewardConf);
	
	/**
	 * 
	 * Description: 删除奖品   
	 * @param ids 
	 * date 2019年4月13日
	 */
	public void deleteRewardConf(List<String> ids);
	
	/**
	 * 
	 * Description: 根据活动ID和奖项查询         
	 * @param actId
	 * @param rewardOption
	 * @return 
	 * date 2019年4月26日
	 */
	public List<RewardConf> selectByIdAndOption(Integer actId, String rewardOption);
	
	/**
	 * 
	 * Description: 根据ID获取奖品    
	 * @param actId
	 * @param rewardIds
	 * @return 
	 * date 2019年5月7日
	 */
	public List<RewardConf> selectByIds(Integer actId, List<String> rewardIds);
	
}
