/**
 * 
 */
package com.activity.manager.reward.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.activity.manager.common.CurrentParamUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.member.entity.Member;
import com.activity.manager.member.entity.MemberActivity;
import com.activity.manager.member.entity.MemberActivityDTO;
import com.activity.manager.member.mapper.MemberActivityMapper;
import com.activity.manager.member.mapper.MemberMapper;
import com.activity.manager.member.service.MemberTransactionalService;
import com.activity.manager.reward.entity.InnerReward;
import com.activity.manager.reward.entity.InnerRewardDTO;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.mapper.InnerRewardMapper;
import com.activity.manager.reward.mapper.RewardConfMapper;
import com.activity.manager.reward.service.InnerRewardService;
import com.activity.manager.security.entity.SysUser;

/**    
 * <p>Description: 内定会员更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月11日   
 * @version 1.0   
 */

@Service
public class InnerRewardServiceImpl implements InnerRewardService {

	@Autowired
	private InnerRewardMapper innerMapper;
	/*
	 * @Autowired private MemberMapper memberMapper;
	 * 
	 * @Autowired private MemberActivityMapper memberActMapper;
	 * 
	 * @Autowired private RewardConfMapper rewardMapper;
	 */
	
	@Autowired
	private MemberTransactionalService memberService;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: queryInnerReward</p>   
	 * <p>Description: 查询所有内定会员   </p>   
	 * @param inner
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.InnerRewardService#queryInnerReward(com.activity.manager.security.entity.InnerRewardDTO, int, int)   
	 */
	@Override
	public List<InnerReward> queryInnerReward(InnerRewardDTO inner, int start, int pageSize) {
		return innerMapper.queryInnerReward(inner, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: innerRewardCount</p>   
	 * <p>Description: 统计所有内定会员</p>   
	 * @param inner
	 * @return   
	 * @see com.activity.manager.security.service.InnerRewardService#innerRewardCount(com.activity.manager.security.entity.InnerRewardDTO)   
	 */
	@Override
	public Integer innerRewardCount(InnerRewardDTO inner) {
		return innerMapper.innerRewardCount(inner);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: getmemberById</p>   
	 * <p>Description: 根据ID获取内定会员</p>   
	 * @param id
	 * @return   
	 * @see com.activity.manager.security.service.InnerRewardService#getmemberById(java.lang.Integer)   
	 */
	@Override
	public InnerReward getmemberById(Integer id) {
		return innerMapper.selectByPrimaryKey(id);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveInnerReward</p>   
	 * <p>Description: 添加内定会员</p>   
	 * @param innerDTO   
	 * @see com.activity.manager.security.service.InnerRewardService#saveInnerReward(com.activity.manager.security.entity.InnerRewardDTO)   
	 */
	@Override
	public String saveInnerReward(InnerRewardDTO innerDTO, String flag) {
		List<InnerRewardDTO> innerDTOs = new ArrayList<InnerRewardDTO>();
		innerDTO.setType(0);
		innerDTOs.add(innerDTO);
		return memberService.saveInnerTra(innerDTOs, flag);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: editInnerReward</p>   
	 * <p>Description: 编辑内定会员</p>   
	 * @param inner   
	 * @see com.activity.manager.security.service.InnerRewardService#editInnerReward(com.activity.manager.security.entity.InnerRewardDTO)   
	 */
	@Override
	public void editInnerReward(InnerReward inner) {
		Assert.notNull(inner.getId(), "内定会员ID数据不存在");
		Assert.notNull(inner.getRewardId(), "奖品ID数据不存在");
		InnerReward innerReward = innerMapper.selectByPrimaryKey(inner.getId());
		innerReward.setRewardId(inner.getRewardId());
		innerMapper.updateByPrimaryKey(innerReward);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: deleteInnerReward</p>   
	 * <p>Description: 删除内定会员</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.InnerRewardService#deleteInnerReward(java.util.List)   
	 */
	@Override
	public void deleteInnerReward(List<String> ids) {
		innerMapper.delInnerReward(ids);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: impExcel</p>   
	 * <p>Description: 内定会员批量导入</p>   
	 * @param actId
	 * @param workbook
	 * @return   
	 * @see com.activity.manager.reward.service.InnerRewardService#impExcel(java.lang.Integer, org.apache.poi.ss.usermodel.Workbook)
	 */
	@Override
	public String impExcel(Integer actId, Workbook workbook, String flag) {
		Assert.notNull(actId, "活动ID数据不存在");
		Pattern pattern = Pattern.compile("[0-9]*");
		if(workbook != null) {
			for(int sheetNum = 0;sheetNum < workbook.getNumberOfSheets();sheetNum++){
				//获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(sheetNum);
                if(sheet == null){
                    continue;
                }
                //获得当前sheet的开始行
                int firstRowNum  = sheet.getFirstRowNum();
                //获得当前sheet的结束行
                int lastRowNum = sheet.getLastRowNum();
                //循环除了第一行的所有行
                List<InnerRewardDTO> innerDTOs = new ArrayList<InnerRewardDTO>();
                InnerRewardDTO innerDTO = new InnerRewardDTO();
                for(int rowNum = firstRowNum+1;rowNum <= lastRowNum;rowNum++){ //为了过滤到第一行因为我的第一行是数据库的列
                    //获得当前行
                    Row row = sheet.getRow(rowNum);
                    if(row == null){
                        continue;
                    }
                    //获取每列数据
                    Cell cell = row.getCell(0);
                    String memberName = ExcelUtil.getCellValue(cell).trim();
                    cell = row.getCell(1);
                    String rewardId = ExcelUtil.getCellValue(cell).trim();
                    Matcher isNum = pattern.matcher(rewardId);
                    if(memberName == null || "".equals(memberName)) {
        				return "导入失败，会员账号不能为空";
        			}else if(rewardId == null || "".equals(rewardId) || !isNum.matches()) {
        				return "导入失败，奖品ID不能为空且只能是数字";
        			}else {
        				innerDTO = new InnerRewardDTO();
        				innerDTO.setActId(actId);
        				innerDTO.setMemberName(memberName);
        				innerDTO.setRewardId(Integer.parseInt(rewardId));
        				innerDTO.setType(0);
        				innerDTOs.add(innerDTO);
        			}
                }
                //新增内定会员
                return memberService.saveInnerTra(innerDTOs, flag);
			}
		}
		return "";
	}
	
	
	
}
