/**
 * 
 */
package com.activity.manager.reward.service.impl;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.activity.manager.reward.entity.RewardRecord;
import com.activity.manager.reward.mapper.RewardRecordMapper;
import com.activity.manager.reward.service.RewardRecordService;

/**    
 * <p>Description: 中奖纪录更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月12日   
 * @version 1.0   
 */

@Service
public class RewardRecordServiceImpl implements RewardRecordService {

	@Autowired
	private RewardRecordMapper recordMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: queryRewardRecord</p>   
	 * <p>Description: 查询所有中奖纪录</p>   
	 * @param record
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.RewardRecordService#queryRewardRecord(com.activity.manager.security.entity.RewardRecord, int, int)   
	 */
	@Override
	public List<RewardRecord> queryRewardRecord(RewardRecord record, int start, int pageSize) {
		return recordMapper.queryRewardRecord(record, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: rewardRecordCount</p>   
	 * <p>Description: 统计所有中奖纪录</p>   
	 * @param record
	 * @return   
	 * @see com.activity.manager.security.service.RewardRecordService#rewardRecordCount(com.activity.manager.security.entity.RewardRecord)   
	 */
	@Override
	public Integer rewardRecordCount(RewardRecord record) {
		return recordMapper.rewardRecordCount(record);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: sendReward</p>   
	 * <p>Description: 派奖</p>   
	 * @param id   
	 * @see com.activity.manager.security.service.RewardRecordService#sendReward(java.lang.Integer)   
	 */
	@Override
	public void sendReward(Integer id, Integer isSend) {
		Assert.notNull(id, "中奖纪录ID数据不存在");
		RewardRecord record = recordMapper.selectByPrimaryKey(id);
		Assert.notNull(record, "ID:" + id + "中奖纪录不存在");
		record.setIsSend(isSend);
		recordMapper.updateByPrimaryKey(record);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: allSendReward</p>   
	 * <p>Description: 全部派奖</p>     
	 * @param actId 
	 * @see com.activity.manager.security.service.RewardRecordService#allSendReward()   
	 */
	@Override
	public void allSendReward(Integer actId) {
		Assert.notNull(actId, "活动ID不存在");
		recordMapper.allSendReward(actId);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: expordExcel</p>   
	 * <p>Description: 导出中奖纪录</p>   
	 * @param record
	 * @return   
	 * @see com.activity.manager.security.service.RewardRecordService#expordExcel(com.activity.manager.security.entity.RewardRecord)   
	 */
	@Override
	public HSSFWorkbook expordExcel(RewardRecord record) {
		//查询中奖纪录
		Integer count = rewardRecordCount(record);
		List<RewardRecord> recordList = queryRewardRecord(record, 0, count);
		// 声明一个工作薄        
        HSSFWorkbook workbook = new HSSFWorkbook();
        //创建一个Excel表单,参数为sheet的名字
        HSSFSheet sheet = workbook.createSheet("中奖纪录");
        //创建表头
        setTitle(workbook, sheet);
        //新增数据行，并且设置单元格数据
        HSSFRow hssfRow = sheet.createRow(1);
        int rows = 1;
        for(RewardRecord re : recordList) {
        	hssfRow = sheet.createRow(rows);
        	hssfRow.createCell(0).setCellValue(re.getId());
        	hssfRow.createCell(1).setCellValue(re.getRemName());
        	if(re.getRewardOption() == null || "".equals(re.getRewardOption())){
        		hssfRow.createCell(2).setCellValue("谢谢参与");
        	}else {
        		hssfRow.createCell(2).setCellValue(re.getRewardOption());
        	}
        	if(re.getRewardCon() == null || "".equals(re.getRewardCon())){
        		hssfRow.createCell(3).setCellValue("谢谢参与");
        	}else {
        		hssfRow.createCell(3).setCellValue(re.getRewardCon());
        	}
        	if(re.getIsGit()) {
            	hssfRow.createCell(4).setCellValue("是");
        	}else {
            	hssfRow.createCell(4).setCellValue("否");
        	}
        	hssfRow.createCell(5).setCellValue(re.getCreateTime());
        	if(re.getIsSend() == null || re.getIsSend() == 0) {
            	hssfRow.createCell(6).setCellValue("否");
        	}else {
            	hssfRow.createCell(6).setCellValue("是");
        	}
        	rows++;
        }
		return workbook;
	}
	
	// 创建表头
    private static void setTitle(HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFRow row = sheet.createRow(0);
        // 设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度
        sheet.setColumnWidth(8, 60 * 256);
        // 设置为居中加粗
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        //导出的Excel头部
        String[] headers = { "编号", "中奖人", "奖项", "奖品", "是否中奖", "中奖时间", "是否派奖" };
        // 设置表格默认列宽度为20个字节
        sheet.setDefaultColumnWidth((short) 20);
        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
            cell.setCellStyle(style);
        }
    }

}
