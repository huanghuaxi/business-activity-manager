/**
 * 
 */
package com.activity.manager.reward.service;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.activity.manager.reward.entity.RewardRecord;

/**    
 * <p>Description: 加载中奖纪录类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月12日   
 * @version 1.0   
 */
public interface RewardRecordService {
	
	/**
	 * 
	 * Description: 查询所有中奖纪录   
	 * @param record
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年4月12日
	 */
	public List<RewardRecord> queryRewardRecord(RewardRecord record, int start, int pageSize);
	
	/**
	 * 
	 * Description: 统计所有中奖纪录    
	 * @param record
	 * @return 
	 * date 2019年4月12日
	 */
	public Integer rewardRecordCount(RewardRecord record);
	
	/**
	 * 
	 * Description: 派奖    
	 * @param id 
	 * date 2019年4月12日
	 */
	public void sendReward(Integer id, Integer isSend);
	
	/**
	 * 
	 * Description: 全部派奖     
	 * @param actId
	 * date 2019年4月12日
	 */
	public void allSendReward(Integer actId);
	
	/**
	 * 
	 * Description: 导出中奖纪录   
	 * @param record
	 * @return 
	 * date 2019年4月12日
	 */
	public HSSFWorkbook expordExcel(RewardRecord record);
	
}
