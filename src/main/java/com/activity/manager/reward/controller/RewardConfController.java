package com.activity.manager.reward.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.reward.entity.RewardConf;
import com.activity.manager.reward.service.RewardConfService;

/**
 * Description: 奖品配置控制类
 * Copyright: Copyright (c) 2019    
 * @author zhangxin   
 * @date 2019年4月11日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/rewardConf")
public class RewardConfController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RewardConfService rewardConfService;
	
	@Autowired
	private BusiActivityService activityService;
	
	@Value("${web.upload.path}")
    private String uploadPath;
	
	/**
	 * 
	 * Description: 跳转到奖品配置页面   
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月13日
	 */
	@RequestMapping("/list")
	public String toMemberActList(Integer actId, String flag, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			model.addAttribute("flag", flag);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到奖品配置页面");
		}catch(Exception e) {
			logger.error("奖品配置页面跳转异常", e);
		}
		return "activitymanage/activity/rewardConf.html";
	}
	
	/**
	 * 
	 * Description: 博饼奖品配置页
	 * @param actId
	 * @param flag
	 * @param model
	 * @return 
	 * date 2019年8月15日
	 */
	@RequestMapping("/bobingList")
	public String bobingList(Integer actId, String flag, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			model.addAttribute("flag", flag);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到博饼奖品配置页面");
		}catch(Exception e) {
			logger.error("博饼奖品配置页面跳转异常", e);
		}
		return "activitymanage/activity/bobingRule.html";
	}
	
	
	
	/**
	 * 
	 * Description: 跳转到奖品编辑页面   
	 * @param id
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月11日
	 */
	@RequestMapping("/toSaveOrEdit")
	public String toSaveOrEdit(Integer id, Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("id", id);
			model.addAttribute("actId", actId);
			logger.info("跳转到奖品编辑页面");
		}catch(Exception e) {
			logger.error("奖品编辑页面 跳转异常", e);
		}
		return "activitymanage/activity/editReward.html";
	}
	
	/**
	 * Description：获取奖品列表
	 * @param pageUtil
	 * @param rewardConf
	 * @date 2019年4月11日 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, RewardConf rewardConf) {
		try {
			Assert.notNull(rewardConf.getActId(), "活动ID数据不存在");
			List<RewardConf> rList = rewardConfService.queryRewardConf(rewardConf, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> aLst = formate(rList);
			Integer count = rewardConfService.rewardConfCount(rewardConf);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(aLst);
			logger.info("获取奖品列表");
		}catch(Exception e) {
			logger.error("获取奖品列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 添加奖品  
	 * @param rewardConf
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage saveRewardConf(RewardConf rewardConf) {
		try {
			rewardConfService.saveRewardConf(rewardConf);
			logger.info("添加奖品");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("添加奖品异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑奖品
	 * @param rewardConf
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage editRewardConf(RewardConf rewardConf) {
		try {
			rewardConfService.editRewardConf(rewardConf);
			logger.info("编辑奖品");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑奖品异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除奖品
	 * @param ids
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delRewardConf(String ids) {
		try {
			Assert.hasText(ids, "奖品Id数据不存在");
			rewardConfService.deleteRewardConf(Arrays.asList(ids.split(",")));
			logger.info("删除奖品");
		}catch(Exception e) {
			logger.error("删除奖品异常",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 根据活动ID获取奖品列表
	 * @param actId
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/getRewardConfList")
	public ResultMessage getRewardConfList(Integer actId) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			List<RewardConf> rList = rewardConfService.getRewardConfListById(actId);
			logger.info("根据活动ID获取奖品列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rList);
		}catch(Exception e) {
			logger.error("获取奖品列表异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取奖品    
	 * @param id
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/getRewardConf")
	public ResultMessage getRewardConf(Integer id) {
		try {
			Assert.notNull(id, "奖品ID数据不存在");
			RewardConf rewardConf = rewardConfService.getRewardConfById(id);
			logger.info("获取奖品");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rewardConf);
		}catch(Exception e) {
			logger.error("获取奖品异常", e);
			return ResultMessage.getFail();
		}
	}
	
	@ResponseBody
	@RequestMapping("/getRewardByOption")
	public ResultMessage getRewardByOption(Integer actId, String rewardOption) {
		try {
			List<RewardConf> rewards = rewardConfService.selectByIdAndOption(actId, rewardOption);
			logger.info("根据活动ID和奖项获取奖品列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, rewards);
		}catch(Exception e) {
			logger.error("获取奖品异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 文件上传    
	 * @param file
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/upload")
	public ResultMessage upload(@RequestParam("file") MultipartFile file, Integer id, 
			HttpServletRequest request) {
		try {
			if(file.isEmpty()) {
				return ResultMessage.getFail();
			}
			String fileName = file.getOriginalFilename();
			//文件保存地址
			String path = uploadPath;
			path += "rewardImg/";
			//新文件名
			String newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf("."));
			File dest = new File(path + newFileName);
			logger.info("文件上传路径："+path);
			//判断文件父目录是否存在
			if(!dest.getParentFile().exists()){ 
				logger.info(path + "targetPath is not exist");  
				dest.getParentFile().mkdirs(); 
			}
			file.transferTo(dest);
			logger.info("文件上传成功");
			//文件访问地址
			String url = "http://" + request.getServerName() + ":" 
					+ request.getServerPort()  + "/uploads/rewardImg/" + newFileName;
			Map<String, Object> map = new HashMap<String, Object>();
			logger.info("前端页面访问路径："+url);
			map.put("imgUrl", url);
			map.put("id", id);
			logger.info("奖品图片上传结束");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, map);
		}catch(Exception e) {
			logger.error("文件上传异常", e);
			return ResultMessage.getFail();
		}
	}
	


	/**
	 * 
	 * Description: 根据ID获取奖品    
	 * @param pageUtil
	 * @param actId
	 * @param rewardIds
	 * @return 
	 * date 2019年5月7日
	 */
	@ResponseBody
	@RequestMapping("getRewardList")
	public PageUtil getRewardList(PageUtil pageUtil, Integer actId, String rewardIds) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			Assert.hasText(rewardIds, "奖品ID数据不存在");
			List<RewardConf> rList = rewardConfService.selectByIds(actId, Arrays.asList(rewardIds.trim().split(",")));
			List<Object> aLst = formate(rList);
			pageUtil.setCode("0");
			pageUtil.setCount(rList.size());
			pageUtil.setMsg("成功");
			pageUtil.setData(aLst);
			logger.info("获取奖品列表");
		}catch(Exception e) {
			logger.error("获取奖品列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	
	private List<Object> formate(List<RewardConf> list){
		List<Object> rLst = new ArrayList<Object>();
		list.forEach(rewardConf -> {
			rLst.add(rewardConf);
		});
		return rLst;
	}
	
}
