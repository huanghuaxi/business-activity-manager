/**
 * 
 */
package com.activity.manager.reward.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.CodeUtil;
import com.activity.manager.common.ExcelUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.reward.entity.InnerReward;
import com.activity.manager.reward.entity.InnerRewardDTO;
import com.activity.manager.reward.service.InnerRewardService;

/**    
 * <p>Description: 内定会员控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年4月11日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/innerReward")
public class InnerRewardController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private InnerRewardService innerService;
	
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到内定会员管理页面    
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月11日
	 */
	@RequestMapping("/list")
	public String toMemberActList(Integer actId, String flag, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			model.addAttribute("flag", flag);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到内定会员管理页面");
		}catch(Exception e) {
			logger.error("内定会员管理页面跳转异常", e);
		}
		return "activitymanage/activity/innerReward.html";
	}
	
	/**
	 * 
	 * Description: 获取内定会员列表    
	 * @param pageUtil
	 * @param innerDTO
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, InnerRewardDTO innerDTO) {
		try {
			Assert.notNull(innerDTO.getActId(), "活动ID数据不存在");
			List<InnerReward> iList = innerService.queryInnerReward(innerDTO, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> aLst = formate(iList);
			Integer count = innerService.innerRewardCount(innerDTO);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(aLst);
			logger.info("获取内定会员列表");
		}catch(Exception e) {
			logger.error("获取内定会员列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 添加内定会员    
	 * @param innerDTO
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage saveInnerReward(InnerRewardDTO innerDTO, String flag) {
		try {
			String msg = innerService.saveInnerReward(innerDTO, flag);
			logger.info("添加内定会员");
			if("".equals(msg)) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("内定会员添加异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑内定会员    
	 * @param inner
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage editInnerReward(InnerReward inner) {
		try {
			innerService.editInnerReward(inner);
			logger.info("编辑内定会员");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("内定会员编辑异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除内定会员    
	 * @param ids
	 * @return 
	 * date 2019年4月11日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delInnerReward(String ids) {
		try {
			Assert.hasText(ids, "内定会员ID数据不存在");
			innerService.deleteInnerReward(Arrays.asList(ids.split(",")));
			logger.info("删除内定会员");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("内定会员删除异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 奖品内定批量导入    
	 * @param file
	 * @param actId
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/impExcel")
	public ResultMessage impExcel(@RequestParam("file") MultipartFile file, Integer actId, String flag) {
		try {
			//读取Excel数据内容
			Workbook workbook = ExcelUtil.getWorkBook(file);
			String msg = innerService.impExcel(actId, workbook, flag);
			logger.info("奖品内定批量导入 ");
			if("".equals(msg)) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("奖品内定批量导入失败", e);
			return ResultMessage.getFail();
		}
	}
	
	private List<Object> formate(List<InnerReward> list){
		List<Object> mLst = new ArrayList<Object>();
		list.forEach(inner -> {
			InnerRewardDTO innerDTO = new InnerRewardDTO();
			innerDTO.setId(inner.getId());
			innerDTO.setActId(inner.getActId());
			innerDTO.setRemId(inner.getRemId());
			innerDTO.setRewardId(inner.getRewardId());
			innerDTO.setRewardOption(inner.getRewardConf().getRewardOption());
			innerDTO.setMemberName(inner.getMember().getMemberName());
			innerDTO.setIsUsed(inner.getIsUsed());
			innerDTO.setCreatePerson(inner.getCreatePerson());
			innerDTO.setCreateTime(inner.getCreateTime());
			innerDTO.setRewardCon(inner.getRewardConf().getRewardCon());
			mLst.add(innerDTO);
		});
		return mLst;
	}
	
	
}
