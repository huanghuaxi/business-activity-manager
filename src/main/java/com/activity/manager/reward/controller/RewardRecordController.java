/**
 * 
 */
package com.activity.manager.reward.controller;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.activity.manager.activity.entity.BusiActivity;
import com.activity.manager.activity.service.BusiActivityService;
import com.activity.manager.business.controller.PageUtil;
import com.activity.manager.common.ResultMessage;
import com.activity.manager.reward.entity.RewardRecord;
import com.activity.manager.reward.service.RewardRecordService;

/**    
 * <p>Description: 中奖记录控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年4月12日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/rewardRecord")
public class RewardRecordController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RewardRecordService recordService;
	
	@Autowired
	private BusiActivityService activityService;
	
	/**
	 * 
	 * Description: 跳转到中奖记录页面    
	 * @param actId
	 * @param model
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/list")
	public String toRewardRecordList(Integer actId, Model model) {
		try {
			Assert.notNull(actId, "活动ID数据不存在");
			model.addAttribute("actId", actId);
			BusiActivity activity = activityService.getActivityById(actId);
			model.addAttribute("actCategory", activity.getActId());
			logger.info("跳转到中奖记录页面");
		}catch(Exception e) {
			logger.error("中奖纪录页面跳转异常", e);
		}
		return "activitymanage/activity/rewardRecord.html";
	}
	
	/**
	 * 
	 * Description: 获取中奖纪录列表    
	 * @param pageUtil
	 * @param record
	 * @return 
	 * date 2019年4月12日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil getList(PageUtil pageUtil, RewardRecord record) {
		try {
			Assert.notNull(record.getActId(), "活动ID数据不存在");
			List<RewardRecord> rList = recordService.queryRewardRecord(record, pageUtil.getCurrIndex(), pageUtil.getLimit());
			List<Object> aLst = formate(rList);
			Integer count = recordService.rewardRecordCount(record);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(aLst);
			logger.info("获取活动ID：" + record.getActId() + "的中奖纪录列表");
		}catch(Exception e) {
			logger.error("获取中奖纪录列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 派奖    
	 * @param id
	 * @return 
	 * date 2019年4月12日
	 */
	@ResponseBody
	@RequestMapping("/sendReward")
	public ResultMessage sendReward(Integer id, Integer isSend) {
		try {
			recordService.sendReward(id, isSend);
			logger.info("中奖纪录ID:" + id + "派奖成功");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("派奖异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 全部派奖    
	 * @param actId
	 * @return 
	 * date 2019年4月12日
	 */
	@ResponseBody
	@RequestMapping("/allSendReward")
	public ResultMessage allSendReward(Integer actId) {
		try {
			recordService.allSendReward(actId);
			logger.info("活动ID:" + actId + "全部派奖成功");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("全部派奖异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 导出中奖纪录    
	 * @param record
	 * @param response 
	 * date 2019年4月12日
	 */
	@RequestMapping("/expordExcel")
	public void expordExcel(RewardRecord record, HttpServletResponse response, HttpServletRequest request) {
		try {
			HSSFWorkbook workbook = recordService.expordExcel(record);
			Assert.notNull(record.getActId(), "活动ID数据不存在");
			BusiActivity activity = activityService.getActivityById(record.getActId());
			Assert.notNull(activity, "活动数据不存在");
			String actName = activity.getActName();
			String fileName = actName + "活动中奖记录_" + new Date().getTime() + ".xls";
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {    
				fileName = URLEncoder.encode(fileName, "UTF-8");// IE浏览器    
			}else{    
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			}
			//清空response  
            response.reset();  
            //设置response的Header  
            response.addHeader("Content-Disposition", "attachment;filename="+ fileName);  
            OutputStream os = new BufferedOutputStream(response.getOutputStream());  
            response.setContentType("application/vnd.ms-excel;charset=utf-8"); 
            //将excel写入到输出流中
            workbook.write(os);
            os.flush();
            os.close();
            logger.info("导出中奖记录");
		}catch(Exception e) {
			logger.error("中奖纪录导出异常", e);
		}
	}
	
	private List<Object> formate(List<RewardRecord> list){
		List<Object> rLst = new ArrayList<Object>();
		list.forEach(record -> {
			rLst.add(record);
		});
		return rLst;
	}
}
