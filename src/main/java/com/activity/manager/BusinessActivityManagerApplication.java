package com.activity.manager;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.activity.manager.common.CertificateUtil;

/**
 * Description: 项目启动类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年3月31日   
 * @version 1.0
 */
@SpringBootApplication
@EnableScheduling
public class BusinessActivityManagerApplication {
	
	private static Logger log = LoggerFactory.getLogger(BusinessActivityManagerApplication.class);
	
	public static void main(String[] args) {
		if(init()) {
			SpringApplication.run(BusinessActivityManagerApplication.class, args);
		}
	}
	
	/**
	 * 
	 * Description: 验证证书     
	 * date 2019年5月29日
	 */
	private static boolean init() {
		try {
			//获取配置文件
//			Resource resource = new ClassPathResource("/application.properties");
			Properties pro = new Properties();
			InputStream inStream = BusinessActivityManagerApplication.class.getResourceAsStream("/application.properties");
			pro.load(inStream);
			inStream.close();  
			String certificate = pro.getProperty("certificate");
			//获取当前服务器证书
			String certificate2 = CertificateUtil.createCertificate();
			if(certificate == null && certificate2 == null) {
				//验证不通过
				log.info("启动失败，证书为空");
				return false;
			}
			if(!certificate.equals(certificate2)) {
				log.info("启动失败，证书验证失败");
				return false;
			}
			
		}catch(Exception e) {
			log.error("启动失败", e);
			return false;
		}
		log.info("证书验证成功");
		return true;
	}
}

